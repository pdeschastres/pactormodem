#ifndef TCPCLIENT_HPP
#define TCPCLIENT_HPP
#include <QtCore/QHash>
#include <QtNetwork/QAbstractSocket>
#include <QtNetwork/QHostAddress>


#include "tcpconnection.hpp"

class TCPClient : public QObject
{
    Q_OBJECT

public:
    TCPClient(QObject *parent=0);
    virtual ~TCPClient();

    int     errNo;
    QString errMsg;

    QTcpSocket    *tcpconnection;

    void setServerPort(quint16 port);
    void sendMessage(const char *message);
    QString nickName() const;
    bool hasConnection(const QHostAddress &senderIp, int senderPort = -1) const;


signals:
    void newConnection();
    void signalConnError();
    void newInData(QString);
    void newParticipant(const QString &nick);
    void participantLeft(const QString &nick);

private slots:
    void slotConnected();
    void slotConnectionError(QAbstractSocket::SocketError);
    void slotNewPeerConnection();
    void slotUpdateClient(qint64);
    void slotReadyRead();
    void disconnected();
//    void readyForUse();
    void connectionError();

private:
    int bytesToWrite;
    int bytesWritten;
    int bytesReceived;
    int bytesToRcv;

    QString      myNickName;

    QByteArray   inBytes;

    void removeConnection(TCPConnection *connection);
};
#endif // TCPCLIENT_HPP
