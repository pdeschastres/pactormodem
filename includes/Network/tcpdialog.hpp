#ifndef TCPDIALOG_HPP
#define TCPDIALOG_HPP
#include <QtCore/QEvent>
#include <QtGui/QTextTableFormat>
#include <QtWidgets/QWidget>
#include <QtWidgets/QDialog>

#include "guiglobals.hpp"

class QGroupBox;
class QVBoxLayout;
class QHBoxLayout;
class QTextEdit;
class QLineEdit;
class QListWidget;
class QLabel;

class QTcpServer;
class QTcpSocket;
class QNetworkSession;
class OpenModem;

class ChatDialog : public QDialog
{
    Q_OBJECT

public:
    ChatDialog(QWidget *parent = 0);
    virtual ~ChatDialog();

protected:
    void changeEvent(QEvent *);
    void closeEvent(QCloseEvent *);

public slots:
    void appendMessage(const QString &from, const QString &message);
    void slotProcessInData(QString data);

private slots:
    void slotTest();
    void slotSendCMD();
    void slotSendMODE();
    void slotSendGreeting();
    void slotSessionOpened();
    void slotNewConnection();
    void slotConnError();
    void returnPressed();
    void participantLeft(const QString &nick);

private:
    bool boolPortOpen;
    bool isGreetingSent;

    int bytesToWrite;
    int bytesWritten;
    int bytesReceived;
    int bytesToRcv;
    int TotalBytes;

    qintptr socketDescriptor;

    QString     text;

    QByteArray  inBytes;
    QStringList datagrams;

    QLabel      *statusLabel;

    QGroupBox   *grpButtons;

    QPushButton *btnClose;
    QPushButton *btnCMD;
    QPushButton *btnCONNECT;
    QPushButton *btnMODE;
    QPushButton *btnBREAK;
    QPushButton *btnCAPTURE;
    QPushButton *btnDISCONNECT;
    QPushButton *btnFECSEND;
    QPushButton *btnLEADEREXT;
    QPushButton *btnMAXCONREQ;
    QPushButton *btnMYC;
    QPushButton *btnSENDID;
    QPushButton *btnSQUELCH;
    QPushButton *btnSUFFIX;
    QPushButton *btnTEST;
    QPushButton *btnVOX;
    QPushButton *quitButton;

    QString myNickName;

    QTextTableFormat tableFormat;

    QVBoxLayout *vboxLayout;
    QHBoxLayout *hboxLayout;

    QTextEdit   *textEdit;

    QListWidget *listWidget;

    QHBoxLayout *hboxLayout1;

    QLabel      *label;

    QLineEdit   *lineEdit;

    QTcpServer  *server;
    QTcpSocket  *svrconnection;
    QNetworkSession *netSession;

    OpenModem   *modem;

    void setupUI();
    void translateText();
    void sendCMD();
    void saveSettings();
    void sendMessage(const char msg);
    void processBytes();
    void removeConnection(QTcpSocket *connection);
    QString nickName();
};
#endif // TCPDIALOG_HPP
