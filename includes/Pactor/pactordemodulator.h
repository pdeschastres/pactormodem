/****************************************************************************
** Copyright (C) 2012 OpenMOR development team.   All rights reserved.
** Contact: Project Leader Ken AD5XJ (ad5xj@arrl.net)
** or visit the project web site openmor.sourceforge.net
**
** This file is part of the OpenMOR Project in the modem or RMSLink executable.
**
** This software is intended for use on amateur radio only and is not a
** commercial product.  The software of the OpenMOR project is
** primarily useful for technical and scientific purposes in non-commercial
** applications only.   Further, this software is distributed in the hope
** that it will be useful.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** === No Commercial Usage ===
** This file contains Open Source code and may not be distributed except as
** outlined in the terms and conditions of the GNU GPL License v3 included
** with this source code.
**
**  === GNU Lesser General Public License Usage ===
** This file may be used under the terms of the GNU Lesser
** General Public License version 3 or higher as published by the
** Free Software Foundation and appearing in the file LICENSE.TXT included
** in the packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 3 (or higher)
** requirements will be met: http://www.gnu.org/licenses/gpl-3.0.txt.
**
** In addition, portions of this software are products originating with
** Nokia and other authors. Each author retains their rights as described
** in their respective LGPL Exception statements.  Any distribution of this
** software carries with it the implied restrictions of the GPLv3 license,
** even with modifications to the original from this source code.
**
** Portions of this software may be modified from the original and are subject
** to GPL or LGPL License restrictions of the original source code.
**
** The developers of the OpenMOR software wish to thank particular individuals
** for their generous contributions.   We wish to thank:
**
**     Thomas Sailer        for the original PACTOR I modem from hfterm
**     Dave Freeze, W1HKJ   from his project FLDIGI
**     Rick Muething, KN6KB of the Amateur Radio Safety Foundation Inc.
**                          for his outstanding WinMOR decoder and
**                          contributions to this project.
**     Volker Schroer       for his contributions from his project LinPSK
**
** - - - The OpenMOR development team.
****************************************************************************/
#ifndef PACTORRECEIVER_H
#define PACTORRECEIVER_H

#include <QtCore/QByteArray>
#include <QtCore/QObject>
#include <QtCore/QVector>
#include <QtCore/QThread>

#include "pactorFSKdemod.h"
#include "pactorsynchro.h"
#include "pactordecoder.h"
#include "pactorhuffman.h"
#include "pactornetstruct.h"
#include "pactortnc.h"

#undef DISPLAY_DATA

#define USE_100BD   1
#define USE_200BD   2

#define AMPLITUDE_HISTORY       4
#define SQUELCH_LEVEL           0.1  // 0.03

#define TARGET_AMPL_MAX         0.2
#define TARGET_AMPL             0.15
#define TARGET_AMPL_MIN         0.1

#define GAP_TABLE_SIZE          4096

#define MAX_AVG_DISTANCE    ((float)2.0)
#define MIN_CS_HDR_VALID_VALUE  ((float)0.1)

#define BURST_TYPE_UNKNOWN      -1
#define BURST_TYPE_HDR100       1
#define BURST_TYPE_HDR200       2
#define BURST_TYPE_CS3          3
#define BURST_TYPE_CS_OTHER     4

#define CS1_ID                  0
#define CS2_ID                  1
#define CS3_ID                  2
#define CS4_ID                  3
#define HDR100_ID               4
#define HDR200_ID               5

#define PT_EXPECT_ANY           0
#define PT_EXPECT_55            1
#define PT_EXPECT_AA            2


typedef struct
{
    int     length;
    int     position;
    int     lo_to_hi;
} t_gap_table_elmt;

#define     SYNC_UKNWN  0
#define     SYNC_LOST   1
#define     SYNC_GOOD   2

typedef struct
{
    float       phase;
    float       amplitude;
} t_phase_ampl;

enum State
{
    Idle,
    Busy,
    Cancelled
};


class PactorTNC;
class PactorTNCThread;
class PactorSession;
class PactorDemod;
class PactorDemodThreadPrivate;

/**
 * Implementation of the DemodData analysis which can be run in a
 * separate thread.
 */
class PactorDemodThread : public QObject
{
        Q_OBJECT
    public:
        PactorDemodThread(PactorDemod *real_parent, PactorTNC *modem_parent, QObject *parent = 0);
        ~PactorDemodThread();

        const QByteArray &GetBitBuffer();
        void SetCenterFrequency(float frequency);
        void    memoryARQReset(void);

    public slots:
        void    calculateDemodFSK(void);
        void    demodSetState(const int state);
        void    release_audio_sem(void);

    signals:
        void calculationComplete(void);
        void PTModifiedFrequency(int, int);
        void burstEvent(const int event, ccharptr PT_argument);
        void  setLED(int count, t_set_LED_arg   *LED_args);
        void  send_waveform(const float *samples, int count, float divider);
        void  send_spectrum_samples(void);

    private:
        // demodulation
        void    initFSKDemod(int);
        void    InitSineTable(bool);
        void    QuadMix(float in_buffer[], int, float re_buffer[], float im_buffer[]);
        t_enum_carrier MixedDemodulate(float re_buffer_down_one[] , float im_buffer_down_one[] , int, t_phase_ampl out_buffer_down_one[], float squelch_level);
        void    OutFilter500(t_phase_ampl out_buffer_down_one[],
                             int, t_phase_ampl demod_dwnspld_data_buffer[],
                             int &last_demod_dwnspld_index,
                             int    store_waveform, // 0=no, 1=phase, 2=amplitude
                             float  *waveform,
                             qint64 current_timestamp, // ms
                             qint64 &last_sample_timestamp, // ms
                             int    &filtered_samples_count);
        float   InputFilter(const float in_buffer[], int, float out_buffer[]);
        // synchronization
        void    initSynchro(void);
        bool    ZeroCrossing(t_phase_ampl data_buffer[], int begin_sync_sample, int last_sync_sample,
                             int processing_state,
                             qint64 window_open, qint64 window_close,
                             qint64 timeStampUsecs);
        void    identifyBurst(t_gap_table_elmt *gap_table, int last_transition_pos, qint64 timeStampUsecs);
        int     analyzeSynchro(int burst_id, int bit_size, int *transitions_table, int first_bit_pos);
        int     burstPhase(int burst_type, int transition_edge);
        int     compute_frequency_shift(const float *decode_samples_buffer, const int decode_samples_count);
        void    recoverBitFromTransitions(int hdr_CS_found, t_burst_parameters &burst_parameters, t_gap_table_elmt *gap_length_table, bool decode_header);
#ifdef  RECOVER_BIT_DEBUG
        void displayRecoverDebug(char *demod_bit_buffer, int hdr_CS_found, int last_bit_index);
#endif

    public:
        PactorDecoder  *m_decoder;
        PactorTNC      *m_pactorTNC;

        QString      PTMyCall;
        QString      PTAuxCall;
        QString      PTAutoAnswerCall;

        t_tx_text_buffs *callsigns;

        t_phase_ampl *demod_dwnspld_data_buffer;

        QThread      *m_thread;

        // memory ARQ
        bool            ARQ_mode;
        bool            prev_ARQ_mode;
        // memory buffer
        int             accumulate_count;
        int             buffer_phase;
        qint64          packet_start_window_msec;
        // spectrum
        int             wave_samples;
        double          *spct_wave_buffer;
        char            demod_bit_buffer_100[LONG_BURST_BIT_COUNT_100 + 16];
        char            demod_bit_buffer_200[LONG_BURST_BIT_COUNT_200 + 16];
        int             sample_sync_count_initial;

    private:
        PactorDemod             *m_pactorDemod;

        float                   *sample_buffer;
        // inter process contention
        QSemaphore              data_semaphore;

        t_enum_carrier          carrier_detector;

        qint64                  packet_timestamp_msec;

        int                     hdr_CS_previous;
        int                     hdr_CS_found;
        int                     hdr_CS_position;
        int                     hdr_CS_phase;
        int                     hdr_CS_fine_sync;
        float                   hdr_CS_average;

        float                    *re_buffer;
        float                    *im_buffer;
        float                    *re_buffer_down_one;
        float                    *im_buffer_down_one;
        t_phase_ampl             *out_buffer_down_one;
        float                    *decode_samples_buffer;

        int         last_demod_dwnspld_index;
        int         burst_processing_state;
        bool        burst_identified;
        int         carrier_loss_count;
        bool        sample_synchronized;
        int         sample_sync_count;
        int         decode_samples_count;
        int         temp_correlator_results;    // temporary result for peak search
        int         pos_sync_100;
        int         pos_sync_200;
        int         plot_first_sample_idx;
        qint64      last_sample_timestamp; // ms

        QByteArray      bit_buffer;

        float   nco_periods;
        int     demod_sample_count;
        float   center_frequency;

        float   amplitude_average;
        float   AGC_amplitude;
        float   last_amplitude[AMPLITUDE_HISTORY];
        int     amplitude_index;
        int     backward_count;
        float   waveform[1024];

        // synchro
        bool    first_phase_transition;
        int     last_transition_pos;
        int     first_phase_transition_pos;
        int     analyse[16];
        int     first_data_sample;
        int     crossing_analyse_state;
        bool    active_burst;
        int     stored_transitions_count;

        int     first_to_process_200;
        int     first_to_process_100;
        int     first_to_process_CS1;
        int     first_to_process_CS2;
        int     first_to_process_CS3;
        int     first_to_process_CS4;
        int     sample_count_200;
        int     sample_count_100;
        int     sample_count_CS1;
        int     sample_count_CS2;
        int     sample_count_CS3;
        int     sample_count_CS4;

        t_gap_table_elmt gap_length_table[GAP_TABLE_SIZE];
        int     gap_table_index;
        int     first_gap_length;
        int     last_gap_length;
        int     expected_transition_count;
        bool    valid_transitions[6];
        t_burst_parameters  burst_parameters;
        bool    found_start;
        bool    first_CS3_received;
        int     missing_bursts;

        int             sample_count;

        t_phase_ampl    samples_fifo[ZERO_CROSS_FIFO_DEPTH];
        float           new_phase_value;
        float           old_phase_value;
        float           fifo_amplitude_sum;
        float           old_fifo_amplitude_sum;
        int             fifo_index;
        int             valid_crossings;

        int             start_pos;
        int             sync_CS_type;
        t_pos_sync      pos_sync;
        int             last_phase_transition;
        int             last_amplitude_transition;
        int             gap_length;
        float           sync_min_value;
        float           sync_max_value;
        float           sync_amplitude_average;
        float           sync_phase_average;
        int             samples_since_last_high;
        int             samples_since_last_low;

        bool            into_window;
        bool            valid_burst_found;
        int             last_packet_number;


        t_set_LED_arg           LED_args[LED_COUNT];

        int             transition_FIFO_index;

        int             delta_transition;
        int             burst_length;

        /*delay variables of iir filter for cosine/sine generation*/

        // low pass filter coefficients
        float blp1;
        float blp2 ;
        float blp3;
        float blp4;
        float blp5;
        float blp6;

        float alp2;
        float alp3;
        float alp4;
        float alp5;
        float alp6;

        /*delay variables of iir filter for low pass filtering the I-path*/

        float zlpc1;
        float zlpc2;
        float zlpc3;
        float zlpc4;
        float zlpc5;
        float zlpc6;

        /*delay variables of iir filter for low pass filtering the Q-path*/

        float zlps1;
        float zlps2;
        float zlps3;
        float zlps4;
        float zlps5;
        float zlps6;

        float    *sine_table;
        float    *cosine_table;
        int     sine_cosine_inc;
        int     sine_cosine_index;

        /*variables of demodulating (beware division by zero!)*/

        float coold;/*old value of I-path*/
        float conew;/*new value of I-path*/
        float siold;/*old value of Q-path*/
        float sinew;/*new value of Q-path*/
        float demo;/*value demodulating*/

        /*constant of demodulating*/

        float decon;

        // input bandpass filter
        float   bpf_xv[BPF_NZEROS + 1];
        float   bpf_yv[BPF_NPOLES + 1];

        // input Chebychev coefficients depending on center frequency
        // 800 to 2300 central frequency (16 freq)
        float   chebychev_coef[16][6];
        float   chebychev_gain[16];
        int     chebychev_frequency_index;

        // output bandpass filter
        float   lpf_xv[LPF_NZEROS + 1];
        float   lpf_yv[LPF_NPOLES + 1];

#ifdef DISPLAY_DATA
        float       disp_input_buffer[8000];
        float       disp_inter_buffer[8000];
        float       disp_output_buffer[8000];
        int         disp_input_count;
        int         disp_inter_count;
        int         disp_output_count;
        int         disp_iterations;
#endif
};

/**
 * Class which performs frequency demoddata analysis on a window of
 * audio samples, provided to it by the Engine.
 */
class PactorDemod : public  QObject
{
        Q_OBJECT
    public:
        PactorDemod(QObject *parent = 0);
        ~PactorDemod();

    public:
        //    void    initialize(void);
        void    setThreadCallsign(const t_tx_text_buffs *callsigns);

        void    demodSetState(const int state);

        const QByteArray &GetBitBuffer(void);

        State              m_state;
        PactorDemodThread *m_thread;

    signals:
        void    PTchangeSampleCount(const int  sample_count);
        void    PTerror(const int  error_code);
        void    PTconnected(const QByteArray &callsign);
        void    PTbusyChannel(const bool busy_channel);

        void    setDemodThreadState(const int  state);

    public slots:
        /*
         * Calculate a frequency demoddata
         *
         * \param buffer       Audio data
         * \param format       Format of audio data
         *
         * Frequency demoddata is calculated asynchronously.
         *
         * An ongoing calculation can be cancelled by calling cancelCalculation().
         *
         */
        //    void    bufferize_audio(cfloatptr/*const float **/ buffer, qint64 timeStampUsecs);
        void    SetCenterFrequency(float frequency);

    private slots:

    private:

        PactorTNC          *m_tnc;
        PactorSession      *m_session;

        int                 in_buffer_data_count;
        float               *in_buffer;
};

#endif // PACTORRECEIVER_H

