/****************************************************************************
** Copyright (C) 2012 OpenMOR development team.   All rights reserved.
** Contact: Project Leader Ken AD5XJ (ad5xj@arrl.net)
** or visit the project web site openmor.sourceforge.net
**
** This file is part of the OpenMOR Project in the modem or RMSLink executable.
**
** This software is intended for use on amateur radio only and is not a
** commercial product.  The software of the OpenMOR project is
** primarily useful for technical and scientific purposes in non-commercial
** applications only.   Further, this software is distributed in the hope
** that it will be useful.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** === No Commercial Usage ===
** This file contains Open Source code and may not be distributed except as
** outlined in the terms and conditions of the GNU GPL License v3 included
** with this source code.
**
**  === GNU Lesser General Public License Usage ===
** This file may be used under the terms of the GNU Lesser
** General Public License version 3 or higher as published by the
** Free Software Foundation and appearing in the file LICENSE.TXT included
** in the packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 3 (or higher)
** requirements will be met: http://www.gnu.org/licenses/gpl-3.0.txt.
**
** In addition, portions of this software are products originating with
** Nokia and other authors. Each author retains their rights as described
** in their respective LGPL Exception statements.  Any distribution of this
** software carries with it the implied restrictions of the GPLv3 license,
** even with modifications to the original from this source code.
**
** Portions of this software may be modified from the original and are subject
** to GPL or LGPL License restrictions of the original source code.
**
** The developers of the OpenMOR software wish to thank particular individuals
** for their generous contributions.   We wish to thank:
**
**     Thomas Sailer        for the original PACTOR I modem from hfterm
**     Dave Freeze, W1HKJ   from his project FLDIGI
**     Rick Muething, KN6KB of the Amateur Radio Safety Foundation Inc.
**                          for his outstanding WinMOR decoder and
**                          contributions to this project.
**     Volker Schroer       for his contributions from his project LinPSK
**
** - - - The OpenMOR development team.
****************************************************************************/
#ifndef PACTORNETSTRUCT_H
#define PACTORNETSTRUCT_H

#include <QtNetwork/QtNetwork>
#include "pactorparams.h"

#define MAX_MESSAGE_BUFFER_SIZE     (LONG_BURST_SAMPLE_COUNT + (4 * sizeof(qint16)))
#define MAX_CALLSIGN_SIZE           16
#define WAVEFORM_CHUNK_SIZE         256

#define UCMD0_MAX       30      // Max 100Bd packet count to speedup to 200Bd
#define UCMD1_MAX       30      // Max 200Bd packet error count to speeddown to 100Bd
#define UCMD2_MAX       9       // Max 200Bd packet count without error to stay to 200Bd
#define UCMD3_MAX       120     // Max Mem ARQ packet count combined before clear
#define UCMD0_DEFAULT   3       // default 100Bd packet count to speedup to 200Bd
#define UCMD1_DEFAULT   6       // default 200Bd packet error count to speeddown to 100Bd
#define UCMD2_DEFAULT   2       // default 200Bd packet count without error to stay to 200Bd
#define UCMD3_DEFAULT   5       // default Mem ARQ packet count combined before clear

#define VOX_CHANNEL_DISABLED    -1
#define VOX_CHANNEL_0           0
#define VOX_CHANNEL_1           1
#define VOX_CHANNEL_START       0
#define VOX_CHANNEL_STOP        0
#define VOX_CHANNEL_FREQUENCY   1000
#define VOX_CHANNEL_MIN_FREQUENCY   500
#define VOX_CHANNEL_MAX_FREQUENCY   3000

#define LED_TX          0
#define LED_RX          1
#define LED_PHASE       2
#define LED_REQ         3
#define LED_TRANS       4
#define LED_ERROR       5
#define LED_IRS         6
#define LED_ISS         7
#define LED_IDLE        8
#define LED_VOX         9
#define LED_COMM        10

#define  LED_COUNT      11

// LED color
#define  GREY_ICON      0
#define  GREEN_ICON     1
#define  RED_ICON       2

// operations
enum
{
    e_send_mycall,
    e_myaux,
    e_remote_call,
    e_frequency_value,
    e_mode_button,
    e_info_message,
    e_unproto_params,
    e_text_send,
    e_text_receive,
    e_debug_text,
    e_waveform_data,
    e_waveform_source,
    e_general_parameters,
    e_LED_status,
    e_break,
    e_shutdown,
    e_jack_audio,
    e_vox_tone,
    e_qrt,
    e_TNC_visibleToggle,
    e_end
};

// modes
enum
{
    e_mode_MASTER,
    e_mode_DISCONNECT,
    e_mode_LISTEN_ARQ,
    e_mode_LISTEN,
    e_mode_UNPROTO,
    e_mode_SLAVE,
    e_mode_STOPALL
};

// parameter indexes
enum
{
    e_Huffman_param,
    e_max_speed,
    e_UCMDs
};

// digiscope waveform source
typedef enum
{
    e_waveform_none,
    e_waveform_raw_input,
    e_waveform_input_filter,
    e_waveform_output_filter_phase,
    e_waveform_output_filter_ampl,
    e_waveform_demod_data,
    e_waveform_memory_ARQ,
    e_waveform_encoder_output
} e_wave_source;

// Pactor modem speed
typedef enum
{
    PT_SPEED_100,
    PT_SPEED_200,
    PT_SPEED_ANY
} e_PT_speed;


typedef struct
{
    const char *text;
    bool        is_enabled;
    int         icon_color;
    bool        is_modified;
    int         timer_init_count;
    int         timer_count;
} t_icon_struct;

typedef struct
{
    qint8       led_index;
    qint8       icon_color;
} t_set_LED_arg;

typedef struct
{
    int         param_index;
    int         param_value;
} t_param_value;

//******************************************************
// Pactor modem network interface structures
// maximum size structure
typedef struct
{
    char    data[MAX_MESSAGE_BUFFER_SIZE - (2 * sizeof(qint16))];
} t_struct_glob;

typedef struct
{
    qint16          modified_LED_count;
    t_set_LED_arg   LED_arg[LED_COUNT];
} t_struct_LED;

// callsigns
typedef struct
{
    qint16  count;
    char    chars[MAX_CALLSIGN_SIZE];
} t_struct_callsign;

typedef struct
{
    qint8   mode_button;
    qint8   speed;
    qint8   repeat_count;
    qint8   mode_status;
    t_struct_callsign remote_call;
} t_struct_mode;

// received or sent text
typedef struct
{
    qint16  count;
    char    chars[MAX_MESSAGE_BUFFER_SIZE - (3 * sizeof(qint16))];
} t_struct_text;

// frequency
typedef struct
{
    qint16  center_frequency;
    qint16  AFC_limit;
    qint16  AFC_on;
} t_struct_freq;

// UCMD value
typedef struct
{
    qint16  index;
    qint16  value;
} t_UCMD_value;

// digiscope source
typedef struct
{
    qint16  source;
    qint16  rate;
    float   ratio;
} t_scope_src;

// digiscope data
typedef struct
{
    qint16  count;
    qint8   wave[MAX_MESSAGE_BUFFER_SIZE - (4 * sizeof(qint16))];
} t_scope_wave;

// union of all data structures
typedef union
{
    t_struct_glob   base;
    t_struct_LED    LEDs;
    t_struct_mode   mode;
    t_struct_text   text;
    t_struct_freq   freq;
    t_scope_src     scope_src;
    t_scope_wave    scope_wave;
    t_struct_callsign   calls;
    t_param_value   param;
    t_UCMD_value    UCMD_value;
} u_netstruct;

// structure including blocksize and opcode
typedef struct
{
    qint16      blocksize;
    qint16      opcode;
    u_netstruct data;
} t_struct_msg;

typedef t_struct_msg   *t_struct_msgptr;

// text buffers structure
typedef struct st_text_buffs
{
    QByteArray                  *ascii_text;
    QByteArray                  *my_callsign;
    QByteArray                  *aux_callsign[10];
    int                         aux_call_count;
    QByteArray                  *remote_callsign;
    QByteArray                  *huffman;
    QByteArray                  *supervisor;
    QByteArray                  *removed_chars;
} t_tx_text_buffs;

typedef struct
{
    /*
     * written by decode_packet
     */
    unsigned char   pkt_data[1024];
    int             pkt_data_len;
} t_ps;

typedef struct
{
    qint64          arrival_time;
    int             type;
    int             phase;
    int             header;
    int             position;
    int             length;
    int             break_request;
    int             packet_counter;
    int             same_byte_count;
    bool            QRT_callsign;
    int             short_path;
    t_ps            huffman_packet;
    float           average_level;
    unsigned char *char_buff;
} t_decode_output;

#endif // PACTORNETSTRUCT_H
