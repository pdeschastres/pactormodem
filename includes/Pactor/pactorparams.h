/****************************************************************************
** Copyright (C) 2012 OpenMOR development team.   All rights reserved.
** Contact: Project Leader Ken AD5XJ (ad5xj@arrl.net)
** or visit the project web site openmor.sourceforge.net
**
** This file is part of the OpenMOR Project in the modem or RMSLink executable.
**
** This software is intended for use on amateur radio only and is not a
** commercial product.  The software of the OpenMOR project is
** primarily useful for technical and scientific purposes in non-commercial
** applications only.   Further, this software is distributed in the hope
** that it will be useful.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** === No Commercial Usage ===
** This file contains Open Source code and may not be distributed except as
** outlined in the terms and conditions of the GNU GPL License v3 included
** with this source code.
**
**  === GNU Lesser General Public License Usage ===
** This file may be used under the terms of the GNU Lesser
** General Public License version 3 or higher as published by the
** Free Software Foundation and appearing in the file LICENSE.TXT included
** in the packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 3 (or higher)
** requirements will be met: http://www.gnu.org/licenses/gpl-3.0.txt.
**
** In addition, portions of this software are products originating with
** Nokia and other authors. Each author retains their rights as described
** in their respective LGPL Exception statements.  Any distribution of this
** software carries with it the implied restrictions of the GPLv3 license,
** even with modifications to the original from this source code.
**
** Portions of this software may be modified from the original and are subject
** to GPL or LGPL License restrictions of the original source code.
**
** The developers of the OpenMOR software wish to thank particular individuals
** for their generous contributions.   We wish to thank:
**
**     Thomas Sailer        for the original PACTOR I modem from hfterm
**     Dave Freeze, W1HKJ   from his project FLDIGI
**     Rick Muething, KN6KB of the Amateur Radio Safety Foundation Inc.
**                          for his outstanding WinMOR decoder and
**                          contributions to this project.
**     Volker Schroer       for his contributions from his project LinPSK
**
** - - - The OpenMOR development team.
****************************************************************************/
#ifndef GOBAL_PARAMS_H
#define GOBAL_PARAMS_H

#define PACTOR_LEVEL                ('1')

#define SAMPLE_RATE_8KHZ            8000 // samples per second
#define SAMPLE_RATE_48KHZ           48000
#define SAMPLE_RATE_RATIO           (48000 / 8000)
#define SAMPLE_48K_DURATION_US      ((double)1000000.0 / (double)SAMPLE_RATE_48KHZ)
#define SAMPLE_8K_DURATION_US       ((double)1000000.0 / (double)SAMPLE_RATE_8KHZ)
#define SAMPLE_DWNSPL_DURATION_US   ((double)(PACTOR_I_DOWNSAMPLE * 1000000) / (double)SAMPLE_RATE_8KHZ)

#define ADJ_INPUT_LATENCY_USECS     2000
#define ADJ_OUTPUT_LATENCY_USECS    2000

#define RETRY_TIMEOUT_MSECS         20000

#define PACTOR_MAX_BAUDRATE         200

#define SPEED_100                   100
#define SPEED_200                   200

#define FILEPATH    "../../data/"
#define NCO_FREQUENCY               2200 // Hz

#define NOTIFY_INTERVAL_MS          5

#define NOTIFY_INTERVAL_US          (NOTIFY_INTERVAL_MS * 1000)
#define AUDIO_SAMPLE_COUNT_8KHZ      (SAMPLE_RATE_8KHZ*NOTIFY_INTERVAL_MS/1000)
#define AUDIO_SAMPLE_COUNT_48KHZ     (SAMPLE_RATE_48KHZ*NOTIFY_INTERVAL_MS/1000)
#define DOWNSAMLPE_RATIO_JACK       (SAMPLE_RATE_48KHZ / SAMPLE_RATE_8KHZ)

#define SINE_COSINE_TABLE_SIZE      (140 * AUDIO_SAMPLE_COUNT_8KHZ)

#define PACTOR_I_DOWNSAMPLE         5
#define BURST_DWNSPL_CNT            (AUDIO_SAMPLE_COUNT_8KHZ/PACTOR_I_DOWNSAMPLE)
#define DOWNSPLD_SAMPL_DURATION     ((qint64)((qint64)PACTOR_I_DOWNSAMPLE / (qint64)SAMPLE_RATE_8KHZ))
#define DOWNSPLD_SAMPL_DURATION_US  (DOWNSPLD_SAMPL_DURATION * (qint64)1000000.0)

#define SYNCHRO_BIT_LENGTH          ((SAMPLE_RATE_8KHZ / PACTOR_MAX_BAUDRATE) / PACTOR_I_DOWNSAMPLE)  // one 200 Bd bit sample count -> 8 samples

#define CORREL_BITS_HDR         8
#define CORREL_BITS_CS          12

#define CORREL_BIT_LENGTH_100   (2 * SYNCHRO_BIT_LENGTH)
#define CORREL_BIT_LENGTH_200   SYNCHRO_BIT_LENGTH
#define GAP_100_MARGIN          3
#define GAP_200_MARGIN          2

#define CORREL_MIN_LENGTH_HDR_100   (CORREL_BITS_HDR * CORREL_BIT_LENGTH_100)
#define CORREL_MIN_LENGTH_HDR_200   (CORREL_BITS_HDR * CORREL_BIT_LENGTH_200)
#define CORREL_MIN_LENGTH_CS        (CORREL_BITS_CS * CORREL_BIT_LENGTH_100)

#define LONG_BURST_BYTE_COUNT_100   8
#define LONG_BURST_BYTE_COUNT_200   20

#define LONG_BURST_BIT_COUNT_100    96
#define LONG_BURST_BIT_COUNT_200    192
#define TX_MIN_SAMPLE_COUNT         (8 * SAMPLE_RATE_8KHZ / PACTOR_MAX_BAUDRATE)

#define LONG_BURST_MS               (LONG_BURST_BIT_COUNT_100 * (1000 / SPEED_100))
#define LONG_BURST_SAMPLE_COUNT    (LONG_BURST_BIT_COUNT_100*CORREL_BIT_LENGTH_100)  // 1536 samples
#define LONG_BURST_AUDIO_SAMPLE_COUNT_48KHZ    (SAMPLE_RATE_RATIO*LONG_BURST_SAMPLE_COUNT*PACTOR_I_DOWNSAMPLE)  // 6*7680 audio samples

#define CS_BURST_MS               (CORREL_BITS_CS * (1000 / SPEED_100))
#define CS_BURST_SAMPLE_COUNT    (CORREL_BITS_CS*CORREL_BIT_LENGTH_100)  // 192 samples
#define CS_BURST_AUDIO_SAMPLE_COUNT_48KHZ    (SAMPLE_RATE_RATIO*CS_BURST_SAMPLE_COUNT*PACTOR_I_DOWNSAMPLE)  // 5760 audio samples

#define DEMOD_BUFFER_SIZE   16384 // power of 2, approx 2 seconds

#define MOD_SMPL_BIT_LENGTH_200   (SAMPLE_RATE_8KHZ / PACTOR_MAX_BAUDRATE)
#define MOD_SMPL_BIT_LENGTH_100   (2 * MOD_SMPL_BIT_LENGTH_200)

#define IMMEDIATE_START             -1      // immediate packet transmission (CS1)

#define SHORT_PATH_TIMING           0
#define LONG_PATH_TIMING            1
#define UNPROTO_TIMING              2

typedef const float *cfloatptr;
typedef const double *cdoubleptr;
typedef const char   *ccharptr;


// waveform display
#define TILE_SIZE_SPL               20
#define TILE_SIZE_HEIGHT            80
#define WAVEFORM_SIZE               (20*TILE_SIZE_SPL)

#define SPECTRUM_HEIGHT             120
#define SPECTRUM_WIDTH              400

#endif // GOBAL_PARAMS_H
