/****************************************************************************
** Copyright (C) 2012 OpenMOR development team.   All rights reserved.
** Contact: Project Leader Ken AD5XJ (ad5xj@arrl.net)
** or visit the project web site openmor.sourceforge.net
**
** This file is part of the OpenMOR Project in the modem or RMSLink executable.
**
** This software is intended for use on amateur radio only and is not a
** commercial product.  The software of the OpenMOR project is
** primarily useful for technical and scientific purposes in non-commercial
** applications only.   Further, this software is distributed in the hope
** that it will be useful.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** === No Commercial Usage ===
** This file contains Open Source code and may not be distributed except as
** outlined in the terms and conditions of the GNU GPL License v3 included
** with this source code.
**
**  === GNU Lesser General Public License Usage ===
** This file may be used under the terms of the GNU Lesser
** General Public License version 3 or higher as published by the
** Free Software Foundation and appearing in the file LICENSE.TXT included
** in the packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 3 (or higher)
** requirements will be met: http://www.gnu.org/licenses/gpl-3.0.txt.
**
** In addition, portions of this software are products originating with
** Nokia and other authors. Each author retains their rights as described
** in their respective LGPL Exception statements.  Any distribution of this
** software carries with it the implied restrictions of the GPLv3 license,
** even with modifications to the original from this source code.
**
** Portions of this software may be modified from the original and are subject
** to GPL or LGPL License restrictions of the original source code.
**
** The developers of the OpenMOR software wish to thank particular individuals
** for their generous contributions.   We wish to thank:
**
**     Thomas Sailer        for the original PACTOR I modem from hfterm
**     Dave Freeze, W1HKJ   from his project FLDIGI
**     Rick Muething, KN6KB of the Amateur Radio Safety Foundation Inc.
**                          for his outstanding WinMOR decoder and
**                          contributions to this project.
**     Volker Schroer       for his contributions from his project LinPSK
**
** - - - The OpenMOR development team.
****************************************************************************/
#ifndef PACTORSESSION_H
#define PACTORSESSION_H
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_knob.h>

#include <QtGui/QCloseEvent>
#include <QtWidgets/QWidget>
#include <QtWidgets/QLabel>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QPlainTextEdit>


#include "waveform.h"
#include "simplewaterfall.h"
#include "pactornetstruct.h"
#include "pactortnc.h"


class QSettings;
class QMenu;
class QAction;
class QIcon;
class QSlider;
class QLineEdit;
class QButtonGroup;
class QPushButton;
class QToolButton;
class QTextEdit;
class QGroupBox;
class QSpinBox;
class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QProgressBar;
class QMenuBar;
class QMessageBox;

class   QwtPlotCurve;
class   QwtPlot;
class   QwtKnob;
class   PactorTestUI;
class   Waveform;
class   SWaterfall;

//-----------------------------------------------------------------------------
// Constants
//-----------------------------------------------------------------------------
#define     FREQ_SLIDER_MIN     800
#define     FREQ_SLIDER_MAX     2300
#define     FREQ_SLIDER_START   2200

#define BTN_NONE                0
#define BTN_WAVE                1
#define BTN_WFALL               2
#define BTN_SPECTR              3

// Disable message timeout defined in spectrum.h
//const int   NullMessageTimeout      = -1;

class PactorTNC;

/**
 * Main application widget, responsible for connecting the various UI
 * elements to the Engine.
 */
class PactorSession : public QWidget
{
    Q_OBJECT
public:
    PactorSession(QWidget *parent = 0);
    ~PactorSession();

    QLineEdit *m_outputText;
    QLineEdit *m_inputText;

    QCheckBox *m_RBlowSpWave;
    QCheckBox *m_RBFreezeWave;

    QRadioButton *m_RBwave;

    Waveform  *my_waveform;  // waveform widget

    void setfrequency(int frequency);

public slots:
    // public entry to Pactor modem control
    void SetCenterFrequencyInt(int frequency);
    void send_spectrum_samples(void);
    void setLEDdirect(int led_index, int color);

protected:
    void closeEvent(QCloseEvent *event);
    //    void resizeEvent(QResizeEvent *event);

signals:

private slots:
    void centerFrequencyTimerCallback(void);
    void any_useTimerCallback(void);
    void setGainCallback(double);
    void setAFCCallback(bool);
    void setAGCCallback(bool);
    void setSpeedCallback(bool);
    void setHuffmanCallback(bool);
    void helpHelpCallback(bool);
    void helpAboutCallback(bool);
    void waveformMode(int);

private:
    bool comm_active;
//    bool AFC_on;

    int  wave_btn;
    int  received_frequency;
    int  center_frequency;
    int  AFC_limit;
    int  timer_counter;

    QLabel       *m_frequency;
    QLabel       *tx_frame_label;
    QLabel       *rx_frame_label;

    QSlider      *m_freq_slider;

    QGroupBox    *m_LEDStatus;
    QGroupBox    *m_tx_frame;
    QGroupBox    *m_tx_text;
    QGroupBox    *m_rx_frame;
    QGroupBox    *m_rx_text;
    QGroupBox    *controlGroup;

    QTextEdit    *help_text;

    QTextStream  *help_stream;

    QFile        *help_file;

    QGridLayout  *grid_ctrl;
    QGridLayout  *grid_call;
    QGridLayout  *TncStateDisplay;
    QGridLayout  *txWidget;
    QGridLayout  *rxWidget;

    QVBoxLayout  *wholeDisplay;
    QVBoxLayout  *leftDisplay;
    QVBoxLayout  *rightDisplay;
    QVBoxLayout  *AfcDisplay;
    QVBoxLayout  *rx_frame_VLayout;
    QVBoxLayout  *waveLayout;
    QVBoxLayout  *help_layout;

    QHBoxLayout  *highDisplay;
    QHBoxLayout  *lowDisplay;
    QHBoxLayout  *m_tx_control;

    QRadioButton *m_RBwaterfall;
    QRadioButton *m_RBspectrum;
    QRadioButton *m_RBnone;

    QButtonGroup *buttonsLayout;

    QToolButton  *btn_AFC;
    QToolButton  *btn_AGC;
    QToolButton  *btn_speed100;
    QToolButton  *btn_Huffman;

    QProgressBar *progressbar;

    QMenuBar     *menu_bar;

    QMenu        *file_menu;
    QMenu        *help_menu;

    QAction      *agcAction;
    QAction      *afcAction;
    QAction      *huffmanAction;
    QAction      *speedLimitAction;
    QAction      *help_help;
    QAction      *help_about;

    QDialog     *help_dialog;

    QMessageBox *about_page;

    QwtKnob     *audio_gain;

    QTimer      *multi_purpose_timer;
    QTimer      *frequency_timer;

    QGridLayout *LEDLayout;

    QLabel      *LED[12];
    QLabel      *LED_text[12];

    QIcon        m_Icon[3];

    t_icon_struct icon_info[LED_COUNT];

    SWaterfall  *my_waterfall; // spectrum / waterfall

    PactorTNC    *m_PactorTNC;

    void createUi();
    void connectUi();
};
#endif // PACTORPANEL_H
