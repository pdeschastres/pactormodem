/****************************************************************************
** Copyright (C) 2012 OpenMOR development team.   All rights reserved.
** Contact: Project Leader Ken AD5XJ (ad5xj@arrl.net)
** or visit the project web site openmor.sourceforge.net
**
** This file is part of the OpenMOR Project in the modem or RMSLink executable.
**
** This software is intended for use on amateur radio only and is not a
** commercial product.  The software of the OpenMOR project is
** primarily useful for technical and scientific purposes in non-commercial
** applications only.   Further, this software is distributed in the hope
** that it will be useful.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** === No Commercial Usage ===
** This file contains Open Source code and may not be distributed except as
** outlined in the terms and conditions of the GNU GPL License v3 included
** with this source code.
**
**  === GNU Lesser General Public License Usage ===
** This file may be used under the terms of the GNU Lesser
** General Public License version 3 or higher as published by the
** Free Software Foundation and appearing in the file LICENSE.TXT included
** in the packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 3 (or higher)
** requirements will be met: http://www.gnu.org/licenses/gpl-3.0.txt.
**
** In addition, portions of this software are products originating with
** Nokia and other authors. Each author retains their rights as described
** in their respective LGPL Exception statements.  Any distribution of this
** software carries with it the implied restrictions of the GPLv3 license,
** even with modifications to the original from this source code.
**
** Portions of this software may be modified from the original and are subject
** to GPL or LGPL License restrictions of the original source code.
**
** The developers of the OpenMOR software wish to thank particular individuals
** for their generous contributions.   We wish to thank:
**
**     Thomas Sailer        for the original PACTOR I modem from hfterm
**     Dave Freeze, W1HKJ   from his project FLDIGI
**     Rick Muething, KN6KB of the Amateur Radio Safety Foundation Inc.
**                          for his outstanding WinMOR decoder and
**                          contributions to this project.
**     Volker Schroer       for his contributions from his project LinPSK
**
** - - - The OpenMOR development team.
****************************************************************************/
#ifndef PACTORTESTUI_H
#define PACTORTESTUI_H

#ifdef GRAPHICS_OUT
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#endif

#include <QtCore/QSettings>
#include <QtCore/QObject>
#include <QtCore/QEvent>

#include <QtCore/QEvent>

#include <QtGui/QIcon>
#include <QtGui/QValidator>
#include <QtGui/QTextCursor>

#include <QtWidgets/QWidget>
#include <QtWidgets/QMainWindow>

#include "globals.h"
#include "pactornetstruct.h"
#include "pactorsession.h"

class QLabel;
class QSlider;
class QPlainTextEdit;
class QGroupBox;
class QLineEdit;
class QMenu;
class QAction;
class QPlainTextEdit;
class QLineEdit;
class QTextEdit;
class QToolButton;
class QSpacerItem;
class QSpinBox;
class QButtonGroup;
class QPushButton;
class QRadioButton;
class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;

#ifdef GRAPHICS_OUT
class QwtPlotCurve;
class QwtPlot;
#endif

class MyValidator;
class MyValidatorMultiple;
class PactorSession;
//-----------------------------------------------------------------------------
// Constants
//-----------------------------------------------------------------------------
#define  FREQ_SLIDER_MIN     800
#define  FREQ_SLIDER_MAX     2300
#define  FREQ_SLIDER_START   2200

#define  FAST_TIMER_INTERVAL 50

#ifdef GRAPHICS_OUT
# define TST_WAVEFORM_PLOT_SIZE LONG_BURST_SAMPLE_COUNT
#endif

// Disable message timeout defined in spectrum.h
const int NullMessageTimeout = -1;


/** Main application widget
 *
 *  responsible for connecting the various UI
 *  elements to the Engine.
 */

class PactorTestUI : public QMainWindow
{
    Q_OBJECT

public:
    explicit PactorTestUI();
    virtual ~PactorTestUI();

    Q_DECL_IMPORT PactorSession *my_tnc;

    void setMyTNC(PactorSession *my_tnc);

public slots:
    void processMessageFromPactorTNC(t_struct_msgptr, qint16 blocksize);
    void SetCenterFrequencyInt(int frequency);
    void changeMode(int buttonChecked);
    void inputTextChanged(void);
    void breakRequest(void);
    void tncVisibleToggle(void);
    void QrtRequest(void);
    void shutdownRequest(void);
    void myCallTextChanged(const QString &);
    void auxCallTextChanged(const QString &);
#ifdef QPLAINTEXT_LOG
    void pactorLogAppendText(const QString &output_string);
#endif

protected:
    bool eventFilter(QObject *obj, QEvent *ev);

signals:

private slots:
    void inputTextTimerCallback(void);
    void myCallTextTimerCallback(void);
    void centerFrequencyTimerCallback(void);
    void any_useTimerCallback(void);

private:
    bool            AFC_on;
    bool            comm_active;

    quint16                 blockSize;

    int             last_received_frequency;
    int             center_frequency;
    int             AFC_limit;
#ifdef GRAPHICS_OUT
    int             graph_first_sample_index;

    double          *xs;
    double          *ys;

    float           wave_samples[MAX_MESSAGE_BUFFER_SIZE - (4 * sizeof(qint16))];

    QString         *wave_title;
#endif

    QByteArray       input_text;

    QString          my_call;
    QString          my_auxCall;
    QString          his_call;

    QLabel          *speedLabel;
    QLabel          *repeatLabel;
    QLabel          *m_frequency;

    QWidget         *buttonPanel;

    QButtonGroup    *m_RBGMode;
    QButtonGroup    *m_beventGroup;

    QSlider             *m_freq_slider;

    QSpinBox            *speedSpinBox;
    QSpinBox            *repeatSpinBox;

    QGroupBox           *spinBoxesGroup;
    QGroupBox           *controlGroup;
    QGroupBox           *modeGroup;
    QGroupBox           *eventGroup;
    QGroupBox           *miscGroup;
    QGroupBox           *m_LEDStatus;

    QRadioButton        *m_RBConnect;
    QRadioButton        *m_RBListenARQ;
    QRadioButton        *m_RBListen;
    QRadioButton        *m_RBUnProto;
    QRadioButton        *m_RBWaitForCall;
    QRadioButton        *m_RBStopAll;

    QPushButton         *m_break;
    QPushButton         *m_qrt;
    QPushButton         *m_shutdown_modem;
    QPushButton         *m_tnc_visible;

    QVBoxLayout         *miscLayout;
    QVBoxLayout         *windowLayout;
    QVBoxLayout         *modeLayout;
    QVBoxLayout         *eventLayout;

    QHBoxLayout         *LEDLayout;
    QHBoxLayout         *spinBoxLayout;
    QHBoxLayout         *ledRow_layout;

    QSpacerItem         *v_spacerItem1;
    QSpacerItem         *v_spacerItem2;
    QSpacerItem         *v_spacerItem3;

    QSpacerItem         *h_spacerItem1;

    QGridLayout         *grid_ctrl;
    QGridLayout         *grid_call;


    QTextCursor         m_cursor;

    QLineEdit           *m_inputCommand;
    QLineEdit           *m_myCall;
    QLineEdit           *m_myauxCall;
    QLineEdit           *m_PTRemoteCall;


    QPlainTextEdit      *m_inputText;
    QPlainTextEdit      *m_debug_msgs;
#ifdef QPLAINTEXT_LOG
    QPlainTextEdit      *m_Pactor_log;
#endif

    QTimer          *input_text_timer;
    QTimer          *myCall_text_timer;
    QTimer          *multi_purpose_timer;
    QTimer          *frequency_timer;

    QSettings       *settings;

    //        QToolButton     *LED[12];
    QLabel          *LED[12][2];

    //        QIcon           m_Icon[3];
    QPixmap        m_Icon[3];

    t_icon_struct   icon_info[LED_COUNT];

#ifdef GRAPHICS_OUT
    QwtPlot         *myPlot;
    QwtPlotCurve    *curve1;
#endif

    MyValidator         *callsignValidator;
    MyValidatorMultiple *callsignValidatorMultiple;

    void createUi();
    void connectUi();
    void sendHighPriorityMessage(t_struct_msg *, qint16 blocksize);
    void translateText();
  //  void readSettings();
  //  void writeAllSettings();
  //  void writeSettings(int setting_idx);
};

class MyValidator : public QValidator
{
        Q_OBJECT
public:
        explicit MyValidator(QObject *parent = 0);
        QValidator::State validate(QString &input, int &) const;

    private:
        QRegExp     *single_callsign;
};

class MyValidatorMultiple : public QValidator
{
        Q_OBJECT

public:
        explicit MyValidatorMultiple(QObject *parent = 0);
        QValidator::State validate(QString &input, int &) const;

private:
        QRegExp         *single_callsign;
};
#endif // PACTORTESTUI_H
