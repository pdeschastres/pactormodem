/*
    QJack make you connect with jack soundserver system
    Copyright (C) 2011  Alessandro Siniscalchi <asiniscalchi@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef QOUTPORT_H
#define QOUTPORT_H

#include "outport.h"

#include <QtCore/QObject>

namespace QJack{

template <typename T>
class QOutPortBase : public QObject, public OutPortBase<T>
{
public:
    QOutPortBase<T>(QString name, quint32 bufferSize, QObject *parent = 0);
};

//! Use QOutPort if you haven't particular needings
typedef QOutPortBase<jack_default_audio_sample_t> QOutPort;

// definitions
template <typename T> QOutPortBase<T>::QOutPortBase(QString name, quint32 bufferSize, QObject *parent):
    QObject(parent),
    OutPortBase<T>(name,bufferSize)
{
}

} // namespace

#endif // QOUTPORT_H
