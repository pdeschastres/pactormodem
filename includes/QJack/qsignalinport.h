/*
    QJack make you connect with jack soundserver system
    Copyright (C) 2011  Alessandro Siniscalchi <asiniscalchi@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#ifndef QSIGNALINPORT_H
#define QSIGNALINPORT_H
#include <QtCore/QObject>
#include <QtCore/QVector>

#include "qjackclient.h"
#include "jackinport.h"


namespace QJack {


class QSignalInPort : public QObject, public JackInPort
{
    Q_OBJECT

public:
    QSignalInPort(QString name, QJackClient *parent);

private:
    QVector<jack_default_audio_sample_t> m_data;

    void setData(void *data, quint32 nFrames);

signals:
    void jackData(const QVector<float>);
};

} // namespace

#endif // SIGNALINPORT_H
