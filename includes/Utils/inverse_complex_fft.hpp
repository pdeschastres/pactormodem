#ifndef INVERSE_COMPLEX_FFT_HPP
#define INVERSE_COMPLEX_FFT_HPP
#include <fftw3.h>

#include <QtCore/QObject>

#include "complex_fft.hpp"

class InverseComplexFFT :  public ComplexFFT
{
    Q_OBJECT

public:
    explicit InverseComplexFFT(int size, QObject *parent = 0);

    void clearInData();

signals:

public slots:
};
#endif // INVERSE_COMPLEX_FFT_HPP
