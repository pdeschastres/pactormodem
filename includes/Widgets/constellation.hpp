#ifndef CONSTELLATION_H
#define CONSTELLATION_H
#include <qwt_plot.h>
#include <qwt_symbol.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_scaleitem.h>

#include <QtCore/QEvent>
#include <QtGui/QCloseEvent>
#include <QtWidgets/QWidget>

#include "iqdata.hpp" // calc thread for plot

class Constellation : public QwtPlot
{
    Q_OBJECT

public:
    Constellation(QWidget *parent=0);
    ~Constellation();

    void setNewSize(quint32 w, quint32 h);

protected:
    void changeEvent(QEvent *);
    void closeEvent(QCloseEvent *);

public slots:
    void drawData(QVector<double> x, QVector<double> y);

private:
    int d_timerId;
    int d_paintedPoints;

    QwtPlotCurve     *curve_1,
                     *curve_2,
                     *curve_3;
    QwtSymbol        *scatter_symbol_1,
                     *scatter_symbol_2,
                     *scatter_symbol_3;
    QwtPlotScaleItem *scaleX;
    QwtPlotScaleItem *scaleY;

    IQData myiq;     // define calc thread

    void translateText();
    void setupPlot();
    void initGradient();
    void updateCurve();
    void incrementInterval();
};
#endif // CONSTELLATION_H
