#ifndef IQDATA_H
#define IQDATA_H
#ifdef Q_OS_LINUX
# include <sys/types.h>
# include <sys/stat.h>
# include <sys/ioctl.h>
# include <iostream>
# include <fcntl.h>
# include <string>
# include <sstream>
# include <time.h>
#endif
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <complex>
using namespace std;

#include <QtCore/QThread>
#include <QtCore/QVector>
#include <QtWidgets/QWidget>

class IQData : public QThread
{
    Q_OBJECT

public:
    IQData(QWidget *parent=0);
    ~IQData();

    void setup(int t_adapter, int t_loop, int t_persistence);
    void sweep();
    void closeadapter();

signals:
    void signaldraw(QVector<double> x, QVector<double> y);

protected:
    void run();

private:
    int adapter, loop, persistence;
    int frontend_fd;

    QVector<double> x;
    QVector<double> y;

    //QString frontend_devname;
};
#endif // IQDATA_H
