#ifndef VU_METER_H
#define VU_METER_H
#include <qwt_thermo.h>

#include <QtWidgets/QWidget>

class VUMeter : public QwtThermo
{
    Q_OBJECT

public:
    VUMeter(QWidget *parent=0);
    ~VUMeter();

public slots:
    void set_value(double val);

protected:


private:
};
#endif // VU_METER_H
