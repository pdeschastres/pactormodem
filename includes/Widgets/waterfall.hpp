#ifndef WATERFALL_HPP
#define WATERFALL_HPP
#include <qwt_plot.h>
#include <qwt_plot_spectrogram.h>

class Waterfall : public QwtPlot
{

public:
    Waterfall(QWidget *parent=0);
    virtual ~Waterfall();


public Q_SLOTS:
    void showContour( bool on );
    void showSpectrogram( bool on );
    void setAlpha( int );

#ifndef QT_NO_PRINTER
    void printPlot();
#endif

private:
    QwtPlotSpectrogram *d_spectrogram;
};
#endif
