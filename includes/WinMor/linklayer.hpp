#ifndef LINKLAYER_HPP
#define LINKLAYER_HPP

#include "qjackclient.h"
#include "qinport.h"
using namespace QJack;

#include <QtCore/QObject>

//#include "reedsolomon.h"

class LinkLayer :  public QJack::QInPort
{
    Q_OBJECT

public:
    explicit LinkLayer(QString portname, quint32 buffersize, QObject *parent = 0);

signals:

public slots:

private:
    /// \brief setData Provide a bufferful of samples to JACK
    ///
    /// \param data This is the buffer supplied by JACK
    /// \param nFrames The number of frames to supply to JACK
    ///
    void setData(void *data, quint32 nFrames);
};
#endif // LINKLAYER_HPP
