#ifndef WINMORSESSION_H
#define WINMORSESSION_H
#include <QtCore/QEvent>
#include <QtGui/QCloseEvent>
#include <QtGui/QResizeEvent>
#include <QtGui/QMoveEvent>

# include <QtWidgets/QMainWindow>

#include "winmor_globals.hpp"
#include "winmortnc.hpp"

QT_FORWARD_DECLARE_CLASS(QMenu)

class QVarian;
class QAction;
class QButtonGroup;
class QComboBox;
class QFrame;
class QHeaderView;
class QLabel;
class QLineEdit;
class QPushButton;
class QTextBrowser;
class QHBoxLayout;
class QVBoxLayout;
class QSpacerItem;

class WinMorSession : public QMainWindow
{
    Q_OBJECT

public:
    WinMorSession(QWidget *parent=0);
    ~WinMorSession();

    void setAFC(bool a);
    void setARQ(bool a);
    void setHuffman(bool h);
    void setCenterFreq(int f);
    void setFreqLimit(int f);
    void setMyCall(QString c);

    QString getRmtCall();

protected:
    void changeEvent(QEvent *);
//    void moveEvent(QMoveEvent *);
//    void resizeEvent(QResizeEvent *);
    void closeEvent(QCloseEvent *);

signals:

private slots:

private:
    QPoint pos;
    QSize  size;

    QWidget *centralwidget;

    QMenuBar *menubar;

    QMenu   *menu_File;
    QMenu   *menuSetup;
    QMenu   *menuShowHide;
    QMenu   *menuClose;
    QMenu   *menuConfig;
    QMenu   *menuHelp;

    QAction *actionE_xit;
    QAction *actionShow_TNC;
    QAction *actionHide_TNC;
    QAction *actionProperties;
    QAction *actionTelnet_Setup;
    QAction *actionWinMor_Setup;
//    QAction *actionWinMor_Channels;
//    QAction *actionAbout_PactorModem;
//    QAction *actionFileAudioSetup;
//    QAction *actionTestOn;
//    QAction *actionTestOff;
    QAction *actionHelp_Idx;
    QAction *actionHelp_Contents;
    QAction *actionHelp_About;

    QStatusBar *statusbar;

    QLabel *lblChan;
    QLabel *lblCall;
    QLabel *lblCFreq;
    QLabel *lblDialFreq;
    QLabel *lblConnTime;

    QComboBox *cboChannel;
    QComboBox *cboCallSign;

    QLineEdit *lineEditCentFreq;

    QPushButton *btnStart;
    QPushButton *btnStop;
    QPushButton *btnClose;
    QPushButton *btnHide;
    QPushButton *btnTeston;
    QPushButton *btnTestoff;

    QFrame *fraStatus;

    QTextBrowser *textBrowser;

    QVBoxLayout  *mainLayout;
    QHBoxLayout  *toolbtnLayout;
    QHBoxLayout  *line1Layout;
    QHBoxLayout  *line2Layout;

    QSpacerItem  *line1Spacer1;
    QSpacerItem  *line1Spacer2;
    QSpacerItem  *line1Spacer3;

    QSpacerItem  *line2Spacer1;
    QSpacerItem  *line2Spacer2;
    QSpacerItem  *line2Spacer3;
    QSpacerItem  *line2Spacer4;
    QSpacerItem  *line2Spacer5;

    QSpacerItem  *btnSpacer1;
    QSpacerItem  *btnSpacer2;
    QSpacerItem  *btnSpacer3;
    QSpacerItem  *btnSpacer4;
    QSpacerItem  *btnSpacer5;
    QSpacerItem  *btnSpacer6;

    void createObjects();
    void createMenus();
    void createActions();
    void translateText();
    void readSettings();
    void writeSettings();
};
#endif // WINMORSESSION_H
