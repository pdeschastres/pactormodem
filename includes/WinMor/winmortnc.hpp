#ifndef WINMORTNC_HPP
#define WINMORTNC_HPP

#include <QtCore/QStateMachine>
#include <QtCore/QEvent>
#include <QtCore/QState>
#include <QtCore/QEventTransition>
#include <QtCore/QTimer>
#include <QtCore/QTimerEvent>
#include <QtGui/QMoveEvent>
#include <QtGui/QCloseEvent>
#include <QtGui/QResizeEvent>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QWidget>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QProgressBar>
#include <QtNetwork/QUdpSocket>

QT_FORWARD_DECLARE_CLASS(QDir)
QT_FORWARD_DECLARE_CLASS(QFile)
QT_FORWARD_DECLARE_CLASS(QFileDialog)
QT_FORWARD_DECLARE_CLASS(QAction)
QT_FORWARD_DECLARE_CLASS(QSettings)
QT_FORWARD_DECLARE_CLASS(QFrame)
QT_FORWARD_DECLARE_CLASS(QImage)
QT_FORWARD_DECLARE_CLASS(QButtonGroup)
QT_FORWARD_DECLARE_CLASS(QHeaderView)
QT_FORWARD_DECLARE_CLASS(QMenu)
QT_FORWARD_DECLARE_CLASS(QMenuBar)
QT_FORWARD_DECLARE_CLASS(QToolBar)
QT_FORWARD_DECLARE_CLASS(QStatusBar)
QT_FORWARD_DECLARE_CLASS(QAudioFormat)
QT_FORWARD_DECLARE_CLASS(QHBoxLayout)
QT_FORWARD_DECLARE_CLASS(QVBoxLayout)
QT_FORWARD_DECLARE_CLASS(QSpacerItem)

#include "winmor_globals.hpp"
#include "modem.hpp"

#include "vu_meter.hpp"
#include "constellation.hpp"
#include "waterfall.hpp"

// TODO: add the back end modules used

#define MAX_METERS 32
#define RMS_BUF_SIZE 256
#define RING_BUF_SIZE 1024

class WinMorSession;

class WinMorTNC : public QMainWindow
{
    Q_OBJECT

public:
    // local storage and control for modem settings of all modes //
    struct WM_control_block
    {
        bool CWId;               // saved in settings
        bool AutoBREAK;
        bool BusyLock;
        bool robustON;
        bool useVOX;
        bool listenON;
        bool debugLogON;         // saved in settings
        bool sessionLogON;       //
        bool fecRcv;
        quint16 Bandwidth;       // saved in settings
        quint16 DriveLevel;      // saved in settings
        quint32 TCPControlPort;  // Data port is next sequential. Saved in settings
        quint16 responseDly;
        quint16 leaderExt;
        quint16 maxReqCon;
        quint16 squelchLvl;
        QString CaptureDevice;   // saved in settings
        QString Mode;
        QString MyCallsign;      // saved in settings
        QString MyGridsquare;    // saved in settings
        QString PlaybackDevice;  // saved in settings
        QString Registration;    // TODO: saved in settings
        QString Suffix;
        QString tcpAddress;
        ModemMode activeCodec;
    } WMCB;

    WinMorTNC();
    ~WinMorTNC();

    /*TODO:  1.  Add states for xmit and rcv
             2.  Develop sound engine receive start, run and stop
             3.  Develop autoswitch logic based on states
             4.  Develop modulator with threads
             5.  Develop sound engine output start, run, and stop
             6.  Develop waterfall widget
             7.  Develop datascope for winmor
    */
    void setTestOn();
    void setTestOff();
    void reset();
    void initTNC();
    void setBW(int bw);  // example control from winmor module dup for all others

public slots:

signals:

protected:
    void resizeEvent(QResizeEvent *);
    void moveEvent(QMoveEvent *);
    void changeEvent(QEvent *);
    void closeEvent(QCloseEvent *);

    //TODO: develop transitions as indicated in state diagram for WinMOR by KN6KB//
    // this is stub code to start state machine development
    // You may want a transition action that flashes the new state LED
    // with a bipolar flip-flop timer that is running
    // until transition has been completed and is in new state.
    /*
    QEventTransition *transIdletoSendID(); // transition from Idle to send ID
    QEventTransition *transModeChgIRSISS();  // mode chg from IRS to ISS
    QEventTransition *transISStoIRS();  // transition from ISS to IRS
    QEventTransition *transISStoIdle(); // transition from ISS to Idle
    QEventTransition *transIRStoIdle(); // transition from IRS to Idle
    QEventTransition *transSendIDtoDiscon(); // transition from ID Sent to Disconn
    QEventTransition *transDisconToConnPend();
    QEventTransition *transConPendToConn(); // accept CONNREQ to Connected target state
    QEventTransition *transIRSACKConnReq(); // send ConnReq ACK and set IRS
    QEventTransition *transDisconRcvACK(); // transition from SendID to ACK Discon
    QEventTransition *trans500to1600(); // transition from narrow to wideband
    QEventTransition *trans1600to500(); // transition from wide to narrowband
    QEventTransition *transDisconToConn(); // transition from disconnected to connd
    QEventTransition *transConnToDisconn(); // transition to disconnected
    */
    //       in the code where they should go.
    //   state machine events   //
    void eventConnecting_bwreqACKrcvd();
    void eventConnecting_connreqTimeout();
    void eventConnPend_callDecoded();
    void eventConnPend_callDecodeFail();
    void eventDiscond_rcvConReqFrame();
    void eventDisconing_ctlTimeout();
    void eventIRS_rcvDatagoodID();
    void eventIRS_rcvDataBADID();
    void eventIRS_ctrlRcvdReqLstPSN();
    void eventIRS_inactivTimeout();
    void eventIRS_rcvConnReqFrame();
    void eventIRS_rcvDisconReqFrame();
    void eventIRS_rcvIDFrame();
    void eventIRS2ISS_ACK00Rcvd();
    void eventIRS2ISS_NOACKTimeout();
    void eventIRS2ISS_inactivTimeout();
    void eventIRS2ISS_disconnRcvd();
    void eventIRSMS_rcvData();
    void eventIRSMS_rcvCtlFrameIdle();
    void eventIRSMS_inactivTimeout();
    void eventIRSMS_disconReqRcv();
    void eventISS_rcvACK();
    void eventISS_inactivTimeout();
    void eventISS_disconReqRcv();
    void eventISS_idTimeout();
    void eventISSMS_psnRcvd();
    void eventISSMS_inactivTimeout();
    void eventISSMS_disconReqRcvd();
    void eventSENDID_idTimeout();
    void eventSENDID_disconReqRcvd();
    void eventFEC500_startCMD();
    void eventFEC500_allDataSent();
    void eventFEC500_abortFEC();
    void eventFEC500_rcvData();
    void eventFEC1600_startCMD();
    void eventFEC1600_allDataSent();
    void eventFEC1600_abortFEC();
    void eventFEC1600_rcvData();
    //    end state machine events  //

signals:
    void emitMsgRcvd();
    void emitMsgSent();
    void emitConnecting();
    void emitDisconnecting();
    void emitDisconnected();
    void signalOutboundData();



private slots:
    // used to play recorded file //
    void slotAudioInfo();
    void slotAudioStateChanged();
    void slotAudioPositionChanged(qint64 position);
    void slotAudioBufferLengthChanged(qint64 length);
    void slotStartPlay();
    void slotPlayStarted();
    void slotSendData();
    // TODO: develop implementation later for playing a .wav file
    void slotPlayPositionChanged(qint64 p, qint64 l, const QByteArray b);
    void slotAudioPosChanged(qint64 l, qint64 p, const QByteArray& b);
    void slotShowFileDialog();
    //                            //
    void slotSmallPanel();
    void slotLrgPanel();
    void slotExit();
    void slotPTTon();
    void slotPTToff();
    void slotFileLoaded();
    void slotTxLevelTest();
    void slotUpdateDisplay();
    void slotSetIdleState();
    void slotDispConstl();

    quint32 sampleAudioLvl();

private:
    bool ptt;  // indicates T/R state
    bool settings_dirty;  // only applies to stored settings

    int view;  // used to set view menu
    int frequency;
    int channels;
    int samplesize;
    int sampletype;
    int byteorder;
    int m_infoMessageTimerId;

    QPoint     pos;
    QSize      size;

    QString    m_InputDevice;
    QString    m_OutputDevice;

    QWidget *centralwidget;

    QDir   *dir;
    QFile  *file;
    QFileDialog *fileDlg;

    QProgressBar *m_progressbar;

    QFont  *font;

    QLabel *m_infoMessage;
    QLabel *ledXMIT;   // displays transmit state
    QLabel *ledRCV;    // displays receive  state
    QLabel *ledUNPROTO; // displays UNPROTO state
    QLabel *ledCONN;   // displays when connected
    QLabel *ledERR;    // displays rcv error
    QLabel *ledIRS;    // displays In-Rcv  State
    QLabel *ledISS;    // displays In-Send State
    QLabel *ledLISTEN; // displays In-Listen State
    QLabel *ledDREQ;   // displays Disconn Req
    QLabel *ledBUSY;   // displays Channel Busy

    QLabel *lblXMIT;
    QLabel *lblRCV;
    QLabel *lblUNPROTO;
    QLabel *lblCONN;
    QLabel *lblERR;
    QLabel *lblIRS;
    QLabel *lblISS;
    QLabel *lblLISTEN;
    QLabel *lblDREQ;
    QLabel *lblBUSY;

    QLabel *lblConnState;
    QLabel *lblConnStateTxt;
    QLabel *lblXmitTitle;
    QLabel *lblFameTitle;
    QLabel *lblFrame;
    QLabel *lblOutBytes;
    QLabel *lblBytesQd;
    QLabel *lblSentBytes;
    QLabel *lblSentBytesConf;
    QLabel *lblThruMsg;
    QLabel *lblRcvTitle;
    QLabel *lblRcvLvl;
    QLabel *lblLED;
    QLabel *lblQd;
    QLabel *lblQueued;
    QLabel *lblSeq;
    QLabel *lblSequenced;
    QLabel *lblRcvFrTitle;
    QLabel *lblRcvFrame;
    QLabel *lblBzyTitle;
    QLabel *lblBuzyLED;
    QLabel *label;
    QLabel *label_2;
    QLabel *lblThruPut;

    QAction *m_txLevelTestAction;
    QAction *m_pactorModeSettingsAction;
    QAction *m_viewSmallPanel;
    QAction *m_viewLrgPanel;
    QAction *m_generateToneAction;
    QAction *m_modechgAction;
    QAction *m_loadFileAction;
    QAction *m_playFileAction;
    QAction *m_pauseFileAction;
    QAction *m_recordAction;
    QAction *m_settingsAction;
    QAction *m_exitWinMorAction;
    // for testing only - remove later
    QAction *m_sendDataAction;

    QMenuBar   *menubar;

    QMenu      *fileMenu; // mostly for testing but could remain for captured audio
    QMenu      *setupMenu;
    QMenu      *modeMenu;
    QMenu      *viewMenu;
    QMenu      *exitMenu;

    QStatusBar *statusbar;

    QToolBar   *buttonPanel;
    // toolbar icons defined //
    QIcon       m_settingsIcon;
    QIcon       m_recordIcon;
    QIcon       m_pauseIcon;
    QIcon       m_playIcon;
    QIcon       m_modeIcon;
    QIcon       m_fileIcon;
    QIcon       m_exitIcon;
    QIcon       m_testIcon;

    QGroupBox  *group;
    QGroupBox  *grpLED;
    QGroupBox  *grpDisplay;

    QPixmap    *redon;
    QPixmap    *greenon;
    QPixmap    *yelon;
    QPixmap    *blueon;

    QImage     *meter_face;

    QFrame *fraConnState;
    QFrame *fraXmit;
    QFrame *fraRcv;

    QVBoxLayout  *mainLayout;
    QVBoxLayout  *leftdispLayout;
    QHBoxLayout  *specLayout;
    QHBoxLayout  *infoLayout;
    QHBoxLayout  *buttonPanelLayout;

    QGridLayout  *ledLayout;
    QGridLayout  *dispLayout;

    QSpacerItem  *line1Spacer1;
    QSpacerItem  *line1Spacer2;
    QSpacerItem  *line1Spacer3;

    QSpacerItem  *ledSpacerL;
    QSpacerItem  *ledSpacerR;

    QSpacerItem  *btnSpacer1;
    QSpacerItem  *btnSpacer2;
    QSpacerItem  *btnSpacer3;
    QSpacerItem  *btnSpacer4;
    QSpacerItem  *btnSpacer5;
    QSpacerItem  *btnSpacer6;

    // This is the WinMOR State Machine //
    // The WinMOR states have been well diagramed by KN6KB and
    // implemented here using Qt StateMachine framework
    QStateMachine    *m_statemachine;
    QState           *m_tncstate;
    // States Definitions //
    QState *state_ISS;
    QState *state_IRS;
    QState *state_IDLE;
    QState *state_SENDID;
    QState *state_ISS2IRS;
    QState *state_IRS2ISS;
    QState *state_CONNECTED;
    QState *state_DISCONNECTED;
    QState *state_CONNPENDING;
    QState *state_DISCONNPENDING;
    QState *state_IRS_MODESHIFT;
    QState *state_ISS_MODESHIFT;
    QState *state_FEC500;
    QState *state_FEC1600;
    // END TNC STATE MACHINE  DEFINITIONS  //

    QTimer        *vuTimer;

    VUMeter       *vu_meter;

    Constellation *scope;

    Waterfall     *waterfall;

    ModemMode      m_mode;

    WinMorSession *session;

    OpenModem     *modem;

    // TODO: add here the back end modules used by the codec
    //       that need to be controlled or interact with the TNC


    void createActions();
    void createObjects();
    void createMenus();
    void connectSignals();
    void createStateMachine();
    void createWinMorStates();
    void translateText();
    void setMode(ModemMode m_Mode);
    void infoMessage(const QString &message, int timeoutMs);
    void updateMenuStates();
    void updateButtonStates();
    void updateTNCDisplay();
    void updateModeMenu();
    void readSettings();
    void writeSettings();
};
#endif // WINMORTNC_HPP
