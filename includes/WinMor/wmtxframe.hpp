#ifndef WMTXFRAME_HPP
#define WMTXFRAME_HPP
#include <queue>
using namespace std;

//#include "qjackclient.h"
#include "qoutport.h"
using namespace QJack;

#include <QtCore/QDebug>
#include <QtCore/QObject>
#include <QtCore/QRegExp>
#include <QtCore/QStringList>
#include <QtCore/QByteArray>

#include "inverse_complex_fft.hpp"
#include "asciipacker.hpp"
#include "reedsolomon.hpp"
#include "crcwinmor.hpp"
#include "modulator.hpp"
#include "stationid.hpp"


typedef unsigned char byte_t;
typedef QByteArray byte_vec;



/// The data type used to represent an output waveform.
///TODO: synchronize this somehow with QJack
typedef float sample_t;
//typedef int16_t wave_t ;  // This is what Rick used. We're going to try floats.

/*! \brief A base class the represents a WinMOR frame to be transmitted.
 * This is (to be) a virtual base class that will contain all the
 * essential machinery for modulating a WinMOR transmit frame.
 * In final form it will need to be told various configurable parameters
 * which are simply hard-wired in in the present development form.
 * The various virtual methods are not yet virtual as we are testing them.
 */
class WMTxFrame : public QObject
{
    Q_OBJECT
public:
    /*! \brief Construct a frame, initialize basics
     * \param modulator The modulator to delegate to
     * \param parent The QObject parent.
     */
    explicit WMTxFrame(Modulator *modulator, QObject *parent = 0);

    static const double centerFrequency = 1500.; // Hz. Protocol specified.

    /// Append pilot tone to wave buffer.
    void appendPilot();
    /// Append the frame type encoded as per specification
    void appendFrameType(int frameType);
    /// Insert a leader into the wave buffer.
    void makeLeader4FSK(byte_t ftype);
    void appendCRC16(byte_vec &data);
    void sendConnectRequest(WMStationID *caller, WMStationID *called);
    void sendStationID(WMStationID *caller);
    void sendGenericFrame(int frametype, byte_vec payload);
    void modulateMultiCarrier4FSK(byte_vec symbols, int nCarriers);
    void sendCodedControlFrame(quint16 sessionId, byte_t code);
    void sendAck(quint16 sessionId, byte_t ackcode);
    /*! \brief Pack callsigns and their SSIDs into 12 bytes.
     *  The callsigns are expected to be 7 ASCII chars, and
     *  optionally a hyphen followed by one or two digits, 1 - 15.
     * \param The callsign of the calling station + optional SSID.
     * \param The callsign of the station being called + optional SSID.
     * \return 12 bytes of packed data.
     */
    byte_vec packCallsign(WMStationID caller, WMStationID called );
    /*! \brief Pack ASCII characters A-Z into bytes taken in 6-bit chunks.
     *  Reduce the size of an ASCII char by starting at 32 and masking 6 bits.
     *  Pack 4 of those into 3 bytes: 4*6 = 3*8 bits.
     * \param The string to be packed.
     * \return The packed bytes.
     */
    byte_vec packASCII(QString str);

public slots:
    /*! \brief Start the transmission of the frame into the sound channel.
     */
    void startTx();

signals:
    /*! \brief The entire frame has been emptied into the sound channel.
     * Though the frame has gone into the channel it hasn't necessarily
     * been converted to sound but whatever is coming up next may start
     * sending more now.
     */
    void txFinished();

protected:
    /// The modulator this frame delegates to.
    Modulator *modulator;

    /// The number of samples used to transmit each symbol.
    static const int samplesPerSymbol = 128; /// Must be a power of two.
    /// The system sampling rate. Currently fixed for no particular reason.
    static const int samplingRate = 12000;
    /// The protocol-defined pilot tone frequency in Hertz.
    static const double pilotFrequency = 1500;
    /// Peak amplitude of output waveform before clipping is applied.
    static const sample_t peakAmplitude = .9;    // 0dB for JACK, I think.
    /// The protocol-defined number of pilot bursts to kick off with.
    static const int leaderLength = 20;
    /// The protocol-defined (?) number of extra pilot bursts to add if
    /// configured for the use of vox PTT.
    static const int voxPilotExtension = 12; // TODO: maybe this is a parameter?

    //    /// Hamming 8,4 encoding table for a single 4-bit word.
//    byte_t hamming84Table[16] = {0x00, 0x71, 0xE2, 0x93, 0xE4, 0xA5, 0x36, 0x47, 0xB8, 0xC9, 0x5A, 0x2B, 0x6C, 0x1D, 0x8E, 0xFF};
    /// Root Raised Cosine envelope table.
    double rootRaisedCosineWindow[samplesPerSymbol];
    /// TODO: this status variable is temporary until we have the actual
    /// TODO: state machine of the modulator implemented.
    bool txIdle;
    /// The buffer used to assemble the parts of the frame.
    /// It's a queue for the time being but this might change.
    queue<sample_t> waveBuffer;

    /// The fft processor objects;
    InverseComplexFFT* icfft128;
    InverseComplexFFT* icfft256;

    /// Reed-Solomon codecs
    ReedSolomon* reedSolomon8x7;
    ReedSolomon* reedSolomon4x4;

    void setData(void *data, quint32 nFrames);
};
#endif // WMTXFRAME_HPP
