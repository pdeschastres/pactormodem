/* modem.hpp ****************************************************************************
 * proposed generic modem interface definition
 *
 * **************************************************************************************/
#ifndef MODEM_HPP
#define MODEM_HPP

#include <QtCore/QObject>
#include <QtCore/QHash>
#include <QtCore/QTime>
#include <QtCore/QByteArray>
#include <QtGui/QTextTableFormat>
#include <QtCore/QEvent>
#include <QtGui/QCloseEvent>
#include <QtNetwork/QAbstractSocket>
#include <QtNetwork/QHostAddress>

#include "modem_globals.hpp" //for global definitions of macros and variables

static const int MaxBufferSize = 1024000;

class QTimer;
class QTcpSocket;
class QNetworkSession;
class WinMor;
class Pactor;

class OpenModem : public QObject
{
    Q_OBJECT   // so that we can use signals, slots, and events

public:
    OpenModem();
    virtual ~OpenModem();

    enum ConnectionState
    {
        WaitingForGreeting,
        ReadingGreeting,
        ReadyForUse
    };
    enum DataType
    {
        Greeting,
        Mode,
        ModeExact,
        PlainText,
        Undefined,
        Ping,
        Pong
    };

    // local storage and control for modem settings of all modes //
    struct OM_control_block
    {
        bool CWId;               // saved in settings
        bool AutoBREAK;
        bool BusyLock;
        bool robustON;
        bool useVOX;
        bool listenON;
        bool debugLogON;         // saved in settings
        bool sessionLogON;       //
        bool fecRcv;
        quint16 Bandwidth;       // saved in settings
        quint16 DriveLevel;      // saved in settings
        quint32 TCPControlPort;  // Data port is next sequential. Saved in settings
        quint16 responseDly;
        quint16 leaderExt;
        quint16 maxReqCon;
        quint16 squelchLvl;
        QString CaptureDevice;   // saved in settings
        QString Mode;
        QString MyCallsign;      // saved in settings
        QString MyGridsquare;    // saved in settings
        QString PlaybackDevice;  // saved in settings
        QString Registration;    // TODO: saved in settings
        QString Suffix;
        QString tcpAddress;
        ModemMode activeCodec;
    } OMCB;

    int errNo;
    QString errMsg;

    ModemMode current_mode;

    void sendTCPMessage(const QString &message);
    void disconnect();
    bool hasConnection(const QHostAddress &senderIp, int senderPort = -1) const;
    QString nickName() const;

signals:
    void newInData(QString);

protected:
    void timerEvent(QTimerEvent *timerEvent);

public slots:

private slots:
    void slotConnected();
    void slotConnectionError(QAbstractSocket::SocketError);
    void slotUpdateClient(qint64);
    void slotReadyRead();
    void disconnected();
    void connectionError();
    void processReadyRead();
    void sendGreetingMessage();
    void sendPing();

private:
    bool boolTcpPortOpen;
    bool isGreetingMessageSent;

    int bytesToWrite;
    int bytesWritten;
    int bytesReceived;
    int bytesToRcv;
    int bytesAvailable;
    int transferTimerId;
    int TotalBytes;
    int numBytesForCurrentDataType;
    int m_buffers;         // number of buffers set or returned
    int m_bw;              // bandwidth size set or returned

    quint16 blockSize;

    QString id;
    QString myNickName;
    QString username;
    QString greetingMessage;
    QString m_grid;        // gridsquare set or returned

    QByteArray buffer;

    QTcpSocket      *tcpconnection;
    QNetworkSession *networkSession;

    QTime   pongTime;
    QTimer *pingTimer;

    DataType   currentDataType;
    ConnectionState state;
    ModemMode  m_mode;

    Pactor *pactor_codec;
    WinMor *winmor_codec;

    void processData();
    void processCMD(QString cmd);
    void commandInterpreter(QString cmd);
    void removeConnection(QTcpSocket *);
    void sendMessage(const char*);
    void readSettings();
    void saveSettings();

    bool readProtocolHeader();
    bool hasEnoughData();

    int dataLengthForCurrentDataType();
    int readDataIntoBuffer(int maxSize);
};
#endif // MODEM_HPP
