# The following macros allow certain features and debugging output
# to be disabled / enabled at compile time.

# Perform pactor decoding calculation in a separate thread
DEFINES += PACTOR_DECODER_SEPARATE_THREAD

# Perform pactor encoding calculation in a separate thread
DEFINES += PACTOR_ENCODER_SEPARATE_THREAD

# Perform pactor modem calculation in a separate thread
DEFINES += PACTOR_MODEM_SEPARATE_THREAD

DEFINES += DISPLAY_DEBUG_VALUES

# Suppress warnings about strncpy potentially being unsafe, emitted by MSVC
win32: DEFINES += _CRT_SECURE_NO_WARNINGS

# display Pactor status in main widget
DEFINES += PROTOCOL_DISPLAY
#DEFINES += DISPLAY_DEBUG_TO_UI
DEFINES += SEND_PKT_DISPLAY
#DEFINES += SHOW_WINDOW
#DEFINES += VOX_CONTROL
DEFINES += DISPLAY_HEX_CONTENT
#DEFINES += APM_ALGO
#DEFINES += LISTEN_MEMORY_ARQ_TEST
#DEFINES += PACKETS_TEST
#DEFINES += HUFFMAN_TEST
#DEFINES += LOG_WAVEFORM
# display Pactor status in main widget
DEFINES += QPLAINTEXT_LOG
# Debug output from Pactor demodulationm calculation
DEFINES += LOG_PACTOR

