#include <QtWidgets/QApplication>

#include "modem.hpp"

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    OpenModem modem;
    modem.setObjectName("modem");

    return app.exec();
}


