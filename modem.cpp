#include <QtCore/QString>
#include <QtCore/QTimer>
#include <QtCore/QList>
#include <QtGui/QPixmap>
#include <QtCore/QSettings>
#include <QtWidgets/QApplication>
#include <QtWidgets/QSplashScreen>
#include <QtNetwork/QNetworkConfigurationManager>
#include <QtNetwork/QNetworkConfiguration>
#include <QtNetwork/QNetworkSession>
#include <QtNetwork/QHostInfo>
#include <QtNetwork/QTcpSocket>

//#define SHOW_MODEM_DEBUG


// includes for codecs go here  //
// connects to public interface of the back end codec
#include "pactor.hpp"
#include "winmor.hpp"

#include "modem.hpp"

static const int  TransferTimeout = 30 * 1000;
static const int  PongTimeout     = 60 * 1000;
static const int  PingInterval    = 5 * 1000;
static const char SeparatorToken  = ' ';

OpenModem::OpenModem() : QObject()
{
#ifdef SHOW_MODEM_DEBUG
    MODEM_DEBUG("Constructor Starting... ");
#endif
    current_mode = ModeIdle;
    boolTcpPortOpen = false;
    isGreetingMessageSent = false;
    state = WaitingForGreeting;
    numBytesForCurrentDataType = -1;
    transferTimerId = 0;
    m_mode = ModeIdle;  // set modem mode to Idle on startup

    greetingMessage = tr("undefined");
    username = tr("unknown");
    currentDataType = Undefined;

    pingTimer = new QTimer(this);
    transferTimerId = pingTimer->timerId();
    pingTimer->setInterval(PingInterval);
    QObject::connect(pingTimer, SIGNAL(timeout()), this, SLOT(sendPing()));

    id = "";
    readSettings();  // get stored settings for modem use

    QNetworkConfigurationManager manager;
    if (manager.capabilities() & QNetworkConfigurationManager::NetworkSessionRequired)
    {
        // Get saved network configuration
        QString tcp = OMCB.tcpAddress.toUtf8();
        quint32 port = (quint32)OMCB.TCPControlPort;
#ifdef SHOW_MODEM_DEBUG
        MODEM_DEBUG("Host ID " + tcp + " : " + port);
#endif
        try
        {
            // If the saved network configuration is not currently discovered use the system default
            QNetworkConfiguration config = manager.configurationFromIdentifier(id);
            if ( (config.state() & QNetworkConfiguration::Discovered) !=
                  QNetworkConfiguration::Discovered
               ) { config = manager.defaultConfiguration(); }

            networkSession = new QNetworkSession(config, this);
            QObject::connect(networkSession, SIGNAL(opened()), this, SLOT(sessionOpened()));
            MODEM_DEBUG("Opening network session.");
            networkSession->open();
        }
        catch (...)
        {
            MODEM_DEBUG("Modem Socket Error configuring:" + networkSession->errorString());
        }
    }

    try
    {
        tcpconnection = new QTcpSocket(this);
        MODEM_DEBUG("Setting Modem Socket Keep Alive ");
        tcpconnection->setSocketOption(QAbstractSocket::KeepAliveOption,true);
        connect(tcpconnection, SIGNAL(connected()),     this, SLOT(slotConnected()));
        connect(tcpconnection, SIGNAL(readyRead()),     this, SLOT(processReadyRead()));
        connect(tcpconnection, SIGNAL(disconnected()),  this, SLOT(disconnected()));
        connect(tcpconnection, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(slotConnectionError(QAbstractSocket::SocketError)));
        qDebug() << "Modem connecting to host " << id << ":" << "8600";
    }
    catch (...)
    {
        MODEM_DEBUG("Modem Socket Error connecting: " + tcpconnection->errorString());
    }
    try
    {
        tcpconnection->connectToHost(id, 8600);
    }
    catch (...)
    {
        MODEM_DEBUG("Modem Socket Error opening port to server: " + tcpconnection->errorString());
    }
    boolTcpPortOpen = tcpconnection->isOpen();
    MODEM_DEBUG( " Modem port to host open: " + boolTcpPortOpen);
}

OpenModem::~OpenModem()
{
    //
    MODEM_DEBUG("...Modem closing");
    try
    {
        if ( tcpconnection->isOpen() ) tcpconnection->close();
        MODEM_DEBUG("Modem connection closed");
    }
    catch (...)
    {
        MODEM_DEBUG("Modem connection closed on exit");
    }
    tcpconnection = 0;
    networkSession = 0;
}

void OpenModem::timerEvent(QTimerEvent *timerEvent)
{
    Q_UNUSED(timerEvent);
    if ( pongTime.elapsed() ) pingTimer->stop();
    MODEM_DEBUG("Modem ping timer timeout");
}

void OpenModem::slotConnected()
{
#ifdef SHOW_MODEM_DEBUG
        MODEM_DEBUG("Modem connected...");
#endif
    QString nick = tcpconnection->peerName();
    tcpconnection->setSocketOption(QAbstractSocket::KeepAliveOption,1);
    numBytesForCurrentDataType = -1;
#ifdef SHOW_MODEM_DEBUG
        MODEM_DEBUG("...Modem " + nick + "ready For Use...");
#endif
    boolTcpPortOpen = tcpconnection->isOpen();
}

void OpenModem::processReadyRead()
{
#ifdef SHOW_MODEM_DEBUG
        MODEM_DEBUG("Processing ReadyRead Data");
#endif
    readProtocolHeader(); // determine header type
    if (state == WaitingForGreeting)
    {
#ifdef SHOW_MODEM_DEBUG
        MODEM_DEBUG("Processing ReadyRead Reading Data - waiting for greeting");
#endif
        readProtocolHeader(); // determine header type
        //buffer = tcpconnection->read(numBytesForCurrentDataType);
#ifdef SHOW_MODEM_DEBUG
        QString msgstr;
        msgstr = "Processing ReadyRead Reading Data - data rcvd" + buffer + " size: " + QString("%1").arg(buffer.size());
        MODEM_DEBUG(msgstr);
#endif
        if (buffer.size() != numBytesForCurrentDataType)
        {
#ifdef SHOW_MODEM_DEBUG
        MODEM_DEBUG("Modem...not enough buffer data returning");
        MODEM_DEBUG("Modem...buffer data" + buffer.size());
        MODEM_DEBUG("Modem...number of bytes " + numBytesForCurrentDataType);
#endif
            pingTimer->start();
            pongTime.start();
            return;
        }
#ifdef SHOW_MODEM_DEBUG
        MODEM_DEBUG("Modem reading greeting");
#endif
        if (currentDataType != Greeting)   return;
#ifdef SHOW_MODEM_DEBUG
        MODEM_DEBUG("Data Type Undefined");
#endif
        QString username = QString(buffer) + '@' + tcpconnection->peerAddress().toString() + ':' + QString::number(tcpconnection->peerPort());
        currentDataType = Undefined;
        numBytesForCurrentDataType = 0;
        buffer.clear();
        MODEM_DEBUG("New state ReadingGreeting");
        if (!isGreetingMessageSent)
        {
            greetingMessage = "greeting";
            sendGreetingMessage();
        }
        state = ReadyForUse;
        return;
    }
    MODEM_DEBUG("process data");
    if ( currentDataType != Greeting )
    {
        MODEM_DEBUG("data not greeting - will return if undefined");
        if (currentDataType == Undefined)
        {
            MODEM_DEBUG("data type undefined");
           return;
        }
    }
    if (currentDataType != Greeting)   return;
    MODEM_DEBUG("Greeting found - New state...ReadyForUse");
    if (!pongTime.isNull()) pongTime.restart();
    if (pingTimer->isActive()) pingTimer->stop();
    state = ReadyForUse;
}

void OpenModem::processData()
{
    MODEM_DEBUG("Reading data to buffer");
    buffer = tcpconnection->read(numBytesForCurrentDataType);
    MODEM_DEBUG("If bytes read != num bytes for data type");
    if (buffer.size() != numBytesForCurrentDataType)   return;
    MODEM_DEBUG("Checking data type");
    switch (currentDataType)
    {
    case Greeting:
        MODEM_DEBUG("New greeting arived: " + QString::fromUtf8(buffer));
        pingTimer->stop();
        pongTime.restart();
        currentDataType = Undefined;
        return;
        break;
    case PlainText:
        MODEM_DEBUG("New message arived: " + QString::fromUtf8(buffer));
        MODEM_DEBUG("Sending Command to Interpreter ");
        commandInterpreter(buffer);
        break;
    case ModeExact:
        MODEM_DEBUG("New message arived: " + QString::fromUtf8(buffer));
        MODEM_DEBUG("Sending Command to Interpreter ");
        commandInterpreter(buffer);
        break;
    case Ping:
        tcpconnection->write("PONG 1 p");
        break;
    case Pong:
        pongTime.restart();
        break;
    default:
        currentDataType = Undefined;
        numBytesForCurrentDataType = 0;
        buffer.clear();
        MODEM_DEBUG("Data Type Undefined");
        MODEM_DEBUG("...Clearing buffer ... done processingData");
        break;
    }
}

void OpenModem::sendGreetingMessage()
{
    MODEM_DEBUG("connection send greeting: " + greetingMessage);
    QByteArray greeting = greetingMessage.toUtf8();
    QByteArray data = "greeting " + QByteArray::number(greeting.size()) + ' ' + greeting;
    if (tcpconnection->write(data) == data.size()) isGreetingMessageSent = true;
}

void OpenModem::sendPing()
{
    if ( pongTime.elapsed() > PongTimeout ) return;
    tcpconnection->write("PING 1 p");
}

bool OpenModem::readProtocolHeader()
{
    MODEM_DEBUG("Reading Protocol Header");
    buffer.clear();
    if ( pingTimer->isActive() )
    {
        MODEM_DEBUG("Ping Timer running - stoping timer");
        pingTimer->stop();
    }
    MODEM_DEBUG("ReadProtocolHeader reading data into buffer");
    readDataIntoBuffer(MaxBufferSize);
    QList<QByteArray> incmd;
    incmd << buffer.split(' ');
    QString msgstr;
    msgstr = "parsing data from buffer - " + buffer;
    msgstr = "  mode: " + incmd[0];
    MODEM_DEBUG(msgstr);
    if ( incmd[0] == "CLOSE" )
    {   // if command is to close do it now.
        exit(0);
    }

    if ( incmd[0] == "MODE" )
    {
        // decypher mode
        MODEM_DEBUG("Decyphering Mode With Command Interpreter");
        currentDataType = PlainText;
        commandInterpreter(buffer);
        return true;
    }
    if ( buffer == "PING " )
    {
        MODEM_DEBUG("Reading Protocol Header - PING");
        currentDataType = Ping;
    }
    else if ( buffer == "PONG " )
    {
        MODEM_DEBUG("Reading Protocol Header - PONG");
        currentDataType = Pong;
    }
    else if ( buffer == "MESSAGE " )
    {
        MODEM_DEBUG("Reading Protocol Header - MESSAGE");
        currentDataType = PlainText;
    }
    else if ( buffer == "GREETING" )
    {
        MODEM_DEBUG("Reading Protocol Header - GREETING");
        currentDataType = Greeting;
        numBytesForCurrentDataType = 8;
        current_mode = ModeIdle;
    }
    else if ( buffer.startsWith("MODE") )
    {
        MODEM_DEBUG("Reading Protocol Header - MODE COMMAND");
        currentDataType = PlainText;
        if ( buffer.startsWith("MODE WINMOR500") )
        {
            currentDataType = ModeExact;
            MODEM_DEBUG("Reading Protocol Header - Setting current mode WinMOR500");
            numBytesForCurrentDataType = buffer.size();
            current_mode = WinMor500;
            winmor_codec = new WinMor(this);
        }
        else if ( buffer.startsWith("MODE WINMOR1500") )
        {
            currentDataType = ModeExact;
            MODEM_DEBUG("Reading Protocol Header - Setting current mode WinMOR1500");
            numBytesForCurrentDataType = buffer.size();
            current_mode = WinMor1500;
        }
        else if ( buffer.startsWith("MODE PACTOR500") )
        {
            currentDataType = ModeExact;
            MODEM_DEBUG("Reading Protocol Header - Setting current mode PACTOR500");
            numBytesForCurrentDataType = buffer.size();
            current_mode = Pactor500;
        }
        else if ( buffer.startsWith("MODE PACTOR1500") )
        {
            currentDataType = ModeExact;
            MODEM_DEBUG("Reading Protocol Header - Setting current mode PACTOR1500");
            numBytesForCurrentDataType = buffer.size();
            current_mode = Pactor1500;
        }
        if ( buffer.startsWith("MODE PAC300") )
        {
            currentDataType = ModeExact;
            MODEM_DEBUG("Reading Protocol Header - Setting current mode PACKET300");
            numBytesForCurrentDataType = buffer.size();
            current_mode = Packet300;
        }
        else if ( buffer.startsWith("MODE PAC1200") )
        {
            currentDataType = ModeExact;
            MODEM_DEBUG("Reading Protocol Header - Setting current mode PACKET1200");
            numBytesForCurrentDataType = buffer.size();
            current_mode = Packet1200;
        }
    }
    else
    {
        MODEM_DEBUG("Reading Protocol Header - current mode undefined");
        currentDataType = Undefined;
        return false;
    }
    MODEM_DEBUG("Reading Protocol Header - returning data: " + buffer);
    return true;
}

bool OpenModem::hasEnoughData()
{
    MODEM_DEBUG("checking for data");
    if ( pingTimer->isActive() ) pingTimer->stop();
    MODEM_DEBUG("checking for data - timer stopped");
    if ( numBytesForCurrentDataType <= 0 ) numBytesForCurrentDataType = dataLengthForCurrentDataType();
    MODEM_DEBUG("checking for data - correct number of bytes: " + numBytesForCurrentDataType);
    if ( (buffer.size() < numBytesForCurrentDataType) ||
         (numBytesForCurrentDataType <= 0)
       )
    {
        MODEM_DEBUG("bytes available < data type or: ");
        MODEM_DEBUG("num bytes not enough - starting timer ");
        MODEM_DEBUG("buffer bytes: " + buffer.size());
        MODEM_DEBUG("tcpconnection bytes= " + tcpconnection->bytesAvailable());
        MODEM_DEBUG("bytes for data type: " + numBytesForCurrentDataType);
        pingTimer->start(TransferTimeout);
        MODEM_DEBUG("checked data returning false");
        return false;
    }
    MODEM_DEBUG("checked data good - returning true ");
    return true;
}

int OpenModem::dataLengthForCurrentDataType()
{
    MODEM_DEBUG("checking for data length");
    if ( buffer.size() <= 0 )  return 0;
    MODEM_DEBUG("bytesAvailable > 0");
    if ( readDataIntoBuffer(MAX_BUF_SIZE) <= 0 )        return 0;
    MODEM_DEBUG("good data in buffer");
    int number = buffer.size();
    MODEM_DEBUG("will return if not ends w/token");
    if ( !buffer.endsWith(SeparatorToken) ) return number;
    MODEM_DEBUG("ends with separator token");
    buffer.chop(1);
    number = buffer.size();
    MODEM_DEBUG("received data length " + number);
    return number;
}

void OpenModem::slotReadyRead()
{
#ifdef SHOW_MODEM_DEBUG
    MODEM_DEBUG("...Modem received message....");
#endif
    buffer.clear();
    QDataStream indata(&buffer, QIODevice::WriteOnly);
    indata.setVersion(QDataStream::Qt_5_0);
    indata << tcpconnection->readAll();
    blockSize = (quint16)(buffer.size() - sizeof(quint16));
#ifdef SHOW_MODEM_DEBUG
        MODEM_DEBUG("Rcvd message size = "+ QString("Modem message size = %1").arg(blockSize-1));
#endif
    indata.device()->seek(0);
    if ( !buffer.endsWith("\0" ) )
    {
#ifdef SHOW_MODEM_DEBUG
        MODEM_DEBUG("Rcvd message ends w/NULL -  " + buffer);
#endif
    }
    else if ( !buffer.endsWith("\n" ) )
    {
#ifdef SHOW_MODEM_DEBUG
        MODEM_DEBUG("Rcvd message ends w/CR -  " + buffer);
#endif
    }
#ifdef SHOW_MODEM_DEBUG
        MODEM_DEBUG("Checking Rcvd message type");
#endif
    if ( buffer.startsWith("CMD") )
    {
        currentDataType = Greeting;
    }
    else if (buffer.startsWith("MODE ") )
    {
        currentDataType = Mode;
    }
    else
    {
        QList<QByteArray> l = buffer.split(' ');
        QString strlist;
        foreach (int i, buffer)
        {
            strlist += buffer.at(i);
        }

#ifdef SHOW_MODEM_DEBUG
        MODEM_DEBUG("Strings " + strlist);
#endif
        currentDataType = Undefined;
    }
#ifdef SHOW_MODEM_DEBUG
        MODEM_DEBUG("Displaying message type");
#endif
    switch (currentDataType)
    {
    case Mode:
#ifdef SHOW_MODEM_DEBUG
        MODEM_DEBUG("Command set to MODE");
#endif
        break;
    case Greeting:
#ifdef SHOW_MODEM_DEBUG
        MODEM_DEBUG("Command set to GREETING");
#endif
        break;
    default:
#ifdef SHOW_MODEM_DEBUG
        MODEM_DEBUG("Command set to UNDEFINED");
#endif
        break;
    }

    buffer.chop(1);
    numBytesForCurrentDataType = buffer.toInt();
#ifdef SHOW_MODEM_DEBUG
    QString msgstr;
    msgstr = "Message Read: " + buffer + " Bytes: " + QString("%1").arg(numBytesForCurrentDataType);
    MODEM_DEBUG(msgstr);
#endif
    processCMD(QString(buffer));
    //currentDataType = Undefined;
    numBytesForCurrentDataType = 0;
    buffer.clear();
    /*
    if ( !isValid() )
    {
        abort();
        return;
    }
    */
#ifdef SHOW_MODEM_DEBUG
        MODEM_DEBUG("Received command verified: " + buffer);
#endif
}

void OpenModem::slotConnectionError(QAbstractSocket::SocketError socketError)
{
#ifdef SHOW_MODEM_DEBUG
    QString msg;
    msg = "Modem " + tcpconnection->peerAddress().toString();
    msg += "port " + tcpconnection->peerPort();
    msg += "Connection Error . . ." + socketError;
    MODEM_DEBUG(msg);
#endif
    if (QTcpSocket *connection = qobject_cast<QTcpSocket *>(sender())) removeConnection(connection);
}

void OpenModem::sendMessage(const char *message)
{
#ifdef SHOW_MODEM_DEBUG
    MODEM_DEBUG("got " + QString(message).toUtf8());
#endif
    QByteArray msg = QString(message).toUtf8();
    QByteArray data = "MESSAGE " + ' ' + msg;
#ifdef SHOW_MODEM_DEBUG
    MODEM_DEBUG("Modem sending message " + data);
#endif
    try
    {
        if ( !boolTcpPortOpen )
        {
#ifdef SHOW_MODEM_DEBUG
            MODEM_DEBUG("Modem connection not open for write");
#endif
            tcpconnection->connectToHost(QHostAddress("127.0.0.1"),8600);
#ifdef SHOW_MODEM_DEBUG
            MODEM_DEBUG("Modem connecting to host for write");
#endif
            boolTcpPortOpen  = tcpconnection->isOpen();
        }
#ifdef SHOW_MODEM_DEBUG
        MODEM_DEBUG("Modem writing to host");
#endif
        tcpconnection->write(data); //== data.size();
        tcpconnection->disconnectFromHost();
    }
    catch (...)
    {
#ifdef SHOW_MODEM_DEBUG
        MODEM_DEBUG("Modem Socket Error connecting for write:" + tcpconnection->errorString());
#endif
    }
    try
    {
        tcpconnection->connectToHost(id, 8600);
    }
    catch (...)
    {
#ifdef SHOW_MODEM_DEBUG
        MODEM_DEBUG("Modem Socket Error opening port for write:" + tcpconnection->errorString());
#endif
    }
    boolTcpPortOpen = tcpconnection->isOpen();
#ifdef SHOW_MODEM_DEBUG
    MODEM_DEBUG("Modem port to host open for write: " + boolTcpPortOpen);
#endif
    tcpconnection->write(message,sizeof(message));
}

void OpenModem::slotUpdateClient(qint64 b)
{
    bytesReceived += (int)b;
    buffer = tcpconnection->readAll();
#ifdef SHOW_MODEM_DEBUG
    MODEM_DEBUG("Client Rcvd msg: " + buffer);
#endif
    if ( bytesReceived == TotalBytes || tcpconnection->atEnd() )
    {
        //processBytes();
        //tcpconnection->close();
    }
}

int OpenModem::readDataIntoBuffer(int x)
{
    Q_UNUSED(x);

#ifdef SHOW_MODEM_DEBUG
    MODEM_DEBUG("Starting...");
#endif
    if ( buffer.size() > MaxBufferSize ) return -1;
#ifdef SHOW_MODEM_DEBUG
    MODEM_DEBUG("Data less than max");
#endif
    int numBytesBeforeRead = buffer.size();
    if ( numBytesBeforeRead == MaxBufferSize ) return 0;
#ifdef SHOW_MODEM_DEBUG
    MODEM_DEBUG("Max not reached");
#endif
    while ( tcpconnection->bytesAvailable() > 0 && buffer.size() < MAX_BUF_SIZE )
    {
#ifdef SHOW_MODEM_DEBUG
        MODEM_DEBUG("Appending data");
#endif
        buffer.append(tcpconnection->read(1));
        //if (buffer.endsWith(SeparatorToken))   break;
    }
#ifdef SHOW_MODEM_DEBUG
    QString msg;
    msg = "Data appended returning Size: " + buffer.size();
    msg += " B4: " + numBytesBeforeRead;
    MODEM_DEBUG(msg);
#endif
    int z = buffer.size() - numBytesBeforeRead;
    if (z == 0) z = buffer.size();
#ifdef SHOW_MODEM_DEBUG
    MODEM_DEBUG("returning ...");
#endif
    return z;
}

void OpenModem::disconnect()
{
    if ( tcpconnection->isOpen() ) tcpconnection->disconnect();
    boolTcpPortOpen = false;
#ifdef SHOW_MODEM_DEBUG
    MODEM_DEBUG("closing connection");
#endif
}

void OpenModem::disconnected()
{
#ifdef SHOW_MODEM_DEBUG
    MODEM_DEBUG("connection disconnected and closing");
#endif
    tcpconnection->close();
#ifdef SHOW_MODEM_DEBUG
    MODEM_DEBUG("connection closed");
#endif
}

void OpenModem::connectionError()
{
    errNo = tcpconnection->error();
    errMsg  = tcpconnection->errorString();
}

void OpenModem::removeConnection(QTcpSocket *connection)
{
    connection->deleteLater();
}

// =========================================================
// PUBLIC METHODS
// =========================================================

// =========================================================
// PRIVATE FUNCTIONS
// =========================================================
void OpenModem::processCMD(QString cmd)
{
#ifdef SHOW_MODEM_DEBUG
    MODEM_DEBUG("Processing CMD: " + cmd);
#endif
}

QString OpenModem::nickName() const
{
    QString strMsg;
    strMsg = "127.0.0.1 ";
    strMsg += '@';
    strMsg += QHostInfo::localHostName();
    strMsg += ':';
    strMsg += "8600";
    return strMsg;
}

bool OpenModem::hasConnection(const QHostAddress &senderIp, int senderPort) const
{
    Q_UNUSED(senderPort)
    Q_UNUSED(senderIp)
    return true;
}




void OpenModem::commandInterpreter(QString cmd)
{
    // start up the appropriate TNC or pass settings to codec
    QList<QString> incmd = cmd.split(" ",QString::KeepEmptyParts);
    QString msgstr = "Interpreting CMD - " + incmd[0] + " " + incmd[1];
    MODEM_DEBUG(msgstr);
    if ( incmd[0] == "MODE" )
    {
        // decypher mode
        if ( incmd[1] == "WINMOR500" )
        {
            MODEM_DEBUG("Starting WinMor codec and TNC");
            m_mode = WinMor500;
            OMCB.activeCodec = WinMor500;
            // create winmor codec object and launch
            winmor_codec = new WinMor(this); // public interface to winmor codec
            winmor_codec->init_modem(); // initialize the codec
            MODEM_DEBUG("Setting BW 500 in codec");
            winmor_codec->setBW(500);    // use public interface to change bandwidth
                                         // using back end function
            MODEM_DEBUG("BW 500 change recorded in control block");
            OMCB.Bandwidth = 500;
        }
        else if ( incmd[1] == "WINMOR1500" )
        {
            m_mode = WinMor1500;
            OMCB.activeCodec = WinMor500;
            // create winmor codec object and launch
            winmor_codec = new WinMor(); // public interface to winmor codec
            MODEM_DEBUG("Setting BW 1500 in codec");
            winmor_codec->setBW(1500);    // use public interface to change bandwidth
                                         // using back end function
            MODEM_DEBUG("BW 1500 change recorded in control block");
            OMCB.Bandwidth = 1500;
        }
        else if ( incmd[1] == "PACTOR500" )
        {
            m_mode = Pactor500;
            OMCB.activeCodec = Pactor500;
            // create pactor codec object and launch
            pactor_codec = new Pactor(this);
            MODEM_DEBUG("Setting BW 500 in pactor codec");
            pactor_codec->setBW(500);
            MODEM_DEBUG("BW 500 change recorded in pactor control block");
            OMCB.Bandwidth = 500;
        }
        else if ( incmd[1] == "PACTOR1500" )
        {
            m_mode = Pactor1500;
            OMCB.activeCodec = Pactor1500;
            // create pactor codec object and launch
            pactor_codec = new Pactor(this);
            MODEM_DEBUG("Setting BW 1500 in pactor codec");
            pactor_codec->setBW(1500);
            MODEM_DEBUG("BW 1500 change recorded in pactor control block");
            OMCB.Bandwidth = 1500;
        }
    }
    // not mode so decypher which command
    else if ( incmd[0] == "CLOSE" )
    {
        MODEM_DEBUG("CLOSE cmd");
        disconnect();
        buffer = 0;
        exit(0);
    }
    else if ( incmd[0] == "AUTOBREAK" )
    {
        MODEM_DEBUG("AutoBreak rcvd");
        if ( incmd.size() == 1 )
        {
            sendMessage("AUTOBREAK" + OMCB.AutoBREAK );
            return;
        }
        if ( incmd.size() > 1 ) OMCB.AutoBREAK = false;    // if value is changing reset
        if ( incmd[1] == "true" ) OMCB.AutoBREAK = true;  // set value to value sent
        // decypher mode
        switch (m_mode)
        {
        case ModeIdle:
            return;
            break;
        case WinMor500:
            winmor_codec->autoBreak(OMCB.AutoBREAK);
            break;
        case WinMor1500:
            winmor_codec->autoBreak(OMCB.AutoBREAK);
            break;
        case Pactor500:
            pactor_codec->autoBreak(OMCB.AutoBREAK);
            break;
        case Pactor1500:
            pactor_codec->autoBreak(OMCB.AutoBREAK);
            break;
        default:
            break;
        }
    }
    else if ( incmd[0] == "BREAK" )
    {
#ifdef SHOW_MODEM_DEBUG
        MODEM_DEBUG("BREAK rcvd");
#endif
        // decypher mode
        switch (m_mode)
        {
        case ModeIdle:
            return;
            break;
        case WinMor500:
            winmor_codec->sendBreak();
            break;
        case WinMor1500:
            winmor_codec->sendBreak();
            break;
        case Pactor500:
            pactor_codec->sendBreak();
            break;
        case Pactor1500:
            pactor_codec->sendBreak();
            break;
        default:
            break;
        }
    }
    else if ( incmd[0] == "BUFFERS" )
    {
#ifdef SHOW_MODEM_DEBUG
        MODEM_DEBUG("BUFFERS rcvd");
#endif
        // decypher mode
        switch (m_mode)
        {
        case ModeIdle:
            return;
            break;
        case WinMor500:
            //m_buffers = winmor_codec->buffers();
            break;
        case WinMor1500:
            //m_buffers = winmor_codec->buffers();
            break;
        case Pactor500:
            //we have to define a local for formating a BUFFERS <num> message to send
            //m_buffers = pactor_codec->buffers();
            // format new message here and send on TCP/IP connection
            break;
        case Pactor1500:
            //we have to define a local for formating a BUFFERS <num> message to send
            //m_buffers = pactor_codec->buffers();
            // format new message here and send on TCP/IP connection
            break;
        default:
            break;
        }
    }
    else if ( incmd[0] == "BUSYLOCK" )
    {
#ifdef SHOW_MODEM_DEBUG
        MODEM_DEBUG("BUSYLOCK rcvd");
#endif
    }
    else if ( incmd[0] == "BW" )
    {
#ifdef SHOW_MODEM_DEBUG
        MODEM_DEBUG("BW rcvd");
#endif
        // BW command  params 500 / 1500 / none
        //   will get set bandwith if no parms
        //
        // decypher mode
        switch (m_mode)
        {
        case ModeIdle:
            return;
            break;
        case WinMor500:
            if ( incmd.size() == 2 )
            {
                int intBW;
                intBW = incmd[1].toInt();
                OMCB.Bandwidth = intBW;
                //we have to define a local for formating a BUFFERS <num> message to send
#ifdef SHOW_MODEM_DEBUG
                 MODEM_DEBUG("BW cmd rcvd and setting BW in codec");
#endif
                m_bw = winmor_codec->setBW(intBW);
               // format new message here and send on TCP/IP connection
            }
            else if ( incmd.size() == 1)
            {
                // format new message here and send on TCP/IP connection
                sendMessage("BW " + OMCB.Bandwidth);
            }
            break;
        case WinMor1500:
            if ( incmd.size() == 2 )
            {
                int intBW;
                intBW = incmd[1].toInt();
                OMCB.Bandwidth = intBW;
                //we have to define a local for formating a BUFFERS <num> message to send
#ifdef SHOW_MODEM_DEBUG
                 MODEM_DEBUG("BW cmd rcvd and setting BW in codec");
#endif
                m_bw = winmor_codec->setBW(intBW);
               // format new message here and send on TCP/IP connection
            }
            else if ( incmd.size() == 1)
            {
                // format new message here and send on TCP/IP connection
#ifdef SHOW_MODEM_DEBUG
                 MODEM_DEBUG("BW cmd rcvd returning from codec");
#endif
                sendMessage("BW " + OMCB.Bandwidth);
            }
            break;
        case Pactor500:
            if ( incmd.size() == 2 )
            {
                int intBW;
                intBW = incmd[1].toInt();
                //we have to define a local for formating a BUFFERS <num> message to send
                m_bw = pactor_codec->setBW(intBW);
               // format new message here and send on TCP/IP connection
            }
            else if ( incmd.size() == 1)
            {
                //we have to define a local for formating a BUFFERS <num> message to send
                m_bw = pactor_codec->setBW(NULL);
               // format new message here and send on TCP/IP connection
            }
            break;
        case Pactor1500:
            if ( incmd.size() == 2 )
            {
                int intBW;
                intBW = incmd[1].toInt();
                //we have to define a local for formating a BUFFERS <num> message to send
                m_bw = pactor_codec->setBW(intBW);
               // format new message here and send on TCP/IP connection
            }
            else if ( incmd.size() == 1)
            {
                //we have to define a local for formating a BUFFERS <num> message to send
                m_bw = pactor_codec->setBW(NULL);
               // format new message here and send on TCP/IP connection
            }
            break;

        default:
            break;
        }
    }
    else if ( incmd[0] == "CAPTURE" )
    {
#ifdef SHOW_MODEM_DEBUG
        MODEM_DEBUG("CAPTURE cmd");
#endif
    }
    else if ( incmd[0] == "CAPTUREDEVICES" )
    {
#ifdef SHOW_MODEM_DEBUG
        MODEM_DEBUG("CAPTUREDEVICES cmd");
#endif
    }
    else if ( incmd[0] == "CODEC" )
    {
#ifdef SHOW_MODEM_DEBUG
        MODEM_DEBUG("CODEC cmd");
#endif
        if ( incmd.size() == 1 )
        {
#ifdef SHOW_MODEM_DEBUG
            MODEM_DEBUG("CAPTURE cmd W/NO PARMS");
#endif
            // return codec in formatted string  //
            switch ( OMCB.activeCodec )
            {
            case WinMor500:
                sendMessage("CODEC WINMOR500");
                break;
            case WinMor1500:
                sendMessage("CODEC WINMOR1500");
                break;
            case Pactor500:
                sendMessage("CODEC PACTOR500");
                break;
            case Pactor1500:
                sendMessage("CODEC PACTOR1500");
                break;
            default:
                break;
            }
            return;
        } // end return current codec
        // process parm 2  //
        if ( incmd[2] == "WINMOR500" )
        {
            if ( OMCB.activeCodec == ModeIdle )
            {
#ifdef SHOW_MODEM_DEBUG
                MODEM_DEBUG("Setting code based on second param WINMOR500");
#endif
                // use WinMor public interface to set mode into TNC
                OMCB.activeCodec = WinMor500;
                OMCB.Bandwidth = 500;
            }
        }
        if ( incmd[2] == "WINMOR1500" )
        {
            if ( OMCB.activeCodec == ModeIdle )
            {
                // use WinMor public interface to set mode into TNC
                OMCB.activeCodec = WinMor1500;
                OMCB.Bandwidth = 1500;
            }
        }
        if ( incmd[2] == "PACTOR500" )
        {
            if ( OMCB.activeCodec == ModeIdle )
            {
                // use Pactor public interface to set mode into TNC
                OMCB.activeCodec = Pactor500;
                OMCB.Bandwidth = 500;
            }
        }
        if ( incmd[2] == "PACTOR1500" )
        {
            if ( OMCB.activeCodec == ModeIdle )
            {
                // use Pactor public interface to set mode into TNC
                OMCB.activeCodec = Pactor1500;
                OMCB.Bandwidth = 1500;
            }
        }
    }
    else if ( incmd[0] == "CONNECT" )
    {
    }
    else if ( incmd[0] == "CONNECTED" )
    {
    }
    else if ( incmd[0] == "DEBUGLOG" )
    {
    }
    else if ( incmd[0] == "DIRTYDISCONNECT" )
    {
    }
    else if ( incmd[0] == "DISCONNECT" )
    {
    }
    else if ( incmd[0] == "DRIVELEVEL" )
    {
        if ( incmd.size() == 1 )
        {
            sendMessage("DRIVELEVEL " + OMCB.DriveLevel);
            return;
        }
        else
        {
            quint32 m_driveLvl;
            m_driveLvl = incmd[1].toInt();
            OMCB.DriveLevel = m_driveLvl;
            switch (m_mode)
            {
            case ModeIdle:
                break;
            case WinMor500:
                winmor_codec->driveLevel(m_driveLvl);
                break;
            case WinMor1500:
                winmor_codec->driveLevel(m_driveLvl);
                break;
            case Pactor500:
                //pactor_codec->driveLevel(m_driveLvl);
                break;
            case Pactor1500:
                //pactor_codec->driveLevel(m_driveLvl);
                break;
            case Packet300:
                break;
            case Packet1200:
                break;
            }
        }
    }
    else if ( incmd[0] == "FAULT" )
    {
    }
    else if ( incmd[0] == "FECRCV" )
    {
    }
    else if ( incmd[0] == "FECSEND" )
    {
    }
    else if ( incmd[0] == "GRIDSQUARE" )
    {
        if ( incmd.size() == 1)
        {
            // GRIDSQUARE message not returned here
            sendMessage("GRIDSQUARE " + OMCB.MyGridsquare.toUtf8());
        }
        else
        {
            m_grid = incmd[1].toUtf8();
            OMCB.MyGridsquare = m_grid;
            switch (m_mode)
            {
            case Pactor500:
                //pactor_codec->gridSquare(m_grid);
                break;
            case Pactor1500:
                //pactor_codec->gridSquare(m_grid);

            default:
                break;
            }
        }
    }
    else if ( incmd[0] == "ISREGISTERED" )
    {
    }
    else if ( incmd[0] == "LEADEREXT" )
    {
    }
    else if ( incmd[0] == "LISTEN" )
    {
    }
    else if ( incmd[0] == "MAXCONREQ" )
    {
    }
    else if ( incmd[0] == "MONCALL" )
    {
    }
    else if ( incmd[0] == "MYAUX" )
    {
    }
    else if ( incmd[0] == "MYC" )
    {
    }
    else if ( incmd[0] == "NEWSTATE" )
    {
    }
    else if ( incmd[0] == "OFFSET" )
    {
    }
    else if ( incmd[0] == "OUTQUEUED" )
    {
    }
    else if ( incmd[0] == "OVER" )
    {
    }
    else if ( incmd[0] == "PLAYBACK" )
    {
    }
    else if ( incmd[0] == "PLAYBACKDEVICES" )
    {
    }
    else if ( incmd[0] == "PORTS" )
    {
    }
    else if ( incmd[0] == "PROCESSID" )
    {
    }
    else if ( incmd[0] == "RESPONSEDLY" )
    {
    }
    else if ( incmd[0] == "SENDID" )
    {
    }
    else if ( incmd[0] == "SHOW" )
    {
    }
    else if ( incmd[0] == "SQUELCH" )
    {
    }
    else if ( incmd[0] == "STATE" )
    {
    }
    else if ( incmd[0] == "SUFFIX" )
    {
    }
    else if ( incmd[0] == "TARGET" )
    {
    }
    else if ( incmd[0] == "TWOTONETEST" )
    {
    }
    else if ( incmd[0] == "VERSION" )
    {
    }
    else if ( incmd[0] == "VOX" )
    {
    }
}

void OpenModem::readSettings()
{
    QSettings settings("OpenModem","Config");
    settings.beginGroup("Sound");
      OMCB.CaptureDevice = settings.value("CaptureDevice","default").toString();
      OMCB.PlaybackDevice = settings.value("PlaybackDevice","default").toString();
    settings.endGroup();
    settings.beginGroup("User");
      OMCB.MyCallsign = settings.value("MyCall","unknown").toString();
      OMCB.MyGridsquare = settings.value("GridSquare","XX00xx").toString();
      OMCB.Registration = settings.value("RegistrationNum","unreg").toString();
    settings.endGroup();
    settings.beginGroup("Modem");
      // general settings to local vars here
      OMCB.CWId = settings.value("CWId",false).toBool();
      OMCB.debugLogON = settings.value("DebugLog",false).toBool();
      OMCB.Bandwidth = settings.value("BandWidth",500).toInt();
      OMCB.DriveLevel = settings.value("DriveLevel",50).toInt();
    settings.endGroup();
    settings.beginGroup("Network");
      // general settings to local vars here
      id = settings.value("NetworkConfiguration","127.0.0.1").toString();
      OMCB.tcpAddress = settings.value("Address","127.0.0.1").toString();
      OMCB.TCPControlPort = settings.value("TCPControlPort",8600).toInt();
    settings.endGroup();
}

void OpenModem::saveSettings()
{
    // use TNC setup menu to save settings
}
