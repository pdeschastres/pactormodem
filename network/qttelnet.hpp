/*********************************************************
 * This code is derived from example code originating in
 * the Nokia Qt SDK examples. It is lisenced under LGPL
 * distribution and use claims and is used here under the
 * terms of that original license.
 ********************************************************/
#include <QtGlobal>

#if defined(Q_OS_LINUX)
 #include <sys/types.h>
 #include <sys/socket.h>
 #include <netinet/in.h>
#endif

#ifdef Q_WS_WIN
 #include <winsock2.h>
#endif

#define QTTELNET_DEBUG

#ifdef QTTELNET_DEBUG
 #include <QtCore/QDebug>
#endif

#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QPair>
#include <QtCore/QVariant>
#include <QtCore/QSocketNotifier>
#include <QtCore/QBuffer>
#include <QtCore/QVarLengthArray>
#include <QtCore/QByteArray>
#include <QtNetwork/QTcpSocket>

#include "qttelnet.h"

QByteArray QtTelnetAuthNull::authStep(const QByteArray &data)
{
    Q_ASSERT(data[0] == Common::Authentication);

    if (data.size() < 2 || data[1] != Common::SEND)
        return QByteArray();

    char buf[8] =
    {
        Common::IAC,
        Common::SB,
        Common::Authentication,
        Common::IS,
        Auth::AUTHNULL,
        0, // CLIENT|ONE-WAY
        Common::IAC, Common::SE
    };

    setState(AuthSuccess);
    return QByteArray(buf, sizeof(buf));
}

QtTelnetPrivate::QtTelnetPrivate(QtTelnet *parent) :
      nullauth(false),
      connected(false),
      nocheckp(false),
      triedlogin(false),
      triedpass(false),
      firsttry(true),
      q(parent),
      socket(0),
      curauth(0),
      notifier(0),
      passp("assword:\\s*$"),
      loginp("ogin:\\s*$"),
      loginq("call:\\s*$")
{
    setSocket(new QTcpSocket(this));
}

QtTelnetPrivate::~QtTelnetPrivate()
{
    delete socket;
    delete notifier;
    delete curauth;
}

void QtTelnetPrivate::setSocket(QTcpSocket *s)
{
    if ( socket )
    {
        q->logout();
        socket->flush();
    }

    delete socket;
    socket = s;
    connected = false;
    if ( socket )
    {
        connect(socket, SIGNAL(connected()),this, SLOT(socketConnected()));
        connect(socket, SIGNAL(disconnected()),this, SLOT(socketConnectionClosed()));
        connect(socket, SIGNAL(readyRead()),this, SLOT(socketReadyRead()));
        connect(socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)),this, SLOT(slotSocketStateChg()));
        connect(socket, SIGNAL(error(QAbstractSocket::SocketError)),this, SLOT(socketError(QAbstractSocket::SocketError)));
    }
}

uchar QtTelnetPrivate::opposite(uchar operation, bool positive)
{
    if (operation == Common::DO)
        return (positive ? Common::WILL : Common::WONT);
    else if (operation == Common::DONT) // Not allowed to say WILL
        return Common::WONT;
    else if (operation == Common::WILL)
        return (positive ? Common::DO : Common::DONT);
    else if (operation == Common::WONT) // Not allowed to say DO
        return Common::DONT;
    return 0;
}

void QtTelnetPrivate::slotSocketStateChg()
{
#ifdef TELNET_DEBUG
    qDebug() << "Socket State : " << socket->state();
#endif
}

void QtTelnetPrivate::consume()
{
    int currpos = 0;
    int prevpos = -1;;

    const QByteArray data = buffer.readAll();
#ifdef TELNET_DEBUG
    qDebug() << "Parsing : " << data;
#endif
    while (
            prevpos < currpos     &&
            currpos < data.size()
          )
    {
        prevpos = currpos;
        const uchar c = uchar(data[currpos]);
#ifdef TELNET_DEBUG
            qDebug() << "Move cursor to " << currpos;
#endif
        if (c == Common::DM)
        {
#ifdef TELNET_DEBUG
            qDebug() << "No Parsing just move cursor";
#endif
            ++currpos;
        }
        else if ( c == Common::IAC )
        {
#ifdef TELNET_DEBUG
            qDebug() << "Parsing IAC subAuth";
#endif
            currpos += parseIAC(data.mid(currpos));
        }
        else // Assume plain text
        {
#ifdef TELNET_DEBUG
            qDebug() << "Assuming plain text - parsing..." << data.mid(currpos);
#endif
            currpos += parsePlaintext(data.mid(currpos));
        }
    }
    if ( currpos < data.size() ) buffer.push_back(data.mid(currpos));
}

bool QtTelnetPrivate::isCommand(const uchar c)
{
    return (c == Common::DM);
}

bool QtTelnetPrivate::isOperation(const uchar c)
{
    return (c == Common::WILL || c == Common::WONT || c == Common::DO || c == Common::DONT);
}

QByteArray QtTelnetPrivate::getSubOption(const QByteArray &data)
{
    Q_ASSERT(!data.isEmpty() && uchar(data[0]) == Common::IAC);

    if (data.size() < 4 || uchar(data[1]) != Common::SB) return QByteArray();

    for ( int i = 2; i < data.size() - 1; ++i )
    {
        if ( uchar(data[i]) == Common::IAC && uchar(data[i+1]) == Common::SE ) return data.mid(2, i-2);
    }
    return QByteArray();
}

void QtTelnetPrivate::parseSubNAWS(const QByteArray &data)
{
    Q_UNUSED(data);
}

void QtTelnetPrivate::parseSubTT(const QByteArray &data)
{
    const char c1[4] = { Common::IAC, Common::SB, Common::TerminalType, Common::IS};
    const char c2[2] = { Common::IAC, Common::SE };

    Q_ASSERT(!data.isEmpty() && data[0] == Common::TerminalType);

    if (data.size() < 2 || data[1] != Common::SEND) return;

    sendCommand(c1, sizeof(c1));
    sendString("UNKNOWN");
    sendCommand(c2, sizeof(c2));
}

void QtTelnetPrivate::parseSubAuth(const QByteArray &data)
{
    Q_ASSERT(data[0] == Common::Authentication);
#ifdef TELNET_DEBUG
    qDebug() << "Parsing SubAuth - " << data;
    qDebug() << "curauth: " << curauth;
    qDebug() << "Data[1]: " << data[1];
#endif

    if ( !curauth && data[1] == Common::SEND )
    {
        int pos = 2;

        while ( pos < data.size() && !curauth )
        {
            curauth = auths[data[pos]];
            pos += 2;

            if ( curauth )
            {
                emit q->loginRequired();
                break;
            }
        }

        if ( !curauth )
        {
            curauth = new QtTelnetAuthNull;
            nullauth = true;
            if ( (loginp.isEmpty() && passp.isEmpty()) ||
                 (loginq.isEmpty() && passp.isEmpty()) )
            {
                emit q->loginRequired();
                nocheckp = true;
            }
        }
    }

    if ( curauth )
    {
        const QByteArray a = curauth->authStep(data);
        if ( !a.isEmpty() ) sendCommand(a);

        if ( curauth->state() == QtTelnetAuth::AuthFailure )
            emit q->loginFailed();
        else if ( curauth->state() == QtTelnetAuth::AuthSuccess )
        {
            if ( (loginp.isEmpty() && passp.isEmpty()) ||
                 (loginq.isEmpty() && passp.isEmpty()) )
                emit q->loggedIn();
            if ( !nullauth ) nocheckp = true;
        }
    }
}

int QtTelnetPrivate::parseIAC(const QByteArray &data)
{
    if ( data.isEmpty() ) return 0;

    Q_ASSERT(uchar(data.at(0)) == Common::IAC);

    if ( data.size() >= 3 && isOperation(data[1]) )
    { // IAC, Operation, Option
        const uchar operation = data[1];
        const uchar option = data[2];

        if ( operation == Common::WONT && option == Common::Logout )
        {
            q->close();
            return 3;
        }
        if ( operation == Common::DONT && option == Common::Authentication )
        {
            if ( (loginp.isEmpty() && passp.isEmpty()) ||
                 (loginq.isEmpty() && passp.isEmpty()) )
                emit q->loggedIn();
            nullauth = true;
        }
        if ( replyNeeded(operation, option) )
        {
            bool allowed = allowOption(operation, option);
            sendCommand(opposite(operation, allowed), option);
            setMode(operation, option);
        }
        return 3;
    }
    if ( data.size() >= 2 && isCommand(data[1]) ) return 2;

    QByteArray suboption = getSubOption(data);
    if (suboption.isEmpty()) return 0;

    // IAC SB Operation SubOption [...] IAC SE
    switch (suboption[0])
    {
    case Common::Authentication:
        parseSubAuth(suboption);
        break;
    case Common::TerminalType:
        parseSubTT(suboption);
        break;
    case Common::NAWS:
        parseSubNAWS(data);
        break;
    default:
        qWarning("QtTelnetPrivate::parseIAC: unknown suboption %d",
                 quint8(suboption.at(0)));
        break;
    }
    return suboption.size() + 4;
}

int QtTelnetPrivate::parsePlaintext(const QByteArray &data)
{
    int consumed = 0;
    int length = data.indexOf('\0');

#ifdef TELNET_DEBUG
    qDebug() << "Now Parsing ..." << data;
#endif
    if ( length == -1 )
    {
        length = data.size();
        consumed = length;
    }
    else
    {
        consumed = length + 1; // + 1 for removing '\0'
    }

    QString text = QString::fromLocal8Bit(data.constData(), length);
#ifdef TELNET_DEBUG
    qDebug() << "Parsing: " << text;
    qDebug() << "nocheckp: " << nocheckp;
    qDebug() << "nullauth: " << nullauth;
#endif
    if ( !nocheckp && nullauth )
    {
        if ( !promptp.isEmpty() && promptp.indexIn(text) != -1 )
        {
#ifdef TELNET_DEBUG
            qDebug() << "Assuming Logged In " << nullauth;
#endif
            emit q->loggedIn();
            nocheckp = true;
        }
    }
    else
    {
        if ( !loginq.isEmpty() && loginq.indexIn(text) != -1 )
        {
            if ( triedlogin || firsttry )
            {
#ifdef TELNET_DEBUG
                qDebug() << "Login is - call:";
#endif
                emit q->message(text);    // Display the login prompt
                text.clear();
                emit q->loginRequired();  // Get a (new) login
                firsttry = false;
            }

            if ( !triedlogin )
            {
#ifdef TELNET_DEBUG
                qDebug() << "!TryingLogin - " << login;
#endif
                q->sendData(login);
                triedlogin = true;
            }
        }
    }


    if ( !nocheckp && nullauth )
    {
        if ( !loginp.isEmpty() && loginp.indexIn(text) != -1 )
        {
            if ( triedlogin || firsttry )
            {
#ifdef TELNET_DEBUG
                qDebug() << "TriedLogin or FirstTry";
#endif
                emit q->message(text);    // Display the login prompt
                text.clear();
                emit q->loginRequired();  // Get a (new) login
                firsttry = false;
            }
            if ( !triedlogin )
            {
#ifdef TELNET_DEBUG
                qDebug() << "!TriedLogin - " << login;
#endif
                q->sendData(login);
                triedlogin = true;
            }
        }
        if ( !passp.isEmpty() && passp.indexIn(text) != -1 )
        {
            if ( triedpass || firsttry )
            {
                emit q->message(text);    // Display the password prompt
                text.clear();
                emit q->loginRequired();  // Get a (new) pass
                firsttry = false;
            }
            if ( !triedpass )
            {
                q->sendData(pass);
                triedpass = true;
                // We don't have to store the password anymore
                pass.fill(' ');
                pass.resize(0);
            }
        }
    }

    if ( !text.isEmpty() ) emit q->message(text);
    return consumed;
}

bool QtTelnetPrivate::replyNeeded(uchar operation, uchar option)
{
    if ( operation == Common::DO || operation == Common::DONT )
    {
        // RFC854 requires that we don't acknowledge
        // requests to enter a mode we're already in
        if ( operation == Common::DO && modes[option] )    return false;
        if ( operation == Common::DONT && !modes[option] ) return false;
    }
    return true;
}

void QtTelnetPrivate::setMode(uchar operation, uchar option)
{
    if ( operation != Common::DO && operation != Common::DONT ) return;

    modes[option] = (operation == Common::DO);
    if ( option == Common::NAWS && modes[Common::NAWS] ) sendWindowSize();
}

void QtTelnetPrivate::sendWindowSize()
{
    if ( !modes[Common::NAWS] )    return;
    if ( !q->isValidWindowSize() ) return;

    short h = htons(windowSize.height());
    short w = htons(windowSize.width());
    const char c[9] =
    {
        Common::IAC,
        Common::SB,
        Common::NAWS,
        (w & 0x00ff),
        (w >> 8),
        (h & 0x00ff),
        (h >> 8),
        Common::IAC,
        Common::SE
    };
    sendCommand(c, sizeof(c));
}

void QtTelnetPrivate::addSent(uchar operation, uchar option)
{
    osent.append(QPair<uchar, uchar>(operation, option));
}

bool QtTelnetPrivate::alreadySent(uchar operation, uchar option)
{
    QPair<uchar, uchar> value(operation, option);

    if ( osent.contains(value) )
    {
        osent.removeAll(value);
        return true;
    }
    return false;
}

void QtTelnetPrivate::sendString(const QString &str)
{
    if ( !connected || str.length() == 0 )return;

    socket->write(str.toLocal8Bit());
}

void QtTelnetPrivate::sendCommand(const QByteArray &command)
{
    if ( !connected || command.isEmpty() ) return;

    if ( command.size() == 3 )
    {
        const char operation = command.at(1);
        const char option = command.at(2);
        if ( alreadySent(operation, option) ) return;
        addSent(operation, option);
    }
    socket->write(command);
}

void QtTelnetPrivate::sendCommand(const char operation, const char option)
{
    const char c[3] =
    {
        Common::IAC,
        operation,
        option
    };
    sendCommand(c, 3);
}

void QtTelnetPrivate::sendCommand(const char *command, int length)
{
    QByteArray a(command, length);
    sendCommand(a);
}

bool QtTelnetPrivate::allowOption(int /*oper*/, int opt)
{
    if ( opt == Common::Authentication ||
         opt == Common::SuppressGoAhead ||
         opt == Common::LineMode ||
         opt == Common::Status ||
         opt == Common::Logout ||
         opt == Common::TerminalType
       ) return true;
    if ( opt == Common::NAWS && q->isValidWindowSize() ) return true;
    return false;
}

void QtTelnetPrivate::sendOptions()
{
    sendCommand(Common::WILL, Common::Authentication);
    sendCommand(Common::DO, Common::SuppressGoAhead);
    sendCommand(Common::WILL, Common::LineMode);
    sendCommand(Common::DO, Common::Status);
    if ( q->isValidWindowSize() ) sendCommand(Common::WILL, Common::NAWS);
}

void QtTelnetPrivate::socketConnected()
{
    connected = true;

    delete notifier;

    notifier = new QSocketNotifier(socket->socketDescriptor(), QSocketNotifier::Exception, this);
    connect(notifier, SIGNAL(activated(int)),this, SLOT(socketException(int)));
    emit connectionOpen();
    sendOptions();
}

void QtTelnetPrivate::socketException(int)
{
    qDebug("out-of-band data received, should handle that here!");
}

void QtTelnetPrivate::socketConnectionClosed()
{
    delete notifier;
    notifier = 0;
    connected = false;
    emit q->loggedOut();
}

void QtTelnetPrivate::socketReadyRead()
{
    buffer.append(socket->readAll());
    consume();
}

void QtTelnetPrivate::socketError(QAbstractSocket::SocketError error)
{
    emit q->connectionError(error);
}

/**********************************************************
 *  QtTelnet::Control
 *
 *   This enum specifies control messages you can send to the Telnet server
 *   using sendControl().
 *
 *   GoAhead Sends the GO AHEAD control message, meaning that the
 *   server can continue to send data.
 *
    InterruptProcess Interrupts the current running process on
    the server. This is the equivalent of pressing {Ctrl+C} in most
    terminal emulators.

    AreYouThere Sends the ARE YOU THERE control
    message, to check if the connection is still alive.

    AbortOutput Temporarily suspends the output from the server.
    The output will resume if you send this control message again.

    \value EraseCharacter Erases the last entered character.

    \value EraseLine Erases the last line.

    \value Break Sends the \c BREAK control message.

    \value EndOfFile Sends the \c END \c OF \c FILE control message.

    \value Suspend Suspends the current running process on the server.
    Equivalent to pressing \key{Ctrl+Z} in most terminal emulators.

    \value Abort Sends the \c ABORT control message.

    \sa sendControl()
*/

/*!
    Constructs a QtTelnet object.

    You must call connectToHost() before calling any of the other
    member functions.

    The \a parent is sent to the QObject constructor.

    \sa connectToHost()
*/
QtTelnet::QtTelnet(QObject *parent) : QObject(parent), d(new QtTelnetPrivate(this))
{
}

/*!
    Destroys the QtTelnet object. This will also close
    the connection to the server.

    \sa logout()
*/

QtTelnet::~QtTelnet()
{
    delete d;
}

/*!
    Calling this function will make the QtTelnet object attempt to
    connect to a Telnet server specified by the given \a host and \a
    port.

    The connected() signal is emitted if the connection
    succeeds, and the connectionError() signal is emitted if the
    connection fails. Once the connection is establishe you must call
    login().

    \sa close()
*/
void QtTelnet::connectToHost(const QString &host, quint16 port)
{
    if ( d->connected ) return;
    d->socket->connectToHost(host, port);
}

void QtTelnet::close()
{
    if ( !d->connected ) return;
    delete d->notifier;
    d->notifier = 0;
    d->connected = false;
    d->socket->close();
    emit loggedOut();
}

void QtTelnet::sendControl(Control ctrl)
{
    bool sendsync = false;

    char c;

    switch (ctrl)
    {
    case InterruptProcess: // Ctrl-C
        c = Common::IP;
        sendsync = true;
        break;
    case AbortOutput: // suspend/resume output
        c = Common::AO;
        sendsync = true;
        break;
    case Break:
        c = Common::BRK;
        break;
    case Suspend: // Ctrl-Z
        c = Common::SUSP;
        break;
    case EndOfFile:
        c = Common::CEOF;
        break;
    case Abort:
        c = Common::ABORT;
        break;
    case GoAhead:
        c = Common::GA;
        break;
    case AreYouThere:
        c = Common::AYT;
        sendsync = true;
        break;
    case EraseCharacter:
        c = Common::EC;
        break;
    case EraseLine:
        c = Common::EL;
        break;
    default:
        return;
    }
    const char command[2] = {Common::IAC, c};
    d->sendCommand(command, sizeof(command));
    if ( sendsync ) sendSync();
}

/*!
    Sends the string \a data to the Telnet server. This is often a
    command the Telnet server will execute.

    \sa sendControl()
*/
void QtTelnet::sendData(const QString &data)
{
    if ( !d->connected )
    {
#ifdef TELNET_DEBUG
        qDebug() << "Socket Not Connected - Returning";
#endif
        return;
    }

    QByteArray str = data.toLocal8Bit();
#ifdef TELNET_DEBUG
        qDebug() << "Sending Data To Socket - " << str << "\r\n\0";
#endif
    d->socket->write(str);
    d->socket->write("\r\n\0", 3);
}

/*!
    This function will log you out of the Telnet server.
    You cannot send any other data after sending this command.

    \sa login() sendData() sendControl()
*/
void QtTelnet::logout()
{
    d->sendCommand(Common::DO, Common::Logout);
}

/*!
    Sets the client window size to \a size.

    The width and height are given in number of characters.
    Non-visible clients should pass an invalid size (i.e. QSize()).

    \sa isValidWindowSize()
*/
void QtTelnet::setWindowSize(const QSize &size)
{
    setWindowSize(size.width(), size.height());
}

/*!
    Sets the client window size.

    The \a width and \a height are given in number of characters.

    \overload
*/
void QtTelnet::setWindowSize(int width, int height)
{
    bool wasvalid = isValidWindowSize();

    d->windowSize.setWidth(width);
    d->windowSize.setHeight(height);

    if ( wasvalid && isValidWindowSize() )
        d->sendWindowSize();
    else if ( isValidWindowSize() )
        d->sendCommand(Common::WILL, Common::NAWS);
    else if ( wasvalid )
        d->sendCommand(Common::WONT, Common::NAWS);
}

/*!
    Returns the window's size. This will be an invalid size
    if the Telnet server is not using the NAWS option (RFC1073).

    \sa isValidWindowSize()
*/
QSize QtTelnet::windowSize() const
{
    return (d->modes[Common::NAWS] ? d->windowSize : QSize());
}

/*!
    Returns true if the window size is valid, i.e.
    windowSize().isValid() returns true; otherwise returns false.

    \sa setWindowSize()
*/
bool QtTelnet::isValidWindowSize() const
{
    return windowSize().isValid();
}

/*!
    Set the \a socket to be used in the communication.

    This function allows you to use your own QSocket subclass. You
    should call this function before calling connectToHost(); if you
    call it after a connection has been established the connection
    will be closed, so in all cases you will need to call
    connectToHost() after calling this function.

    \sa socket(), connectToHost(), logout()
*/
void QtTelnet::setSocket(QTcpSocket *socket)
{
    d->setSocket(socket);
}

/*!
    Returns the QTcpSocket instance used by this telnet object.

    \sa setSocket()
*/
QTcpSocket *QtTelnet::socket() const
{
    return d->socket;
}

/*!
    Sends the Telnet \c SYNC sequence, meaning that the Telnet server
    should discard any data waiting to be processed once the \c SYNC
    sequence has been received. This is sent using a TCP urgent
    notification.

    \sa sendControl()
*/
void QtTelnet::sendSync()
{
    if ( !d->connected ) return;

    d->socket->flush(); // Force the socket to send all the pending data before
                        // sending the SYNC sequence.
    int s = d->socket->socketDescriptor();
    char tosend = (char)Common::DM;
    ::send(s, &tosend, 1, MSG_OOB); // Send the DATA MARK as out-of-band
}

/*!
    Sets the expected shell prompt pattern.

    The \a pattern is used to automatically recognize when the client
    has successfully logged in. When a line is read that matches the
    \a pattern, the loggedIn() signal will be emitted.

    \sa login(), loggedIn()
*/
void QtTelnet::setPromptPattern(const QRegExp &pattern)
{
    d->promptp = pattern;
}

/*!
    \fn void QtTelnet::setPromptString(const QString &pattern)

    Sets the expected shell prompt to \a pattern.

    \overload
*/

/*!
    Sets the expected login pattern.

    The \a pattern is used to automatically recognize when the server
    asks for a username. If no username has been set, the
    loginRequired() signal will be emitted.

    \sa login()
*/
void QtTelnet::setLoginPattern(const QRegExp &pattern)
{
    d->loginp = pattern;
}

/*!
    \fn void QtTelnet::setLoginString(const QString &login)

    Sets the expected login string to \a login.

    \overload
*/

/*!
    Sets the expected password prompt pattern.

    The \a pattern is used to automatically recognize when the server
    asks for a password. If no password has been set, the loginRequired()
    signal will be emitted.

    \sa login()
*/
void QtTelnet::setPasswordPattern(const QRegExp &pattern)
{
    d->passp = pattern;
}

/*!
    \fn void QtTelnet::setPasswordString(const QString &pattern)

    Sets the expected password prompt to \a pattern.

    \overload
*/

/*!
    Sets the \a username and \a password to be used when logging in to
    the server.

    \sa setLoginPattern(), setPasswordPattern()
*/
void QtTelnet::login(const QString &username, const QString &password)
{
    d->triedpass = d->triedlogin = false;
    d->login = username;
    d->pass = password;
}

/*!
    \fn void QtTelnet::loginRequired()

    This signal is emitted when the QtTelnet class sees
    that the Telnet server expects authentication and you
    have not already called login().

    As a reply to this signal you should either call
    login() or logout()

    \sa login(), logout()
*/

/*!
    \fn void QtTelnet::loginFailed()

    This signal is emitted when the login has failed.
    Do note that you might in certain cases see several
    loginRequired() signals being emitted but no
    loginFailed() signals. This is due to the Telnet
    specification not requiring the Telnet server to
    support reliable authentication methods.

    \sa login(), loginRequired()
*/

/*!
    \fn void QtTelnet::loggedOut()

    This signal is emitted when you have called logout()
    and the Telnet server has actually logged you out.

    \sa logout(), login()
*/

/*!
    \fn void QtTelnet::loggedIn()

    This signal is emitted when you have been logged in
    to the server as a result of the login() command
    being called. Do note that you might never see this
    signal even if you have been logged in, due to the
    Telnet specification not requiring Telnet servers
    to notify clients when users are logged in.

    \sa login(), setPromptPattern()
*/

/*!
    \fn void QtTelnet::connectionError(QAbstractSocket::SocketError error)

    This signal is emitted if the underlying socket
    implementation receives an error. The \a error
    argument is the same as being used in
    QSocket::connectionError()
*/

/*!
    \fn void QtTelnet::message(const QString &data)

    This signal is emitted when the QtTelnet object
    receives more \a data from the Telnet server.

    \sa sendData()
*/

