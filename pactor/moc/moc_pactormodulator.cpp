/****************************************************************************
** Meta object code from reading C++ file 'pactormodulator.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.1.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../includes/Pactor/pactormodulator.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'pactormodulator.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.1.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_PactorModulatorThread_t {
    QByteArrayData data[18];
    char stringdata[207];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_PactorModulatorThread_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_PactorModulatorThread_t qt_meta_stringdata_PactorModulatorThread = {
    {
QT_MOC_LITERAL(0, 0, 21),
QT_MOC_LITERAL(1, 22, 13),
QT_MOC_LITERAL(2, 36, 0),
QT_MOC_LITERAL(3, 37, 12),
QT_MOC_LITERAL(4, 50, 7),
QT_MOC_LITERAL(5, 58, 5),
QT_MOC_LITERAL(6, 64, 7),
QT_MOC_LITERAL(7, 72, 18),
QT_MOC_LITERAL(8, 91, 15),
QT_MOC_LITERAL(9, 107, 14),
QT_MOC_LITERAL(10, 122, 9),
QT_MOC_LITERAL(11, 132, 8),
QT_MOC_LITERAL(12, 141, 5),
QT_MOC_LITERAL(13, 147, 5),
QT_MOC_LITERAL(14, 153, 6),
QT_MOC_LITERAL(15, 160, 20),
QT_MOC_LITERAL(16, 181, 4),
QT_MOC_LITERAL(17, 186, 19)
    },
    "PactorModulatorThread\0send_waveform\0"
    "\0const float*\0samples\0count\0divider\0"
    "SetCenterFrequency\0calculateModFSK\0"
    "unsigned char*\0in_buffer\0bit_size\0"
    "speed\0phase\0float*\0baseband_data_buffer\0"
    "int*\0audio_samples_count\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PactorModulatorThread[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    3,   29,    2, 0x05,

 // slots: name, argc, parameters, tag, flags
       7,    1,   36,    2, 0x0a,
       8,    6,   39,    2, 0x0a,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3, QMetaType::Int, QMetaType::Float,    4,    5,    6,

 // slots: parameters
    QMetaType::Void, QMetaType::Float,    2,
    QMetaType::Void, 0x80000000 | 9, QMetaType::Int, QMetaType::Int, QMetaType::Int, 0x80000000 | 14, 0x80000000 | 16,   10,   11,   12,   13,   15,   17,

       0        // eod
};

void PactorModulatorThread::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        PactorModulatorThread *_t = static_cast<PactorModulatorThread *>(_o);
        switch (_id) {
        case 0: _t->send_waveform((*reinterpret_cast< const float*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< float(*)>(_a[3]))); break;
        case 1: _t->SetCenterFrequency((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 2: _t->calculateModFSK((*reinterpret_cast< unsigned char*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< float*(*)>(_a[5])),(*reinterpret_cast< int*(*)>(_a[6]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (PactorModulatorThread::*_t)(const float * , int , float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PactorModulatorThread::send_waveform)) {
                *result = 0;
            }
        }
    }
}

const QMetaObject PactorModulatorThread::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_PactorModulatorThread.data,
      qt_meta_data_PactorModulatorThread,  qt_static_metacall, 0, 0}
};


const QMetaObject *PactorModulatorThread::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PactorModulatorThread::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_PactorModulatorThread.stringdata))
        return static_cast<void*>(const_cast< PactorModulatorThread*>(this));
    return QObject::qt_metacast(_clname);
}

int PactorModulatorThread::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void PactorModulatorThread::send_waveform(const float * _t1, int _t2, float _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
struct qt_meta_stringdata_PactorModulator_t {
    QByteArrayData data[8];
    char stringdata[102];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_PactorModulator_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_PactorModulator_t qt_meta_stringdata_PactorModulator = {
    {
QT_MOC_LITERAL(0, 0, 15),
QT_MOC_LITERAL(1, 16, 19),
QT_MOC_LITERAL(2, 36, 0),
QT_MOC_LITERAL(3, 37, 14),
QT_MOC_LITERAL(4, 52, 6),
QT_MOC_LITERAL(5, 59, 4),
QT_MOC_LITERAL(6, 64, 18),
QT_MOC_LITERAL(7, 83, 17)
    },
    "PactorModulator\0startModCalculation\0"
    "\0unsigned char*\0float*\0int*\0"
    "SetCenterFrequency\0slCalculateModFSK\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PactorModulator[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    6,   29,    2, 0x05,

 // slots: name, argc, parameters, tag, flags
       6,    1,   42,    2, 0x0a,
       7,    6,   45,    2, 0x0a,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3, QMetaType::Int, QMetaType::Int, QMetaType::Int, 0x80000000 | 4, 0x80000000 | 5,    2,    2,    2,    2,    2,    2,

 // slots: parameters
    QMetaType::Void, QMetaType::Float,    2,
    QMetaType::Void, 0x80000000 | 3, QMetaType::Int, QMetaType::Int, QMetaType::Int, 0x80000000 | 4, 0x80000000 | 5,    2,    2,    2,    2,    2,    2,

       0        // eod
};

void PactorModulator::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        PactorModulator *_t = static_cast<PactorModulator *>(_o);
        switch (_id) {
        case 0: _t->startModCalculation((*reinterpret_cast< unsigned char*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< float*(*)>(_a[5])),(*reinterpret_cast< int*(*)>(_a[6]))); break;
        case 1: _t->SetCenterFrequency((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 2: _t->slCalculateModFSK((*reinterpret_cast< unsigned char*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< float*(*)>(_a[5])),(*reinterpret_cast< int*(*)>(_a[6]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (PactorModulator::*_t)(unsigned char * , int , int , int , float * , int * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PactorModulator::startModCalculation)) {
                *result = 0;
            }
        }
    }
}

const QMetaObject PactorModulator::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_PactorModulator.data,
      qt_meta_data_PactorModulator,  qt_static_metacall, 0, 0}
};


const QMetaObject *PactorModulator::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PactorModulator::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_PactorModulator.stringdata))
        return static_cast<void*>(const_cast< PactorModulator*>(this));
    return QObject::qt_metacast(_clname);
}

int PactorModulator::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void PactorModulator::startModCalculation(unsigned char * _t1, int _t2, int _t3, int _t4, float * _t5, int * _t6)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)), const_cast<void*>(reinterpret_cast<const void*>(&_t5)), const_cast<void*>(reinterpret_cast<const void*>(&_t6)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
