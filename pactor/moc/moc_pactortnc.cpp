/****************************************************************************
** Meta object code from reading C++ file 'pactortnc.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.1.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../includes/Pactor/pactortnc.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'pactortnc.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.1.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_PactorTNCThread_t {
    QByteArrayData data[15];
    char stringdata[149];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_PactorTNCThread_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_PactorTNCThread_t qt_meta_stringdata_PactorTNCThread = {
    {
QT_MOC_LITERAL(0, 0, 15),
QT_MOC_LITERAL(1, 16, 15),
QT_MOC_LITERAL(2, 32, 0),
QT_MOC_LITERAL(3, 33, 14),
QT_MOC_LITERAL(4, 48, 6),
QT_MOC_LITERAL(5, 55, 4),
QT_MOC_LITERAL(6, 60, 15),
QT_MOC_LITERAL(7, 76, 5),
QT_MOC_LITERAL(8, 82, 8),
QT_MOC_LITERAL(9, 91, 11),
QT_MOC_LITERAL(10, 103, 6),
QT_MOC_LITERAL(11, 110, 5),
QT_MOC_LITERAL(12, 116, 14),
QT_MOC_LITERAL(13, 131, 8),
QT_MOC_LITERAL(14, 140, 7)
    },
    "PactorTNCThread\0calculateModFSK\0\0"
    "unsigned char*\0float*\0int*\0createNewPacket\0"
    "event\0ccharptr\0PT_argument\0setLED\0"
    "count\0t_set_LED_arg*\0LED_args\0PTEvent\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PactorTNCThread[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    6,   34,    2, 0x05,
       6,    2,   47,    2, 0x05,
      10,    2,   52,    2, 0x05,

 // slots: name, argc, parameters, tag, flags
      14,    2,   57,    2, 0x0a,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3, QMetaType::Int, QMetaType::Int, QMetaType::Int, 0x80000000 | 4, 0x80000000 | 5,    2,    2,    2,    2,    2,    2,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 8,    7,    9,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 12,   11,   13,

 // slots: parameters
    QMetaType::Void, QMetaType::Int, 0x80000000 | 8,    7,    9,

       0        // eod
};

void PactorTNCThread::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        PactorTNCThread *_t = static_cast<PactorTNCThread *>(_o);
        switch (_id) {
        case 0: _t->calculateModFSK((*reinterpret_cast< unsigned char*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< float*(*)>(_a[5])),(*reinterpret_cast< int*(*)>(_a[6]))); break;
        case 1: _t->createNewPacket((*reinterpret_cast< const int(*)>(_a[1])),(*reinterpret_cast< ccharptr(*)>(_a[2]))); break;
        case 2: _t->setLED((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< t_set_LED_arg*(*)>(_a[2]))); break;
        case 3: _t->PTEvent((*reinterpret_cast< const int(*)>(_a[1])),(*reinterpret_cast< ccharptr(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (PactorTNCThread::*_t)(unsigned char * , int , int , int , float * , int * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PactorTNCThread::calculateModFSK)) {
                *result = 0;
            }
        }
        {
            typedef void (PactorTNCThread::*_t)(const int , ccharptr );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PactorTNCThread::createNewPacket)) {
                *result = 1;
            }
        }
        {
            typedef void (PactorTNCThread::*_t)(int , t_set_LED_arg * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PactorTNCThread::setLED)) {
                *result = 2;
            }
        }
    }
}

const QMetaObject PactorTNCThread::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_PactorTNCThread.data,
      qt_meta_data_PactorTNCThread,  qt_static_metacall, 0, 0}
};


const QMetaObject *PactorTNCThread::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PactorTNCThread::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_PactorTNCThread.stringdata))
        return static_cast<void*>(const_cast< PactorTNCThread*>(this));
    return QObject::qt_metacast(_clname);
}

int PactorTNCThread::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void PactorTNCThread::calculateModFSK(unsigned char * _t1, int _t2, int _t3, int _t4, float * _t5, int * _t6)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)), const_cast<void*>(reinterpret_cast<const void*>(&_t5)), const_cast<void*>(reinterpret_cast<const void*>(&_t6)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void PactorTNCThread::createNewPacket(const int _t1, ccharptr _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void PactorTNCThread::setLED(int _t1, t_set_LED_arg * _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
struct qt_meta_stringdata_PactorTNC_t {
    QByteArrayData data[30];
    char stringdata[374];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_PactorTNC_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_PactorTNC_t qt_meta_stringdata_PactorTNC = {
    {
QT_MOC_LITERAL(0, 0, 9),
QT_MOC_LITERAL(1, 10, 24),
QT_MOC_LITERAL(2, 35, 0),
QT_MOC_LITERAL(3, 36, 9),
QT_MOC_LITERAL(4, 46, 15),
QT_MOC_LITERAL(5, 62, 5),
QT_MOC_LITERAL(6, 68, 8),
QT_MOC_LITERAL(7, 77, 11),
QT_MOC_LITERAL(8, 89, 6),
QT_MOC_LITERAL(9, 96, 5),
QT_MOC_LITERAL(10, 102, 14),
QT_MOC_LITERAL(11, 117, 8),
QT_MOC_LITERAL(12, 126, 7),
QT_MOC_LITERAL(13, 134, 13),
QT_MOC_LITERAL(14, 148, 19),
QT_MOC_LITERAL(15, 168, 11),
QT_MOC_LITERAL(16, 180, 21),
QT_MOC_LITERAL(17, 202, 6),
QT_MOC_LITERAL(18, 209, 10),
QT_MOC_LITERAL(19, 220, 12),
QT_MOC_LITERAL(20, 233, 10),
QT_MOC_LITERAL(21, 244, 20),
QT_MOC_LITERAL(22, 265, 16),
QT_MOC_LITERAL(23, 282, 12),
QT_MOC_LITERAL(24, 295, 12),
QT_MOC_LITERAL(25, 308, 7),
QT_MOC_LITERAL(26, 316, 19),
QT_MOC_LITERAL(27, 336, 15),
QT_MOC_LITERAL(28, 352, 10),
QT_MOC_LITERAL(29, 363, 9)
    },
    "PactorTNC\0ChangeModulatorFrequency\0\0"
    "frequency\0createNewPacket\0event\0"
    "ccharptr\0PT_argument\0setLED\0count\0"
    "t_set_LED_arg*\0LED_args\0setMode\0"
    "buttonChecked\0sendWaveformToPanel\0"
    "setPanelAFC\0SetCenterFrequencyInt\0"
    "source\0changeMode\0setSlaveMode\0"
    "emitSetLED\0LED_refresh_callback\0"
    "huffman_callback\0sendWaveform\0"
    "const float*\0samples\0readHighPrioMessage\0"
    "t_struct_msgptr\0message_in\0blocksize\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PactorTNC[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   84,    2, 0x05,
       4,    2,   87,    2, 0x05,
       8,    2,   92,    2, 0x05,
      12,    1,   97,    2, 0x05,
      14,    1,  100,    2, 0x05,
      15,    1,  103,    2, 0x05,

 // slots: name, argc, parameters, tag, flags
      16,    2,  106,    2, 0x0a,
      18,    1,  111,    2, 0x0a,
      19,    0,  114,    2, 0x0a,
      20,    2,  115,    2, 0x0a,
      21,    0,  120,    2, 0x0a,
      22,    0,  121,    2, 0x0a,
      23,    3,  122,    2, 0x0a,
      26,    2,  129,    2, 0x0a,

 // signals: parameters
    QMetaType::Void, QMetaType::Float,    3,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 6,    5,    7,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 10,    9,   11,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void, 0x80000000 | 6,    2,
    QMetaType::Void, QMetaType::Bool,    2,

 // slots: parameters
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    3,   17,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 10,    9,   11,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 24, QMetaType::Int, QMetaType::Float,   25,    9,    2,
    QMetaType::Void, 0x80000000 | 27, QMetaType::Short,   28,   29,

       0        // eod
};

void PactorTNC::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        PactorTNC *_t = static_cast<PactorTNC *>(_o);
        switch (_id) {
        case 0: _t->ChangeModulatorFrequency((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 1: _t->createNewPacket((*reinterpret_cast< const int(*)>(_a[1])),(*reinterpret_cast< ccharptr(*)>(_a[2]))); break;
        case 2: _t->setLED((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< t_set_LED_arg*(*)>(_a[2]))); break;
        case 3: _t->setMode((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->sendWaveformToPanel((*reinterpret_cast< ccharptr(*)>(_a[1]))); break;
        case 5: _t->setPanelAFC((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->SetCenterFrequencyInt((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 7: _t->changeMode((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->setSlaveMode(); break;
        case 9: _t->emitSetLED((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< t_set_LED_arg*(*)>(_a[2]))); break;
        case 10: _t->LED_refresh_callback(); break;
        case 11: _t->huffman_callback(); break;
        case 12: _t->sendWaveform((*reinterpret_cast< const float*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< float(*)>(_a[3]))); break;
        case 13: _t->readHighPrioMessage((*reinterpret_cast< t_struct_msgptr(*)>(_a[1])),(*reinterpret_cast< qint16(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (PactorTNC::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PactorTNC::ChangeModulatorFrequency)) {
                *result = 0;
            }
        }
        {
            typedef void (PactorTNC::*_t)(const int , ccharptr );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PactorTNC::createNewPacket)) {
                *result = 1;
            }
        }
        {
            typedef void (PactorTNC::*_t)(int , t_set_LED_arg * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PactorTNC::setLED)) {
                *result = 2;
            }
        }
        {
            typedef void (PactorTNC::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PactorTNC::setMode)) {
                *result = 3;
            }
        }
        {
            typedef void (PactorTNC::*_t)(ccharptr );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PactorTNC::sendWaveformToPanel)) {
                *result = 4;
            }
        }
        {
            typedef void (PactorTNC::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PactorTNC::setPanelAFC)) {
                *result = 5;
            }
        }
    }
}

const QMetaObject PactorTNC::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_PactorTNC.data,
      qt_meta_data_PactorTNC,  qt_static_metacall, 0, 0}
};


const QMetaObject *PactorTNC::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PactorTNC::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_PactorTNC.stringdata))
        return static_cast<void*>(const_cast< PactorTNC*>(this));
    return QDialog::qt_metacast(_clname);
}

int PactorTNC::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 14;
    }
    return _id;
}

// SIGNAL 0
void PactorTNC::ChangeModulatorFrequency(float _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void PactorTNC::createNewPacket(const int _t1, ccharptr _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void PactorTNC::setLED(int _t1, t_set_LED_arg * _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void PactorTNC::setMode(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void PactorTNC::sendWaveformToPanel(ccharptr _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void PactorTNC::setPanelAFC(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}
QT_END_MOC_NAMESPACE
