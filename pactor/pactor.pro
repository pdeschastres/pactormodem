include (pactormodem.pri)
# configuring for Qt 5.1.1 #
# configuring for Qwt 6.1.0 #
TEMPLATE = lib
TARGET   = pactormodem

#unix: DESTDIR = ../lib
win32: DLLDESTDIR = ../lib
win32: DESTDIR = ../lib

DEFINES += PACTORMODEM_LIB # tells compiler this is the library export

DEBUGBUILD = 1
#RELEASEBUILD = 1

QT      += core gui widgets

win32: CONFIG += windows
CONFIG  += qt thread exceptions shared
#unix:  CONFIG += x11inc x11

MOC_DIR = moc
OBJECTS_DIR = obj
RCC_DIR = rcc


#unix: {
#INCLUDEPATH  += /usr/local/qwt-6.1.0/include/
#INCLUDEPATH += /usr/include/jack/
#}

win32: {
INCLUDEPATH += "C:\\Program Files\\Jack\\includes\\"
INCLUDEPATH += "C:\\Qwt-6.1.0\\include\\"
}


INCLUDEPATH += \
    ../includes \
    ../includes/gui \
    ../includes/utils \
    ../includes/widgets \
    ../includes/Pactor \
    ../sound \
    ../utils \
    ../widgets \
    ../pactor \
    .


#unix:  DEPENDPATH  += /usr/lib/jack/
win32: DEPENDPATH += "C:\\Program Files\\Jack\\32bit\\"
win32: DEPENDPATH += "C:\\Qwt-6.1.0\\include\\"
win32: DEPENDPATH += "C:\\Qwt-6.1.0\\lib\\"

DEPENDPATH += \
    ../includes \
    ../includes/gui \
    ../includes/utils \
    ../includes/widgets \
    ../includes/Pactor \
    ../sound  \
    ../utils \
    ../widgets \
    ../pactor \
    .

HEADERS += \
    ../includes/globals.h


#   Widgets           #
HEADERS += \
    ../includes/widgets/waveform.h \
    ../includes/widgets/simplewaterfall.h

HEADERS += \
    ../includes/Pactor/pactor.hpp \
    ../includes/Pactor/synchro.h \
    ../includes/Pactor/pactortnc.h \
    ../includes/Pactor/pactorsynchro.h \
    ../includes/Pactor/pactorsession.h \
    ../includes/Pactor/pactorparams.h \
    ../includes/Pactor/pactornetstruct.h \
    ../includes/Pactor/pactormodulator.h \
    ../includes/Pactor/pactorhuffman.h \
    ../includes/Pactor/pactorFSKdemod.h \
    ../includes/Pactor/pactordemodulator.h \
    ../includes/Pactor/pactordecoder.h \
    ../includes/Pactor/pactoraudioengine.h

#   Utilities         #
HEADERS += \
    ../includes/utils/crccalc.h \
    ../includes/utils/simple_fft.h



#    PACTOR MODEM #
SOURCES += \
    pactor.cpp \
    pactorinterface.cpp \
    pactoraudioengine.cpp \
    pactorsynchro.cpp \
    pactormodulator.cpp \
    pactorFSKdemod.cpp \
    pactorhuffman.cpp \
    pactordecoder.cpp \
    pactormodutils.cpp \
    pactordemodulator.cpp \
    pactortnc.cpp \
    pactorsession.cpp

#    WIDGETS      #
SOURCES += \
    ../widgets/waveform.cpp \
    ../widgets/simplewaterfall.cpp

#    UTILITIES    #
SOURCES += \
    ../utils/crccalc.cpp \
    ../utils/simple_fft.cpp



#unix:  {
#LIBS += -L/usr/local/qwt-6.1.0/lib/ -lqwt
#LIBS += -L/usr/lib/i386-linux-gnu/jack/ -ljack
#}

win32: {
LIBS += -L"C:\\Qwt-6.1.0\\lib\\" -lqwt
LIBS += -L"C:\\Program Files\\Jack\\32bit\\" -ljack
}

# definitions display
#!isEmpty(DEBUGBUILD)   { message("DLL Build debug - " $$QT_VERSION) }
#!isEmpty(RELEASEBUILD) { message("DLL Build release - " $$QT_VERSION) }
#message(include: $$INCLUDEPATH)
#message(libs: $$LIBS)
#message(defines: $$DEFINES)
#message(depends: $$DEPENDPATH)
