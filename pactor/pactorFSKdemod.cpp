/****************************************************************************
** Copyright (C) 2012 OpenMOR development team.   All rights reserved.
** Contact: Project Leader Ken AD5XJ (ad5xj@arrl.net)
** or visit the project web site openmor.sourceforge.net
**
** This file is part of the OpenMOR Project in the modem or RMSLink executable.
**
** This software is intended for use on amateur radio only and is not a
** commercial product.  The software of the OpenMOR project is
** primarily useful for technical and scientific purposes in non-commercial
** applications only.   Further, this software is distributed in the hope
** that it will be useful.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** === No Commercial Usage ===
** This file contains Open Source code and may not be distributed except as
** outlined in the terms and conditions of the GNU GPL License v3 included
** with this source code.
**
**  === GNU Lesser General Public License Usage ===
** This file may be used under the terms of the GNU Lesser
** General Public License version 3 or higher as published by the
** Free Software Foundation and appearing in the file LICENSE.TXT included
** in the packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 3 (or higher)
** requirements will be met: http://www.gnu.org/licenses/gpl-3.0.txt.
**
** In addition, portions of this software are products originating with
** Nokia and other authors. Each author retains their rights as described
** in their respective LGPL Exception statements.  Any distribution of this
** software carries with it the implied restrictions of the GPLv3 license,
** even with modifications to the original from this source code.
**
** Portions of this software may be modified from the original and are subject
** to GPL or LGPL License restrictions of the original source code.
**
** The developers of the OpenMOR software wish to thank particular individuals
** for their generous contributions.   We wish to thank:
**
**     Thomas Sailer        for the original PACTOR I modem from hfterm
**     Dave Freeze, W1HKJ   from his project FLDIGI
**     Rick Muething, KN6KB of the Amateur Radio Safety Foundation Inc.
**                          for his outstanding WinMOR decoder and
**                          contributions to this project.
**     Volker Schroer       for his contributions from his project LinPSK
**
** - - - The OpenMOR development team.
****************************************************************************/
#include <math.h>
#include "pactordemodulator.h"



void PactorDemodThread::initFSKDemod(int center_frequency_start)
{
    //    PACTOR_TNC_DEBUG << "FSK demodulator initialization";
    demod_sample_count      = 0;

    /* compute NCO samples per NCO period */
    nco_periods  = (float)SAMPLE_RATE_8KHZ / (float)center_frequency_start;
    /*delay variables of iir filter for cosine/sine generation*/

    // initialize input bandpass filter intermediate values
    bpf_xv[1] = 0;
    bpf_xv[2] = 0;
    bpf_xv[3] = 0;
    bpf_xv[4] = 0;
    bpf_xv[5] = 0;
    bpf_xv[6] = 0;

    bpf_yv[1] = 0;
    bpf_yv[2] = 0;
    bpf_yv[3] = 0;
    bpf_yv[4] = 0;
    bpf_yv[5] = 0;
    bpf_yv[6] = 0;

    // initialize Chebychev filter coefficients
    //     filtertype   =   Chebychev
    //     passtype     =   Bandpass
    //     ripple   =   -2
    //     order    =   3
    //     samplerate   =   8000

    //     corner1  =   550
    //     corner2  =   1050
    chebychev_coef[0][0]    = -0.7489709105;
    chebychev_coef[0][1]    = 3.8088477618;
    chebychev_coef[0][2]    = -8.8974034898;
    chebychev_coef[0][3]    = 11.9811368748;
    chebychev_coef[0][4]    = -9.8025695868;
    chebychev_coef[0][5]    = 4.6228407472;
    chebychev_gain[0]       = 4.425444452e+02;
    //     corner1  =   650
    //     corner2  =   1150
    chebychev_coef[1][0]    = -0.7489709105;
    chebychev_coef[1][1]    = 3.5799872936;
    chebychev_coef[1][2]    = -8.1366240219;
    chebychev_coef[1][3]    = 10.8477824961;
    chebychev_coef[1][4]    = -8.9635257852;
    chebychev_coef[1][5]    = 4.3450702601;
    chebychev_gain[1]       = 4.475962601e+02;
    //     corner1  =   750
    //     corner2  =   1250
    chebychev_coef[2][0]    = -0.7489709105;
    chebychev_coef[2][1]    = 3.3290550132;
    chebychev_coef[2][2]    = -7.3566387275;
    chebychev_coef[2][3]    = 9.6932528393;
    chebychev_coef[2][4]    = -8.1033003791;
    chebychev_coef[2][5]    = 4.0405109700;
    chebychev_gain[2]       = 4.513732188e+02;
    //     corner1  =   850
    //     corner2  =   1350
    chebychev_coef[3][0]    = -0.7489709105;
    chebychev_coef[3][1]    = 3.0575980016;
    chebychev_coef[3][2]    = -6.5766534330;
    chebychev_coef[3][3]    = 8.5408143367;
    chebychev_coef[3][4]    = -7.2430749730;
    chebychev_coef[3][5]    = 3.7110405862;
    chebychev_gain[3]       = 4.542386580e+02;
    //     corner1  =   950
    //     corner2  =   1450
    chebychev_coef[4][0]    = -0.7489709105;
    chebychev_coef[4][1]    = 2.7672898817;
    chebychev_coef[4][2]    = -5.8158739651;
    chebychev_coef[4][3]    = 7.4103025055;
    chebychev_coef[4][4]    = -6.4040311714;
    chebychev_coef[4][5]    = 3.3586904030;
    chebychev_gain[4]       = 4.564371362e+02;
    //     corner1  =   1050
    //     corner2  =   1550
    chebychev_coef[5][0]    = -0.7489709105;
    chebychev_coef[5][1]    = 2.4599204993;
    chebychev_coef[5][2]    = -5.0930332392;
    chebychev_coef[5][3]    = 6.3172961742;
    chebychev_coef[5][4]    = -5.6068290172;
    chebychev_coef[5][5]    = 2.9856327767;
    chebychev_gain[5]       = 4.581356837e+02;
    //     corner1  =   1150
    //     corner2  =   1650
    chebychev_coef[6][0]    = -0.7489709105;
    chebychev_coef[6][1]    = 2.1373848891;
    chebychev_coef[6][2]    = -4.4259299929;
    chebychev_coef[6][3]    = 5.2725457081;
    chebychev_coef[6][4]    = -4.8710982732;
    chebychev_coef[6][5]    = 2.5941677315;
    chebychev_gain[6]       = 4.594500591e+02;
    //     corner1  =   1250
    //     corner2  =   1750
    chebychev_coef[7][0]    = -0.7489709105;
    chebychev_coef[7][1]    = 1.8016715903;
    chebychev_coef[7][2]    = -3.8309905220;
    chebychev_coef[7][3]    = 4.2816850466;
    chebychev_coef[7][4]    = -4.2149550722;
    chebychev_coef[7][5]    = 2.1867087796;
    chebychev_gain[7]       = 4.604614290e+02;
    //     corner1  =   1350
    //     corner2  =   1850
    chebychev_coef[8][0]    = -0.7489709105;
    chebychev_coef[8][1]    = 1.4548503870;
    chebychev_coef[8][2]    = -3.3228642110;
    chebychev_coef[8][3]    = 3.3452415793;
    chebychev_coef[8][4]    = -3.6545558373;
    chebychev_coef[8][5]    = 1.7657680409;
    chebychev_gain[8]       = 4.612270491e+02;
    //     corner1  =   1450
    //     corner2  =   1950
    chebychev_coef[9][0]    = -0.7489709105;
    chebychev_coef[9][1]    = 1.0990595473;
    chebychev_coef[9][2]    = -2.9140628159;
    chebychev_coef[9][3]    = 2.4589413350;
    chebychev_coef[9][4]    = -3.2036994575;
    chebychev_coef[9][5]    = 1.3339407550;
    chebychev_gain[9]       = 4.617871667e+02;
    //     corner1  =   1550
    //     corner2  =   2050
    chebychev_coef[10][0]    = -0.7489709105;
    chebychev_coef[10][1]    = 0.7364926401;
    chebychev_coef[10][2]    = -2.6146523838;
    chebychev_coef[10][3]    = 1.6142905553;
    chebychev_coef[10][4]    = -2.8734875133;
    chebychev_coef[10][5]    = 0.8938892808;
    chebychev_gain[10]       = 4.621695002e+02;
    //     corner1  =   1650
    //     corner2  =   2150
    chebychev_coef[11][0]    = -0.7489709105;
    chebychev_coef[11][1]    = 0.3693850107;
    chebychev_coef[11][2]    = -2.4320053932;
    chebychev_coef[11][3]    = 0.7993993886;
    chebychev_coef[11][4]    = -2.6720509186;
    chebychev_coef[11][5]    = 0.4483266819;
    chebychev_gain[11]       = 4.623921201e+02;
    //     corner1  =   1750
    //     corner2  =   2250
    chebychev_coef[12][0]    = -0.7489709105;
    chebychev_coef[12][1]    = 0.0;
    chebychev_coef[12][2]    = -2.3706192192;
    chebychev_coef[12][3]    = 0.0;
    chebychev_coef[12][4]    = -2.6043497109;
    chebychev_coef[12][5]    = 0.0;
    chebychev_gain[12]       = 4.624652274e+02;
    //     corner1  =   1850
    //     corner2  =   2350
    chebychev_coef[13][0]    = -0.7489709105 ;
    chebychev_coef[13][1]    = -0.3693850107;
    chebychev_coef[13][2]    = -2.4320053932;
    chebychev_coef[13][3]    = -0.7993993886;
    chebychev_coef[13][4]    = -2.6720509186;
    chebychev_coef[13][5]    = -0.4483266819;
    chebychev_gain[13]       = 4.623921201e+02;
    //     corner1  =   1950
    //     corner2  =   2450
    chebychev_coef[14][0]    = -0.7489709105;
    chebychev_coef[14][1]    = -0.7364926401;
    chebychev_coef[14][2]    = -2.6146523838;
    chebychev_coef[14][3]    = -1.6142905553;
    chebychev_coef[14][4]    = -2.8734875133;
    chebychev_coef[14][5]    = -0.8938892808;
    chebychev_gain[14]       = 4.621695002e+02;
    //     corner1  =   2050
    //     corner2  =   2550
    chebychev_coef[15][0]    = -0.7489709105;
    chebychev_coef[15][1]    = -1.0990595473;
    chebychev_coef[15][2]    = -2.9140628159;
    chebychev_coef[15][3]    = -2.4589413350;
    chebychev_coef[15][4]    = -3.2036994575;
    chebychev_coef[15][5]    = -1.3339407550;
    chebychev_gain[15]       = 4.617871667e+02;

    //*********************************************************************************
    //                  output lowpass filter
    /*
        filtertype  =   Butterworth
        passtype    =   Lowpass
        ripple  =   -2
        order   =   5
        samplerate  =   8000
        corner1     =   220

    Command line: /www/usr/fisher/helpers/mkfilter -Bu -Lp -o 5 -a 2.7500000000e-02 0.0000000000e+00
    */
    lpf_xv[1] = 0;
    lpf_xv[2] = 0;
    lpf_xv[3] = 0;
    lpf_xv[4] = 0;
    lpf_xv[5] = 0;
    lpf_yv[1] = 0;
    lpf_yv[2] = 0;
    lpf_yv[3] = 0;
    lpf_yv[4] = 0;
    lpf_yv[5] = 0;



    //%sample frequency in Hz
    //fa=8000;
    //%pass frequency in Hz
    //fp=500;
    //%stop frequency in Hz
    //fs=2500;
    blp1 = 1.8965e-06;
    blp2 = 9.4826e-06;
    blp3 = 1.8965e-05 ;
    blp4 = 1.8965e-05 ;
    blp5 = 9.4826e-06;
    blp6 = 1.8965e-06;

    alp2 = -4.51376;
    alp3 = 8.17117;
    alp4 = -7.41416;
    alp5 = 3.3713;
    alp6 = -0.6145;

    /*delay variables of iir filter for low pass filtering the I-path*/

    zlpc1 = 0.0;
    zlpc2 = 0.0;
    zlpc3 = 0.0;
    zlpc4 = 0.0;
    zlpc5 = 0.0;
    zlpc6 = 0.0;

    /*delay variables of iir filter for low pass filtering the Q-path*/

    zlps1 = 0.0;
    zlps2 = 0.0;
    zlps3 = 0.0;
    zlps4 = 0.0;
    zlps5 = 0.0;
    zlps6 = 0.0;

    coold = 0.0;/*old value of I-path*/
    conew = 1.0;/*new value of I-path*/
    siold = 0.0;/*old value of Q-path*/
    sinew = 1.0;/*new value of Q-path*/
    demo = 0.0;/*value demodulating*/

    /*constant of demodulating*/

    decon = 0.3;


    // initialize sine-cosine table
    InitSineTable(true);

    // initialize Chebychev input filter
    SetCenterFrequency(m_pactorTNC->center_frequency_start);

}

/*------------------------------------------------------------------
initialization of sine/cosine table and input filter index when
frequency changes
------------------------------------------------------------------*/
void PactorDemodThread::SetCenterFrequency(float frequency)
{
    //    qDebug()<<"demod thread set freq"<<frequency;
    //PACTOR_TNC_DEBUG << "Demodulator Center frequency now:"  << frequency;
    if (fabs(center_frequency - frequency) > 2.0)
    {
        // store center frequency
        center_frequency        = frequency;
        // send a signal to modem object
        // set filtering parameters
        chebychev_frequency_index   = (frequency - 800) / 100;
        if (chebychev_frequency_index < 0)
        {
            chebychev_frequency_index   = 0;
        }
        if (chebychev_frequency_index > 15)
        {
            chebychev_frequency_index   = 15;
        }
        /* compute NCO samples per NCO period */
        nco_periods  = (float)SAMPLE_RATE_8KHZ / (float)frequency;
        InitSineTable(false);
    }
}

/*------------------------------------------------------------------
initialization of sine/cosine tables used by mixer function
------------------------------------------------------------------*/
void PactorDemodThread::InitSineTable(bool initialize)
{
    int     i;
    float   sine_cosine_angle, sine_cosine_inc;
    int     sine_count;

    if (initialize)
    {
        try
        {
            sine_table   = new float[SINE_COSINE_TABLE_SIZE];
        }
        catch
            (...)
        {
            PACTOR_TNC_DEBUG << "erreur alloc sine_table"  ;
        }
        try
        {
            cosine_table   = new float[SINE_COSINE_TABLE_SIZE];
        }
        catch
            (...)
        {
            PACTOR_TNC_DEBUG << "erreur alloc cosine_table"  ;
        }
    }
    sine_count      = (int)(0.5 + ((float)SINE_COSINE_TABLE_SIZE  /  nco_periods));
    sine_cosine_inc    = (2.0 * M_PI) / ((float)SINE_COSINE_TABLE_SIZE / (float)sine_count);

    /*__cosine/sine generation__*/
    sine_cosine_angle   = 0.0;
    sine_cosine_index   = 0;

    for (i = 0 ; i < SINE_COSINE_TABLE_SIZE ; i++)
    {
        sine_table[i]       = sin(sine_cosine_angle);
        cosine_table[i]     = cos(sine_cosine_angle);
        sine_cosine_angle   += sine_cosine_inc;
    }
}

/*------------------------------------------------------------------
implementation of function
------------------------------------------------------------------*/
float PactorDemodThread::InputFilter(const float in_buffer[], int data_count, float out_buffer[])
{
    float   accumulator = 0.0;

    for (int i = 0; i < data_count; i++)
    {
        bpf_xv[0] = bpf_xv[1];
        bpf_xv[1] = bpf_xv[2];
        bpf_xv[2] = bpf_xv[3];
        bpf_xv[3] = bpf_xv[4];
        bpf_xv[4] = bpf_xv[5];
        bpf_xv[5] = bpf_xv[6];
        bpf_xv[6] = in_buffer[i] / chebychev_gain[chebychev_frequency_index];
        bpf_yv[0] = bpf_yv[1];
        bpf_yv[1] = bpf_yv[2];
        bpf_yv[2] = bpf_yv[3];
        bpf_yv[3] = bpf_yv[4];
        bpf_yv[4] = bpf_yv[5];
        bpf_yv[5] = bpf_yv[6];
        bpf_yv[6] = (bpf_xv[6] - bpf_xv[0]) + 3 * (bpf_xv[2] - bpf_xv[4])
                    + (chebychev_coef[chebychev_frequency_index][0] * bpf_yv[0])
                    + (chebychev_coef[chebychev_frequency_index][1] * bpf_yv[1])
                    + (chebychev_coef[chebychev_frequency_index][2] * bpf_yv[2])
                    + (chebychev_coef[chebychev_frequency_index][3] * bpf_yv[3])
                    + (chebychev_coef[chebychev_frequency_index][4] * bpf_yv[4])
                    + (chebychev_coef[chebychev_frequency_index][5] * bpf_yv[5]);
        out_buffer[i] = bpf_yv[6];
        accumulator     += fabs(bpf_yv[6]);
    }
    return accumulator / (float)data_count;
}

/*------------------------------------------------------------------
Quadrature mixer
------------------------------------------------------------------*/
void PactorDemodThread::QuadMix(float in_buffer[], int data_count, float re_buffer[], float im_buffer[])
{
    int i;
    /*__mixing__*/

    for (i = 0; i < data_count; i++)
    {
        /*mixing*/
        re_buffer[i] = cosine_table[sine_cosine_index] * in_buffer[i];/*I-path*/
        im_buffer[i] = sine_table[sine_cosine_index] * in_buffer[i];/*Q-path*/
        // increment sine_cosine_index
        sine_cosine_index   = (sine_cosine_index + 1) % SINE_COSINE_TABLE_SIZE;
    }

    /*__low pass__*/

    for (i = 0; i < data_count; i++)
    {
        /*feedback*/
        zlpc1 = re_buffer[i] - (alp2 * zlpc2 + alp3 * zlpc3 + alp4 * zlpc4 + alp5 * zlpc5 + alp6 * zlpc6);/*I-path*/
        zlps1 = im_buffer[i] - (alp2 * zlps2 + alp3 * zlps3 + alp4 * zlps4 + alp5 * zlps5 + alp6 * zlps6);/*Q-path*/

        /*forward*/
        re_buffer[i] =   blp1 * zlpc1 + blp2 * zlpc2 + blp3 * zlpc3 + blp4 * zlpc4 + blp5 * zlpc5 + blp6 * zlpc6;/*I-path*/
        im_buffer[i] = -(blp1 * zlps1 + blp2 * zlps2 + blp3 * zlps3 + blp4 * zlps4 + blp5 * zlps5 + blp6 * zlps6);/*Q-path*/

        /*shift delays*/
        /*I-path*/
        zlpc6 = zlpc5;
        zlpc5 = zlpc4;
        zlpc4 = zlpc3;
        zlpc3 = zlpc2;
        zlpc2 = zlpc1;
        /*Q-path*/
        zlps6 = zlps5;
        zlps5 = zlps4;
        zlps4 = zlps3;
        zlps3 = zlps2;
        zlps2 = zlps1;
    }
}

/*------------------------------------------------------------------
implementation of function
------------------------------------------------------------------*/
t_enum_carrier PactorDemodThread::MixedDemodulate(float re_buffer_down_one[] , float im_buffer_down_one[] ,
        int data_count,
        t_phase_ampl out_buffer_down_one[], float squelch_level)
{
    int i;
    float   temp_float;
    float   amplitude;

    t_enum_carrier    carrier_detected;

    carrier_detected    = CARRIER_UNKNOWN;

    for (i = 0; i < data_count; i++)
    {
        conew = re_buffer_down_one[i];
        sinew = im_buffer_down_one[i];

        /*__calculating demodulated value__*/
        temp_float  = atan2((coold * sinew - siold * conew), (coold * conew + siold * sinew)); /*atan argument*/
        amplitude   = sqrt(conew * conew + sinew * sinew);

        // process amplitude if it is a valid floating point value
        if (amplitude == amplitude)
        {
            amplitude_index = (amplitude_index + 1) % AMPLITUDE_HISTORY;
            amplitude_average   = amplitude_average + amplitude - last_amplitude[amplitude_index];
            last_amplitude[amplitude_index]      = amplitude;

            // if amplitude is over squelch threshold, store and flag carrier_detected to true
            if (amplitude_average > squelch_level)
            {
                out_buffer_down_one[i].amplitude     = amplitude;
                out_buffer_down_one[i].phase        = temp_float;
                // carrier detector
                if (carrier_detected == CARRIER_UNKNOWN)
                {
                    carrier_detected        = CARRIER_HIGH;
                }
                else
                    if (carrier_detected == NO_CARRIER)
                    {
                        carrier_detected        = NEW_CARRIER;
                    }
                    else
                        if (carrier_detected == LOST_CARRIER)
                        {
                            carrier_detected        = CARRIER_HI_LO_HI;
                        }
            }
            else
            {
                out_buffer_down_one[i].amplitude     = 0.0;
                out_buffer_down_one[i].phase        = 0.0;
                // carrier detector
                if (carrier_detected == CARRIER_UNKNOWN)
                {
                    carrier_detected        = NO_CARRIER;
                }
                else
                    if (carrier_detected == CARRIER_HIGH)
                    {
                        carrier_detected        = LOST_CARRIER;
                    }
                    else
                        if (carrier_detected == NEW_CARRIER)
                        {
                            carrier_detected        = CARRIER_LO_HI_LO;
                        }
            }
        }
        /*__shifting values__*/
        coold = conew;
        siold = sinew;
    }

    return carrier_detected;
}

/*------------------------------------------------------------------
implementation of function
low-pass filter, 500 Hz
demod_dwnspld_data_buffer argument is the demodulater, low-pass filtered
    and downsampled data buffer base address
last_demod_dwnspld_index argument is the index of last demodulated sample

demod_sample_count is a private variable which stores the last processed
    demodulated sample index modulo PACTOR_I_DOWNSAMPLE. It keeps the
    synchronization between sub-samples and demodulated samples.
------------------------------------------------------------------*/
void PactorDemodThread::OutFilter500(t_phase_ampl in_buffer[],
                                     int data_count,
                                     t_phase_ampl demod_dwnspld_data_buffer[],
                                     int &last_demod_dwnspld_index,
                                     int    store_waveform, // 0=no, 1=phase, 2=amplitude
                                     float  *waveform,
                                     qint64 current_timestamp, // ms
                                     qint64 &last_sample_timestamp, // ms
                                     int    &filtered_samples_count)
{
    int     waveform_idx    = 0;
    float   iter_phase;

    filtered_samples_count  = 0;
    iter_phase  = (float)buffer_phase;
    // start storing samples into first buffer index if downcount reaches zero
    if ((sample_synchronized == true)
        && (sample_sync_count <= 0))
    {
        // reset index
        last_demod_dwnspld_index    = 0;
        sample_sync_count           = INT_MAX;
    }

    /*__low pass__   corner = 520 Hz */
    for (int i = 0; i < data_count; i++)
    {
        lpf_xv[0] = lpf_xv[1];
        lpf_xv[1] = lpf_xv[2];
        lpf_xv[2] = lpf_xv[3];
        lpf_xv[3] = lpf_xv[4];
        lpf_xv[4] = lpf_xv[5];
        lpf_xv[5] = in_buffer[i].phase / 5.113194074e+03;
        lpf_yv[0] = lpf_yv[1];
        lpf_yv[1] = lpf_yv[2];
        lpf_yv[2] = lpf_yv[3];
        lpf_yv[3] = lpf_yv[4];
        lpf_yv[4] = lpf_yv[5];
        lpf_yv[5] = (lpf_xv[0] + lpf_xv[5]) + 5 * (lpf_xv[1] + lpf_xv[4]) + 10 * (lpf_xv[2] + lpf_xv[3])
                    + (0.2636472043 * lpf_yv[0]) + (-1.6660095295 * lpf_yv[1])
                    + (4.2677619753 * lpf_yv[2]) + (-5.5525742179 * lpf_yv[3])
                    + (3.6809162488 * lpf_yv[4]);
        demod_sample_count++;
        if (demod_sample_count >= PACTOR_I_DOWNSAMPLE)
        {
            if (store_waveform == 1)
            {
                waveform[waveform_idx]  = lpf_yv[5];
                waveform_idx++;
            }
            else
                if (store_waveform == 2)
                {
                    waveform[waveform_idx]  = in_buffer[i].amplitude / 2.0;
                    waveform_idx++;
                }
            /***************************************************/
            /*   direct write or memory ARQ adder              */
            /***************************************************/
            if (ARQ_mode == true)
            {
                demod_dwnspld_data_buffer[last_demod_dwnspld_index].phase       += (iter_phase * lpf_yv[5]);
            }
            else
            {
                demod_dwnspld_data_buffer[last_demod_dwnspld_index].phase       = lpf_yv[5];
            }
            demod_dwnspld_data_buffer[last_demod_dwnspld_index].amplitude   = in_buffer[i].amplitude;
            last_demod_dwnspld_index    = (last_demod_dwnspld_index + 1) % DEMOD_BUFFER_SIZE;
            // last downsampled sample timestamp
            last_sample_timestamp = current_timestamp + ((i * SAMPLE_8K_DURATION_US) / 1000);

            demod_sample_count  = 0;
            filtered_samples_count++;
            // decrement sample count until we reach new burst start
            if (sample_synchronized == true)
            {
                sample_sync_count--;
            }
        }
    }
}
