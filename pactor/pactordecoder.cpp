/****************************************************************************
** Copyright (C) 2012 OpenMOR development team.   All rights reserved.
** Contact: Project Leader Ken AD5XJ (ad5xj@arrl.net)
** or visit the project web site openmor.sourceforge.net
**
** This file is part of the OpenMOR Project in the modem or RMSLink executable.
**
** This software is intended for use on amateur radio only and is not a
** commercial product.  The software of the OpenMOR project is
** primarily useful for technical and scientific purposes in non-commercial
** applications only.   Further, this software is distributed in the hope
** that it will be useful.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** === No Commercial Usage ===
** This file contains Open Source code and may not be distributed except as
** outlined in the terms and conditions of the GNU GPL License v3 included
** with this source code.
**
**  === GNU Lesser General Public License Usage ===
** This file may be used under the terms of the GNU Lesser
** General Public License version 3 or higher as published by the
** Free Software Foundation and appearing in the file LICENSE.TXT included
** in the packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 3 (or higher)
** requirements will be met: http://www.gnu.org/licenses/gpl-3.0.txt.
**
** In addition, portions of this software are products originating with
** Nokia and other authors. Each author retains their rights as described
** in their respective LGPL Exception statements.  Any distribution of this
** software carries with it the implied restrictions of the GPLv3 license,
** even with modifications to the original from this source code.
**
** Portions of this software may be modified from the original and are subject
** to GPL or LGPL License restrictions of the original source code.
**
** The developers of the OpenMOR software wish to thank particular individuals
** for their generous contributions.   We wish to thank:
**
**     Thomas Sailer        for the original PACTOR I modem from hfterm
**     Dave Freeze, W1HKJ   from his project FLDIGI
**     Rick Muething, KN6KB of the Amateur Radio Safety Foundation Inc.
**                          for his outstanding WinMOR decoder and
**                          contributions to this project.
**     Volker Schroer       for his contributions from his project LinPSK
**
** - - - The OpenMOR development team.
****************************************************************************/
#include "pactorhuffman.h"
#include "pactordecoder.h"
#include "pactordemodulator.h"
#include "pactortnc.h"
#include "pactoraudioengine.h"
//#include "utils.h"
#include "crccalc.h"

extern qint64              global_date_time_mSecs;

// none, HDR100, HDR200, CS3, other CS
static int burst_shift[] = {0, 16, 8, 16, 16};

#ifdef IAD_EXTERNAL
#if 0
static float            matched_filter_100_K[CORREL_BIT_LENGTH_100] = {0.2 / 0.7, 0.38 / 0.7, 0.55 / 0.7, 0.7 / 0.7,
        0.83 / 0.7, 0.92 / 0.7, 0.98 / 0.7, 1.0 / 0.7,
        1.0 / 0.7, 0.98 / 0.7, 0.92 / 0.7, 0.83 / 0.7,
        0.7 / 0.7, 0.55 / 0.7, 0.38 / 0.7, 0.2 / 0.7
                                                                      }; // 11/16
static float            matched_filter_200_K[CORREL_BIT_LENGTH_200] = {0.2 / 0.64, 0.55 / 0.64, 0.83 / 0.64, 0.98 / 0.64,
        0.98 / 0.64, 0.83 / 0.64, 0.55 / 0.64, 0.2 / 0.64
                                                                      };// 5/8
#endif
#if 0
static float            matched_filter_100_K[CORREL_BIT_LENGTH_100] = {0.0, 0.0, 1.0, 1.0,
        1.5, 2.0, 2.0, 2.5,
        2.5, 2.0, 2.0, 1.5,
        1.0, 1.0, 0.0, 0.0
                                                                      };
static float            matched_filter_200_K[CORREL_BIT_LENGTH_200] = {0.0, 1.0, 1.0, 2.0,
        2.0, 1.0, 1.0, 0.0
                                                                      };
#endif
#if 1
static float            matched_filter_100_K[CORREL_BIT_LENGTH_100] = {1.0, 1.0, 1.0, 1.0,
        1.0, 1.0, 1.0, 1.0,
        1.0, 1.0, 1.0, 1.0,
        1.0, 1.0, 1.0, 1.0
                                                                      };
static float            matched_filter_200_K[CORREL_BIT_LENGTH_200] = {1.0, 1.0, 1.0, 1.0,
        1.0, 1.0, 1.0, 1.0
                                                                      };
#endif
#endif // IAD_EXTERNAL

PactorDecoder::PactorDecoder(QObject *parent) :
    QObject(parent)
{
    m_demod_thread  = (PactorDemodThread *)parent;
}

//*******************************************************************************
// CRC verification function. Allows for 1 wrong bit in the CRC itself
//*******************************************************************************
int verify_CRC(unsigned int embedded_CRC, char *data_buffer, int length)
{
    unsigned int    computed_CRC;
    int             CRC_compare;
    int             CRC_error_count;
    int             k;

    // CRC on 191 bytes: 18 data bytes + 1 status byte
    computed_CRC    = CRC_check(0x00, length, data_buffer);
    // allow for 1 CRC erroneous bit
    CRC_compare         = embedded_CRC ^ computed_CRC;
    CRC_error_count     = 0;
    for (k = 0; k < 16; k++)
    {
        if (((CRC_compare >> k) & 0x01) != 0)
        {
            CRC_error_count++;
        }
    }
    return CRC_error_count;
}

//*******************************************************************************
// Build char buffer from bit buffer
//*******************************************************************************
#undef DEBUG_BUILDCHARS

void PactorDecoder::buildChars(const char *bit_buffer, int start_position, int phase, int count, unsigned char *char_buffer)
{
    int bit_index   = start_position;
    int byte_index  = 0;
    int bit_shift   = 0;
#ifdef DEBUG_BUILDCHARS
    QString debug_string;
#endif
    unsigned int   temp_char;

    temp_char       = 0;
    for (int i = 0; i < count; i++)
    {
        // add one bit
        if (phase == PT_PH_NORMAL)
        {
            temp_char = temp_char | ((bit_buffer[bit_index] & 1) << bit_shift);
        }
        else
        {
            temp_char = temp_char | ((1 - (bit_buffer[bit_index] & 1)) << bit_shift);
        }
        // increment bit shift
        bit_shift++;
        bit_index++;
        if (bit_shift >= 8)
        {
            char_buffer[byte_index] = (unsigned char)temp_char;
#ifdef DEBUG_BUILDCHARS
            debug_string.append(QString("%1 ").arg(QString(QByteArray(1, (char)temp_char).toHex())));
#endif
            bit_shift   = 0;
            byte_index++;
            temp_char   = 0;
        }
    }
#ifdef DEBUG_BUILDCHARS
    qDebug() << "chars" << debug_string;
#endif
}

#ifdef IAD_EXTERNAL
//*******************************************************************************
// Integrate and dump
//*******************************************************************************
void PactorDecoder::integrate_and_dump(const float decode_samples_buffer[], int start_position, int count, e_PT_speed speed, int phase, float average, int result_pos)
{
    int     j, k;
    float   iad_bit_value[LONG_BURST_BIT_COUNT_200];
    int     idx_char;
    int     idx_bit;
    int     bit_length;
#ifdef APM_ALGO
    float   map_HI_threshold;
    float   map_HI_threshold_05;
    float   map_HI_threshold_13;
    float   map_LO_threshold;
    float   map_LO_threshold_05;
    float   map_LO_threshold_13;
    float   slope;
    float   slope_past;
    float   slope_threshold;
    float   iad_previous;
#endif
    t_histo_values  histo_values;
    int     correlation_bit_length;
    int     bit_position;
    float   *matched_filter;

    if (speed == PT_SPEED_100)
    {
        correlation_bit_length  = CORREL_BIT_LENGTH_100;
        matched_filter          = matched_filter_100_K;
        bit_length  = CORREL_BIT_LENGTH_100;
    }
    else
    {
        correlation_bit_length  = CORREL_BIT_LENGTH_200;
        matched_filter          = matched_filter_200_K;
        bit_length  = CORREL_BIT_LENGTH_200;
    }
    // integrate samples
    for (j = 0 ; j < count; j++)
    {
        bit_position  = (int)(((float)(j + 1) * bit_length) + 0.5);
        // integrate one bit at a time
        iad_bit_value[j] = 0.0;
        for (k = 0 ; k < correlation_bit_length ; k++)
        {
            // positive values are positive (regular phase)
            if (phase == PT_PH_NORMAL)
            {
                iad_bit_value[j] += matched_filter[k] * decode_samples_buffer[start_position + bit_position + k];
            }
            else
                // positive values are negative (inverted phase)
            {
                iad_bit_value[j] -= matched_filter[k] * decode_samples_buffer[start_position + bit_position + k];
            }
        }
        iad_bit_value[j] = iad_bit_value[j] - average;
    }
    // compute min and max average from values histogram
    //    histo_values        = computeMinMaxHisto(iad_bit_value, 9 * BIT_PER_CHAR);
    histo_values        = computeMinMaxHisto(iad_bit_value, count);
    // set return value average value for frequency correction
    decode_output.average_level   = histo_values.average;
#ifdef APM_ALGO
    // compute a posteriori maximum algorithm (APM) thresholds
    map_HI_threshold    = histo_values.max_threshold;
    map_HI_threshold_05 = ((histo_values.max_threshold - histo_values.average) * 0.5) + histo_values.average;
    map_HI_threshold_13 = ((histo_values.max_threshold - histo_values.average) * 1.3) + histo_values.average;
    map_LO_threshold    = histo_values.min_threshold;
    map_LO_threshold_05 = ((histo_values.min_threshold - histo_values.average) * 0.5) + histo_values.average;
    map_LO_threshold_13 = ((histo_values.min_threshold - histo_values.average) * 1.3) + histo_values.average;
    slope_threshold     = (histo_values.max_threshold - histo_values.min_threshold) / 2.0;
#endif
    // initialize char value
    for (j = result_pos ; j < (result_pos + count); j++)
    {
        data_char[j]    = 0;
    }
    /****************************************/
    /*           dump bit value             */
    /*  and apply A Posteriori Maximum algo */
    /****************************************/
    idx_char = result_pos;
    idx_bit  = 0;
#ifdef APM_ALGO
    // consider there is no doubt for the first iad value
    if (iad_bit_value[0] > histo_values.average)
    {
        data_char[idx_char] |= 1 << idx_bit;
    }
    idx_bit++;
    // initialize
    iad_current     = iad_bit_value[0];
    iad_next        = iad_bit_value[1];
    // process all elements except first and last (special cases)
    for (i = 1 ; i < count; i++)
    {
        // hard decision
        iad_previous    = iad_current;
        iad_current     = iad_next;
        iad_next        = iad_bit_value[i + 2];
        // compute variation between bit values
        slope_past      = iad_current  - iad_previous;
        slope           = iad_next  - iad_current;

        // if there is no doubt set corresponding to 1
        if (iad_current > map_HI_threshold_13)
        {
            iad_bit_value[i] = iad_current;
        }
        else
            // if value is higher than average value and lower than 1.3 * threshold
            if (iad_current > histo_values.average)
            {
                // close to average and decreased strongly or will increase strongly: see it as a low
                if ((iad_current < map_HI_threshold_05)
                    && ((slope_past < -slope_threshold) || (slope > slope_threshold)))
                {
                    iad_bit_value[i] = -iad_current;
                }
                // between 0.5 and 1.3 * threshold and decreased and will increase
                else
                    if ((slope_past < 0) && (slope > 0))
                    {
                        iad_bit_value[i] = -iad_current;
                    }
                // otherwise consider as a 1
                    else
                    {
                        iad_bit_value[i] =  iad_current;
                    }
            }
            else
                // if value is lower than average value and higher than 1.3 * low threshold
                if ((iad_current < 0) && (iad_current > map_LO_threshold_13))
                {
                    // close to average and increased strongly or will decrease strongly: see it as a high
                    if ((iad_current > (-slope_threshold * 0.5)) && ((slope_past > slope_threshold) || (slope < -slope_threshold)))
                    {
                        iad_bit_value[i] = -iad_current;
                    }
                    // between 0.5 and 1.3 * threshold and increased and will decrease
                    else
                        if ((slope_past > 0) && (slope < 0))
                        {
                            iad_bit_value[i] = -iad_current;
                        }
                    // otherwise consider as a 0
                        else
                        {
                            iad_bit_value[i] =  iad_current;
                        }
                }
        // compute chunk index
        if ((i & (BIT_PER_CHAR - 1)) == (BIT_PER_CHAR - 1))
        {
            idx_char++;
            idx_bit  = 0;
        }
        else
        {
            idx_bit++;
        }
    }
    // consider there is no doubt for the last iad value
    if (iad_bit_value[count - 1] > histo_values.average)
    {
        data_char[idx_char] |= 1 << idx_bit;
    }
#else
    for (j = 0 ; j < count; j++)
    {
        // hard decision
        if (iad_bit_value[j] > histo_values.average)
        {
            data_char[idx_char] |= 1 << idx_bit;
        }
        // compute chunk index
        if ((j & (BIT_PER_CHAR - 1)) == (BIT_PER_CHAR - 1))
        {
            idx_char++;
            idx_bit  = 0;
        }
        else
        {
            idx_bit++;
        }
    }
#endif
}
#endif

//*******************************************************************************
// Packet decoder
// header type has been determined previously by zero crossings pattern
//*******************************************************************************
t_decode_output   *PactorDecoder::DecodePacket(const float decode_samples_buffer[],
        int header_type,
        int  expected_phase,
        float average,
        QString PTMyCall,
        int  pactor_mode,
        int  pactor_state,
        e_PT_speed   PT_speed)
{
    int     i, j;
    int     start_position;
    int     phase;
    bool    decode_done;
    int     i_PT_speed;
#ifndef IAD_EXTERNAL
    int     idx_bit;
    float   iad_bit_value[LONG_BURST_BIT_COUNT_200];
    float   iad_current;
    float   iad_next;
    t_histo_values  histo_values;
#endif

    unsigned int    embedded_CRC;
    int     CRC_error_count;

    t_ps   *huffman_packet;

    int             header_phase;

#ifdef DISPLAY_HEX_CONTENT
    int             display_hex;
    display_hex = -1;
#endif
    start_position  = 0;
#if 1
    PACTOR_TNC_DEBUG << "DecodePacket: start" << start_position
                     << "header_type" << header_type << "average" << average
                     << "mode" << pactor_mode << "state" << pactor_state
                     << "expect phase" << expected_phase;
#endif
    decode_output.type              = 0;
    decode_output.length            = 0;
    decode_output.char_buff         = data_char;
    decode_output.average_level     = 0.0;

    // do it once if expected_header is equal to PT_EXPECT_55 or PT_EXPECT_AA
    // do it twice with modified start position if expected_header
    // is equal to PT_EXPECT_ANY
    decode_done     = false;
    header_phase    = -1;
    expected_phase = USE_ANY_PHASE;// ************************************ TEST TOUTES PHASES
    while (decode_done == false)
    {
        header_phase++;
        // try both phase polarities in two consecutive positions
        if (expected_phase == USE_ANY_PHASE)
        {
            if ((header_phase & 0x001) == 0x001)
            {
                phase           = PT_PH_NORMAL;
            }
            else
            {
                phase           = PT_PH_INVERTED;
            }
        }
        else
            // expected phase is known
        {
            // skip alternate phase value if expected phase is known
            if ((header_phase & 0x001) == 0x001)
            {
                if (header_phase >= 3)
                {
                    decode_done    = true;
                }
                continue;
            }
            else
            {
                phase           = expected_phase;
            }
        }
        // compute start position
        start_position = header_phase >> 1;

        // finished?
#ifdef LISTEN_MEMORY_ARQ_TEST
        if (header_phase >= 3)
#else
        if (header_phase >= 3)
#endif
        {
            decode_done    = true;
        }
        //PACTOR_TNC_DEBUG << "decode. try start pos"<<start_position<<"phase"<<phase;
        // decode packets
        switch (header_type)
        {
            case
                    BURST_TYPE_HDR100:
                // build char buffer
                buildChars(m_demod_thread->demod_bit_buffer_100,  start_position, phase, LONG_BURST_BIT_COUNT_100 + 2, data_char);
#if 0
                PACTOR_TNC_DEBUG << "B1 received 100 phase " << phase << ", position:" << start_position
                                 << " data" << hex << data_char[0] << data_char[1] << data_char[2] << data_char[3]
                                 << data_char[4] << data_char[5] << data_char[6] << data_char[7]
                                 << data_char[8] << data_char[9] << data_char[10] << data_char[11];
#endif
                /*****************************************************/
                /*       100 Bd                                      */
                /*****************************************************/
#ifdef IAD_EXTERNAL
                integrate_and_dump(decode_samples_buffer, start_position, LONG_BURST_BIT_COUNT_100, PT_SPEED_100, phase, average, 0);
#endif
                decode_output.length    = 12;
                /*****************************************************/
                /*****************************************************/
                /*   is it a connection packet?                      */
                /*****************************************************/
                /*****************************************************/
                if ((pactor_mode == PT_SLAVE)
                    && (pactor_state == tnc_slave_call))
                {
                    // try to build char buffer for 200 Bd connection packet
                    buildChars(m_demod_thread->demod_bit_buffer_200,  start_position + (18 * BIT_PER_CHAR), phase, (6 * BIT_PER_CHAR), &data_char[12]);

                    /*****************************************************/
                    /*   connection packet 200 Bd part                   */
                    /*****************************************************/
#ifdef IAD_EXTERNAL
                    int cnx_pkt_start;
                    cnx_pkt_start   = start_position + (9 * BIT_PER_CHAR * CORREL_BIT_LENGTH_100);
                    integrate_and_dump(decode_samples_buffer, cnx_pkt_start, 6 * BIT_PER_CHAR, PT_SPEED_200, phase, average, 12);
#endif
#ifdef DISPLAY_HEX_CONTENT
                    display_hex = 0;
#endif
                    // compare packet content with PTMyCall
                    checkCallsign(m_demod_thread->callsigns, (const char *)&data_char[1], &decode_output);

                    if (decode_output.type != NO_CONNECTION)
                    {
                        // long path
                        if (decode_output.short_path == LONG_PATH_TIMING)
                        {
                            m_demod_thread->m_pactorTNC->short_path     = LONG_PATH_TIMING;
                            m_demod_thread->m_pactorTNC->m_pactoraudioengine->thread_info.short_path  = LONG_PATH_TIMING;
                            m_demod_thread->m_pactorTNC->m_pactoraudioengine->thread_info.burst_gap_msecs = TIMER_LONG_PATH_MS;
                            m_demod_thread->m_pactorTNC->m_pactoraudioengine->thread_info.cycle_time   = TIMER_LONG_PATH_MS;
                            m_demod_thread->sample_sync_count_initial   = 2240;
                        }
                        else
                        {
                            m_demod_thread->m_pactorTNC->short_path     = SHORT_PATH_TIMING;
                            m_demod_thread->m_pactorTNC->m_pactoraudioengine->thread_info.short_path  = SHORT_PATH_TIMING;
                            m_demod_thread->m_pactorTNC->m_pactoraudioengine->thread_info.burst_gap_msecs = TIMER_SHORT_PATH_MS;
                            m_demod_thread->m_pactorTNC->m_pactoraudioengine->thread_info.cycle_time   = TIMER_SHORT_PATH_MS;
                            m_demod_thread->sample_sync_count_initial   = 2000;
                        }

                        decode_output.packet_counter    = 0;
                        decode_output.break_request     = 0;
                        decode_output.phase             = phase;
                        decode_output.header            = data_char[0];
                        decode_output.position          = start_position;
                        // finished
                        decode_done    = true;
                    }
                }
                else
                    if (pactor_mode == PT_Listen)
                    {
                        /*****************************************************/
                        /*   check CRC                                       */
                        /*****************************************************/
                        embedded_CRC    = (data_char[CRC_BYTES_POS_100 + 1] << 8) | data_char[CRC_BYTES_POS_100];
                        // call CRC verification function
                        CRC_error_count = verify_CRC(embedded_CRC, (char *)&data_char[1], 9);
                        //                if (embedded_CRC == computed_CRC)
                        if (CRC_error_count <= 2)
                        {
                            /*****************************************************/
                            /*   check status                                    */
                            /*****************************************************/
                            // ASCII encoding?
                            if ((data_char[STATUS_BYTE_POS_100] & 0X0c) == 0)
                            {
                                decode_output.type  = PACKET_ASCII_100;
                            }
                            else
                                // Huffman encoding?
                                if ((data_char[STATUS_BYTE_POS_100] & 0X0c) == 0x04)
                                {
                                    decode_output.type  = PACKET_HUFFMAN_100;
                                    /***************************************************/
                                    /*       extract Huffman encoded data              */
                                    /***************************************************/
                                    // skip header (0x55)
                                    huffman_packet = huffman_decode_packet(&data_char[1], decode_output.length - 1);
                                    decode_output.huffman_packet  = *huffman_packet;
                                    // text length
                                    decode_output.length            = huffman_packet->pkt_data_len;
                                }
                            // break request packet
                            decode_output.break_request     = 0;
                            if ((data_char[STATUS_BYTE_POS_100] & 0X040) == 0x40)
                            {
                                decode_output.break_request     = 1;
                                if (decode_output.type == PACKET_ASCII_100)
                                {
                                    decode_output.type  = PACKET_BREAK_ASCII_100;
                                }
                                else
                                {
                                    decode_output.type  = PACKET_BREAK_HUFFMAN_100;
                                }
                            }
                            // QRT packet?
                            if ((data_char[STATUS_BYTE_POS_100] & 0X80) == 0x80)
                            {
                                decode_output.type  = PACKET_QRT_100;
                            }
                            // packet counter
                            decode_output.packet_counter    = data_char[STATUS_BYTE_POS_100] & 0x03;
                            decode_output.phase             = phase;
                            decode_output.header            = data_char[0];
                            decode_output.position          = start_position;
                            // finished
                            decode_done    = true;
#ifdef DISPLAY_HEX_CONTENT
                            display_hex = 1;
#endif
                            break;
                        }
                        /*****************************************************/
                        /* try to identify connect requests while in listen  */
                        /* mode.                                             */
                        /*   connection packet 200 Bd part                   */
                        /*****************************************************/
#ifdef IAD_EXTERNAL
                        int cnx_pkt_start;
                        cnx_pkt_start   = start_position + (9 * BIT_PER_CHAR * CORREL_BIT_LENGTH_100);
                        integrate_and_dump(decode_samples_buffer, cnx_pkt_start, 6 * BIT_PER_CHAR, PT_SPEED_200, phase, average, 12);
#endif
                        // try to build char buffer for 200 Bd connection packet
                        buildChars(m_demod_thread->demod_bit_buffer_200,  start_position + (18 * BIT_PER_CHAR), phase, (6 * BIT_PER_CHAR), &data_char[12]);
#ifdef DISPLAY_HEX_CONTENT
                        display_hex = 0;
#endif
                        // long path callsign has all bits inverted (1->0, 0->1)
                        // data_char contains called station callsign @ 100 Bd in position 1
                        // and called station callsign @ 200 Bd in position 12
                        int     similar_char_count_100  = 0;
                        int     high_value_char_count   = 0;
                        for (i = 0; i < 6; i++)
                        {
                            // long path call
                            if ((i == 0)
                                && ((255 - data_char[1]) == data_char[12]))
                            {
                            }
                            else
                                // other cases: not first char or first char is the same
                            {
                                if (data_char[i + 1] != data_char[i + 12])
                                {
                                    break;
                                }
                            }
                            // count big values because we may be in reverse phase condition
                            // and have no other means to verify it because there is no
                            // checksum in connection packets
                            if (data_char[i + 1] > 127)
                            {
                                high_value_char_count++;
                            }
                            similar_char_count_100++;
                        }
                        // short path if first char is equal
                        if ((similar_char_count_100 >= 4)
                            && (data_char[1] == data_char[12])
                            && (high_value_char_count < 2))
                        {
                            PACTOR_TNC_DEBUG << "short path decoder l=492 count" << similar_char_count_100;
                            similar_char_count_100++;
                            decode_output.short_path                    = SHORT_PATH_TIMING;
                            m_demod_thread->m_pactorTNC->short_path   = SHORT_PATH_TIMING;
                            m_demod_thread->m_pactorTNC->m_pactoraudioengine->thread_info.short_path  = SHORT_PATH_TIMING;
                            m_demod_thread->m_pactorTNC->m_pactoraudioengine->thread_info.burst_gap_msecs = TIMER_SHORT_PATH_MS;
                            decode_output.type  = PACKET_CONNECT_100;
                            decode_output.packet_counter    = 0;
                            decode_output.break_request     = 0;
                            decode_output.phase             = phase;
                            decode_output.header            = data_char[0];
                            decode_output.position          = start_position;
                            // similar bit count
                            decode_output.same_byte_count   = similar_char_count_100;
                            // finished
                            decode_done    = true;
                        }
                        // long path if first char is inverted
                        if ((similar_char_count_100 >= 3)
                            && ((255 - data_char[1]) == data_char[12])
                            && (high_value_char_count < 2))
                        {
                            PACTOR_TNC_DEBUG << "long path decoder l=515 count" << similar_char_count_100;
                            similar_char_count_100++;
                            decode_output.short_path                    = LONG_PATH_TIMING;
                            m_demod_thread->m_pactorTNC->short_path   = LONG_PATH_TIMING;
                            m_demod_thread->m_pactorTNC->m_pactoraudioengine->thread_info.short_path  = LONG_PATH_TIMING;
                            m_demod_thread->m_pactorTNC->m_pactoraudioengine->thread_info.burst_gap_msecs = TIMER_LONG_PATH_MS;
                            decode_output.type  = PACKET_CONNECT_100;
                            decode_output.packet_counter    = 0;
                            decode_output.break_request     = 0;
                            decode_output.phase             = phase;
                            decode_output.header            = data_char[0];
                            decode_output.position          = start_position;
                            // similar bit count
                            decode_output.same_byte_count   = similar_char_count_100;
                            // invert first byte of callsign
                            data_char[1]                    = data_char[12];
                            // finished
                            decode_done    = true;
                        }
                        switch (decode_output.short_path)
                        {
                            case
                                    SHORT_PATH_TIMING: // short PATH
                                m_demod_thread->m_pactorTNC->m_pactoraudioengine->thread_info.cycle_time   = TIMER_SHORT_PATH_MS;
                                m_demod_thread->sample_sync_count_initial   = 2000;
                                break;
                            case
                                    LONG_PATH_TIMING: // long PATH
                                m_demod_thread->m_pactorTNC->m_pactoraudioengine->thread_info.cycle_time   = TIMER_LONG_PATH_MS;
                                m_demod_thread->sample_sync_count_initial   = 2240;
                                break;
                            case
                                    UNPROTO_TIMING: // listen mode: delta T = 1000ms
                                m_demod_thread->m_pactorTNC->m_pactoraudioengine->thread_info.cycle_time   = TIMER_UNPROTO_MS;
                                m_demod_thread->sample_sync_count_initial   = 1600;
                                break;
                        }
                    }
                // otherwise, check status byte and CRC
                if (decode_output.type == 0)
                {
#ifdef DISPLAY_HEX_CONTENT
                    display_hex = 1;
#endif
#if 0
                    PACTOR_TNC_DEBUG << "B2 received 100 phase " << phase << ", position:" << start_position
                                     << " data" << hex << data_char[0] << data_char[1] << data_char[2] << data_char[3]
                                     << data_char[4] << data_char[5] << data_char[6] << data_char[7]
                                     << data_char[8] << data_char[9] << data_char[10] << data_char[11];
#endif
                    /*****************************************************/
                    /*   check CRC                                       */
                    /*****************************************************/
                    embedded_CRC    = (data_char[CRC_BYTES_POS_100 + 1] << 8) | data_char[CRC_BYTES_POS_100];
                    // call CRC verification function
                    CRC_error_count = verify_CRC(embedded_CRC, (char *)&data_char[1], 9);
                    //                if (embedded_CRC == computed_CRC)
                    if (CRC_error_count <= 2)
                    {
                        /*****************************************************/
                        /*   check status                                    */
                        /*****************************************************/
                        // ASCII encoding?
                        if ((data_char[STATUS_BYTE_POS_100] & 0X0c) == 0)
                        {
                            decode_output.type  = PACKET_ASCII_100;
                        }
                        else
                            // Huffman encoding?
                            if ((data_char[STATUS_BYTE_POS_100] & 0X0c) == 0x04)
                            {
                                decode_output.type  = PACKET_HUFFMAN_100;
                                /***************************************************/
                                /*       extract Huffman encoded data              */
                                /***************************************************/
                                // skip header (0x55)
                                huffman_packet = huffman_decode_packet(&data_char[1], decode_output.length - 1);
                                decode_output.huffman_packet  = *huffman_packet;
                                // text length
                                decode_output.length            = huffman_packet->pkt_data_len;
                            }
                        // break request packet
                        decode_output.break_request     = 0;
                        if ((data_char[STATUS_BYTE_POS_100] & 0X040) == 0x40)
                        {
                            decode_output.break_request     = 1;
                            if (decode_output.type == PACKET_ASCII_100)
                            {
                                decode_output.type  = PACKET_BREAK_ASCII_100;
                            }
                            else
                            {
                                decode_output.type  = PACKET_BREAK_HUFFMAN_100;
                            }
                        }
                        // QRT packet?
                        if ((data_char[STATUS_BYTE_POS_100] & 0X80) == 0x80)
                        {
                            decode_output.type  = PACKET_QRT_100;
                        }
                        // packet counter
                        decode_output.packet_counter    = data_char[STATUS_BYTE_POS_100] & 0x03;
                        decode_output.phase             = phase;
                        decode_output.header            = data_char[0];
                        decode_output.position          = start_position;
                        // finished
                        decode_done    = true;
                    }
#if 0
                    else
                    {
                        decode_output.type  = WRONG_CRC_100;
                        decode_output.break_request     = 0;
                    }
#endif
                }
                break;

            case
                    BURST_TYPE_HDR200:
                // build char buffer
                buildChars(m_demod_thread->demod_bit_buffer_200,  start_position, phase, LONG_BURST_BIT_COUNT_200 + 2, data_char);

                // integrate bit sample values
#ifdef IAD_EXTERNAL
                integrate_and_dump(decode_samples_buffer, start_position, LONG_BURST_BIT_COUNT_200, PT_SPEED_200, phase, average, 0);
#endif
                // display for debug
#ifdef DISPLAY_HEX_CONTENT
                display_hex = 2;
#endif
                decode_output.length    = 24;
                /*****************************************************/
                /*   check CRC                                       */
                /*****************************************************/
                embedded_CRC    = (data_char[CRC_BYTES_POS_200 + 1] << 8) | data_char[CRC_BYTES_POS_200];
                // CRC on 21 bytes: 20 data bytes + 1 status byte
                // call CRC verification function
                CRC_error_count = verify_CRC(embedded_CRC, (char *)&data_char[1], 21);
                //                if (embedded_CRC == computed_CRC)
                if (CRC_error_count <= 2)
                {
                    /*   check status                                    */
                    /*****************************************************/
                    // ASCII encoding?
                    if ((data_char[STATUS_BYTE_POS_200] & 0X0c) == 0)
                    {
                        decode_output.type  = PACKET_ASCII_200;
                    }
                    else
                        // Huffman encoding?
                        if ((data_char[STATUS_BYTE_POS_200] & 0X0c) == 0x04)
                        {
                            decode_output.type  = PACKET_HUFFMAN_200;
                            /***************************************************/
                            /*       extract Huffman encoded data              */
                            /***************************************************/
                            // skip header (0x55)
                            huffman_packet = huffman_decode_packet(&data_char[1], decode_output.length - 1);
                            decode_output.huffman_packet  = *huffman_packet;
                            // text length
                            decode_output.length            = huffman_packet->pkt_data_len;
                        }
                    // store header there because it will be modified later
                    decode_output.header            = data_char[0];
                    // break request packet
                    decode_output.break_request     = 0;
                    if ((data_char[STATUS_BYTE_POS_200] & 0X040) == 0x40)
                    {
                        decode_output.break_request     = 1;
                        if (decode_output.type == PACKET_ASCII_200)
                        {
                            decode_output.type  = PACKET_BREAK_ASCII_200;
                        }
                        else
                        {
                            decode_output.type  = PACKET_BREAK_HUFFMAN_200;
                        }
                    }
                    // QRT packet?
                    if ((data_char[STATUS_BYTE_POS_200] & 0X80) == 0x80)
                    {
                        decode_output.type  = PACKET_QRT_200;

                        /**********************************************************/
                        /*   special case: QRT 200 is in fact encoded at 100 Bd ! */
                        /**********************************************************/
#ifdef IAD_EXTERNAL
                        integrate_and_dump(decode_samples_buffer, start_position, LONG_BURST_BIT_COUNT_100, PT_SPEED_100, phase, average, 0);
#endif
                        decode_output.length    = 12;
                        // recover 100 Bd message content
                        buildChars(m_demod_thread->demod_bit_buffer_100,  start_position, phase, LONG_BURST_BIT_COUNT_100 + 2, data_char);
                    }
                    // packet counter
                    decode_output.packet_counter    = data_char[STATUS_BYTE_POS_200] & 0X03;
                    // break packet
                    decode_output.break_request     = data_char[STATUS_BYTE_POS_200] & 0X040 ? 1 : 0;
                    decode_output.phase             = phase;
                    decode_output.position          = start_position;
                    // finished
                    decode_done    = true;
                }
                else
                {
                    decode_output.type  = WRONG_CRC_200;
                    decode_output.break_request     = 0;
                }
                break;

                /*****************************************************/
                /*   CS3 can be followed by 100 Bd or 200 Bd data    */
                /*****************************************************/
            case
                    BURST_TYPE_CS3:
                // current speed is 100 Bd
                //            PACTOR_TNC_DEBUG <<"decoder l 738 BURST_TYPE_CS3, speed"<<PT_speed<<"mode"<<pactor_mode
                //                               <<"phase"<<phase;
                if (pactor_mode != PT_Listen)
                {
                    // ARQ modes, speed is already known
                    i_PT_speed = PT_speed;
                }
                else
                {
                    // listen mode, try 100 Bd
                    i_PT_speed = PT_SPEED_100;
                }

                if (i_PT_speed == PT_SPEED_100)
                {
                    // build char buffer
                    buildChars(m_demod_thread->demod_bit_buffer_100,  start_position + 6, phase, LONG_BURST_BIT_COUNT_100 + 2, data_char + 1);

                    // compute start position
                    start_position = (header_phase >> 1) * burst_shift[BURST_TYPE_HDR100];

                    //                PACTOR_TNC_DEBUG <<"decoder l 717,decode CS3 100Bd. start="<<start_position;
                    /*****************************************************/
                    /*      CS3 100 Bd                                   */
                    /*****************************************************/
#ifdef IAD_EXTERNAL
                    integrate_and_dump(decode_samples_buffer, start_position, LONG_BURST_BIT_COUNT_100, PT_SPEED_100, phase, average, 0);
#endif
                    decode_output.length    = 12;
                    // display for debug
#ifdef DISPLAY_HEX_CONTENT
                    display_hex = 3;
#endif
#if 0
                    PACTOR_TNC_DEBUG << "D received CS3 100 (" << phase << "):" << hex << data_char[2] << data_char[3]
                                     << data_char[4] << data_char[5] << data_char[6] << data_char[7]
                                     << data_char[8] << data_char[9] << data_char[10] << data_char[11];
#endif
                    /*****************************************************/
                    /*   check CRC                                       */
                    /*****************************************************/
                    embedded_CRC    = (data_char[CRC_BYTES_POS_100 + 1] << 8) | data_char[CRC_BYTES_POS_100];
                    // call CRC verification function
                    CRC_error_count = verify_CRC(embedded_CRC, (char *)&data_char[2], 8);
                    //                if (embedded_CRC == computed_CRC)
                    if (CRC_error_count <= 2)
                    {
                        /*****************************************************/
                        /*   check status                                    */
                        /*****************************************************/
                        // ASCII encoding?
                        if ((data_char[STATUS_BYTE_POS_100] & 0X0c) == 0)
                        {
                            decode_output.type  = PACKET_CS3_ASCII_100;
                        }
                        else
                            // Huffman encoding?
                            if ((data_char[STATUS_BYTE_POS_100] & 0X0c) == 0x04)
                            {
                                decode_output.type  = PACKET_CS3_HUFFMAN_100;
                                /***************************************************/
                                /*       extract Huffman encoded data              */
                                /***************************************************/
                                // skip header (0x55)
                                huffman_packet = huffman_decode_packet(&data_char[2], decode_output.length - 2);
                                decode_output.huffman_packet  = *huffman_packet;
                                // text length
                                decode_output.length            = huffman_packet->pkt_data_len;
                            }
                        // QRT packet?
                        if ((data_char[STATUS_BYTE_POS_100] & 0X80) == 0x80)
                        {
                            decode_output.type  = PACKET_QRT_100;
                        }
                        // packet counter
                        decode_output.packet_counter    = data_char[STATUS_BYTE_POS_100] & 0X03;
                        // break packet
                        decode_output.break_request     = data_char[STATUS_BYTE_POS_100] & 0X040 ? 1 : 0;
                        decode_output.phase             = phase;
                        decode_output.header            = data_char[0];
                        decode_output.position          = start_position;
                        // finished
                        decode_done    = true;
                    }
                    else
                    {
                        decode_output.type  = WRONG_CRC_100;
                        decode_output.break_request     = 0;
                        /*                    if (pactor_mode != PT_Listen)
                                            {
                                                // finished, except if in listen mode (we shall try 200 Bd)
                                                decode_done    = true;
                                            }*/
                    }
                }
                // listen mode, if no good CRC at 100 Bd, try 200 Bd
                if ((pactor_mode == PT_Listen) && (decode_done == false))
                {
                    i_PT_speed = PT_SPEED_200;
                }
                // current speed is 200 Bd
                if (i_PT_speed == PT_SPEED_200)
                {
                    // build char buffer
                    buildChars(m_demod_thread->demod_bit_buffer_200,  start_position + 4, phase, LONG_BURST_BIT_COUNT_200 + 2, data_char + 1);

                    // compute start position
                    start_position = (header_phase >> 1) * burst_shift[BURST_TYPE_HDR200];

                    //                PACTOR_TNC_DEBUG <<"decoder l 793,decode CS3 200Bd. start="<<start_position;
                    /*****************************************************/
                    /*      CS3 200 Bd                                   */
                    /*****************************************************/
#ifdef IAD_EXTERNAL
                    integrate_and_dump(decode_samples_buffer, start_position, LONG_BURST_BIT_COUNT_200, PT_SPEED_200, phase, average, 0);
#endif
                    // display for debug
#ifdef DISPLAY_HEX_CONTENT
                    display_hex = 4;
#endif
                    decode_output.length    = 24;
                    /*****************************************************/
                    /*   check CRC                                       */
                    /*****************************************************/
                    embedded_CRC    = (data_char[CRC_BYTES_POS_200 + 1] << 8) | data_char[CRC_BYTES_POS_200];
                    //                PACTOR_TNC_DEBUG << "embedded_CRC"<<hex<<embedded_CRC;
                    // call CRC verification function
                    CRC_error_count = verify_CRC(embedded_CRC, (char *)&data_char[3], 19);
                    //                if (embedded_CRC == computed_CRC)
                    if (CRC_error_count <= 2)
                    {
                        /*****************************************************/
                        /*   check status                                    */
                        /*****************************************************/
                        // ASCII encoding?
                        if ((data_char[STATUS_BYTE_POS_200] & 0X0c) == 0)
                        {
                            decode_output.type  = PACKET_CS3_ASCII_200;
                        }
                        else
                            // Huffman encoding?
                            if ((data_char[STATUS_BYTE_POS_200] & 0X0c) == 0x04)
                            {
                                decode_output.type  = PACKET_CS3_HUFFMAN_200;
                                /***************************************************/
                                /*       extract Huffman encoded data              */
                                /***************************************************/
                                // skip header (0x55)
                                huffman_packet = huffman_decode_packet(&data_char[3], decode_output.length - 3);
                                decode_output.huffman_packet  = *huffman_packet;
                                // text length
                                decode_output.length            = huffman_packet->pkt_data_len;
                            }
                        // QRT packet?
                        if ((data_char[STATUS_BYTE_POS_200] & 0X80) == 0x80)
                        {
                            decode_output.type  = PACKET_QRT_200;
                        }
                        // packet counter
                        decode_output.packet_counter    = data_char[STATUS_BYTE_POS_200] & 0X03;
                        // break packet
                        decode_output.break_request     = data_char[STATUS_BYTE_POS_200] & 0X040 ? 1 : 0;
                        decode_output.phase             = phase;
                        decode_output.header            = data_char[0];
                        decode_output.position          = start_position;
                        // finished
                        decode_done    = true;
                    }
                    else
                    {
                        decode_output.type  = WRONG_CRC_200;
                        decode_output.break_request     = 0;
                    }
                    // finished anyway NON, il faut essayer les autres phases et positions
                    //                decode_done    = true;
                }
                break;

            default
                    :
                break;
        }
    };

#ifdef LISTEN_MEMORY_ARQ_TEST
    display_hex = 1;
#endif
#ifdef DISPLAY_HEX_CONTENT
    switch (display_hex)
    {
        case
                0:
            PACTOR_TNC_DEBUG << "A received 100 phase " << phase << ", position:" << start_position
                             << " data" << hex << data_char[0] << data_char[1] << data_char[2] << data_char[3]
                             << data_char[4] << data_char[5] << data_char[6] << data_char[7]
                             << data_char[8] << data_char[9] << data_char[10] << data_char[11] << "200Bd"
                             << data_char[12] << data_char[13] << data_char[14] << data_char[15]
                             << data_char[16] << data_char[17];
            break;

        case
                1:
            PACTOR_TNC_DEBUG << "B received 100 phase " << phase << ", position:" << start_position
                             << " data" << hex << data_char[0] << data_char[1] << data_char[2] << data_char[3]
                             << data_char[4] << data_char[5] << data_char[6] << data_char[7]
                             << data_char[8] << data_char[9] << data_char[10] << data_char[11];
            break;

        case
                2:
            PACTOR_TNC_DEBUG << "C received 200 phase " << phase << ", position:" << start_position
                             << hex << data_char[0] << data_char[1] << data_char[2] << data_char[3]
                             << data_char[4] << data_char[5] << data_char[6] << data_char[7]
                             << data_char[8] << data_char[9] << data_char[10] << data_char[11]
                             << data_char[12] << data_char[13] << data_char[14] << data_char[15]
                             << data_char[16] << data_char[17] << data_char[18] << data_char[19]
                             << data_char[20] << data_char[21] << data_char[22] << data_char[23] ;
            break;

        case
                3:
            PACTOR_TNC_DEBUG << "D received CS3 100 (" << phase << "):" << hex << data_char[2] << data_char[3]
                             << data_char[4] << data_char[5] << data_char[6] << data_char[7]
                             << data_char[8] << data_char[9] << data_char[10] << data_char[11];
            break;

        case
                4:
            PACTOR_TNC_DEBUG << "E received CS3 200 (" << phase << "):" << hex << data_char[3]
                             << data_char[4] << data_char[5] << data_char[6] << data_char[7]
                             << data_char[8] << data_char[9] << data_char[10] << data_char[11]
                             << data_char[12] << data_char[13] << data_char[14] << data_char[15]
                             << data_char[16] << data_char[17] << data_char[18] << data_char[19]
                             << data_char[20] << data_char[21] << data_char[22] << data_char[23] ;
            break;

        default
                :
            break;
    }
#endif

    // wrong CRC
    // increment error count
    if ((decode_output.type == WRONG_CRC_100) || (decode_output.type == WRONG_CRC_200))
    {
        // if too much CRC errors, try to reset the window
        m_demod_thread->m_pactorTNC->m_pactorTNCThread->PT_wrong_CRC_count++;
        if (m_demod_thread->m_pactorTNC->m_pactorTNCThread->PT_wrong_CRC_count > m_demod_thread->m_pactorTNC->m_pactorTNCThread->UCMD[1])
        {
            m_demod_thread->m_pactorTNC->m_pactoraudioengine->thread_info.window_state      = e_PT_rcv_win_not_init;
            m_demod_thread->m_pactorTNC->m_pactoraudioengine->thread_info.expected_phase    = USE_ANY_PHASE;
            m_demod_thread->m_pactorTNC->m_pactorTNCThread->PT_wrong_CRC_count              = 0;
            PACTOR_TNC_DEBUG << "too much CRC errors: window init=false, phase=ANY";
        }
    }

    return &decode_output;
}

//*******************************************************************************
// Packet identifier
// header type has been determined previously by zero crossings pattern
//*******************************************************************************
t_decode_output   *PactorDecoder::identifyPacket(const float decode_samples_buffer[],
        int header_type,
        int  expected_phase,
        float average,
        QString PTMyCall,
        int  pactor_mode,
        int  pactor_state,
        e_PT_speed   PT_speed)
{
    int     i, j;
    int     start_position;
    int     phase;
    bool    decode_done;

    int             header_phase;

#ifdef DISPLAY_HEX_CONTENT
    int             display_hex;
    display_hex = -1;
#endif
    start_position  = 0;
#if 1
    PACTOR_TNC_DEBUG << "identifyPacket: start" << start_position
                     << "header_type" << header_type << "average" << average
                     << "mode" << pactor_mode << "state" << pactor_state
                     << "expect phase" << expected_phase;
#endif
    decode_output.type              = 0;
    decode_output.length            = 0;
    decode_output.char_buff         = data_char;
    decode_output.average_level     = 0.0;

    // do it once if expected_header is equal to PT_EXPECT_55 or PT_EXPECT_AA
    // do it twice with modified start position if expected_header
    // is equal to PT_EXPECT_ANY
    decode_done     = false;
    header_phase    = -1;
    expected_phase = USE_ANY_PHASE;// ************************************ TEST TOUTES PHASES
    while (decode_done == false)
    {
        header_phase++;
        // try both phase polarities in two consecutive positions
        if (expected_phase == USE_ANY_PHASE)
        {
            if ((header_phase & 0x001) == 0x001)
            {
                phase           = PT_PH_NORMAL;
            }
            else
            {
                phase           = PT_PH_INVERTED;
            }
        }
        else
            // expected phase is known
        {
            // skip alternate phase value if expected phase is known
            if ((header_phase & 0x001) == 0x001)
            {
                if (header_phase >= 3)
                {
                    decode_done    = true;
                }
                continue;
            }
            else
            {
                phase           = expected_phase;
            }
        }
        // compute start position
        start_position = header_phase >> 1;

        // finished?
#ifdef LISTEN_MEMORY_ARQ_TEST
        if (header_phase >= 3)
#else
        if (header_phase >= 3)
#endif
        {
            decode_done    = true;
        }
        //PACTOR_TNC_DEBUG << "decode. try start pos"<<start_position<<"phase"<<phase;
        // decode packets
        switch (header_type)
        {
            case
                    BURST_TYPE_HDR100:
                // build char buffer
                buildChars(m_demod_thread->demod_bit_buffer_100,  start_position, phase, CORREL_BITS_HDR + 2, data_char);
#if 0
                PACTOR_TNC_DEBUG << "B1 received 100 phase " << phase << ", position:" << start_position
                                 << " data" << hex << data_char[0] << data_char[1] << data_char[2] << data_char[3]
                                 << data_char[4] << data_char[5] << data_char[6] << data_char[7]
                                 << data_char[8] << data_char[9] << data_char[10] << data_char[11];
#endif
                /*****************************************************/
                /*       100 Bd                                      */
                /*****************************************************/
#ifdef IAD_EXTERNAL
                integrate_and_dump(decode_samples_buffer, start_position, CORREL_BITS_HDR, PT_SPEED_100, phase, average, 0);
#endif
                decode_output.length    = 1;
                // otherwise, check status byte and CRC
                if (decode_output.type == 0)
                {
#ifdef DISPLAY_HEX_CONTENT
                    display_hex = 1;
#endif
#if 0
                    PACTOR_TNC_DEBUG << "B2 received 100 phase " << phase << ", position:" << start_position
                                     << " data" << hex << data_char[0] << data_char[1] << data_char[2] << data_char[3]
                                     << data_char[4] << data_char[5] << data_char[6] << data_char[7]
                                     << data_char[8] << data_char[9] << data_char[10] << data_char[11];
#endif
                    /*****************************************************/
                    /*   check header                                    */
                    /*****************************************************/
                    if ((data_char[0] == 0x55) || (data_char[0] == 0xaa))
                    {
                        decode_output.type  = PACKET_ASCII_100;
                        decode_done    = true;
                    }
                }
                break;

            case
                    BURST_TYPE_HDR200:
                // build char buffer
                buildChars(m_demod_thread->demod_bit_buffer_200,  start_position, phase, CORREL_BITS_HDR + 2, data_char);

                // integrate bit sample values
#ifdef IAD_EXTERNAL
                integrate_and_dump(decode_samples_buffer, start_position, CORREL_BITS_HDR, PT_SPEED_200, phase, average, 0);
#endif
                // display for debug
#ifdef DISPLAY_HEX_CONTENT
                display_hex = 2;
#endif
                decode_output.length    = 1;
                /*****************************************************/
                /*   check header                                    */
                /*****************************************************/
                if ((data_char[0] == 0x55) || (data_char[0] == 0xaa))
                {
                    decode_output.type  = PACKET_ASCII_200;
                    decode_done    = true;
                }
                break;

                /*****************************************************/
                /*   CS3 can be followed by 100 Bd or 200 Bd data    */
                /*****************************************************/
            case
                    BURST_TYPE_CS3:
                // build char buffer
                buildChars(m_demod_thread->demod_bit_buffer_100,  start_position + 6, phase, CORREL_BITS_CS + 2, data_char + 1);

                // compute start position
                start_position = (header_phase >> 1) * burst_shift[BURST_TYPE_HDR100];

                //                PACTOR_TNC_DEBUG <<"decoder l 717,decode CS3 100Bd. start="<<start_position;
                /*****************************************************/
                /*      CS3 100 Bd                                   */
                /*****************************************************/
#ifdef IAD_EXTERNAL
                integrate_and_dump(decode_samples_buffer, start_position, CORREL_BITS_CS, PT_SPEED_100, phase, average, 0);
#endif
                decode_output.length    = 12;
                // display for debug
#ifdef DISPLAY_HEX_CONTENT
                display_hex = 3;
#endif
#if 0
                PACTOR_TNC_DEBUG << "D received CS3 100 (" << phase << "):" << hex << data_char[2] << data_char[3]
                                 << data_char[4] << data_char[5] << data_char[6] << data_char[7]
                                 << data_char[8] << data_char[9] << data_char[10] << data_char[11];
#endif
                if ((data_char[0] == 0x4b) && (data_char[1] & 0x0f == 0x03))
                {
                    decode_output.type  = PACKET_CS3_ASCII_100;
                    decode_done    = true;
                }
                break;

            default
                    :
                break;
        }
    };

#ifdef LISTEN_MEMORY_ARQ_TEST
    display_hex = 1;
#endif
#ifdef DISPLAY_HEX_CONTENT
    switch (display_hex)
    {
        case
                0:
            PACTOR_TNC_DEBUG << "A received 100 phase " << phase << ", position:" << start_position
                             << " data" << hex << data_char[0] << data_char[1] << data_char[2] << data_char[3]
                             << data_char[4] << data_char[5] << data_char[6] << data_char[7]
                             << data_char[8] << data_char[9] << data_char[10] << data_char[11] << "200Bd"
                             << data_char[12] << data_char[13] << data_char[14] << data_char[15]
                             << data_char[16] << data_char[17];
            break;

        case
                1:
            PACTOR_TNC_DEBUG << "B received 100 phase " << phase << ", position:" << start_position
                             << " data" << hex << data_char[0] << data_char[1] << data_char[2] << data_char[3]
                             << data_char[4] << data_char[5] << data_char[6] << data_char[7]
                             << data_char[8] << data_char[9] << data_char[10] << data_char[11];
            break;

        case
                2:
            PACTOR_TNC_DEBUG << "C received 200 phase " << phase << ", position:" << start_position
                             << hex << data_char[0] << data_char[1] << data_char[2] << data_char[3]
                             << data_char[4] << data_char[5] << data_char[6] << data_char[7]
                             << data_char[8] << data_char[9] << data_char[10] << data_char[11]
                             << data_char[12] << data_char[13] << data_char[14] << data_char[15]
                             << data_char[16] << data_char[17] << data_char[18] << data_char[19]
                             << data_char[20] << data_char[21] << data_char[22] << data_char[23] ;
            break;

        case
                3:
            PACTOR_TNC_DEBUG << "D received CS3 100 (" << phase << "):" << hex << data_char[2] << data_char[3]
                             << data_char[4] << data_char[5] << data_char[6] << data_char[7]
                             << data_char[8] << data_char[9] << data_char[10] << data_char[11];
            break;

        case
                4:
            PACTOR_TNC_DEBUG << "E received CS3 200 (" << phase << "):" << hex << data_char[3]
                             << data_char[4] << data_char[5] << data_char[6] << data_char[7]
                             << data_char[8] << data_char[9] << data_char[10] << data_char[11]
                             << data_char[12] << data_char[13] << data_char[14] << data_char[15]
                             << data_char[16] << data_char[17] << data_char[18] << data_char[19]
                             << data_char[20] << data_char[21] << data_char[22] << data_char[23] ;
            break;

        default
                :
            break;
    }
#endif
    return &decode_output;
}

// this function computes average, minimum and maximum values of integrate and dump buffer
// these values are used by dump and map computations
#define     MAX_HISTO_VALUE     2.0
#define     HISTO_COUNT         (20+1)

#ifdef IAD_EXTERNAL
t_histo_values   PactorDecoder::computeMinMaxHisto(float iad_bit_value[], int byte_count)
{
    t_histo_values  l_histo_values;
    static int       histo[HISTO_COUNT];
    int       histo_index = 0;
    int       histo_count = 0;

    // initialize histo table
    for (int i = 0 ; i < HISTO_COUNT ; i++)
    {
        histo[i]    = 0;
    }
    // scan iad table
    for (int i = 0 ; i < byte_count ; i++)
    {
        // values greater than maximum
        if (iad_bit_value[i] >  MAX_HISTO_VALUE)
        {
            histo_index     = HISTO_COUNT - 1;
        }
        else
            // values smaller than maximum
            if (iad_bit_value[i] < (0.0 - MAX_HISTO_VALUE))
            {
                histo_index     = 0;
            }
            else
                // other values
            {
                histo_index = (int)((iad_bit_value[i] + MAX_HISTO_VALUE) * (float)((HISTO_COUNT - 1) / (2 * (int)MAX_HISTO_VALUE)));
            }
        // update histogram
        histo[histo_index]++;
    }
    // compute barycenter for positive iad values
    l_histo_values.min_average    = 0.0;
    histo_count                 = 0;
    for (int i = 0 ; i < ((HISTO_COUNT / 2) - 1) ; i++)
    {
        l_histo_values.min_average    = l_histo_values.min_average + (histo[i] * (float)(i + 1));
        histo_count                 += histo[i];
    }
    //PACTOR_TNC_DEBUG << "l_histo_values.min_average="<<l_histo_values.min_average<<"count"<<histo_count;
    l_histo_values.min_average    = 0.0 - MAX_HISTO_VALUE + ((MAX_HISTO_VALUE / (float)(HISTO_COUNT / 2)) *
                                    ((l_histo_values.min_average / (float)histo_count) - 1.0));
    //PACTOR_TNC_DEBUG << "l_histo_values.min_average="<<l_histo_values.min_average;
    // compute barycenter for positive iad values
    l_histo_values.max_average    = 0.0;
    histo_count                 = 0;
    for (int i = ((HISTO_COUNT / 2) + 1) ; i < HISTO_COUNT ; i++)
    {
        l_histo_values.max_average    = l_histo_values.max_average + (histo[i] * (float)(i + 1));
        histo_count                 += histo[i];
    }
    //PACTOR_TNC_DEBUG << "l_histo_values.max_average="<<l_histo_values.max_average<<"count"<<histo_count;
    l_histo_values.max_average    = 0.0 + ((MAX_HISTO_VALUE / (float)(HISTO_COUNT / 2)) *
                                           ((l_histo_values.max_average / (float)histo_count) - 1.0 - (float)(HISTO_COUNT / 2)));
    //PACTOR_TNC_DEBUG << "l_histo_values.max_average="<<l_histo_values.max_average;

    l_histo_values.min_threshold    = (l_histo_values.min_average / 3.0);
    l_histo_values.max_threshold    = (l_histo_values.max_average / 3.0);
    l_histo_values.average    = ((l_histo_values.max_threshold + l_histo_values.min_threshold) / 2.0);
    /*PACTOR_TNC_DEBUG << "l_histo_values.min_average="<<l_histo_values.min_average
                               << "l_histo_values.max_average="<<l_histo_values.max_average
                                  << "l_histo_values.min_threshold="<<l_histo_values.min_threshold
                                     << "l_histo_values.max_threshold="<<l_histo_values.max_threshold
                                        << "l_histo_values.average="<<l_histo_values.average;
    */
    return l_histo_values;
}
#endif

#define     CHECK_MYCALL    0
#define     CHECK_MYAUX     1
#define     CHECK_COMPLETE  2

void PactorDecoder::checkCallsign(t_tx_text_buffs *callsigns, const char *arg_data_char, t_decode_output *decode_output)
{
    // copy received data to a new buffer having the same length than PTMyCall
    unsigned char   temp_short_path_call[12];
    unsigned char   temp_long_path_char0;
    unsigned char   temp_mycall[12];
    int             mycall_length;
    int             call100_length;
    int             call_type;
    int             aux_call_idx;
    char            data_char[20]    = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    // compute received callsign length
    for (call100_length = 0 ; call100_length < 8; call100_length++)
    {
        // make a local copy
        data_char[call100_length]   = arg_data_char[call100_length];
        if ((arg_data_char[call100_length] == 0x0f)         // end of callsign in valid packet
            || ((arg_data_char[call100_length] & 0x80) == 0x0f))
            // invalid packet
        {
            break;
        }
    }
    // replace first 0x0f or first char after callsign with end of line char
    data_char[call100_length]   = 0;
    // make a local copy of 200 Bd part
    for (int i = 11; i < (11 + 6); i++)
    {
        data_char[i]   = arg_data_char[i];
    }

    aux_call_idx            = 0;
    decode_output->type     = NO_CONNECTION;
    call_type               = CHECK_MYCALL;

    //    PACTOR_TNC_DEBUG <<"dist 100"<<QString((const char *)&data_char[0])<<"dist 200"<<QString((const char *)&data_char[11])<<"len"<<call100_length;

    do
    {
        switch (call_type)
        {
            case
                    CHECK_MYCALL:
                // local station callsign
                mycall_length   = callsigns->my_callsign->length();
                strncpy((char *)temp_mycall, callsigns->my_callsign->constData(), mycall_length);
                // next iteration will check aux callsigns
                call_type       = CHECK_MYAUX;
                break;

            case
                    CHECK_MYAUX:
                // local station callsign
                mycall_length   = callsigns->aux_callsign[aux_call_idx]->length();
                strncpy((char *)temp_mycall, callsigns->aux_callsign[aux_call_idx]->constData(), mycall_length);
                aux_call_idx++;
                // if we're done with aux callsigns next iteration will exit
                if (aux_call_idx >= callsigns->aux_call_count)
                {
                    call_type   = CHECK_COMPLETE;
                }
                break;

            default
                    :
                break;
        }
        // do not check if length does not match
        if (mycall_length != call100_length)
        {
            //            PACTOR_TNC_DEBUG <<"wrong length"<<QString((const char *)temp_mycall)<<mycall_length<<call100_length;
            continue;
        }
        //PACTOR_TNC_DEBUG <<"local len+call"<<mycall_length<<QString((const char *)temp_mycall)<<"dist 100"<<QString((const char *)&data_char[0])<<"dist 200"<<QString((const char *)&data_char[11])<<"len"<<mycall_length;
        // copy MyCall length of received data to a temporary buffer
        // maximum length of callsign during connection request is 8 chars
        if (mycall_length > 8)
        {
            mycall_length   = 8;
        }
        int     call_200_length = mycall_length;
        if (call_200_length > 6)
        {
            call_200_length   = 6;
        }
        temp_mycall[mycall_length]    = 0;
        // long path callsign has first 8 callsign bits inverted (1->0, 0->1)
        // data_char[] contains called station callsign @ 100 Bd in position 1
        // and called station callsign @ 200 Bd in position 12
        strncpy((char *)temp_short_path_call, (const char *)data_char, mycall_length);
        temp_short_path_call[mycall_length]    = 0;
        // invert first byte for long path call
        temp_long_path_char0      = ~temp_short_path_call[0];
        // if MyCall is at the beginning of burst, then it is a connection request packet
        // 100 Bd request part
        int     similar_char_count_100  = 0;
        // long path, first character of callsign is inverted
        if (temp_mycall[0] == temp_long_path_char0)
        {
            similar_char_count_100++;
        }
        // remaining characters of 100 Bd part of connect request burst
        for (int j = 0 ; j < mycall_length; j++)
        {
            if (temp_mycall[j] == temp_short_path_call[j])
            {
                similar_char_count_100++;
            }
        }
        // 200 Bd request part
        int     similar_char_count_200  = 0;
        for (int j = 0 ; j < call_200_length; j++)
        {
            if (temp_mycall[j] == (data_char[j + 11] & 0x7f))
            {
                similar_char_count_200++;
            }
        }
        // similar bit count
        decode_output->same_byte_count   = similar_char_count_100 + similar_char_count_200;

        if (similar_char_count_100 == mycall_length)
        {
            // is first char bit-inverted? -> long path
            if (temp_mycall[0] == temp_long_path_char0)
            {
                decode_output->short_path                    = LONG_PATH_TIMING;
                m_demod_thread->sample_sync_count_initial   = 2240;
                PACTOR_TNC_DEBUG << "LONG_PATH l=1301" << "callsign cnt" << similar_char_count_100 << "+" << similar_char_count_200;
            }
            else
            {
                decode_output->short_path                    = SHORT_PATH_TIMING;
                m_demod_thread->sample_sync_count_initial   = 2000;
                PACTOR_TNC_DEBUG << "SHORT_PATH l=1307" << "callsign cnt" << similar_char_count_100 << "+" << similar_char_count_200;
            }
            // errors in 200 Bd callsign part?
            if (similar_char_count_200 < call_200_length)
            {
                PACTOR_TNC_DEBUG << "PACKET_CONNECT_100 cnt 200" << similar_char_count_200 << "max" << call_200_length << "to" << QString((const char *)temp_mycall);
                decode_output->type  = PACKET_CONNECT_100;
            }
            else
            {
                PACTOR_TNC_DEBUG << "PACKET_CONNECT_200 cnt 200" << similar_char_count_200 << "max" << call_200_length << "to" << QString((const char *)temp_mycall);
                decode_output->type  = PACKET_CONNECT_200;
            }
        }
    }
    while ((call_type != CHECK_COMPLETE) && (decode_output->type == NO_CONNECTION));
}
