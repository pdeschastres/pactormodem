/****************************************************************************
** Copyright (C) 2012 OpenMOR development team.   All rights reserved.
** Contact: Project Leader Ken AD5XJ (ad5xj@arrl.net)
** or visit the project web site openmor.sourceforge.net
**
** This file is part of the OpenMOR Project in the modem or RMSLink executable.
**
** This software is intended for use on amateur radio only and is not a
** commercial product.  The software of the OpenMOR project is
** primarily useful for technical and scientific purposes in non-commercial
** applications only.   Further, this software is distributed in the hope
** that it will be useful.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** === No Commercial Usage ===
** This file contains Open Source code and may not be distributed except as
** outlined in the terms and conditions of the GNU GPL License v3 included
** with this source code.
**
**  === GNU Lesser General Public License Usage ===
** This file may be used under the terms of the GNU Lesser
** General Public License version 3 or higher as published by the
** Free Software Foundation and appearing in the file LICENSE.TXT included
** in the packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 3 (or higher)
** requirements will be met: http://www.gnu.org/licenses/gpl-3.0.txt.
**
** In addition, portions of this software are products originating with
** Nokia and other authors. Each author retains their rights as described
** in their respective LGPL Exception statements.  Any distribution of this
** software carries with it the implied restrictions of the GPLv3 license,
** even with modifications to the original from this source code.
**
** Portions of this software may be modified from the original and are subject
** to GPL or LGPL License restrictions of the original source code.
**
** The developers of the OpenMOR software wish to thank particular individuals
** for their generous contributions.   We wish to thank:
**
**     Thomas Sailer        for the original PACTOR I modem from hfterm
**     Dave Freeze, W1HKJ   from his project FLDIGI
**     Rick Muething, KN6KB of the Amateur Radio Safety Foundation Inc.
**                          for his outstanding WinMOR decoder and
**                          contributions to this project.
**     Volker Schroer       for his contributions from his project LinPSK
**
** - - - The OpenMOR development team.
****************************************************************************/
#include <unistd.h>
#include <ctype.h>
#include <math.h>
#include <sched.h>

#include <QtCore/qmath.h>
#include <QtCore/QMetaType>
#include <QtCore/QThread>
#include <QtCore/QBitArray>
#include <QtCore/QTime>
#include <QtCore/QSemaphore>

#include "globals.h"
#include "pactoraudioengine.h"
#include "pactornetstruct.h"
#include "pactorhuffman.h"
#include "pactordecoder.h"
#include "pactordemodulator.h"
#include "pactorFSKdemod.h"
#include "pactorsynchro.h"
#include "pactortnc.h"

extern qint64 global_date_time_mSecs;

const int bit_patterns[10] =
{
    0x055,   // header
    0x0AA,   // header, inverted phase
    0x4d5,   // CS1
    0xb2a,   // CS1, inverted phase
    0xab2,   // CS2
    0x54d,   // CS2, inverted phase
    0x34b,   // CS3
    0xcb4,   // CS3, inverted phase
    0xd2c,   // CS4
    0x2d3    // CS4, inverted phase
};

PactorDemodThread::PactorDemodThread(PactorDemod *real_parent, PactorTNC *mdm_parent, QObject *parent)
    :   QObject(parent)
    ,   m_pactorTNC(mdm_parent)
    ,   m_thread(new QThread(this))
{
    valid_burst_found           = false;
    // initialize transition detection fifo
    old_fifo_amplitude_sum      = 0.0;
    fifo_amplitude_sum          = 0.0;
    // initialize fifo values
    for (int i = 0; i < ZERO_CROSS_FIFO_DEPTH; i++)
    {
        samples_fifo[i].amplitude   = 0.0;
        samples_fifo[i].phase       = 0.0;
    }

    //    PACTOR_TNC_DEBUG << "Pactor demod thread constructor";
    // create PactorDecoder object
    m_decoder       = new PactorDecoder(this);

    moveToThread(m_thread);
    m_thread->start(QThread::TimeCriticalPriority);
    m_pactorTNC   = mdm_parent;
    m_pactorDemod   = real_parent;

    qRegisterMetaType<ccharptr>("ccharptr");
    CHECKED_CONNECT(this, SIGNAL(burstEvent(const int, ccharptr)), m_pactorTNC->m_pactorTNCThread, SLOT(PTEvent(const int, ccharptr)));

    PTAutoAnswerCall    = "";

    /* allocate sample_buffer,  */
    PACTOR_TNC_DEBUG << "Sample_buffer size " << AUDIO_SAMPLE_COUNT_8KHZ;
    try
    {
        sample_buffer   = new float[16 * AUDIO_SAMPLE_COUNT_8KHZ];
    }
    catch
        (...)
    {
        PACTOR_TNC_DEBUG << "erreur alloc sample_buffer buffer"  ;
    }
    // initialize demodulator
    center_frequency    = 500;
    initFSKDemod(center_frequency);

    /* allocate re_buffer,  */
#ifdef _DISPLAY_DEBUG_VALUES
    PACTOR_TNC_DEBUG << "re_buffer size " << AUDIO_SAMPLE_COUNT_8KHZ;
#endif
    try
    {
        re_buffer   = new float[16 * AUDIO_SAMPLE_COUNT_8KHZ];
    }
    catch
        (...)
    {
        PACTOR_TNC_DEBUG << "erreur alloc re_buffer buffer"  ;
    }
    /* allocate im_buffer */
#ifdef _DISPLAY_DEBUG_VALUES
    PACTOR_TNC_DEBUG << "im_buffer size " << AUDIO_SAMPLE_COUNT_8KHZ;
#endif
    try
    {
        im_buffer   = new float[16 * AUDIO_SAMPLE_COUNT_8KHZ];
    }
    catch
        (...)
    {
        PACTOR_TNC_DEBUG << "erreur alloc im_buffer buffer"  ;
    }

    /* allocate out_buffer_down_one */
#ifdef _DISPLAY_DEBUG_VALUES
    PACTOR_TNC_DEBUG << "out_buffer_down_one size " << AUDIO_SAMPLE_COUNT_8KHZ;
#endif
    try
    {
        out_buffer_down_one   = new t_phase_ampl[16 * AUDIO_SAMPLE_COUNT_8KHZ];
    }
    catch
        (...)
    {
        PACTOR_TNC_DEBUG << "erreur alloc out_buffer_down_one buffer"  ;

    }

    /* allocate demod_dwnspld_data_buffer */
#ifdef _DISPLAY_DEBUG_VALUES
    PACTOR_TNC_DEBUG << "demod_dwnspld_data_buffer size " << DEMOD_BUFFER_SIZE;
#endif
    try
    {
        demod_dwnspld_data_buffer   = new t_phase_ampl[DEMOD_BUFFER_SIZE];
    }
    catch
        (...)
    {
        PACTOR_TNC_DEBUG << "erreur alloc demod_dwnspld_data_buffer buffer"  ;

    }
    last_demod_dwnspld_index    = 0;
    sample_synchronized         = false;
    burst_processing_state      = CORR_WAIT; //CORR_IDLE;
    carrier_detector            = CARRIER_UNKNOWN;
    pos_sync_100                = -1;
    pos_sync_200                = -1;
    pos_sync.pos_sync_16        = -1;
    gap_length                  = 0;

    into_window                 = false;

    fifo_index                  = 0;
    new_phase_value             = 0.0;

    gap_table_index             = 0;

    center_frequency            = m_pactorTNC->center_frequency_start;

    AGC_amplitude               = 0.0;
    amplitude_average           = 0.0;
    amplitude_index             = 0;
    for (int i = 0; i < AMPLITUDE_HISTORY; i++)
    {
        last_amplitude[i]              = 0.0;
    }

    // initialize synchronization
    initSynchro();
    hdr_CS_previous                 = -1;
    burst_parameters.hdr100.valid  = false;
    burst_parameters.hdr200.valid  = false;
    burst_parameters.CS100.valid   = false;
    burst_parameters.hdr100.new_burst  = false;
    burst_parameters.hdr200.new_burst  = false;
    burst_parameters.CS100.new_burst   = false;
    found_start                     = false;
    first_CS3_received              = false;
    //    burst_parameters                    = temp_burst_parameters;
    last_packet_number              = -1;

    try
    {
        decode_samples_buffer   = new float[DEMOD_BUFFER_SIZE];
    }
    catch
        (...)
    {
        PACTOR_TNC_DEBUG << "erreur alloc decode_samples_buffer"  ;
    }
#ifdef DISPLAY_DATA
    disp_input_count    = 0;
    disp_inter_count    = 0;
    disp_output_count    = 0;
    disp_iterations    = 0;
#endif
    // synchro
    first_phase_transition        = true;
    last_transition_pos     = 0;
    ZeroCrossing(demod_dwnspld_data_buffer,
                 0,
                 0,
                 CORR_SEARCH_SYNC,
                 LLONG_MAX, LLONG_MAX,
                 0);
    // memory ARQ
    accumulate_count    = 0;
    prev_ARQ_mode       = false;
    ARQ_mode            = false;
    // spectrum samples counter
    wave_samples        = FFT_POINTS;
    spct_wave_buffer    = new double[FFT_POINTS];
}

PactorDemodThread::~PactorDemodThread()
{
    try
    {
        PACTOR_TNC_DEBUG << "Pactor thread destructor";
        free(re_buffer);
        free(im_buffer);
        free(out_buffer_down_one);
        free(sample_buffer);
        free(demod_dwnspld_data_buffer);
        free(decode_samples_buffer);
        free(m_decoder);
        free(m_thread);
        free(sine_table);
        free(cosine_table);
    }
    catch
        (...)
    {
        PACTOR_TNC_DEBUG << "dealloc buffers error"  ;
    }
}

/*************************************************************************************************/
/*                                                                                               */
/*    calculateDemodFSK function                                                                 */
/*                                                                                               */
/*************************************************************************************************/
/*void PactorDemodThread::calculateDemodFSK(const float * in_sample_buffer,
                                          int   data_count,
                                          qint64 timeStampUsecs)*/
void PactorDemodThread::calculateDemodFSK(void)
{
    /***************************************************/
    /*    new data set received                        */
    /***************************************************/
    int         data_count;
    float       *in_sample_buffer;
    qint64      timeStampUsecs;
    qint64      window_open_msecs;
    qint64      window_close_msecs;
    int         filtered_samples_count;
    int         copy_count;
    t_decode_output *decode_output;
    int         i;
    static t_decode_output l_decode_output;
    float       AGC_temp_gain;
    float       current_frequency;
    int         freq_shift;
#ifdef DISPLAY_DEBUG_VALUES
    static qint64 prev_packet_start_msec = 0;
    static int diff_prev_sample = 0;
    static int prev_start_pos   = 0;
    static qint64 total_gaps = 0;
    static qint64 gaps_count = -4;
#endif
    bool    c;

    do
    {
        // acquire data semaphore (low latency kernel gives 2 to 4 ms sem latency
        data_semaphore.acquire();
        // get buffer address
        in_sample_buffer    = &m_pactorTNC->m_pactoraudioengine->thread_info.audio_in_buffer[m_pactorTNC->m_pactoraudioengine->thread_info.audio_in_read_buffer_index];
        // increment read buffer
        m_pactorTNC->m_pactoraudioengine->thread_info.audio_in_read_buffer_index   = (m_pactorTNC->m_pactoraudioengine->thread_info.audio_in_read_buffer_index + AUDIO_SAMPLE_COUNT_8KHZ) % (DWNSPLD_AUDIO_BUFF_COUNT * AUDIO_SAMPLE_COUNT_8KHZ);
        // get timestamp and data count
        int frame_index = m_pactorTNC->m_pactoraudioengine->thread_info.audio_in_read_buffer_index / AUDIO_SAMPLE_COUNT_8KHZ;
        timeStampUsecs      = m_pactorTNC->m_pactoraudioengine->thread_info.first_dwnspl_frame_time[frame_index];
        window_open_msecs   = m_pactorTNC->m_pactoraudioengine->thread_info.current_window_open_msecs;
        window_close_msecs  = m_pactorTNC->m_pactoraudioengine->thread_info.current_window_close_msecs;
        data_count          = AUDIO_SAMPLE_COUNT_8KHZ;

        // spectrum samples acquisition
        if (wave_samples < FFT_POINTS)
        {
            int spl_count = 0;
            do
            {
                spct_wave_buffer[wave_samples]  = (double)in_sample_buffer[spl_count];
                wave_samples++;
                spl_count++;
                // finished
                if (wave_samples >= FFT_POINTS)
                {
                    // signal
                    emit send_spectrum_samples();
                    break;
                }
            }
            while (spl_count < data_count);
        }

        /***************************************************/
        /*                input data                       */
        /***************************************************/
        // scope display
        if (m_pactorTNC->waveform_src == e_waveform_raw_input)
        {
            emit send_waveform(in_sample_buffer, data_count, 1.0);
        }
        /***************************************************/
        /*                input filter                     */
        /***************************************************/
        float temp_amplitude    = InputFilter(in_sample_buffer, data_count, sample_buffer);

        /***************************************************/
        // AGC
        /***************************************************/
        // compute average amplitude
        if (temp_amplitude < AGC_amplitude)
        {
            AGC_amplitude           = AGC_amplitude - ((AGC_amplitude - temp_amplitude) / 1024.0);
        }
        else
        {
            AGC_amplitude           = AGC_amplitude - ((AGC_amplitude - temp_amplitude) / 64.0);
        }
        m_pactorTNC->m_pactoraudioengine->thread_info.AGC_amplitude = AGC_amplitude;
        if (m_pactorTNC->m_pactoraudioengine->thread_info.AGC_on == true)
        {
            AGC_temp_gain = m_pactorTNC->m_pactoraudioengine->thread_info.audio_in_gain;
            // adjust gain
            AGC_temp_gain   = AGC_temp_gain * (1.0 + (((TARGET_AMPL / AGC_amplitude) - 1.0) / 100.0));
            // limit gain variations
            if (AGC_temp_gain > MAX_AUDIO_INPUT_GAIN)
            {
                AGC_temp_gain   = MAX_AUDIO_INPUT_GAIN;
            }
            if (AGC_temp_gain < MIN_AUDIO_INPUT_GAIN)
            {
                AGC_temp_gain   = MIN_AUDIO_INPUT_GAIN;
            }
            // set audio engine gain
            m_pactorTNC->m_pactoraudioengine->thread_info.audio_in_gain = AGC_temp_gain;
        }

        /*
        #define AMPL_PLOT_SIZE 1536
        static float fl_ampl[AMPL_PLOT_SIZE];
        static int fl_ampl_idx = 0;
        if (m_pactorTNC->waveform_src == e_waveform_raw_input)
        {
        fl_ampl[fl_ampl_idx]   = AGC_amplitude / 50.0;
        fl_ampl_idx++;
        if (fl_ampl_idx >= AMPL_PLOT_SIZE)
        {
            fl_ampl_idx = 0;
            emit send_waveform(fl_ampl, AMPL_PLOT_SIZE, 1.0);
        }
        }
        */
        // scope display
        if (m_pactorTNC->waveform_src == e_waveform_input_filter)
        {
            emit send_waveform((const float *)sample_buffer, data_count, 1.0);
        }
        /***************************************************/
        /*                quadrature                       */
        /***************************************************/
        QuadMix(sample_buffer, data_count, re_buffer, im_buffer);

        /***************************************************/
        /*               mixed demodulator                 */
        /***************************************************/
        carrier_detector = MixedDemodulate(re_buffer, im_buffer, data_count, out_buffer_down_one, SQUELCH_LEVEL);

        /***************************************************/
        /*               output filter                     */
        /***************************************************/
        int previous_last_sample    = last_demod_dwnspld_index;   // first sample in output filter buffer. Corresponds to timeStampUsecs arg

        OutFilter500(out_buffer_down_one, data_count, demod_dwnspld_data_buffer, last_demod_dwnspld_index,
                     m_pactorTNC->store_filter500_waveform, // 0=no, 1=phase, 2=amplitude
                     waveform, // stored waveform for scope display
                     timeStampUsecs,
                     last_sample_timestamp,
                     filtered_samples_count);

        // window management        // scope display
#if 1
        if (m_pactorTNC->store_filter500_waveform != 0)
        {
            emit send_waveform((const float *)waveform, filtered_samples_count, 1.0);
        }
#endif

        if (m_pactorTNC->m_pactoraudioengine->thread_info.window_state == e_PT_rcv_win_open)
        {
            if (into_window == false)
            {
#ifdef SHOW_WINDOW

                PACTOR_TNC_DEBUG << "window opens at" << global_date_time_mSecs;
#endif
                // phase status LED
                if (valid_burst_found == false)
                {
                    // increment missing bursts counter.
                    missing_bursts++;
                    if (missing_bursts >= MAX_MISSING_BURSTS)
                    {
                        // initialize window process
                        m_pactorTNC->m_pactoraudioengine->thread_info.window_state      = e_PT_rcv_win_not_init;
                        missing_bursts      = 0;
                    }
                }
                else
                {
                    // reset missing bursts counter.
                    missing_bursts      = 0;
                }
                into_window         = true;
                valid_burst_found   = false;
            }
        }
        else
        {
            if (into_window == true)
            {
#ifdef SHOW_WINDOW
                PACTOR_TNC_DEBUG << "window closes at" << global_date_time_mSecs;
#endif
                into_window         = false;
            }
        }
        // store data and continuously search for burst header or CS
        // initialize returned value
        found_start = ZeroCrossing(demod_dwnspld_data_buffer,
                                   previous_last_sample,
                                   last_demod_dwnspld_index,
                                   burst_processing_state,
                                   //window_open_msecs,
                                   0,
                                   window_close_msecs,
                                   last_sample_timestamp); // ms
        // processing depending on synchronization status
        switch (burst_processing_state)
        {
                /***************************************************/
                /*         start listenning                        */
                /***************************************************/
            case
                    CORR_SEARCH_SYNC:
                burst_identified    = false;
                /***************************************************/
                /*       new burst or CS processing                */
                /***************************************************/
#ifdef SHOW_WINDOW
                if (found_start == true)
                {
                    if (burst_parameters.hdr100.valid == true)
                    {
                        PACTOR_TNC_DEBUG << "========================= hdr100 @" << burst_parameters.hdr100.time_usecs
                                         << "delta T" << global_date_time_mSecs - burst_parameters.hdr100.time_msecs / 1000
                                         << "window" << m_pactorTNC->m_pactoraudioengine->thread_info.window_state;
                    }
                    if (burst_parameters.hdr200.valid == true)
                    {
                        PACTOR_TNC_DEBUG << "========================= hdr200 @" << burst_parameters.hdr200.time_usecs
                                         << "delta T" << global_date_time_mSecs - burst_parameters.hdr200.time_msecs / 1000
                                         << "window" << m_pactorTNC->m_pactoraudioengine->thread_info.window_state;
                    }
                    if (burst_parameters.CS100.valid == true)
                    {
                        PACTOR_TNC_DEBUG << "========================= CS100 @" << burst_parameters.CS100.time_usecs
                                         << "delta T" << global_date_time_mSecs - burst_parameters.CS100.time_msecs / 1000
                                         << "window" << m_pactorTNC->m_pactoraudioengine->thread_info.window_state;
                    }
                }
#endif
                if (found_start == true)
                {
                    // status LED
                    LED_args[0].led_index   = LED_RX; // receive LED
                    LED_args[0].icon_color = GREEN_ICON;
                    emit setLED(1, LED_args);

                    if (burst_parameters.hdr100.valid == true)
                    {
                        start_pos       = burst_parameters.hdr100.fine_sync;
                        hdr_CS_found        = BURST_TYPE_HDR100;
                        hdr_CS_position     = burst_parameters.hdr100.fine_sync + 15;
                        hdr_CS_phase        = burst_parameters.hdr100.phase;
                        hdr_CS_average      = burst_parameters.average_value;
                        hdr_CS_fine_sync    = burst_parameters.hdr100.fine_sync + 15;
                        burst_processing_state    = CORR_FOUND_LONG;
                        packet_timestamp_msec   = burst_parameters.hdr100.time_msecs - m_pactorTNC->m_pactoraudioengine->thread_info.total_latency;
                        // update tx time if next_tx_time is not initialized
                        if (m_pactorTNC->m_pactoraudioengine->thread_info.next_tx_time == LLONG_MAX)
                        {
                            m_pactorTNC->m_pactoraudioengine->thread_info.next_tx_time  = packet_timestamp_msec + CS_TX_DELAY_MS;
                            m_pactorTNC->m_pactoraudioengine->thread_info.tx_time_update    = 0;
                        }
                        // otherwise compute difference with estimated timestamp
                        else
                        {
                            m_pactorTNC->m_pactoraudioengine->thread_info.tx_time_update    = packet_timestamp_msec + CS_TX_DELAY_MS - m_pactorTNC->m_pactoraudioengine->thread_info.next_tx_time;
                        }
                        burst_parameters.first_trans_index  = burst_parameters.hdr100.first_trans_index;
                        first_CS3_received  = false;
                    }
                    else
                        if (burst_parameters.hdr200.valid == true)
                        {
                            start_pos       = burst_parameters.hdr200.fine_sync;
                            hdr_CS_found        = BURST_TYPE_HDR200;
                            hdr_CS_position     = burst_parameters.hdr200.fine_sync + 15;
                            hdr_CS_phase        = burst_parameters.hdr200.phase;
                            hdr_CS_average      = burst_parameters.average_value;
                            hdr_CS_fine_sync    = burst_parameters.hdr200.fine_sync + 15;
                            burst_processing_state    = CORR_FOUND_LONG;
                            packet_timestamp_msec   = burst_parameters.hdr200.time_msecs - m_pactorTNC->m_pactoraudioengine->thread_info.total_latency;
                            // update tx time if next_tx_time is not initialized
                            if (m_pactorTNC->m_pactoraudioengine->thread_info.next_tx_time == LLONG_MAX)
                            {
                                m_pactorTNC->m_pactoraudioengine->thread_info.next_tx_time  = packet_timestamp_msec + CS_TX_DELAY_MS;
                                m_pactorTNC->m_pactoraudioengine->thread_info.tx_time_update    = 0;
                            }
                            // otherwise compute difference with estimated timestamp
                            else
                            {
                                m_pactorTNC->m_pactoraudioengine->thread_info.tx_time_update    = packet_timestamp_msec + CS_TX_DELAY_MS - m_pactorTNC->m_pactoraudioengine->thread_info.next_tx_time;
                            }
                            burst_parameters.first_trans_index  = burst_parameters.hdr200.first_trans_index;
                            first_CS3_received  = false;
                        }
                        else
                            if (burst_parameters.CS100.valid == true)
                            {
                                start_pos       = burst_parameters.CS100.fine_sync;
                                sync_CS_type    = burst_parameters.CS100.CS_type;
                                hdr_CS_position     = burst_parameters.CS100.fine_sync + 15;
                                hdr_CS_phase        = burst_parameters.CS100.phase;
                                hdr_CS_average      = burst_parameters.average_value;
                                hdr_CS_fine_sync    = burst_parameters.CS100.fine_sync + 15;
                                packet_timestamp_msec   = burst_parameters.CS100.time_msecs - m_pactorTNC->m_pactoraudioengine->thread_info.total_latency;
                                // separate CS3 which is the header of a complete burst
                                if (burst_parameters.CS100.CS_type == CS3_ID)
                                {
                                    hdr_CS_found        = BURST_TYPE_CS3;
                                    burst_processing_state    = CORR_FOUND_LONG;
                                    m_pactorTNC->m_pactoraudioengine->thread_info.new_burst       = true;
                                    m_pactorTNC->m_pactoraudioengine->thread_info.tx_data_or_CS3_burst      = false;
                                    packet_timestamp_msec   = burst_parameters.CS100.time_msecs;
                                    // stop automatic transmission by PactorAudioEngine only while we are in ISS mode
                                    m_pactorTNC->m_pactoraudioengine->thread_info.next_tx_time  = packet_timestamp_msec + CS_TX_DELAY_MS;
                                    // set default tx time to computed tx time when changing direction
                                    if (first_CS3_received == false)
                                    {
                                        first_CS3_received  = true;
                                        // stop automatic transmission by PactorAudioEngine only while we are in ISS mode
                                        // update tx time if next_tx_time is not initialized
                                        m_pactorTNC->m_pactoraudioengine->thread_info.next_tx_time  = packet_timestamp_msec + CS_TX_DELAY_MS;
                                        m_pactorTNC->m_pactoraudioengine->thread_info.tx_time_update    = 0;
                                        m_pactorTNC->m_pactoraudioengine->thread_info.can_transmit    = true;
                                        // default is CS2 (request)
                                        m_pactorTNC->m_pactoraudioengine->thread_info.current_CS      = CS2_ID;
                                        // now, we transmit CS
                                        m_pactorTNC->m_pactoraudioengine->thread_info.receiving_CS    = false;
                                    }
                                    burst_parameters.first_trans_index  = burst_parameters.CS100.first_trans_index;
                                }
                                else
                                {
                                    hdr_CS_found        = BURST_TYPE_CS_OTHER;
                                    burst_processing_state    = CORR_FOUND_SHORT;
                                    first_CS3_received  = false;
                                }
                            }

                    // update as soon as a header or CS has been found into the packet window
                    switch (m_pactorTNC->short_path)
                    {
                        case
                                SHORT_PATH_TIMING:
                            //                    PACTOR_TNC_DEBUG << "SHORT_PATH_TIMING"<<TIMER_SHORT_PATH_MS;
                            packet_start_window_msec            = packet_timestamp_msec + TIMER_SHORT_PATH_MS;
                            sample_sync_count_initial   = 2000;
                            break;
                        case
                                LONG_PATH_TIMING:
                            //                    PACTOR_TNC_DEBUG << "LONG_PATH_TIMING"<<TIMER_LONG_PATH_MS;
                            packet_start_window_msec            = packet_timestamp_msec + TIMER_LONG_PATH_MS;
                            sample_sync_count_initial   = 2240;
                            break;
                        case
                                UNPROTO_TIMING:
                            //                    PACTOR_TNC_DEBUG << "UNPROTO_TIMING"<<TIMER_UNPROTO_MS;
                            packet_start_window_msec            = packet_timestamp_msec + TIMER_UNPROTO_MS;
                            sample_sync_count_initial   = 1600;
                            break;
                    }
                    m_pactorTNC->m_pactoraudioengine->thread_info.Rx_time_demod_update  = true;
                    m_pactorTNC->m_pactoraudioengine->thread_info.next_Rx_time_ON   = packet_start_window_msec
                            - SHIFT_PACKET_START;
                    m_pactorTNC->m_pactoraudioengine->thread_info.next_Rx_time_OFF  = m_pactorTNC->m_pactoraudioengine->thread_info.next_Rx_time_ON + PACKET_WINDOW_MARGIN_MS;
                    m_pactorTNC->m_pactoraudioengine->thread_info.window_state      = e_PT_rcv_win_waiting;// e_PT_rcv_win_updated;
                    m_pactorTNC->m_pactoraudioengine->thread_info.current_window_open_msecs      = m_pactorTNC->m_pactoraudioengine->thread_info.next_Rx_time_ON;
                    m_pactorTNC->m_pactoraudioengine->thread_info.current_window_close_msecs     = m_pactorTNC->m_pactoraudioengine->thread_info.next_Rx_time_OFF;
#ifdef DISPLAY_DEBUG_VALUES
                    diff_prev_sample    = (hdr_CS_position - prev_start_pos + DEMOD_BUFFER_SIZE) % DEMOD_BUFFER_SIZE;
                    prev_start_pos      = hdr_CS_position;
                    int     average_gap;
                    gaps_count++;
                    if (gaps_count > 0) // skip first gap
                    {
                        total_gaps         += diff_prev_sample;
                        average_gap         = total_gaps / gaps_count;
                    }
                    else
                    {
                        average_gap         = 0;
                    }
                    if (burst_processing_state == CORR_FOUND_LONG)
                    {
                        PACTOR_TNC_DEBUG << "1-found LONG" << hdr_CS_found << "at T=" << packet_timestamp_msec
                                         << "current T=" << global_date_time_mSecs
                                         << "next Tx=" << m_pactorTNC->m_pactoraudioengine->thread_info.next_tx_time
                                         << "gain" << m_pactorTNC->m_pactoraudioengine->thread_info.audio_in_gain
                                         << "offset" << m_pactorTNC->m_pactoraudioengine->thread_info.audio_offset
                                         << "AGC ampl" << AGC_amplitude
                                         << "new header" << hdr_CS_found << "window open"
                                         << m_pactorTNC->m_pactoraudioengine->thread_info.next_Rx_time_ON
                                         << "window close"
                                         << m_pactorTNC->m_pactoraudioengine->thread_info.next_Rx_time_OFF
                                         << "path val" << m_pactorTNC->short_path
                                         << "pos" << hdr_CS_position << "fine sync" << hdr_CS_fine_sync
                                         << "diff prev spl" << diff_prev_sample
                                         << "avg gap" << average_gap << "diff avg" << diff_prev_sample - average_gap;
#if 0
                        // show samples values around first transition
                        if (hdr_CS_position > 26)
                        {
                            int temp_pos = hdr_CS_position - 26;
                            PACTOR_TNC_DEBUG << "0==P" << temp_pos - 6 << demod_dwnspld_data_buffer[temp_pos - 6].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 5].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 4].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 3].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 2].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 1].phase
                                             << demod_dwnspld_data_buffer[temp_pos].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 1].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 2].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 3].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 4].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 5].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 6].phase;
                            PACTOR_TNC_DEBUG << "0==A" << demod_dwnspld_data_buffer[temp_pos - 6].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos - 5].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos - 4].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos - 3].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos - 2].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos - 1].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos + 1].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos + 2].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos + 3].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos + 4].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos + 5].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos + 6].amplitude;
                            temp_pos += 13;
                            PACTOR_TNC_DEBUG << "1==P" << temp_pos - 6 << demod_dwnspld_data_buffer[temp_pos - 6].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 5].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 4].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 3].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 2].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 1].phase
                                             << demod_dwnspld_data_buffer[temp_pos].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 1].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 2].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 3].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 4].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 5].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 6].phase;
                            PACTOR_TNC_DEBUG << "1==A" << demod_dwnspld_data_buffer[temp_pos - 6].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos - 5].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos - 4].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos - 3].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos - 2].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos - 1].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos + 1].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos + 2].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos + 3].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos + 4].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos + 5].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos + 6].amplitude;
                            temp_pos += 13;
                            PACTOR_TNC_DEBUG << "2==P" << temp_pos - 6 << demod_dwnspld_data_buffer[temp_pos - 6].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 5].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 4].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 3].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 2].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 1].phase
                                             << demod_dwnspld_data_buffer[temp_pos].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 1].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 2].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 3].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 4].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 5].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 6].phase;
                            PACTOR_TNC_DEBUG << "2==A" << demod_dwnspld_data_buffer[temp_pos - 6].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos - 5].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos - 4].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos - 3].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos - 2].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos - 1].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos + 1].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos + 2].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos + 3].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos + 4].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos + 5].amplitude
                                             << demod_dwnspld_data_buffer[temp_pos + 6].amplitude;
                            temp_pos += 13;
                            PACTOR_TNC_DEBUG << "3==P" << temp_pos - 6 << demod_dwnspld_data_buffer[temp_pos - 6].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 5].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 4].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 3].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 2].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 1].phase
                                             << demod_dwnspld_data_buffer[temp_pos].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 1].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 2].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 3].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 4].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 5].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 6].phase;
                            temp_pos += 13;
                            PACTOR_TNC_DEBUG << "4==P" << temp_pos - 6 << demod_dwnspld_data_buffer[temp_pos - 6].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 5].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 4].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 3].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 2].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 1].phase
                                             << demod_dwnspld_data_buffer[temp_pos].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 1].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 2].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 3].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 4].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 5].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 6].phase;
                            temp_pos = hdr_CS_position + 160 - 26;
                            PACTOR_TNC_DEBUG << "\n0==P" << temp_pos - 6 << demod_dwnspld_data_buffer[temp_pos - 6].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 5].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 4].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 3].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 2].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 1].phase
                                             << demod_dwnspld_data_buffer[temp_pos].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 1].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 2].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 3].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 4].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 5].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 6].phase;
                            temp_pos += 13;
                            PACTOR_TNC_DEBUG << "1==P" << temp_pos - 6 << demod_dwnspld_data_buffer[temp_pos - 6].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 5].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 4].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 3].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 2].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 1].phase
                                             << demod_dwnspld_data_buffer[temp_pos].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 1].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 2].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 3].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 4].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 5].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 6].phase;
                            temp_pos += 13;
                            PACTOR_TNC_DEBUG << "2==P" << temp_pos - 6 << demod_dwnspld_data_buffer[temp_pos - 6].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 5].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 4].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 3].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 2].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 1].phase
                                             << demod_dwnspld_data_buffer[temp_pos].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 1].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 2].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 3].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 4].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 5].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 6].phase;
                            temp_pos += 13;
                            PACTOR_TNC_DEBUG << "3==P" << temp_pos - 6 << demod_dwnspld_data_buffer[temp_pos - 6].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 5].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 4].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 3].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 2].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 1].phase
                                             << demod_dwnspld_data_buffer[temp_pos].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 1].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 2].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 3].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 4].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 5].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 6].phase;
                            temp_pos += 13;
                            PACTOR_TNC_DEBUG << "4==P" << temp_pos - 6 << demod_dwnspld_data_buffer[temp_pos - 6].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 5].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 4].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 3].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 2].phase
                                             << demod_dwnspld_data_buffer[temp_pos - 1].phase
                                             << demod_dwnspld_data_buffer[temp_pos].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 1].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 2].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 3].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 4].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 5].phase
                                             << demod_dwnspld_data_buffer[temp_pos + 6].phase;
                        }
#endif // #if 0
                    }
                    else
                    {
                        PACTOR_TNC_DEBUG << "1-found CS" << burst_parameters.CS100.CS_type + 1
                                         << "phase" << burst_parameters.CS100.phase
                                         << "at Tpkt=" << packet_timestamp_msec
                                         << "current T=" << global_date_time_mSecs;
                    }
#endif
                    // reset memoryARQ if burst header changed
                    if (hdr_CS_previous != hdr_CS_found)
                    {
                        PACTOR_TNC_DEBUG << "reset memoryARQ";
                        hdr_CS_previous     = hdr_CS_found;
                        memoryARQReset();
                        // reset previous packet counter
                        last_packet_number  = -1;
                    }
                    carrier_loss_count  = 0;
                    decode_samples_count = 0;
                    // data buffer contains 512 samples. Each audio iteration brings BURST_DWNSPL_CNT new filtered samples (downsampling factor 5).
                    // compute first sample position (correlator start bit - margin)
                    /******************************************************************/

                    /******************************************************************/
                    // compute copy sample count
                    if ((last_demod_dwnspld_index - start_pos) > 0)
                    {
                        copy_count  = last_demod_dwnspld_index - start_pos;
                    }
                    else
                    {
                        copy_count  = last_demod_dwnspld_index + DEMOD_BUFFER_SIZE - start_pos;
                    }
                    /***************************************************/
                    /*          copy data to decoder buffer            */
                    /***************************************************/
                    for (i = 0 ; i < copy_count ; i++, decode_samples_count++)
                    {
                        decode_samples_buffer[decode_samples_count] = demod_dwnspld_data_buffer[start_pos].phase;
                        start_pos   = (start_pos + 1) % DEMOD_BUFFER_SIZE;
                    }
                }

                // found a short burst: CS1, CS2 or CS4
                /***************************************************/
                /*            CS processing                        */
                /***************************************************/
                if (burst_processing_state == CORR_FOUND_SHORT)
                {
                    /***************************************************/
                    /*        update elapsed time at end of burst      */
                    /***************************************************/
                    // store arrival time in decode_output structure
                    l_decode_output.arrival_time = packet_timestamp_msec;
                    // callback modem
                    l_decode_output.type                = ACK_CS1 + burst_parameters.CS100.CS_type;
                    l_decode_output.length              = 2;
                    l_decode_output.char_buff           = NULL;
                    l_decode_output.average_level       = burst_parameters.average_value; //burst_parameters.CS100.average_value;
                    l_decode_output.break_request       = 0;
                    l_decode_output.phase               = burst_parameters.CS100.phase;
                    l_decode_output.header              = 0;
                    l_decode_output.position            = burst_parameters.CS100.pos;
                    emit burstEvent(l_decode_output.type, (ccharptr)&l_decode_output);
#ifdef _DISPLAY_DEBUG_VALUES
                    PACTOR_TNC_DEBUG << "->->-> found a short burst: CS1, CS2 or CS4. start:" << packet_timestamp_msec
                                     << "end:" << global_date_time_mSecs
                                     << "GAP" << packet_timestamp_msec - prev_packet_start_msec;
#endif
                    prev_packet_start_msec          = packet_timestamp_msec;
                    // restart analyze
                    burst_processing_state                    = CORR_SEARCH_SYNC;
                    // update timing data
                    valid_burst_found   = true;
                    // if CS4 is received, next received burst without error should be acknowledged by a CS1
                    // so send a CS2 as long as a good burst is not received
                    if (burst_parameters.CS100.CS_type == CS4_ID)
                    {
                        m_pactorTNC->m_pactoraudioengine->thread_info.current_CS        = CS2_ID;
                    }

                    /***************************************************/
                    /*         correct channel center frequency        */
                    /***************************************************/
                    freq_shift = compute_frequency_shift(decode_samples_buffer, decode_samples_count);
                    // PACTOR_TNC_DEBUG << "=======FREQ D"<<freq_shift<<"center"<<center_frequency;
                    if (m_pactorTNC->AFC_on)
                    {
                        // correct central frequency according to average value measured
                        current_frequency   = center_frequency + freq_shift;
                        if (fabs((float)m_pactorTNC->center_frequency_start - current_frequency)
                            <= (float)m_pactorTNC->center_frequency_AFC_limit)
                        {
                            emit PTModifiedFrequency((int)current_frequency, e_freq_mod_demod);
                        }
                    }
                }
                break;

                // found a long burst: either header 100 or 200 Bd, or CS3
                /***************************************************/
                /*          long burst processing                  */
                /***************************************************/
            case
                    CORR_FOUND_LONG:
                // compute first sample position (correlator start bit - margin)
                start_pos   = previous_last_sample;
                // compute copy sample count
                if ((last_demod_dwnspld_index - start_pos) > 0)
                {
                    copy_count  = last_demod_dwnspld_index - start_pos;
                }
                else
                {
                    copy_count  = last_demod_dwnspld_index + DEMOD_BUFFER_SIZE - start_pos;
                }
#ifdef _DISPLAY_DEBUG_VALUES
                PACTOR_TNC_DEBUG << "long burst copy" << copy_count << "samples, from" << start_pos << "to"
                                 << decode_samples_count
                                 << "diff" << start_pos - decode_samples_count;
#endif
                /***************************************************/
                /*     copy data from demod to decode buffer       */
                /***************************************************/
                // copy data to decoder buffer
                for (i = 0 ; i < copy_count ; i++)
                {
                    // eviter le plantage si trop de données
                    if (decode_samples_count < DEMOD_BUFFER_SIZE)
                    {
                        decode_samples_buffer[decode_samples_count] = demod_dwnspld_data_buffer[start_pos].phase;
                        decode_samples_count++;
                        start_pos   = (start_pos + 1) % DEMOD_BUFFER_SIZE;
                    }
                }
#ifdef _DISPLAY_DEBUG_VALUES
                PACTOR_TNC_DEBUG << "finished from last" << start_pos << "to last index" << decode_samples_count
                                 << "diff" << start_pos - decode_samples_count;
#endif
                /***************************************************/
                /* enough data received for header identification  */
                /***************************************************/
                // are we finished with this burst ?
                if ((decode_samples_count > CORREL_MIN_LENGTH_CS + 48)
                    && (burst_identified == false))
                {
                    burst_identified    = true;
                    // store last transition
                    burst_parameters.last_trans_index    = gap_table_index;
                    // recover data bits
                    recoverBitFromTransitions(hdr_CS_found, burst_parameters, gap_length_table, true);
                    // call identifier
                    decode_output   = m_decoder->identifyPacket((const float *)decode_samples_buffer,
                                      hdr_CS_found,
                                      hdr_CS_phase,
                                      //                                                              m_pactorTNC->m_pactoraudioengine->thread_info.expected_phase,
                                      hdr_CS_average,
                                      PTMyCall,
                                      m_pactorTNC->PT_mode,
                                      m_pactorTNC->m_pactorTNCThread->tnc_state,
                                      m_pactorTNC->m_pactorTNCThread->PT_speed);
                    // wrong header
                    if (decode_output->type == 0)
                    {
                        // change state to search for next synchronization
                        burst_processing_state    = CORR_SEARCH_SYNC;
                        break;
                    }
                }
                /***************************************************/
                /*         long burst receive completed            */
                /***************************************************/
                // are we finished with this burst ?
                if (decode_samples_count > LONG_BURST_SAMPLE_COUNT + 48)
                {
                    // store last transition
                    burst_parameters.last_trans_index    = gap_table_index;
                    // recover data bits
                    recoverBitFromTransitions(hdr_CS_found, burst_parameters, gap_length_table, false);

                    /***************************************************/
                    /*    send demodulated buffer for visualization    */
                    /***************************************************/
                    // scope display
                    if (m_pactorTNC->waveform_src == e_waveform_demod_data)
                    {
                        int spl_count   = decode_samples_count;
                        if (spl_count > LONG_BURST_SAMPLE_COUNT)
                        {
                            spl_count = LONG_BURST_SAMPLE_COUNT;
                        }
                        emit send_waveform((const float *)decode_samples_buffer, spl_count, 1.0);
                    }
                    valid_burst_found               = true;
                    /***************************************************/
                    /*         correct channel center frequency        */
                    /***************************************************/
                    freq_shift = compute_frequency_shift(decode_samples_buffer, decode_samples_count);
                    if (m_pactorTNC->AFC_on)
                    {
                        // PACTOR_TNC_DEBUG << "=======FREQ D"<<freq_shift<<"center"<<center_frequency;
                        // correct central frequency according to average value measured
                        current_frequency   = center_frequency + freq_shift;
                        if (fabs((float)m_pactorTNC->center_frequency_start - current_frequency)
                            <= (float)m_pactorTNC->center_frequency_AFC_limit)
                        {
                            //                        PACTOR_TNC_DEBUG << "=======FREQ modified"<<current_frequency;
                            emit PTModifiedFrequency((int)current_frequency, e_freq_mod_demod);
                        }
                    }

                    // call decoder
                    decode_output   = m_decoder->DecodePacket((const float *)decode_samples_buffer,
                                      hdr_CS_found,
                                      hdr_CS_phase,
                                      //                                                              m_pactorTNC->m_pactoraudioengine->thread_info.expected_phase,
                                      hdr_CS_average,
                                      PTMyCall,
                                      m_pactorTNC->PT_mode,
                                      m_pactorTNC->m_pactorTNCThread->tnc_state,
                                      m_pactorTNC->m_pactorTNCThread->PT_speed);

                    // expected phase is known
                    m_pactorTNC->m_pactoraudioengine->thread_info.expected_phase    = decode_output->phase;
                    // store ARQ mode
                    prev_ARQ_mode   = ARQ_mode;
                    /***************************************************/
                    /*        update timestamp at start_pos            */
                    /***************************************************/
                    i   = ((decode_output->position * SAMPLE_DWNSPL_DURATION_US) / 1000);
                    packet_timestamp_msec   = packet_timestamp_msec + ((decode_output->position * SAMPLE_DWNSPL_DURATION_US) / 1000);
#ifdef DISPLAY_DEBUG_VALUES
                    PACTOR_TNC_DEBUG << "->->-> found a long burst"
                                     << "start:" << packet_timestamp_msec << "ecart ms" << i
                                     << "GAP:" << packet_timestamp_msec - prev_packet_start_msec
                                     << "start pos" << hdr_CS_position << "shift" << decode_output->position
                                     << "phase" << decode_output->phase
                                     ;
                    prev_packet_start_msec  = packet_timestamp_msec;
#endif
                    // store arrival time in decode_output structure
                    decode_output->arrival_time = packet_timestamp_msec;
                    // update timing data
                    // only if short/long path info is known
                    if ((decode_output->type == WRONG_CRC_100)
                        || (decode_output->type == WRONG_CRC_200))
                    {
                        // error(s), invert phase for addition and reset memory ARQ if necessary
                        if (ARQ_mode == true)
                        {
                            // increment accumulation counter
                            accumulate_count++;

                            if (accumulate_count > 1)
                            {
                                PACTOR_TNC_DEBUG << "memoryARQ count" << accumulate_count;
                            }
                            if (accumulate_count > m_pactorTNC->m_pactorTNCThread->UCMD[3])
                            {
                                memoryARQReset();
                            }
                            // invert phase
                            buffer_phase        = 0 - buffer_phase;
                        }
                        // m_pactorTNC->m_pactor_panel->setLEDdirect(LED_ERROR, RED_ICON);
                        c = QMetaObject::invokeMethod(m_pactorTNC->m_pactor_session, "setLEDdirect",
                                                      Qt::QueuedConnection,
                                                      Q_ARG(int, LED_ERROR),
                                                      Q_ARG(int, RED_ICON));
                        Q_ASSERT(c);
                        PACTOR_TNC_DEBUG << "CRC ERROR";
                    }
                    else
                    {
                        sample_synchronized     = true;
                        sample_sync_count       = sample_sync_count_initial;
                        // CS type and Tx time
                        m_pactorTNC->m_pactoraudioengine->thread_info.new_burst     = true;
                        m_pactorTNC->m_pactoraudioengine->thread_info.tx_data_or_CS3_burst    = false;
                        //#ifdef SHOW_WINDOW
                        //                    m_pactorTNC->m_pactoraudioengine->thread_info.window_state      = e_PT_rcv_win_initialized;
                        // no error, reset memory ARQ if in use
                        if (ARQ_mode == true)
                        {
                            memoryARQReset();
                            PACTOR_TNC_DEBUG << "memoryARQReset";
                        }
                        // update last_packet number if TX and receiving CS3
                        if ((m_pactorTNC->m_pactorTNCThread->engine_info->PT_Tx_Rx == PT_TX) && (hdr_CS_found == BURST_TYPE_CS3))
                        {
                            last_packet_number  = -1;
                        }
                        // callback to TNC
                        if (decode_output->packet_counter != last_packet_number)
                        {
                            // detect phase errors
                            if (((last_packet_number + 1) % 4) == decode_output->packet_counter)
                            {
                                // m_pactorTNC->m_pactor_panel->setLEDdirect(LED_PHASE, GREEN_ICON);
                                c = QMetaObject::invokeMethod(m_pactorTNC->m_pactor_session, "setLEDdirect",
                                                              Qt::QueuedConnection,
                                                              Q_ARG(int, LED_PHASE),
                                                              Q_ARG(int, GREEN_ICON));
                                Q_ASSERT(c);
                            }
                            else
                            {
                                // m_pactorTNC->m_pactor_panel->setLEDdirect(LED_PHASE, RED_ICON);
                                c = QMetaObject::invokeMethod(m_pactorTNC->m_pactor_session, "setLEDdirect",
                                                              Qt::QueuedConnection,
                                                              Q_ARG(int, LED_PHASE),
                                                              Q_ARG(int, RED_ICON));
                                Q_ASSERT(c);
                            }

                            if (hdr_CS_found == BURST_TYPE_CS3)
                            {
                                // update audioengine
                                m_pactorTNC->m_pactorTNCThread->engine_info->PT_Tx_Rx         = PT_TX;
                                // update phase
                                m_pactorTNC->m_pactoraudioengine->thread_info.can_transmit    = true;
                                // we send a CS1
                                m_pactorTNC->m_pactoraudioengine->thread_info.current_CS      = CS1_ID;
                                PACTOR_TNC_DEBUG << "CS3 --> next CS (invert false)"
                                                 << m_pactorTNC->m_pactoraudioengine->thread_info.current_CS + 1
                                                 << "phase" << m_pactorTNC->m_pactoraudioengine->thread_info.next_phase;
                            }
                            else
                            {
                                // update audioengine
                                m_pactorTNC->m_pactorTNCThread->engine_info->PT_Tx_Rx         = PT_RX;
                                // update phase
                                PACTOR_TNC_DEBUG << "--> was CS"
                                                 << m_pactorTNC->m_pactoraudioengine->thread_info.current_CS + 1;
                                if ((decode_output->type == PACKET_CONNECT_100)
                                    || (decode_output->type == PACKET_CONNECT_200))
                                {
                                    // connection packet received
                                    // limit connection speed to 100 Bd?
                                    if (m_pactorTNC->m_pactorTNCThread->PT_speed_limit == PT_SPEED_100)
                                    {
                                        decode_output->type = PACKET_CONNECT_100;
                                        PACTOR_TNC_DEBUG << "speed limited";
                                    }

                                    // we send a CS1 or CS4
                                    if (decode_output->type == PACKET_CONNECT_100)
                                    {
                                        m_pactorTNC->m_pactoraudioengine->thread_info.current_CS    = CS4_ID;
                                    }
                                    else
                                    {
                                        m_pactorTNC->m_pactoraudioengine->thread_info.current_CS    = CS1_ID;
                                    }
                                }
                                else
                                {
                                    // other bursts, force CS depending on header value
                                    if (decode_output->char_buff[0] == 0x55)
                                    {
                                        m_pactorTNC->m_pactoraudioengine->thread_info.current_CS    = CS1_ID;
                                    }
                                    else
                                    {
                                        m_pactorTNC->m_pactoraudioengine->thread_info.current_CS    = CS2_ID;
                                    }
                                }
                                PACTOR_TNC_DEBUG << "hdr" << decode_output->header
                                                 << "pkt type" << decode_output->type
                                                 << "--> next CS"
                                                 << 2 - m_pactorTNC->m_pactoraudioengine->thread_info.current_CS
                                                 << "phase" << m_pactorTNC->m_pactoraudioengine->thread_info.next_phase;
                            }
                            // send event to TNC
                            emit burstEvent(decode_output->type, (ccharptr)decode_output);
                            // clear REQ LED
                            LED_args[0].led_index   = LED_REQ; // req LED
                            LED_args[0].icon_color = GREY_ICON;
                            emit setLED(1, LED_args);
                        }
                        else
                        {
                            // set REQ LED
                            LED_args[0].led_index   = LED_REQ; // req LED
                            LED_args[0].icon_color = RED_ICON;
                            emit setLED(1, LED_args);
                            PACTOR_TNC_DEBUG << "already received";
                        }
                        last_packet_number  = decode_output->packet_counter;
                        // m_pactorTNC->m_pactor_panel->setLEDdirect(LED_ERROR, GREY_ICON);
                        c = QMetaObject::invokeMethod(m_pactorTNC->m_pactor_session, "setLEDdirect",
                                                      Qt::QueuedConnection,
                                                      Q_ARG(int, LED_ERROR),
                                                      Q_ARG(int, GREY_ICON));
                        Q_ASSERT(c);
                    }
                    // change state
                    burst_processing_state    = CORR_SEARCH_SYNC;
                }
                break;

            case
                    CORR_WAIT:
                break;

            default
                    :
                break;
        }
        //    m_pactorDemod->m_state  = Idle;
    }
    while (1);
}

void    PactorDemodThread::memoryARQReset(void)
{
    //    PACTOR_TNC_DEBUG << "memoryARQ reset";
    for (int i = 0 ; i < DEMOD_BUFFER_SIZE ; i++)
    {
        demod_dwnspld_data_buffer[i].phase    = 0.0;
    }
    accumulate_count    = 0;
    buffer_phase        = 1;
}

// this function computes the center of an histogram of demodulated samples
int PactorDemodThread::compute_frequency_shift(const float *decode_samples_buffer, const int decode_samples_count)
{
    int histo[256];
    int index;
    int histo_sum;
    int freq_shift;

    // init histogram
    for (int i = 0; i < 256; i++)
    {
        histo[i]    = 0;
    }
    for (int i = 0; i < decode_samples_count; i++)
    {
        index   = (int)(500.0 * decode_samples_buffer[i]);
        // limit amplitude
        if (index < -128)
        {
            index   = -128;
        }
        else
            if (index > 127)
            {
                index   = 127;
            }
        index   += 128;
        // update histogram
        histo[index]    += 1;
    }
    // compute histogram center
    histo_sum   = 0;
    index       = 0;
    for (int i = 0; i < 256; i++)
    {
        histo_sum   += histo[i] * (i + 1);
        index       += histo[i];
    }
    // compute freq shift from histogram values
    //freq_shift    = (int)(((((float)histo_sum / (float)index) - 129.0) * 2.75) + 0.5);
    // filter: apply 1/4th of frequency difference
    freq_shift    = (int)(((((float)histo_sum / (float)index) - 129.0) * (2.75 / 4.0)) + 0.5);
    return freq_shift;
}

const QByteArray &PactorDemodThread::GetBitBuffer(void)
{
    return bit_buffer;
}

/**********************************************************************************/

void PactorDemodThread::demodSetState(const int state)
{
    burst_processing_state    = state;
}


void PactorDemodThread::release_audio_sem(void)
{
    data_semaphore.release();
}

//=============================================================================
// PactorDemod
//=============================================================================

PactorDemod::PactorDemod(QObject *parent) : QObject(parent)
    ,   m_thread(new PactorDemodThread(this, (PactorTNC *)parent, 0))
{
    // get parent address
    m_tnc = (PactorTNC *)parent;
    m_session = (PactorSession *)parent;

    CHECKED_CONNECT(m_thread, SIGNAL(PTModifiedFrequency(int, int)), m_tnc, SLOT(SetCenterFrequencyInt(int, int)));
    CHECKED_CONNECT(this, SIGNAL(setDemodThreadState(const int)), m_thread, SLOT(demodSetState(const int)));
    CHECKED_CONNECT(m_thread, SIGNAL(send_waveform(const float *, int, float)), m_tnc, SLOT(sendWaveform(const float *, int, float)));
//    CHECKED_CONNECT(m_thread, SIGNAL(send_spectrum_samples(void)), m_session, SLOT(send_spectrum_samples(void)));

    PACTOR_TNC_DEBUG << "Audio samples count per output buffer" << BURST_DWNSPL_CNT;
    //    m_state = Idle;

    in_buffer_data_count    = 0;
    in_buffer               = new float [16 * AUDIO_SAMPLE_COUNT_8KHZ];
}

PactorDemod::~PactorDemod()
{
    PACTOR_TNC_DEBUG << "Pactor destructor"  ;

    delete m_thread;
}

//-----------------------------------------------------------------------------
// Public functions
//-----------------------------------------------------------------------------
// change demodulator thread status
void PactorDemod::demodSetState(const int state)
{
    emit setDemodThreadState(state);
}

void    PactorDemod::setThreadCallsign(const t_tx_text_buffs *callsigns)
{
    m_thread->callsigns         = (t_tx_text_buffs *)callsigns;
}

//-----------------------------------------------------------------------------
// Public slots
//-----------------------------------------------------------------------------

void PactorDemod::SetCenterFrequency(float frequency)
{
    m_thread->SetCenterFrequency(frequency);
}

//-----------------------------------------------------------------------------
// Private slots
//-----------------------------------------------------------------------------


