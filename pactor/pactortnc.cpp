/****************************************************************************
** Copyright (C) 2012 OpenMOR development team.   All rights reserved.
** Contact: Project Leader Ken AD5XJ (ad5xj@arrl.net)
** or visit the project web site openmor.sourceforge.net
**
** This file is part of the OpenMOR Project in the modem or RMSLink executable.
**
** This software is intended for use on amateur radio only and is not a
** commercial product.  The software of the OpenMOR project is
** primarily useful for technical and scientific purposes in non-commercial
** applications only.   Further, this software is distributed in the hope
** that it will be useful.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** === No Commercial Usage ===
** This file contains Open Source code and may not be distributed except as
** outlined in the terms and conditions of the GNU GPL License v3 included
** with this source code.
**
**  === GNU Lesser General Public License Usage ===
** This file may be used under the terms of the GNU Lesser
** General Public License version 3 or higher as published by the
** Free Software Foundation and appearing in the file LICENSE.TXT included
** in the packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 3 (or higher)
** requirements will be met: http://www.gnu.org/licenses/gpl-3.0.txt.
**
** In addition, portions of this software are products originating with
** Nokia and other authors. Each author retains their rights as described
** in their respective LGPL Exception statements.  Any distribution of this
** software carries with it the implied restrictions of the GPLv3 license,
** even with modifications to the original from this source code.
**
** Portions of this software may be modified from the original and are subject
** to GPL or LGPL License restrictions of the original source code.
**
** The developers of the OpenMOR software wish to thank particular individuals
** for their generous contributions.   We wish to thank:
**
**     Thomas Sailer        for the original PACTOR I modem from hfterm
**     Dave Freeze, W1HKJ   from his project FLDIGI
**     Rick Muething, KN6KB of the Amateur Radio Safety Foundation Inc.
**                          for his outstanding WinMOR decoder and
**                          contributions to this project.
**     Volker Schroer       for his contributions from his project LinPSK
**
** - - - The OpenMOR development team.
****************************************************************************/
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <cstdio>
#ifdef Q_OS_WIN32
# include <windows.h>
#endif

#include <QtCore/QMetaType>
#include <QtCore/QBuffer>
#include <QtCore/QByteArray>
#include <QtCore/QThread>
#include <QtCore/QTimer>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QRadioButton>

#include "globals.h"
#include "crccalc.h"
#include "pactordecoder.h"
#include "pactormodulator.h"
#include "pactordemodulator.h"
#include "pactoraudioengine.h"
#include "pactornetstruct.h"

#include "pactortnc.h"

extern qint64  global_date_time_mSecs;

class   PactorModulator;
class   PactorModulatorThread;
class   PactorTestUI;

/****************************************************************************/
// TNC thread constructor
/****************************************************************************/
PactorTNCThread::PactorTNCThread(PactorTNC *real_parent, QWidget *parent) : QObject(parent)
  ,m_pactorTNC(real_parent)
  ,m_thread(new QThread(this))
{
    PACTOR_TNC_DEBUG << "Pactor modem thread constructor";

    moveToThread(m_thread);
    m_thread->start();

    PT_current_request      = e_PTPendingNone;
    previous_datetime       = 0;
    PT_wrong_CRC_count      = 0;
    packet_counter          = 0;
    PT_CS4_in_a_row_count   = 0;
    PT_pkt_counter          = 0;
    next_PT_phase           = PT_PH_INVERTED;
    send_break              = 0;
    packet_number           = 0;
    last_packet_number      = -1;

    si_encoded              = false;

    // create 100Bd to 200Bd conversion table
    for (int i = 0 ; i < 256; i++)
    {
        int k = 0;
        int  byte_couple = 0;
        for (int j = 0; j < 8; j++, k += 2)
        {
            byte_couple |= (((i >> j) & 1) * 3) << k;
        }
        conv_100_to_200[i] = byte_couple;
    }
    qRegisterMetaType<ccharptr>("ccharptr");
    CHECKED_CONNECT(this, SIGNAL(createNewPacket(int, ccharptr)), this, SLOT(PTEvent(int, ccharptr)));
    // allocate a buffer for Pactor messages received
    txt_received    = new char[1024];
    txt_rcv_count   = 0;

    // state machine
    tnc_state   = tnc_idle;

    // test
#ifdef PACKETS_TEST
    QByteArray  tx_qbyte_array;
    QByteArray  call_qbyte_array;
    m_pactorTNC->tx_text_buffs.ascii_text    = &tx_qbyte_array;
#if 1 // test connection packet
    tx_qbyte_array.clear();
    tx_qbyte_array.append("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    m_pactorTNC->tx_text_buffs.remote_callsign = &tx_qbyte_array;
    PACTOR_TNC_DEBUG << "test connection packet";
    send_conn_request_packet(true);
#endif
#if 1 // test callsign packet
    call_qbyte_array.clear();
    call_qbyte_array.append("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    m_pactorTNC->tx_text_buffs.my_callsign = &call_qbyte_array;
    PACTOR_TNC_DEBUG << "test callsign packet 100Bd";
    PT_speed    = PT_SPEED_100;
    send_mycall_packet();
    PACTOR_TNC_DEBUG << "test callsign packet 200Bd";
    PT_speed    = PT_SPEED_200;
    send_mycall_packet();
#endif
#if 1 // test BREAK packet
    PT_CS   = PT_CS1;
    tx_qbyte_array.clear();
    tx_qbyte_array.append("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    m_pactorTNC->tx_text_buffs.ascii_text = &tx_qbyte_array;
    PACTOR_TNC_DEBUG << "test BREAK packet 100Bd";
    PT_speed    = PT_SPEED_100;
    send_BREAK_packet();
    PT_CS   = PT_CS2;
    PACTOR_TNC_DEBUG << "test BREAK packet 200Bd";
    PT_speed    = PT_SPEED_200;
    send_BREAK_packet();
#endif
#if 1 // test QRT packet
    tx_qbyte_array.clear();
    tx_qbyte_array.append("ABCDEF");
    m_pactorTNC->tx_text_buffs.my_callsign = &tx_qbyte_array;
    PACTOR_TNC_DEBUG << "test QRT packet 100Bd";
    PT_speed    = PT_SPEED_100;
    send_QRT_packet();
    PT_CS   = PT_CS1;
    PACTOR_TNC_DEBUG << "test QRT packet 200Bd";
    PT_speed    = PT_SPEED_200;
    send_QRT_packet();
#endif
#if 1 // test ASCII 100Bd packet
    tx_qbyte_array.clear();
    tx_qbyte_array.append("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    m_pactorTNC->tx_text_buffs.ascii_text = &tx_qbyte_array;
    PACTOR_TNC_DEBUG << "test ASCII 100Bd CS1";
    PT_speed    = PT_SPEED_100;
    PT_huffman_encode   = false;
    PT_pkt_counter      = 1;
    PTEnqueuePacket(PT_CS1,
                    0); // break_request
    PACTOR_TNC_DEBUG << "test ASCII 200Bd CS1";
    PT_speed    = PT_SPEED_200;
    PTEnqueuePacket(PT_CS1,
                    0); // break_request
    PACTOR_TNC_DEBUG << "test ASCII 100Bd CS2";
    PT_speed    = PT_SPEED_100;
    PT_pkt_counter      = 2;
    PTEnqueuePacket(PT_CS2,
                    0); // break_request
    PACTOR_TNC_DEBUG << "test ASCII 200Bd CS2";
    PT_speed    = PT_SPEED_200;
    PTEnqueuePacket(PT_CS2,
                    0); // break_request
    PACTOR_TNC_DEBUG << "test ASCII 100Bd BREAK CS2";
    PT_speed    = PT_SPEED_100;
    PT_pkt_counter      = 3;
    PTEnqueuePacket(PT_CS2,
                    1); // break_request
    PACTOR_TNC_DEBUG << "test ASCII 200Bd BREAK CS1";
    PT_speed    = PT_SPEED_200;
    PTEnqueuePacket(PT_CS1,
                    1); // break_request
#endif
#endif

#ifdef HUFFMAN_TEST
    QByteArray  tx_qbyte_arrayh;
    m_pactorTNC->tx_text_buffs.huffman    = &tx_qbyte_arrayh;

    tx_qbyte_arrayh.clear();
    tx_qbyte_arrayh.append("&(-_)=$!:;,?./%+<> If you simply want to check whether a QByteArray contains a particular character or substring, use contains(). If you want to find out how many times a particular character or substring occurs in the byte array, use count(). If you want to replace all occurrences of a particular value with another, use one of the two-parameter replace() overloads.");
    PACTOR_TNC_DEBUG << "test huffman encoder" << tx_qbyte_arrayh;
    QByteArray *encoded_buffer;
    // encode data
    encoded_buffer  = m_pactorTNC->huffman_encode_buffer(&tx_qbyte_arrayh);
    PACTOR_TNC_DEBUG << "raw size" << tx_qbyte_arrayh.size() << "encoded size" << encoded_buffer->size();
    // decode data
    m_pactorTNC->m_pactorDemodulator->m_thread->m_decoder->huffman_decode_packet((unsigned char *)encoded_buffer->constData(), encoded_buffer->size());
#endif
}

PactorTNCThread::~PactorTNCThread()
{
    //
}

// put sent characters into an intermediate buffer
inline void PactorTNCThread::saveSentChars(int tx_char_count, bool PT_huffman_encode)
{
    if (tx_char_count > 0)
    {
        // put sent characters into an intermediate buffer
        m_pactorTNC->tx_text_buffs.removed_chars->clear();
        if (PT_huffman_encode == true)
        {
            m_pactorTNC->tx_text_buffs.removed_chars->insert(0, m_pactorTNC->tx_text_buffs.huffman->right(tx_char_count));
            // remove sent chars
            m_pactorTNC->tx_text_buffs.ascii_text->remove(0, tx_char_count);
        }
        else
        {
            m_pactorTNC->tx_text_buffs.removed_chars->insert(0, m_pactorTNC->tx_text_buffs.ascii_text->right(tx_char_count));
            // remove sent chars
            m_pactorTNC->tx_text_buffs.ascii_text->remove(0, tx_char_count);
        }
    }
}

// restore sent characters into at the end of transmission buffer
inline void PactorTNCThread::restoreSentChars(bool PT_huffman_encode)
{
    if (PT_huffman_encode == true)
    {
        m_pactorTNC->tx_text_buffs.huffman->insert(0, m_pactorTNC->tx_text_buffs.removed_chars->constData());
    }
    else
    {
        m_pactorTNC->tx_text_buffs.ascii_text->insert(0, m_pactorTNC->tx_text_buffs.removed_chars->constData());
    }
}

// store and display received text
// blink "req" LED if same packet number received
void PactorTNCThread::storeReceivedChars(int event, t_decode_output    *decode_output)
{
    int         idx_max;

    PT_wrong_CRC_count  = 0;
    int ichar           = 1;
    txt_rcv_count       = 0;

    switch ((e_packet_type)event)
    {
    // packet acknowledged: sent another data chunk
    case PACKET_CS3_ASCII_100:
        break;
    case PACKET_CS3_ASCII_200:
        idx_max = (e_packet_type)event == PACKET_BREAK_ASCII_100 ? 9 : 21;
        ichar   = (e_packet_type)event == PACKET_BREAK_ASCII_100 ? 2 : 3;
        // read buffer until 0x1e or max char count
        while ((decode_output->char_buff[ichar] != 0x1e)
               && (ichar < idx_max)
               && (isprint(decode_output->char_buff[ichar]))
               && (txt_rcv_count < 1024))
        {
            txt_received[txt_rcv_count] = decode_output->char_buff[ichar];
            txt_rcv_count++;
            ichar++;
        }
        break;

    case PACKET_HUFFMAN_100:
        break;
    case PACKET_HUFFMAN_200:
        break;
    case PACKET_BREAK_HUFFMAN_100:
        break;
    case PACKET_BREAK_HUFFMAN_200:
        break;
    case PACKET_CS3_HUFFMAN_100:
        break;
    case PACKET_CS3_HUFFMAN_200:
        txt_rcv_count   = 0;
        for (ichar = 0; ichar < (decode_output->huffman_packet.pkt_data_len) && (txt_rcv_count < 1024); ichar++, txt_rcv_count++)
        {
            txt_received[txt_rcv_count] = decode_output->huffman_packet.pkt_data[ichar];
        }
        break;

    case PACKET_ASCII_100:
        break;
    case PACKET_ASCII_200:
        break;
    case PACKET_BREAK_ASCII_100:
        break;
    case PACKET_BREAK_ASCII_200:
        break;
    case PACKET_QRT_100:
        break;
    case PACKET_QRT_200:
        ichar = 1;
        txt_rcv_count   = 0;
        idx_max = (e_packet_type)event == PACKET_ASCII_100 ? 9 : 21;
        while ((decode_output->char_buff[ichar] != 0x1e)
               && (ichar < idx_max)
               && (isprint(decode_output->char_buff[ichar]))
               && (txt_rcv_count < 1024))
        {
            txt_received[txt_rcv_count] = decode_output->char_buff[ichar];
            txt_rcv_count++;
            ichar++;
        }
        break;

    case PACKET_CONNECT_100:
        break;
    case PACKET_CONNECT_200:
        ichar = 1;
        txt_rcv_count   = 0;
        idx_max = 9;
        while ((decode_output->char_buff[ichar] != 0x1e)
               && (ichar < idx_max)
               && (isprint(decode_output->char_buff[ichar]))
               && (txt_rcv_count < 1024))
        {
            txt_received[txt_rcv_count] = decode_output->char_buff[ichar];
            txt_rcv_count++;
            ichar++;
        }
        break;

    default:
        break;
    } // switch ((e_packet_type)event)
    txt_received[txt_rcv_count] = 0;
    packet_counter++;

#ifdef QPLAINTEXT_LOG
    pactorSendTextToClient(txt_rcv_count, txt_received);
#endif

    // reset LED's
    LED_args[0].led_index   = LED_ERROR; // error LED
    LED_args[0].icon_color = GREY_ICON;
    LED_args[1].led_index   = LED_REQ; // req LED
    LED_args[1].icon_color = GREY_ICON;
    emit setLED(2, LED_args);
}

//-----------------------------------------------------------------------------
// Pactor event dispatcher
//-----------------------------------------------------------------------------
void PactorTNCThread::PTEvent(const int event, ccharptr PT_argument)
{
    t_decode_output    *decode_output;
    int         tx_char_count;

    decode_output   = (t_decode_output *)PT_argument;

    switch (tnc_state)
    {
    case
    tnc_idle:
        break;

    case
    tnc_master_call:
        switch ((e_packet_type)event)
        {
        // packet acknowledged: send another data chunk
        case
        ACK_CS1:
            PT_pkt_counter  = 1;
            m_pactorTNC->wait_callsign_ack      = true;
            // send AA packet with pactor level + mycall + 0x0d
            send_mycall_packet();
            // switch to next state
            tnc_state   = tnc_tx200cs1;
            break;
        case
        ACK_CS4:
            // reduce speed
            PT_speed        = PT_SPEED_100;
            PT_pkt_counter  = 1;
            m_pactorTNC->wait_callsign_ack      = true;
            // send AA packet with pactor level + mycall + 0x0d
            send_mycall_packet();
            // switch to next state
            tnc_state   = tnc_tx200_to_tx100;
            break;
        default
        :
            break;
        }
        break;

    case
    tnc_slave_call:
        switch ((e_packet_type)event)
        {
        case
        PACKET_CONNECT_100:
            PACTOR_TNC_DEBUG << "connection packet 100Bd";
            m_pactorTNC->wait_pactor_level      = true;
            PT_speed                            = PT_SPEED_100;
            // switch to next state
            tnc_state   = tnc_rx200_to_rx100;
            break;

        case
        PACKET_CONNECT_200:
            PACTOR_TNC_DEBUG << "connection packet 200Bd";
            m_pactorTNC->wait_pactor_level      = true;
            PT_speed                            = PT_SPEED_200;
            // switch to next state
            tnc_state   = tnc_rx200cs1;
            break;
        default
        :
            break;
        }
        break;

    case
    tnc_tx200cs2:
        switch ((e_packet_type)event)
        {
        // QRT request
        if (m_pactorTNC->QRTrequest == true)
        {
            send_QRT_packet();
            tnc_state   = tnc_QRTreqcs1;
            break;
        }
        // packet acknowledged: send another data chunk
        case
        ACK_CS1:
            PT_pkt_counter++;
            // send hdr 0xAA packet
            tx_char_count = PTEnqueuePacket(PT_CS2,
                                            send_break);
            // put sent characters into an intermediate buffer
            saveSentChars(tx_char_count, PT_huffman_encode);
            // switch to next state
            tnc_state   = tnc_tx200cs1;
            break;

            // break received: store received data, then listen
        case
        PACKET_CS3_ASCII_200:
        case
        PACKET_CS3_HUFFMAN_200:
            storeReceivedChars(event, decode_output);
            // switch to next state
            tnc_state   = tnc_tx_to_rx200;
            break;

        case
        ACK_CS4:
            // append characters which have not been transmitted to tx buffer
            restoreSentChars(PT_huffman_encode);

            //PT_pkt_counter++;
            PT_pkt_counter  = 0;
            // reduce speed
            PT_speed        = PT_SPEED_100;
            // create a new packet
            tx_char_count = PTEnqueuePacket(PT_CS1,
                                            send_break);
            // switch to next state
            tnc_state   = tnc_tx200_to_tx100;
            break;
        default
        :
            break;
        }
        break;

    case
    tnc_tx200cs1:
        // QRT request
        if (m_pactorTNC->QRTrequest == true)
        {
            send_QRT_packet();
            tnc_state   = tnc_QRTreqcs2;
            break;
        }
        switch ((e_packet_type)event)
        {
        // packet acknowledged: send another data chunk
        case
        ACK_CS2:
            PT_pkt_counter++;
            m_pactorTNC->wait_callsign_ack      = false;
            // send hdr 0x55 packet
            tx_char_count = PTEnqueuePacket(PT_CS1,
                                            send_break);
            // put sent characters into an intermediate buffer
            saveSentChars(tx_char_count, PT_huffman_encode);
            // switch to next state
            tnc_state   = tnc_tx200cs2;
            break;

            // break received: store received data, then listen
        case
        PACKET_CS3_ASCII_200:
        case
        PACKET_CS3_HUFFMAN_200:
            m_pactorTNC->wait_callsign_ack      = false;
            storeReceivedChars(event, decode_output);
            // switch to next state
            m_pactorTNC->m_pactoraudioengine->thread_info.tx_data_or_CS3_burst    = false;
            m_pactorTNC->m_pactoraudioengine->thread_info.receiving_CS  = false;
            tnc_state   = tnc_tx_to_rx200;
            break;

        case
        ACK_CS4:
            //PT_pkt_counter++;
            PT_pkt_counter  = 0;
            // reduce speed
            PT_speed        = PT_SPEED_100;
            // if myCallsign was not acknowledged, resent it at 100Bd, otherwise resend data
            if (m_pactorTNC->wait_callsign_ack == true)
            {
                // create a new packet
                send_mycall_packet();
            }
            else
            {
                // append characters which have not been transmitted to tx buffer
                restoreSentChars(PT_huffman_encode);

                // create a new packet
                tx_char_count = PTEnqueuePacket(PT_CS1,
                                                send_break);
            }
            // switch to next state
            tnc_state   = tnc_tx200_to_tx100;
            break;
        default
        :
            break;
        }
        break;

    case
    tnc_tx_to_rx200:
        switch ((e_packet_type)event)
        {
        case
        PACKET_BREAK_ASCII_200:
        case
        PACKET_BREAK_HUFFMAN_200:
            storeReceivedChars(event, decode_output);
            if (m_pactorTNC->wait_pactor_level == true)
            {
                PACTOR_TNC_DEBUG << "tnc_rx200cs1 CS3 wait_pactor_level";
                m_pactorTNC->wait_pactor_level  = false;
            }
            PT_pkt_counter  = 0;
            // switch to next state
            tnc_state   = tnc_rx_to_tx200;
            break;
        case
        PACKET_ASCII_200:
        case
        PACKET_HUFFMAN_200:
            storeReceivedChars(event, decode_output);
            PACTOR_TNC_DEBUG << "tnc_tx_to_rx200 PACKET_ASCII/Huffman";
            // become IRS
            engine_info->PT_Tx_Rx   = PT_RX;
            LED_args[0].led_index   = LED_IRS; // IRS LED
            LED_args[0].icon_color = RED_ICON;
            LED_args[1].led_index   = LED_ISS; // ISS LED
            LED_args[1].icon_color = GREY_ICON;
            emit setLED(2, LED_args);
            // switch to next state
            tnc_state   = tnc_rx200cs1;
            break;
        case
        SPEED_DOWN:
            // switch to speeddown
            PT_speed        = PT_SPEED_100;
            tnc_state   = tnc_rx200_to_rx100;
            break;
        default
        :
            break;
        }
        break;

    case
    tnc_rx200cs1:
        switch ((e_packet_type)event)
        {
        case
        PACKET_BREAK_ASCII_200:
        case
        PACKET_BREAK_HUFFMAN_200:
            storeReceivedChars(event, decode_output);
            if (m_pactorTNC->wait_pactor_level == true)
            {
                PACTOR_TNC_DEBUG << "tnc_rx200cs1 CS3 wait_pactor_level";
                m_pactorTNC->wait_pactor_level  = false;
            }
            PT_pkt_counter  = 0;
            // switch to next state
            tnc_state   = tnc_rx_to_tx200;
            break;
        case
        PACKET_ASCII_200:
        case
        PACKET_HUFFMAN_200:
            storeReceivedChars(event, decode_output);
            if (m_pactorTNC->wait_pactor_level == true)
            {
                PACTOR_TNC_DEBUG << "tnc_rx200cs1 PACKET_ASCII/Huffman received_pactor_level";
                m_pactorTNC->wait_pactor_level  = false;
            }
            else
            {
                PACTOR_TNC_DEBUG << "tnc_rx200cs1 PACKET_ASCII";
            }
            // become ISS if break requested
            if (send_break == 1)
            {
                send_break  = 0;
                PT_pkt_counter  = 0;
                send_BREAK_packet();
                // switch to next state
                tnc_state   = tnc_rx_to_tx200;
            }
            else
            {
                // switch to next state
                tnc_state   = tnc_rx200cs2;
            }
            break;
        case
        SPEED_DOWN:
            // switch to speeddown
            PT_speed        = PT_SPEED_100;
            tnc_state   = tnc_rx200_to_rx100;
            break;
        default
        :
            break;
        }
        break;

    case
    tnc_rx200cs2:
        switch ((e_packet_type)event)
        {
        case
        PACKET_BREAK_ASCII_200:
        case
        PACKET_BREAK_HUFFMAN_200:
            storeReceivedChars(event, decode_output);
            PT_pkt_counter  = 0;
            // switch to next state
            tnc_state   = tnc_rx_to_tx200;
            break;
        case
        PACKET_ASCII_200:
        case
        PACKET_HUFFMAN_200:
            storeReceivedChars(event, decode_output);
            PACTOR_TNC_DEBUG << "tnc_rx200cs2 PACKET_ASCII/Huffman";
            // become ISS if break requested
            if (send_break == 1)
            {
                send_break  = 0;
                PT_pkt_counter  = 0;
                send_BREAK_packet();
                // switch to next state
                tnc_state   = tnc_rx_to_tx200;
            }
            else
            {
                // switch to next state
                tnc_state   = tnc_rx200cs1;
            }
            break;
        default
        :
            break;
        }
        break;

    case
    tnc_rx_to_tx200:
        switch ((e_packet_type)event)
        {
        // packet acknowledged: send another data chunk
        case
        ACK_CS1:
            PT_pkt_counter++;
            // send hdr 0xAA packet
            tx_char_count = PTEnqueuePacket(PT_CS2,
                                            send_break);
            // put sent characters into an intermediate buffer
            saveSentChars(tx_char_count, PT_huffman_encode);
            // switch to next state
            tnc_state   = tnc_tx200cs1;
            break;

            // break received: store received data, then listen
        case
        PACKET_CS3_ASCII_100:
        case
        PACKET_CS3_HUFFMAN_100:
            storeReceivedChars(event, decode_output);
            // switch to next state
            tnc_state   = tnc_tx_to_rx200;
            break;

            // speedup to 200Bd
        case
        ACK_CS4:
            // append characters which have not been transmitted to tx buffer
            restoreSentChars(PT_huffman_encode);

            //PT_pkt_counter++;
            PT_pkt_counter  = 0;
            // reduce speed
            PT_speed        = PT_SPEED_100;
            // create a new packet
            tx_char_count = PTEnqueuePacket(PT_CS1,
                                            send_break);
            // switch to next state
            tnc_state   = tnc_tx200_to_tx100;
            break;
        default
        :
            break;
        }
        // become ISS
        engine_info->PT_Tx_Rx     = PT_TX;
        LED_args[0].led_index   = LED_IRS; // IRS LED
        LED_args[0].icon_color = GREY_ICON;
        LED_args[1].led_index   = LED_ISS; // ISS LED
        LED_args[1].icon_color = RED_ICON;
        emit setLED(2, LED_args);
        break;

    case
    tnc_rx200_to_rx100:
        switch ((e_packet_type)event)
        {
        case
        PACKET_BREAK_ASCII_100:
        case
        PACKET_BREAK_HUFFMAN_100:
            storeReceivedChars(event, decode_output);
            PT_pkt_counter  = 0;
            // switch to next state
            tnc_state   = tnc_rx_to_tx100;
            break;
        case
        PACKET_ASCII_100:
        case
        PACKET_HUFFMAN_100:
            storeReceivedChars(event, decode_output);
            PACTOR_TNC_DEBUG << "tnc_rx200_to_rx100 PACKET_ASCII/Huffman";
            // become ISS if break requested
            if (send_break == 1)
            {
                send_break  = 0;
                PT_pkt_counter  = 0;
                send_BREAK_packet();
                // switch to next state
                tnc_state   = tnc_rx_to_tx100;
            }
            else
            {
                tnc_state   = tnc_rx100cs1;
            }
            break;
        default
        :
            break;
        }

    case
    tnc_rx100_to_rx200cs2:
        switch ((e_packet_type)event)
        {
        case
        PACKET_BREAK_ASCII_200:
        case
        PACKET_BREAK_HUFFMAN_200:
            storeReceivedChars(event, decode_output);
            PT_pkt_counter  = 0;
            // switch to next state
            tnc_state   = tnc_rx_to_tx200;
            break;
        default
        :
            break;
        }
        break;

    case
    tnc_rx100_to_rx200cs1:
        switch ((e_packet_type)event)
        {
        case
        PACKET_BREAK_ASCII_200:
        case
        PACKET_BREAK_HUFFMAN_200:
            storeReceivedChars(event, decode_output);
            PT_pkt_counter  = 0;
            // switch to next state
            tnc_state   = tnc_rx_to_tx200;
            break;
        default
        :
            break;
        }
        break;

    case
    tnc_tx200_to_tx100:
        switch ((e_packet_type)event)
        {
        // packet acknowledged: send another data chunk
        case
        ACK_CS1:
            PT_pkt_counter++;
            m_pactorTNC->wait_callsign_ack      = false;
            // send hdr 0x55 packet
            tx_char_count = PTEnqueuePacket(PT_CS2,
                                            send_break);
            // put sent characters into an intermediate buffer
            saveSentChars(tx_char_count, PT_huffman_encode);
            // switch to next state
            tnc_state   = tnc_tx100cs1;
            break;
        default
        :
            break;
        }
        break;

    case
    tnc_tx100_to_tx200cs2:
        switch ((e_packet_type)event)
        {
        // packet acknowledged: send another data chunk
        case
        ACK_CS1:
            PT_pkt_counter++;
            // send hdr 0xAA packet
            tx_char_count = PTEnqueuePacket(PT_CS2,
                                            send_break);
            // put sent characters into an intermediate buffer
            saveSentChars(tx_char_count, PT_huffman_encode);
            // switch to next state
            tnc_state   = tnc_tx200cs1;
            break;

            // break received: store received data, then listen
        case
        PACKET_CS3_ASCII_200:
        case
        PACKET_CS3_HUFFMAN_200:
            storeReceivedChars(event, decode_output);
            // switch to next state
            tnc_state   = tnc_tx_to_rx200;
            break;
        default
        :
            break;
        }
        break;

    case
    tnc_tx100_to_tx200cs1:
        switch ((e_packet_type)event)
        {
        // packet acknowledged: send another data chunk
        case
        ACK_CS2:
            PT_pkt_counter++;
            // send hdr 0x55 packet
            tx_char_count = PTEnqueuePacket(PT_CS1,
                                            send_break);
            // put sent characters into an intermediate buffer
            saveSentChars(tx_char_count, PT_huffman_encode);
            // switch to next state
            tnc_state   = tnc_tx200cs2;
            break;

            // break received: store received data, then listen
        case
        PACKET_CS3_ASCII_200:
        case
        PACKET_CS3_HUFFMAN_200:
            m_pactorTNC->wait_callsign_ack      = false;
            storeReceivedChars(event, decode_output);
            // switch to next state
            m_pactorTNC->m_pactoraudioengine->thread_info.tx_data_or_CS3_burst    = false;
            m_pactorTNC->m_pactoraudioengine->thread_info.receiving_CS  = false;
            tnc_state   = tnc_tx_to_rx200;
            break;
        default
        :
            break;
        }
        break;

    case
    tnc_tx_to_rx100:
        switch ((e_packet_type)event)
        {
        case
        PACKET_BREAK_ASCII_100:
        case
        PACKET_BREAK_HUFFMAN_100:
            storeReceivedChars(event, decode_output);
            PT_pkt_counter  = 0;
            // switch to next state
            tnc_state   = tnc_rx_to_tx100;
            break;
        case
        PACKET_ASCII_100:
        case
        PACKET_HUFFMAN_100:
            storeReceivedChars(event, decode_output);
            PACTOR_TNC_DEBUG << "tnc_tx_to_rx100 PACKET_ASCII/Huffman";
            // become IRS
            engine_info->PT_Tx_Rx     = PT_RX;
            LED_args[0].led_index   = LED_IRS; // IRS LED
            LED_args[0].icon_color = RED_ICON;
            LED_args[1].led_index   = LED_ISS; // ISS LED
            LED_args[1].icon_color = GREY_ICON;
            emit setLED(2, LED_args);
            // switch to next state
            tnc_state   = tnc_rx100cs1;
            break;
        default
        :
            break;
        }
        break;

    case
    tnc_rx100cs1:
        switch ((e_packet_type)event)
        {
        case
        PACKET_BREAK_ASCII_100:
        case
        PACKET_BREAK_HUFFMAN_100:
            storeReceivedChars(event, decode_output);
            // switch to next state
            tnc_state   = tnc_rx_to_tx100;
            break;
        case
        PACKET_ASCII_100:
        case
        PACKET_HUFFMAN_100:
            storeReceivedChars(event, decode_output);
            PACTOR_TNC_DEBUG << "tnc_rx100cs1 PACKET_ASCII/Huffman";
            // become ISS if break requested
            if (send_break == 1)
            {
                send_break  = 0;
                PT_pkt_counter  = 0;
                send_BREAK_packet();
                // switch to next state
                tnc_state   = tnc_rx_to_tx100;
            }
            else
            {
                tnc_state   = tnc_rx100cs2;
            }
            break;
        default
        :
            break;
        }
        break;

    case
    tnc_rx100cs2:
        switch ((e_packet_type)event)
        {
        case
        PACKET_BREAK_ASCII_100:
        case
        PACKET_BREAK_HUFFMAN_100:
            storeReceivedChars(event, decode_output);
            // switch to next state
            tnc_state   = tnc_rx_to_tx100;
            break;
        case
        PACKET_ASCII_100:
        case
        PACKET_HUFFMAN_100:
            storeReceivedChars(event, decode_output);
            PACTOR_TNC_DEBUG << "tnc_rx100cs2 PACKET_ASCII/Huffman";
            // become ISS if break requested
            if (send_break == 1)
            {
                send_break  = 0;
                PT_pkt_counter  = 0;
                send_BREAK_packet();
                // switch to next state
                tnc_state   = tnc_rx_to_tx100;
            }
            else
            {
                tnc_state   = tnc_rx100cs1;
            }
            break;
        default
        :
            break;
        }
        break;

    case
    tnc_rx_to_tx100:
        switch ((e_packet_type)event)
        {
        // packet acknowledged: send another data chunk
        case
        ACK_CS1:
            PT_pkt_counter++;
            // send hdr 0xAA packet
            tx_char_count = PTEnqueuePacket(PT_CS2,
                                            send_break);
            // put sent characters into an intermediate buffer
            saveSentChars(tx_char_count, PT_huffman_encode);
            // switch to next state
            tnc_state   = tnc_tx100cs1;
            break;

            // break received: store received data, then listen
        case
        PACKET_CS3_ASCII_100:
        case
        PACKET_CS3_HUFFMAN_100:
            storeReceivedChars(event, decode_output);
            // switch to next state
            tnc_state   = tnc_tx_to_rx100;
            break;

            // speedup to 200Bd
        case
        ACK_CS4:
            PT_pkt_counter++;
            // reduce speed
            PT_speed        = PT_SPEED_200;
            // create a new packet
            tx_char_count = PTEnqueuePacket(PT_CS2,
                                            send_break);
            // switch to next state
            tnc_state   = tnc_tx100_to_tx200cs1;
            break;
        default
        :
            break;
        }
        // become ISS
        engine_info->PT_Tx_Rx     = PT_TX;
        LED_args[0].led_index   = LED_IRS; // IRS LED
        LED_args[0].icon_color = GREY_ICON;
        LED_args[1].led_index   = LED_ISS; // ISS LED
        LED_args[1].icon_color = RED_ICON;
        emit setLED(2, LED_args);
        break;

    case
    tnc_tx100cs1:
        // QRT request
        if (m_pactorTNC->QRTrequest == true)
        {
            send_QRT_packet();
            tnc_state   = tnc_QRTreqcs2;
            break;
        }
        switch ((e_packet_type)event)
        {
        // packet acknowledged: send another data chunk
        case
        ACK_CS2:
            PT_pkt_counter++;
            // send hdr 0x55 packet
            tx_char_count = PTEnqueuePacket(PT_CS1,
                                            send_break);
            // put sent characters into an intermediate buffer
            saveSentChars(tx_char_count, PT_huffman_encode);
            // switch to next state
            tnc_state   = tnc_tx100cs2;
            break;

            // break received: store received data, then listen
        case
        PACKET_CS3_ASCII_100:
        case
        PACKET_CS3_HUFFMAN_100:
            m_pactorTNC->wait_callsign_ack      = false;
            storeReceivedChars(event, decode_output);
            // switch to next state
            m_pactorTNC->m_pactoraudioengine->thread_info.tx_data_or_CS3_burst    = false;
            m_pactorTNC->m_pactoraudioengine->thread_info.receiving_CS  = false;
            tnc_state   = tnc_tx_to_rx100;
            break;

            // speedup to 200Bd
        case
        ACK_CS4:
            PT_pkt_counter++;
            // reduce speed
            PT_speed        = PT_SPEED_200;
            // create a new packet
            tx_char_count = PTEnqueuePacket(PT_CS1,
                                            send_break);
            // switch to next state
            tnc_state   = tnc_tx100_to_tx200cs2;
            break;
        default
        :
            break;
        }
        break;

    case
    tnc_tx100cs2:
        // QRT request
        if (m_pactorTNC->QRTrequest == true)
        {
            send_QRT_packet();
            tnc_state   = tnc_QRTreqcs1;
            break;
        }
        switch ((e_packet_type)event)
        {
        // packet acknowledged: send another data chunk
        case
        ACK_CS1:
            PT_pkt_counter++;
            // send hdr 0xAA packet
            tx_char_count = PTEnqueuePacket(PT_CS2,
                                            send_break);
            // put sent characters into an intermediate buffer
            saveSentChars(tx_char_count, PT_huffman_encode);
            // switch to next state
            tnc_state   = tnc_tx100cs1;
            break;

            // break received: store received data, then listen
        case
        PACKET_CS3_ASCII_100:
        case
        PACKET_CS3_HUFFMAN_100:
            storeReceivedChars(event, decode_output);
            // switch to next state
            tnc_state   = tnc_tx_to_rx100;
            break;

            // speedup to 200Bd
        case
        ACK_CS4:
            PT_pkt_counter++;
            // reduce speed
            PT_speed        = PT_SPEED_200;
            // create a new packet
            tx_char_count = PTEnqueuePacket(PT_CS2,
                                            send_break);
            // switch to next state
            tnc_state   = tnc_tx100_to_tx200cs1;
            break;
        default
        :
            break;
        }
        break;

    case
    tnc_listen_ARQ:
    case
    tnc_listen_unproto:
        switch ((e_packet_type)event)
        {
        // break received: store received data, then listen
        case PACKET_CS3_ASCII_100:
            break;
        case PACKET_CS3_ASCII_200:
            break;
        case PACKET_CS3_HUFFMAN_100:
            break;
        case PACKET_CS3_HUFFMAN_200:
            break;
        case PACKET_BREAK_ASCII_100:
            break;
        case PACKET_BREAK_ASCII_200:
            break;
        case PACKET_BREAK_HUFFMAN_100:
            break;
        case PACKET_BREAK_HUFFMAN_200:
            break;
        case PACKET_HUFFMAN_100:
            break;
        case PACKET_HUFFMAN_200:
            break;
        case PACKET_ASCII_100:
            break;
        case PACKET_ASCII_200:
            break;
        case PACKET_CONNECT_100:
            break;
        case PACKET_CONNECT_200:
            break;
        case PACKET_QRT_100:
            break;
        case PACKET_QRT_200:
            storeReceivedChars(event, decode_output);
            break;
        default:
            break;
        }
        break;

    case tnc_tx_unproto:
        switch ((e_packet_type)event)
        {
        case AUDIO_ENGINE_SENT_BURST:
            // create a new packet
            tx_char_count = PTEnqueuePacket(PT_CS_ANY,
                                            0);
            // put sent characters into an intermediate buffer (remove chars)
            saveSentChars(tx_char_count, PT_huffman_encode);
            m_pactorTNC->m_pactoraudioengine->thread_info.burst_gap_msecs = TIMER_UNPROTO_MS;
            m_pactorTNC->m_pactoraudioengine->thread_info.tx_data_or_CS3_burst      = true;
#ifdef PROTOCOL_DISPLAY
            PACTOR_TNC_DEBUG << "TMR PT_UnProto send pkt" << PT_pkt_counter << "sent" << tx_char_count
                             << "next" << global_date_time_mSecs + TIMER_UNPROTO_MS
                             << "speed" << PT_speed;
#endif
            PT_pkt_counter++;
            break;
        default:
            break;
        }
        break;

    case tnc_QRTreqcs1:
        switch ((e_packet_type)event)
        {
        case ACK_CS2:
            PACTOR_TNC_DEBUG << "tnc_QRTreqcs1 ack CS2";
            m_pactorTNC->QRTrequest = false;
            // reset slave mode
            m_pactorTNC->setIdleMode();
            // switch to next state
            tnc_state   = tnc_idle;
            break;
            // speedown to 100Bd
        case ACK_CS4:
            PACTOR_TNC_DEBUG << "tnc_QRTreqcs1 speeddown";
            // reduce speed
            PT_speed        = PT_SPEED_100;
            // create a new packet
            send_QRT_packet();
            break;
        default:
            break;
        }
        break;

    case tnc_QRTreqcs2:
        switch ((e_packet_type)event)
        {
        case ACK_CS1:
            PACTOR_TNC_DEBUG << "tnc_QRTreqcs2 ack CS1";
            m_pactorTNC->QRTrequest = false;
            // reset slave mode
            m_pactorTNC->setIdleMode();
            // switch to next state
            tnc_state   = tnc_idle;
            break;
            // speedown to 100Bd
        case ACK_CS4:
            PACTOR_TNC_DEBUG << "tnc_QRTreqcs2 speeddown";
            // reduce speed
            PT_speed        = PT_SPEED_100;
            // create a new packet
            send_QRT_packet();
            break;
        default:
            break;
        }
        break;

    case tnc_send_break:
        // send BREAK packet
        tx_char_count = PTEnqueuePacket(PT_CS1, 0);
        // put sent characters into an intermediate buffer
        saveSentChars(tx_char_count, PT_huffman_encode);
        break;
    default:
        break;
    }
}

//-----------------------------------------------------------------------------
// connection request. args are callsign and long path boolean (not required)
//-----------------------------------------------------------------------------

void PactorTNCThread::PTConnect(bool   arg_short_path = true)
{
    QByteArray     tx_qbyte_array;

    // start reception
    m_pactorTNC->m_pactoraudioengine->thread_info.window_state      = e_PT_rcv_win_not_init;
    m_pactorTNC->m_pactoraudioengine->thread_info.can_capture     = true;

    //    PACTOR_TNC_DEBUG << "short path ="<<arg_short_path;
    if (arg_short_path == true)
    {
        m_timer_ms                                                  = TIMER_SHORT_PATH_MS;
        m_pactorTNC->short_path                                   = SHORT_PATH_TIMING;
        m_pactorTNC->m_pactoraudioengine->thread_info.short_path  = SHORT_PATH_TIMING;
        m_pactorTNC->m_pactorDemodulator->m_thread->sample_sync_count_initial   = 2000;
    }
    else
    {
        m_timer_ms                                                  = TIMER_LONG_PATH_MS;
        m_pactorTNC->short_path                                   = LONG_PATH_TIMING;
        m_pactorTNC->m_pactoraudioengine->thread_info.short_path  = LONG_PATH_TIMING;
        m_pactorTNC->m_pactorDemodulator->m_thread->sample_sync_count_initial   = 2240;
    }

    PT_CS       = PT_CS1;
    // generate burst
    PT_speed        = PT_SPEED_200;
    PT_pkt_counter  = 0;
    send_conn_request_packet(arg_short_path);
    // remove sent chars
    tx_qbyte_array.resize(0);

    // ARQ mode
    /*    while (m_pactorTNC->m_pactoraudioengine->m_pactorDemodulator->m_state == Busy)
        {
    #ifdef WIN32
            Sleep(1);
    #else
            sleep(1);
    #endif
        };*/
    m_pactorTNC->m_pactorDemodulator->m_thread->ARQ_mode          = true;
    m_pactorTNC->m_pactorDemodulator->m_thread->memoryARQReset();
    // start time: now + 10ms
    m_pactorTNC->m_pactoraudioengine->thread_info.next_tx_time   = *m_pactorTNC->m_pactoraudioengine->thread_info.date_time_mSecs_addr + 200;
    m_pactorTNC->m_pactoraudioengine->thread_info.can_transmit    = true;
    m_pactorTNC->m_pactoraudioengine->thread_info.burst_gap_msecs = m_timer_ms;
    m_pactorTNC->m_pactoraudioengine->thread_info.tx_data_or_CS3_burst      = true;
    last_packet_number  = -1;
    return;
}

// send Pactor message text to client
void PactorTNCThread::pactorSendTextToClient(int len, const char *output_string)
{
    bool          c;
    QString       output_qstring;
    t_struct_msg *message_out;

    output_qstring  = QString(QByteArray(output_string, len));
    m_pactorTNC->m_pactor_session->m_inputText->setText(output_qstring);

    if (len > 0)
    {
        // copy data
        message_out     = new t_struct_msg;
        strncpy(message_out->data.text.chars, output_string, len);
        //    PACTOR_TNC_DEBUG <<m_MainWidget->m_cursor.selectedText();
        message_out->data.text.count = (qint16)len;
        message_out->opcode          = (qint16)e_text_receive;
        int blocksize   = (2 * sizeof(qint16)) + len;
        // send message to modem
        m_pactorTNC->sendMessageToClient(message_out, blocksize);

        // m_pactorTNC->m_pactor_panel->setLEDdirect(LED_TRANS, RED_ICON);
        c = QMetaObject::invokeMethod(m_pactorTNC->m_pactor_session, "setLEDdirect",
                                      Qt::QueuedConnection,
                                      Q_ARG(int, LED_TRANS),
                                      Q_ARG(int, RED_ICON));
        Q_ASSERT(c);
        //m_pactorTNC->m_pactor_panel->setLEDdirect(LED_IDLE, GREY_ICON);
        c = QMetaObject::invokeMethod(m_pactorTNC->m_pactor_session, "setLEDdirect",
                                      Qt::QueuedConnection,
                                      Q_ARG(int, LED_IDLE),
                                      Q_ARG(int, GREY_ICON));
        Q_ASSERT(c);
    }
    else
    {
        // m_pactorTNC->m_pactor_panel->setLEDdirect(LED_TRANS, GREY_ICON);
        c = QMetaObject::invokeMethod(m_pactorTNC->m_pactor_session, "setLEDdirect",
                                      Qt::QueuedConnection,
                                      Q_ARG(int, LED_TRANS),
                                      Q_ARG(int, GREY_ICON));
        Q_ASSERT(c);
        // m_pactorTNC->m_pactor_panel->setLEDdirect(LED_IDLE, GREEN_ICON);
        c = QMetaObject::invokeMethod(m_pactorTNC->m_pactor_session, "setLEDdirect",
                                      Qt::QueuedConnection,
                                      Q_ARG(int, LED_IDLE),
                                      Q_ARG(int, GREEN_ICON));
        Q_ASSERT(c);
    }
}



//-----------------------------------------------------------------------------
// Pactor Modem
//-----------------------------------------------------------------------------
PactorTNC::PactorTNC(QWidget *parent) : QDialog(parent)
  ,m_pactorTNCThread(new PactorTNCThread(this))
{

    m_pactor_session  = (PactorSession *)parent;

    //    PACTOR_TNC_DEBUG << "PactorTNC constructor"  ;
    comm_active                 = false;
    last_received_frequency     = 0;

    // waveform out of 500Hz filter
    store_filter500_waveform = 0;
    // default mode
    m_pactorTNCThread->PT_CS    = PT_CS1;
    m_pactorTNCThread->PT_phase = PT_PH_NORMAL;

    // intialize data to transmit buffer
    ascii_qbyte_array           = new QByteArray();
    ascii_qbyte_array->clear();
    tx_text_buffs.ascii_text    = ascii_qbyte_array;

    callsign_qbyte_array        = new QByteArray();
    callsign_qbyte_array->clear();
    tx_text_buffs.my_callsign      = callsign_qbyte_array;

    for ( quint32 i = 0; i < 10 ; i++ )
    {
        tx_text_buffs.aux_callsign[i] = new QByteArray();
    }
    tx_text_buffs.aux_call_count    = 0;

    remote_callsign_qbyte_array = new QByteArray();
    remote_callsign_qbyte_array->clear();
    tx_text_buffs.remote_callsign  = remote_callsign_qbyte_array;

    huffman_qbyte_array         = new QByteArray();
    huffman_qbyte_array->clear();
    tx_text_buffs.huffman       = huffman_qbyte_array;

    supervisor_qbyte_array      = new QByteArray();
    supervisor_qbyte_array->clear();
    tx_text_buffs.supervisor    = supervisor_qbyte_array;

    incoming_huffman_qbyte_array = new QByteArray();
    incoming_huffman_qbyte_array->clear();

    tx_removed_qbyte_array      = new QByteArray;
    tx_removed_qbyte_array->clear();
    tx_text_buffs.removed_chars  = tx_removed_qbyte_array;

    wait_callsign_ack           = false;
    wait_pactor_level           = false;
    QRTrequest                  = false;

    // create decoder object
    m_pactorDemodulator     = new PactorDemod(this);
    // create encoder object
    m_pactorModulator   = new PactorModulator(this);
    // create audio engine
    m_pactoraudioengine  = new PactorAudioEngine(this, m_pactorModulator, m_pactorDemodulator);
    // store audioengine linkage section (thread_info) to thread info pointer
    // read config settings
    readSettings();


    // initialize Pactor: allocate memory
    // read ARQ settings
    readARQCallSetting();

    // ARQ mode
    m_pactorDemodulator->m_thread->ARQ_mode      = false;
    m_pactorDemodulator->m_thread->memoryARQReset();

    // default callsign
    m_pactorDemodulator->setThreadCallsign(&tx_text_buffs);

    // create LED refresh timer
    m_LEDRefreshTimer           = new QTimer(this);

    // create Huffman text refresh timer
    m_HuffmanTimer              = new QTimer(this);
    m_HuffmanTimer->setSingleShot(true);

    m_pactorTNCThread->engine_info                  = &m_pactoraudioengine->thread_info;


    // connect slots and signals
    CHECKED_CONNECT(m_pactoraudioengine, SIGNAL(computeBurstAudio(unsigned char *, int, int, int, float *, int *)), m_pactorModulator, SLOT(slCalculateModFSK(unsigned char *, int, int, int, float *, int *)));
    CHECKED_CONNECT(m_pactorTNCThread, SIGNAL(calculateModFSK(unsigned char *, int, int, int, float *, int *)),     m_pactorModulator, SLOT(slCalculateModFSK(unsigned char *, int, int, int, float *, int *)));

    // connect change frequency to modulator setfrequency
    CHECKED_CONNECT(this, SIGNAL(ChangeModulatorFrequency(float)), m_pactorModulator, SLOT(SetCenterFrequency(float)));
    CHECKED_CONNECT(this, SIGNAL(createNewPacket(int, ccharptr)), m_pactorTNCThread, SLOT(PTEvent(int, ccharptr)));
    CHECKED_CONNECT(this, SIGNAL(setLED(int, t_set_LED_arg *)), this, SLOT(emitSetLED(int, t_set_LED_arg *)));
    CHECKED_CONNECT(m_pactorTNCThread, SIGNAL(setLED(int, t_set_LED_arg *)), this, SLOT(emitSetLED(int, t_set_LED_arg *)));
    CHECKED_CONNECT(m_pactorDemodulator->m_thread, SIGNAL(setLED(int, t_set_LED_arg *)), this, SLOT(emitSetLED(int, t_set_LED_arg *)));
    CHECKED_CONNECT(this, SIGNAL(setMode(int)), this, SLOT(changeMode(int)));
    CHECKED_CONNECT(m_LEDRefreshTimer, SIGNAL(timeout()), this, SLOT(LED_refresh_callback()));
    CHECKED_CONNECT(m_HuffmanTimer, SIGNAL(timeout()), this, SLOT(huffman_callback()));
//    CHECKED_CONNECT(this, SIGNAL(sendWaveformToPanel(ccharptr)), m_pactor_session->my_waveform, SLOT(newData(ccharptr)));
//    CHECKED_CONNECT(this, SIGNAL(setPanelAFC(bool)), m_pactor_session, SLOT(setAFCCallback(bool)));

    // start LED refresh timer
    m_pactorTNCThread->engine_info->LED_modified    = false;
    m_LEDRefreshTimer->start(50);

    switch (short_path)
    {
    case SHORT_PATH_TIMING: // short PATH
        m_pactorTNCThread->m_timer_ms                 = TIMER_SHORT_PATH_MS;
        m_pactoraudioengine->thread_info.cycle_time   = TIMER_SHORT_PATH_MS;
        m_pactorDemodulator->m_thread->sample_sync_count_initial   = 2000;
        break;

    case LONG_PATH_TIMING: // long PATH
        m_pactorTNCThread->m_timer_ms                 = TIMER_LONG_PATH_MS;
        m_pactoraudioengine->thread_info.cycle_time   = TIMER_LONG_PATH_MS;
        m_pactorDemodulator->m_thread->sample_sync_count_initial   = 2240;
        break;

    case UNPROTO_TIMING: // listen mode: delta T = 1000ms
        m_pactorTNCThread->m_timer_ms                 = TIMER_UNPROTO_MS;
        m_pactoraudioengine->thread_info.cycle_time   = TIMER_UNPROTO_MS;
        m_pactorDemodulator->m_thread->sample_sync_count_initial   = 1600;
        break;
    }

    // read settings
//    readAudioEngineSettings();
    m_pactorTNCThread->engine_info->short_path      = short_path;
    m_pactorTNCThread->engine_info->PT_Tx_Rx        = PT_RX;
    PT_mode                                         = PT_Idle;

    // packet counter for debug
    for ( quint32 i = 0 ; i < PACKET_END_COUNT; i++)
    {
        m_pactoraudioengine->packet_count[i] = 0;
    }

    // put demodulator into wait mode
    m_pactorDemodulator->m_thread->demodSetState(CORR_WAIT);
    // set demodulator center frequency
    const bool b = QMetaObject::invokeMethod(this, "SetCenterFrequencyInt",
                                             Qt::AutoConnection,
                                             Q_ARG(int, center_frequency_start),
                                             Q_ARG(int, e_freq_mod_init));
    Q_ASSERT(b);

    // start jack audio IO
    m_pactoraudioengine->startAudioIO();
    // set packet window time
    m_pactorDemodulator->m_thread->packet_start_window_msec = global_date_time_mSecs;
    // writeback config settings (if default values have been generated)
    writeAllSettings();
    // initialize IDLE mode
    setIdleMode();
    // display buffer management
    display_buffer_count            = 0;
    tile_sample_count               = 0;
}

PactorTNC::~PactorTNC()
{
    PACTOR_TNC_DEBUG << "PactorTNC destructor"  ;
    try
    {
        delete m_pactorModulator;
        delete m_pactorDemodulator;
    }
    catch
    (...)
    {
        PACTOR_TNC_DEBUG << "PactorTNC dealloc buffers error"  ;
    }
    //    m_pactor_panel->closeEvent((QCloseEvent *)NULL);
    //    m_pactor_panel->closeWidget();
}
//-----------------------------------------------------------------------------
// Public slots
//-----------------------------------------------------------------------------
void PactorTNC::LED_refresh_callback(void)
{
    quint32 i, j;

    /********************************************************************/
    /*  event source is LED refresh timer                        */
    /********************************************************************/
    // LED array modified by pactoraudioengine?
    if (m_pactoraudioengine->thread_info.LED_modified)
    {
        m_pactoraudioengine->thread_info.LED_modified    = false;
        j   = 0;
        for (i = 0; i < LED_COUNT; i++)
        {
            if (m_pactoraudioengine->thread_info.LED_args[i].is_modified)
            {
                m_pactoraudioengine->thread_info.LED_args[i].is_modified     = false;
                // change send LED status
                LED_args[j].led_index   = i; // LED number
                LED_args[j].icon_color = m_pactoraudioengine->thread_info.LED_args[i].icon_color;
                j++;
            }
        }
        emit setLED(j, LED_args);
    }
}


//-----------------------------------------------------------------------------
// Public methods
//-----------------------------------------------------------------------------
void  PactorTNC::emitSetLED(int count, t_set_LED_arg *LED_args)
{
    t_struct_msg    *message_out;
    // prepare LED status message
    message_out     = new t_struct_msg;
    message_out->data.LEDs.modified_LED_count    = (qint16)count;
    // set LED data
    for (int i = 0; i < count; i++)
    {
        message_out->data.LEDs.LED_arg[i].led_index  = LED_args[i].led_index;
        message_out->data.LEDs.LED_arg[i].icon_color  = LED_args[i].icon_color;
        // my panel update
        // m_pactor_panel->setLEDdirect(LED_args[i].led_index, LED_args[i].icon_color);
        bool c = QMetaObject::invokeMethod(m_pactor_session, "setLEDdirect",
                                           Qt::QueuedConnection,
                                           Q_ARG(int, LED_args[i].led_index),
                                           Q_ARG(int, LED_args[i].icon_color));
        Q_ASSERT(c);
    }
    //                                    opcode          modified LED count
    qint16 blocksize   = sizeof(qint16) + sizeof(qint16) +
            // LED count   LED argument
            count * sizeof(t_set_LED_arg);
    message_out->opcode                  = (qint16)e_LED_status;
    // emit message to UI
    // send message to modem
    sendMessageToClient(message_out, blocksize);
}

//-----------------------------------------------------------------------------
// set center frequency for modulator and demodulator integer argument
//-----------------------------------------------------------------------------
void PactorTNC::SetCenterFrequencyInt(int frequency, int source)
{
    bool    send_to_client = false;

    //    qDebug()<<"set freq INT, src"<<source<<"freq"<<frequency;
    switch (source)
    {
    case e_freq_mod_client:
        emit ChangeModulatorFrequency((float)frequency);
        m_pactorDemodulator->SetCenterFrequency((float)frequency);
        m_pactor_session->setfrequency(frequency);
        break;

    case e_freq_mod_demod:
        emit ChangeModulatorFrequency((float)frequency);
        m_pactor_session->setfrequency(frequency);
        send_to_client = true;
        break;

    case e_freq_mod_panel:
        emit ChangeModulatorFrequency((float)frequency);
        m_pactorDemodulator->SetCenterFrequency((float)frequency);
        send_to_client = true;
        break;

    case e_freq_mod_init:
        emit ChangeModulatorFrequency((float)frequency);
        m_pactorDemodulator->SetCenterFrequency((float)frequency);
        m_pactor_session->setfrequency(frequency);
        send_to_client = true;
        break;
    }
    if ( send_to_client )
    {
        // update frequency display on UI
        t_struct_msg  *message_out;
        // prepare frequency change message
        message_out     = new t_struct_msg;
        message_out->data.freq.center_frequency  = (qint16)frequency;
        qint16 blocksize   = 2 * sizeof(qint16);
        message_out->opcode                  = (qint16)e_frequency_value;
        // emit message to UI
        // send message to modem
        sendMessageToClient(message_out, blocksize);
    }
}

//-----------------------------------------------------------------------------
// change Pactor mode
//-----------------------------------------------------------------------------
void PactorTNC::changeMode(int buttonChecked)
{
    bool          short_path_connect;
    int           command = buttonChecked;
    t_struct_msg *message_out;

    PACTOR_TNC_DEBUG << "mode change" << command;
    //PACTOR_TNC_DEBUG << "Mode change. Audio out gain" << m_pactoraudioengine->thread_info.audio_out_gain;
    switch (command)
    {
    /********************************************************************/
    case e_mode_LISTEN_ARQ: // listen ARQ mode (timing 1250 ms)
    case e_mode_LISTEN: // listen unproto mode (timing 1000 ms)
        if (m_pactorTNCThread->PT_state == PT_IDLE)
        {
            m_pactorTNCThread->tnc_state       = tnc_listen_unproto;
            m_pactoraudioengine->thread_info.window_state      = e_PT_rcv_win_not_init;

            m_pactoraudioengine->thread_info.receiving_CS    = false;
            m_pactorTNCThread->last_packet_number  = -1;
            // change buttons state
            message_out     = new t_struct_msg;
            message_out->data.mode.mode_status    = 1 << e_mode_STOPALL;
            int blocksize   = sizeof(qint16) + sizeof(t_struct_mode);
            message_out->opcode                  = (qint16)e_mode_button;
            // send message
            // send message to modem
            sendMessageToClient(message_out, blocksize);

            PACTOR_TNC_DEBUG << "mode changed to LISTEN at" << global_date_time_mSecs
                             << "button enable" << bin << (1 << e_mode_STOPALL);
#ifdef DISPLAY_DEBUG_TO_UI
            // display debug text in UI
            output_string = QString("mode changed to LISTEN\n");
            pactorDebugSendString(output_string);
#endif
            m_pactorTNCThread->PT_current_request  = e_PTListen;
            m_pactorTNCThread->PT_state            = PT_LISTENING;
            tx_text_buffs.ascii_text->clear();
            m_pactorDemodulator->m_thread->demodSetState(CORR_SEARCH_SYNC);
            m_pactoraudioengine->thread_info.can_capture     = true;
            m_pactoraudioengine->thread_info.can_transmit    = false;
            m_pactoraudioengine->thread_info.unproto_mode   = false;
            m_pactoraudioengine->thread_info.new_burst       = false;
            // update audioengine
            m_pactorTNCThread->engine_info->PT_Tx_Rx         = PT_RX;
            PT_mode                                         = PT_Listen;
            if (command == e_mode_LISTEN)
            {
                PACTOR_TNC_DEBUG << "LISTEN 1000";
                // set to 1000 ms
                short_path                                  = UNPROTO_TIMING;
            }
            else
            {
                PACTOR_TNC_DEBUG << "LISTEN 1250";
                // set to short path only 1250 ms
                short_path                                  = SHORT_PATH_TIMING;
            }
            m_pactoraudioengine->thread_info.short_path  = short_path;
            switch (short_path)
            {
            case SHORT_PATH_TIMING: // short PATH
                m_pactoraudioengine->thread_info.cycle_time   = TIMER_SHORT_PATH_MS;
                m_pactorDemodulator->m_thread->sample_sync_count_initial   = 2000;
                break;

            case LONG_PATH_TIMING: // long PATH
                m_pactoraudioengine->thread_info.cycle_time   = TIMER_LONG_PATH_MS;
                m_pactorDemodulator->m_thread->sample_sync_count_initial   = 2240;
                break;
            case UNPROTO_TIMING: // listen mode: delta T = 1000ms
                m_pactoraudioengine->thread_info.cycle_time   = TIMER_UNPROTO_MS;
                m_pactorDemodulator->m_thread->sample_sync_count_initial   = 1600;
                break;
            }
            // ARQ mode
#ifdef LISTEN_MEMORY_ARQ_TEST
            if (command == e_mode_LISTEN_ARQ)
            {
                m_pactorDemodulator->m_thread->ARQ_mode     = true;
                m_pactorDemodulator->m_thread->memoryARQReset();
                //                    m_pactorDemodulator->m_thread->ARQ_mode     = false;
            }
            else
            {
                m_pactorDemodulator->m_thread->ARQ_mode     = false;
                m_pactorDemodulator->m_thread->memoryARQReset();
            }
#else
            m_pactorDemodulator->m_thread->ARQ_mode         = false;
            m_pactorDemodulator->m_thread->memoryARQReset();
#endif
            m_pactorTNCThread->PT_pkt_counter             = 0;
            m_pactorTNCThread->PT_wrong_CRC_count         = 0;
        }
        break;
        /********************************************************************/
    case
    e_mode_MASTER: // Connect
        if (m_pactorTNCThread->PT_state == PT_IDLE)
        {
            m_pactoraudioengine->thread_info.window_state      = e_PT_rcv_win_not_init;
            m_pactoraudioengine->thread_info.receiving_CS    = true;
            // change send LED status
            LED_args[0].led_index   = LED_ISS; // ISS LED
            LED_args[0].icon_color = RED_ICON;
            LED_args[1].led_index   = LED_IRS; // IRS LED
            LED_args[1].icon_color = GREY_ICON;
            emit setLED(2, LED_args);

            // change buttons state
            message_out     = new t_struct_msg;
            message_out->data.mode.mode_status    = 1 << e_mode_STOPALL;
            int blocksize   = sizeof(qint16) + sizeof(t_struct_mode);
            message_out->opcode                  = (qint16)e_mode_button;
            // send message to modem
            sendMessageToClient(message_out, blocksize);

            // connection attempt
            m_pactorTNCThread->PT_current_request  = e_PTMaster;
            m_pactorTNCThread->PT_pkt_counter      = 0;
            tx_text_buffs.ascii_text->clear();

            // short/long path decision
            // if callsign begins with '!' we shall use long path
            tx_text_buffs.remote_callsign->clear();
            if (*(const char *)m_PTremoteCall.toLatin1().constData() == '!')
            {
                short_path_connect  = false;
                // if first character is '!', remove it and change timing to long path
                QString temp_text(m_PTremoteCall.mid(1, -1));
                // create connection packet
                tx_text_buffs.remote_callsign->append(temp_text.toLatin1());
                m_pactoraudioengine->thread_info.cycle_time   = TIMER_LONG_PATH_MS;
                m_pactorDemodulator->m_thread->sample_sync_count_initial   = 2240;
            }
            else
            {
                short_path_connect  = true;
                tx_text_buffs.remote_callsign->append(m_PTremoteCall.toLatin1());
                m_pactoraudioengine->thread_info.cycle_time   = TIMER_SHORT_PATH_MS;
                m_pactorDemodulator->m_thread->sample_sync_count_initial   = 2000;
            }

            // change state to master call
            m_pactorTNCThread->tnc_state       = tnc_master_call;
            PACTOR_TNC_DEBUG << "mode changed to Connecting to"
                             << QString(tx_text_buffs.remote_callsign->constData()) << " at" << global_date_time_mSecs
                             << "short path" << short_path_connect
                             << "at" << *m_pactoraudioengine->thread_info.date_time_mSecs_addr
                             << "button enable" << bin << (1 << e_mode_STOPALL);
#ifdef DISPLAY_DEBUG_TO_UI
            // display debug text in UI
            output_string = QString("mode changed to Connecting to %1\n")
                    .arg(m_PTremoteCall);
            pactorDebugSendString(output_string);
#endif
            m_pactorDemodulator->m_thread->demodSetState(CORR_SEARCH_SYNC);
            m_pactoraudioengine->thread_info.tx_data_or_CS3_burst      = true;
            // start phase: positive
            m_pactoraudioengine->thread_info.next_phase     = PT_PH_INVERTED;
            m_pactoraudioengine->thread_info.expected_phase = USE_ANY_PHASE;
            // packet counter
            m_pactoraudioengine->thread_info.pkt_counter    = 0;
            m_pactoraudioengine->thread_info.unproto_mode   = false;
            m_pactoraudioengine->thread_info.new_burst       = true;
            m_pactoraudioengine->thread_info.transmit_state  = WAITING;
            // create connection packet
            m_pactorTNCThread->PTConnect(short_path_connect);
#ifdef VOX_CONTROL
            // set vox start and stop time if vox enabled
            if (m_pactoraudioengine->thread_info.vox_channel != VOX_CHANNEL_DISABLED)
            {
                m_pactoraudioengine->thread_info.next_vox_start_time = m_pactoraudioengine->thread_info.next_tx_time
                        + m_pactoraudioengine->thread_info.vox_start_delay;
                m_pactoraudioengine->thread_info.next_vox_stop_time = m_pactoraudioengine->thread_info.next_tx_time + (qint64)(((double)m_pactoraudioengine->thread_info.burst_audio_count * SAMPLE_48K_DURATION_US / 1000.0) + 0.5)
                        + m_pactoraudioengine->thread_info.vox_stop_delay;
                m_pactoraudioengine->thread_info.vox_state      = WAITING;
            }
#endif
            // update audioengine
            m_pactorTNCThread->engine_info->PT_Tx_Rx         = PT_TX;
            PT_mode                                         = PT_MASTER;
            // limit connection speed to 100 Bd
            if (m_pactorTNCThread->PT_speed_limit == PT_SPEED_100)
            {
                m_pactorTNCThread->PT_speed = PT_SPEED_100;
            }
            else
            {
                m_pactorTNCThread->PT_speed = PT_SPEED_200;
            }
        }
        break;
        /********************************************************************/
        // Un Proto
    case
    e_mode_UNPROTO:
        if (m_pactorTNCThread->PT_state == PT_IDLE)
        {
            m_pactoraudioengine->thread_info.window_state      = e_PT_rcv_win_not_init;
            m_pactoraudioengine->thread_info.receiving_CS    = false;
            // change send LED status
            LED_args[0].led_index   = LED_ISS; // ISS LED
            LED_args[0].icon_color  = RED_ICON;
            LED_args[1].led_index   = LED_IRS; // IRS LED
            LED_args[1].icon_color  = GREY_ICON;
            emit setLED(2, LED_args);

            // change buttons state
            message_out     = new t_struct_msg;
            message_out->data.mode.mode_status    = 1 << e_mode_STOPALL;
            int blocksize   = sizeof(qint16) + sizeof(t_struct_mode);
            message_out->opcode                  = (qint16)e_mode_button;
            // send message to modem
            sendMessageToClient(message_out, blocksize);

            PACTOR_TNC_DEBUG << "mode changed to UNPROTO broadcast at" << global_date_time_mSecs
                             << "button enable" << bin << (1 << e_mode_STOPALL);
#ifdef DISPLAY_DEBUG_TO_UI
            // display debug text in UI
            output_string = QString("mode changed to UNPROTO broadcast\n");
            pactorDebugSendString(output_string);
#endif
            m_pactorTNCThread->PT_state            = PT_UNPROTO;
            m_pactorTNCThread->PT_current_request  = e_PTUnprotoSend;
            tx_text_buffs.ascii_text->clear();

            m_pactorTNCThread->unproto_retransmit  = 0;
            // ARQ mode
            m_pactorDemodulator->m_thread->ARQ_mode      = false;
            m_pactorDemodulator->m_thread->memoryARQReset();
            // set timer value
            m_pactorTNCThread->m_timer_ms      = TIMER_UNPROTO_MS;
            m_pactoraudioengine->thread_info.cycle_time   = TIMER_UNPROTO_MS;
            m_pactorDemodulator->m_thread->demodSetState(CORR_SEARCH_SYNC);
            //                m_pactoraudioengine->thread_info.can_capture     = false;
            m_pactoraudioengine->thread_info.can_transmit    = true;
            m_pactoraudioengine->thread_info.tx_data_or_CS3_burst      = true;
            // start time: now + 10ms
            m_pactoraudioengine->thread_info.next_tx_time   = *m_pactoraudioengine->thread_info.date_time_mSecs_addr + 200;
#ifdef VOX_CONTROL
            // set vox start and stop time if vox enabled
            if (m_pactoraudioengine->thread_info.vox_channel != VOX_CHANNEL_DISABLED)
            {
                m_pactoraudioengine->thread_info.next_vox_start_time = m_pactoraudioengine->thread_info.next_tx_time
                        + m_pactoraudioengine->thread_info.vox_start_delay;
                m_pactoraudioengine->thread_info.next_vox_stop_time = m_pactoraudioengine->thread_info.next_tx_time + (qint64)(((double)m_pactoraudioengine->thread_info.burst_audio_count * SAMPLE_48K_DURATION_US / 1000.0) + 0.5)
                        + m_pactoraudioengine->thread_info.vox_stop_delay;
                m_pactoraudioengine->thread_info.vox_state      = WAITING;
            }
#endif
            // start phase: positive
            m_pactoraudioengine->thread_info.next_phase     = PT_PH_INVERTED;
            m_pactoraudioengine->thread_info.expected_phase = USE_ANY_PHASE;
            // packet counter
            m_pactoraudioengine->thread_info.pkt_counter    = 0;
            // set unproto params
            m_pactoraudioengine->thread_info.unproto_mode   = true;
            m_pactoraudioengine->thread_info.unproto_repeat = m_pactorTNCThread->unproto_repeat;
            m_pactoraudioengine->thread_info.unproto_count  = 0;
            m_pactoraudioengine->thread_info.new_burst       = true;
            m_pactoraudioengine->thread_info.transmit_state  = WAITING;
            // limit connection speed to 100 Bd
            if (m_pactorTNCThread->PT_speed_limit == PT_SPEED_100)
            {
                m_pactorTNCThread->PT_speed = PT_SPEED_100;
            }
            else
            {
                m_pactorTNCThread->PT_speed = PT_SPEED_200;
            }
            // create first packet
            emit createNewPacket(AUDIO_ENGINE_SENT_BURST, NULL);
            // update state machine state
            m_pactorTNCThread->tnc_state                    = tnc_tx_unproto;
            // update audioengine
            m_pactorTNCThread->engine_info->PT_Tx_Rx         = PT_TX;
            PT_mode                                         = PT_UnProto;
        }
        break;

        // wait for call (slave)
    case e_mode_SLAVE:
        if ( m_pactorTNCThread->PT_state == PT_IDLE ) setSlaveMode();  // set slave mode
        break;

        // disconnected
    case SWITCH_TO_IDLE:
        PACTOR_TNC_DEBUG << "disconnected or timeout";
        if (m_pactorTNCThread->PT_current_request == e_PTServer)
        {
            setSlaveMode();
            break;
        }
        else
        {
            m_pactorTNCThread->PT_state                = PT_DISCONNECTED;
        }

        // stop all, go to Idle mode
    case e_mode_STOPALL:
        if ((m_pactorTNCThread->PT_state == PT_WAITING_FOR_CALL)
                || (m_pactorTNCThread->PT_state == PT_UNPROTO)
                || (m_pactorTNCThread->PT_state == PT_LISTENING)
                || (m_pactorTNCThread->PT_state == PT_DISCONNECTED))
        {
            PACTOR_TNC_DEBUG << "mode changed to IDLE. Pkt counter"
                             << m_pactorTNCThread->PT_pkt_counter << " at" << global_date_time_mSecs
                             << "enable" << bin << (1 << e_mode_MASTER
                                                    | 1 << e_mode_LISTEN_ARQ
                                                    | 1 << e_mode_LISTEN
                                                    | 1 << e_mode_UNPROTO
                                                    | 1 << e_mode_SLAVE);
#ifdef DISPLAY_DEBUG_TO_UI
            // display debug text in UI
            output_string = QString("mode changed to IDLE. Pkt counter %1\n")
                    .arg(m_pactorTNCThread->PT_pkt_counter);
            pactorDebugSendString(output_string);
#endif
            // Idle
            setIdleMode();
        }
        break;

    default:
        break;
    }
}

//-----------------------------------------------------------------------------
// break button clicked
//-----------------------------------------------------------------------------
void PactorTNC::breakRequest(void)
{
    PACTOR_TNC_DEBUG << "break_request tnc l 3192--------------------------------------";
    // receiving station: send a CS3
    if (m_pactorTNCThread->engine_info->PT_Tx_Rx == PT_TX)
    {
        //        m_pactoraudioengine->thread_info.send_break  = true;
        m_pactorTNCThread->send_break             = 1;
    }
    // sending station: set break bit in next packet
    if (m_pactorTNCThread->engine_info->PT_Tx_Rx == PT_RX)
    {
        //        m_pactoraudioengine->thread_info.send_break  = true;
        m_pactorTNCThread->send_break             = 1;
    }
}

//-----------------------------------------------------------------------------
// do slaveserver mode setup or cleanup
//-----------------------------------------------------------------------------
void PactorTNC::setSlaveMode(void)
{
    t_struct_msg    *message_out;

    m_pactoraudioengine->thread_info.window_state      = e_PT_rcv_win_not_init;
    m_pactoraudioengine->thread_info.receiving_CS    = false;
    // change send LED status
    LED_args[0].led_index   = LED_ISS; // ISS LED
    LED_args[0].icon_color = GREY_ICON;
    LED_args[1].led_index   = LED_IRS; // IRS LED
    LED_args[1].icon_color = RED_ICON;
    LED_args[2].led_index   = LED_ERROR; // error LED
    LED_args[2].icon_color = GREY_ICON;
    LED_args[3].led_index   = LED_TX; // Send LED
    LED_args[3].icon_color = GREY_ICON;
    emit setLED(4, LED_args);

    m_pactorTNCThread->last_packet_number  = -1;
    // change buttons state
    message_out     = new t_struct_msg;
    message_out->data.mode.mode_status    = 1 << e_mode_STOPALL;
    int blocksize   = sizeof(qint16) + sizeof(t_struct_mode);
    message_out->opcode                  = (qint16)e_mode_button;
    // send message to modem
    sendMessageToClient(message_out, blocksize);

    PACTOR_TNC_DEBUG << "mode changed to SLAVE at" << global_date_time_mSecs
                     << "button enable" << bin << (1 << e_mode_STOPALL);

    m_pactorTNCThread->PT_current_request  = e_PTServer;
#ifdef DISPLAY_DEBUG_TO_UI
    // display debug text in UI
    output_string = QString("mode changed to SLAVE");
    pactorDebugSendString(output_string);
#endif
    m_pactorTNCThread->engine_info->PT_Tx_Rx            = PT_RX;
    PT_mode                                             = PT_SLAVE;
    short_path                                          = SHORT_PATH_TIMING;
    m_pactorDemodulator->m_thread->sample_sync_count_initial   = 2000;
    m_pactorTNCThread->PT_state                         = PT_WAITING_FOR_CALL;
    m_pactorTNCThread->PT_wrong_CRC_count               = 0;
    m_pactorDemodulator->m_thread->demodSetState(CORR_SEARCH_SYNC);
    m_pactoraudioengine->thread_info.can_capture     = true;
    m_pactoraudioengine->thread_info.can_transmit    = true;
    m_pactoraudioengine->thread_info.tx_data_or_CS3_burst      = false;
    m_pactoraudioengine->thread_info.next_tx_time    = LLONG_MAX;
    m_pactoraudioengine->thread_info.tx_time_update = 0;
    m_pactoraudioengine->thread_info.vox_state      = WAITING;
    m_pactoraudioengine->thread_info.transmit_state  = WAITING;
    // set vox start time if enabled
    m_pactoraudioengine->thread_info.next_vox_start_time = 0;
    m_pactoraudioengine->thread_info.vox_state      = WAITING;
    m_pactoraudioengine->thread_info.unproto_mode   = false;
    m_pactoraudioengine->thread_info.current_CS        = CS2_ID;
    m_pactoraudioengine->thread_info.last_CS         = CS1_ID;
    m_pactoraudioengine->thread_info.new_burst       = false;
    // start phase: positive
    m_pactoraudioengine->thread_info.next_phase     = PT_PH_INVERTED;
    m_pactoraudioengine->thread_info.expected_phase = USE_ANY_PHASE;
    // ARQ mode
    m_pactorDemodulator->m_thread->ARQ_mode      = true;
    m_pactorDemodulator->m_thread->memoryARQReset();
    // update state machine state
    m_pactorTNCThread->tnc_state       = tnc_slave_call;
}

//-----------------------------------------------------------------------------
// do idle mode setup or cleanup
//-----------------------------------------------------------------------------
void PactorTNC::setIdleMode()
{
    t_struct_msg    *message_out;

    m_pactorDemodulator->m_thread->ARQ_mode      = false;
    m_pactorDemodulator->m_thread->memoryARQReset();

    m_pactoraudioengine->thread_info.receiving_CS    = false;
    m_pactoraudioengine->thread_info.current_CS     = CS2_ID;
    m_pactoraudioengine->thread_info.last_CS         = CS1_ID;
    m_pactorTNCThread->PT_state                = PT_IDLE;
    m_pactorTNCThread->engine_info->PT_Tx_Rx    = PT_RX;
    PT_mode                                     = PT_Idle;
    m_pactorTNCThread->PT_wrong_CRC_count      = 0;
    m_pactorTNCThread->PT_speed                = PT_SPEED_100;
    m_pactorTNCThread->PT_CS                   = PT_CS1;
    m_pactorTNCThread->PT_phase                = PT_PH_NORMAL;
    m_pactorTNCThread->PT_pkt_counter          = 0;

    m_pactorTNCThread->packet_counter          = 0;
    m_pactorTNCThread->PT_CS4_in_a_row_count   = 0;
    m_pactorTNCThread->PT_huffman_encode       = false;
    m_pactorTNCThread->PT_current_request      = e_PTPendingNone;
    m_pactorTNCThread->last_packet_number       = -1;
    // stop writing to audio output
    m_pactoraudioengine->thread_info.transmit_state = STOPPED;
    // start phase: positive
    m_pactoraudioengine->thread_info.next_phase     = PT_PH_INVERTED;
    m_pactoraudioengine->thread_info.expected_phase = USE_ANY_PHASE;

    m_pactorDemodulator->m_thread->demodSetState(CORR_WAIT);
    //                m_pactoraudioengine->thread_info.can_capture     = false;
    m_pactoraudioengine->thread_info.next_tx_time    = LLONG_MAX;
    m_pactoraudioengine->thread_info.tx_time_update = 0;
    m_pactoraudioengine->thread_info.vox_state      = WAITING;
    // set vox start time if enabled
    m_pactoraudioengine->thread_info.next_vox_start_time = 0;
    m_pactoraudioengine->thread_info.unproto_mode   = false;
    m_pactoraudioengine->thread_info.new_burst       = false;
    m_pactoraudioengine->thread_info.window_state      = e_PT_rcv_win_not_init;

    // change send LED status
    for (int i = 0; i < 9; i++)
    {
        LED_args[i].led_index           = i; // switch off all led LED
        LED_args[i].icon_color = GREY_ICON;
    }
    LED_args[2].icon_color = GREEN_ICON;
    emit setLED(9, LED_args);

    // change UI buttons state
    message_out     = new t_struct_msg;
    message_out->data.mode.mode_status    = 1 << e_mode_MASTER
                                                 | 1 << e_mode_LISTEN_ARQ
                                                 | 1 << e_mode_LISTEN
                                                 | 1 << e_mode_UNPROTO
                                                 | 1 << e_mode_SLAVE;
    int blocksize   = sizeof(qint16) + sizeof(t_struct_mode);
    message_out->opcode                  = (qint16)e_mode_button;
    // send message to modem
    sendMessageToClient(message_out, blocksize);
}

// send text to UI
void PactorTNC::pactorDebugSendString(const QString &output_string)
{
    int         count;
    t_struct_msg            *message_out;

    count                   = output_string.size();
    if (count > 0)
    {
        message_out     = new t_struct_msg;
        // copy data
        strncpy(message_out->data.text.chars, (const char *)output_string.toLatin1().constData(), count);
        //    PACTOR_TNC_DEBUG <<m_MainWidget->m_cursor.selectedText();
        message_out->data.text.count = (qint16)count;
        message_out->opcode          = e_debug_text;
        qint16 blocksize   = (2 * sizeof(qint16)) + count;
        // send message to modem
        sendMessageToClient(message_out, blocksize);
    }
}

// waveform sampling for digiscope display
void    PactorTNC::sendWaveform(const float *samples,
                                int count,
                                float divider)
{
    bool        send_wave_to_panel = m_pactor_session->m_RBwave->isChecked();
    qint8       temp_char;
    quint32     i, j;
    const int   *intptr;
    static bool  new_buffer = true;
    static int   downsample_cnt = 0;
    static int   samples_count   = 0;
    static t_struct_msg *message_out;

    // do nothing if neither debug nor visualization
    if ((send_wave_to_panel == false) && (debug_waveform == false))
    {
        return;
    }
    if (count > 0)
    {
        j                           = samples_count;
        if (new_buffer)
        {
            message_out             = new t_struct_msg;
            new_buffer                  = false;
        }
        // encoder stores ints
        if (waveform_src == e_waveform_encoder_output)
        {
            if (debug_waveform == true)
            {
                intptr      = (const int *)samples;
                for (i = 0; i < count; i += waveform_rate, j++)
                {
                    temp_char   = (qint8)((float)intptr[i] * waveform_ratio / divider);
                    message_out->data.scope_wave.wave[j]    = temp_char;
                }
                for (; j < LONG_BURST_SAMPLE_COUNT; j++)
                {
                    message_out->data.scope_wave.wave[j]    = (qint8)0;
                }
            }
        }
        else
        {
            for (i = 0; i < count; i++, j++)
            {
                temp_char   = (qint8)(-samples[i] * waveform_ratio);
                if (debug_waveform == true)
                {
                    message_out->data.scope_wave.wave[j]    = temp_char;
                }
                // panel waveform
                if ((send_wave_to_panel)
                        && (m_pactor_session->m_RBFreezeWave->isChecked() == false)
                        && ((downsample_cnt >= waveform_rate)
                            || (m_pactor_session->m_RBlowSpWave->isChecked() == false)))
                {
                    downsample_cnt  = 0;
                    if (tile_sample_count == 0)
                    {
                        wave_buffer     = new char[TILE_SIZE_SPL];
                    }
                    // send 20 samples chunks
                    wave_buffer[tile_sample_count]   = temp_char;
                    tile_sample_count++;
                    if (tile_sample_count >= TILE_SIZE_SPL)
                    {
                        tile_sample_count               = 0;
                        emit sendWaveformToPanel(wave_buffer);
                    }
                }
                downsample_cnt++;
            }
        }
        if (debug_waveform == true)
        {
            samples_count                           = j;
            message_out->data.scope_wave.count  = (qint16)j;
            qint16 blocksize   = (2 * sizeof(qint16)) + (j * sizeof(qint8));
            if (samples_count >= WAVEFORM_CHUNK_SIZE)
            {
                message_out->opcode     = (qint16)e_waveform_data;
                // send message to modem
                // send message to modem
                sendMessageToClient(message_out, blocksize);
                samples_count   = 0;
                new_buffer      = true;
            }
        }
    }
}

// communications with client (sending function)
inline void PactorTNC::sendMessageToClient(t_struct_msgptr message_out, qint16 blocksize)
{
    if (comm_active)
    {
        // invoke method because signal/slot does not work
        qRegisterMetaType<t_struct_msgptr>("t_struct_msgptr");
        /* TODO: convert to UDP message to TCP client GUI
        const bool b = QMetaObject::invokeMethod(m_pactor_session->my_UI_client, "processMessageFromPactorTNC",
                                                 Qt::QueuedConnection,
                                                 Q_ARG(t_struct_msgptr, message_out),
                                                 Q_ARG(qint16, blocksize));
        */
        // comm LED update
        // m_pactor_panel->setLEDdirect(LED_COMM, RED_ICON);
        bool c = QMetaObject::invokeMethod(m_pactor_session, "setLEDdirect",
                                           Qt::QueuedConnection,
                                           Q_ARG(int, LED_COMM),
                                           Q_ARG(int, RED_ICON));
        Q_ASSERT(c);
    }
}

void PactorTNC::saveSettings()
{
    QSettings settings("OpenModem", QLatin1String("Config"));
    //  === Pactor settings   === //
    settings.beginGroup(QLatin1String("PactorTNC"));
    settings.endGroup();
}

//  called before showing the dialog so that current settings are retrieved and displayed  //
void PactorTNC::readSettings()
{
    QSettings settings("OpenModem", QLatin1String("Config"));
    //  === Pactor settings   === //
    settings.beginGroup(QLatin1String("PactorTNC"));
    // TNC window position
    window_visible  = settings.value("visible", true).toBool();
    window_point    = settings.value("pos", QPoint(100, 100)).toPoint();
    window_size     = settings.value("size", QSize(680, 470)).toSize();
    window_rectangle    = QRect(window_point, window_size);
    PACTOR_TNC_DEBUG << "openWidget" << window_rectangle;
//    m_pactor_session->setGeometry(window_rectangle);
    center_frequency_start     = settings.value("CenterFrequency", 2200).toUInt();
    center_frequency_AFC_limit = settings.value("AFC_limit", 0).toInt();
    AFC_on                     = settings.value("AFC_on", true).toBool();
    AGC_on                     = settings.value("AGC_on", true).toBool();
    m_pactoraudioengine->thread_info.AGC_on    = AGC_on;
    m_pactorTNCThread->PT_state          = (e_PT_status)settings.value("state", (int)PT_IDLE).toInt();
    PT_mode                              = (e_PT_mode)settings.value("mode", (int)PT_Idle).toInt();
    m_pactorTNCThread->PT_speed          = (e_PT_speed)settings.value("speed", (int)PT_SPEED_100).toInt();
    m_pactorTNCThread->PT_speed_limit    = (e_PT_speed)settings.value("connect_speed_limit", (int)PT_SPEED_200).toInt();
    //    qDebug()<<"settings read speed limit"<<m_pactorTNCThread->PT_speed_limit;

    m_pactorTNCThread->PT_allow_huffman  = (bool)settings.value("huffman_encode", false).toBool();
    short_path                           = (e_PT_status)settings.value("path", (int)SHORT_PATH_TIMING).toInt();
    m_pactorTNCThread->UCMD[0]   = settings.value("UCMD0", (int)UCMD0_DEFAULT).toInt();
    m_pactorTNCThread->UCMD[1]   = settings.value("UCMD1", (int)UCMD1_DEFAULT).toInt();
    m_pactorTNCThread->UCMD[2]   = settings.value("UCMD2", (int)UCMD2_DEFAULT).toInt();
    m_pactorTNCThread->UCMD[3]   = settings.value("UCMD3", (int)UCMD3_DEFAULT).toInt();
    waveform_src                   = settings.value("WavfrmSrc", (int)e_waveform_demod_data).toInt();
    waveform_ratio                 = settings.value("WaveRatio", (double)700.0).toDouble();
    waveform_rate                  = settings.value("WaveRate", (int)5).toInt();
    switch (waveform_src)
    {
        case
                e_waveform_output_filter_phase:
            // update 500Hz filter waveform switch
            store_filter500_waveform    = 1; // 1=phase
            break;
        case
                e_waveform_output_filter_ampl:
            // update 500Hz filter waveform switch
            store_filter500_waveform    = 2; // 2=amplitude
            break;
        default
                :
            // update 500Hz filter waveform switch
            store_filter500_waveform    = 0; // 0=none
            break;
    }
    debug_waveform = settings.value("DebugWavfrm", (bool)false).toBool();
    settings.endGroup();
    // Jack audio
    settings.beginGroup(QLatin1String("JackAudio"));
    m_pactoraudioengine->thread_info.hardware_audio_latency    = settings.value("hard_audio_latency", (int)0).toInt();
    m_pactoraudioengine->jack_input_offset     = settings.value("input_offset", (int)0).toInt();
    m_pactoraudioengine->jack_output_offset    = settings.value("output_offset", (int)0).toInt();
    m_pactoraudioengine->thread_info.audio_in_gain   = (float)(settings.value("input_gain", (int)DEF_INPUT_GAIN).toInt()) / 100.0;
    int raw_gain   = settings.value("output_gain", (int)DEF_OUTPUT_GAIN).toInt();
    m_pactoraudioengine->thread_info.audio_out_gain   = (float)(settings.value("output_gain", (int)DEF_OUTPUT_GAIN).toInt()) / 1000.0;
    PACTOR_TNC_DEBUG << "readAudioEngineSettings. Audio out gain" << m_pactoraudioengine->thread_info.audio_out_gain
                     << "raw gain" << raw_gain;
    settings.endGroup();

#ifdef VOX_CONTROL
    // vox tone generator
    settings.beginGroup(QLatin1String("VOXcontrol"));
    m_pactoraudioengine->thread_info.vox_channel   = settings.value("channel", (int)VOX_CHANNEL_DISABLED).toInt();
    // parameter is in milliseconds, convert to microseconds
    m_pactoraudioengine->thread_info.vox_start_delay     = settings.value("start", (int)VOX_CHANNEL_START).toInt();
    m_pactoraudioengine->thread_info.vox_stop_delay      = settings.value("stop", (int)VOX_CHANNEL_STOP).toInt();
    m_pactoraudioengine->thread_info.vox_frequency = settings.value("frequency", (int)VOX_CHANNEL_FREQUENCY).toInt();
    PACTOR_TNC_DEBUG << "vox_channel" << m_pactoraudioengine->thread_info.vox_channel
                     << "vox_start" << m_pactoraudioengine->thread_info.vox_start_delay
                     << "vox_stop" << m_pactoraudioengine->thread_info.vox_stop_delay
                     << "vox_frequency" << m_pactoraudioengine->thread_info.vox_frequency;
    // check frequency value
    if (m_pactoraudioengine->thread_info.vox_frequency < VOX_CHANNEL_MIN_FREQUENCY)
    {
        m_pactoraudioengine->thread_info.vox_frequency = VOX_CHANNEL_MIN_FREQUENCY;
    }
    if (m_pactoraudioengine->thread_info.vox_frequency > VOX_CHANNEL_MAX_FREQUENCY)
    {
        m_pactoraudioengine->thread_info.vox_frequency = VOX_CHANNEL_MAX_FREQUENCY;
    }
    settings.endGroup();
#endif
}

void PactorTNC::readARQCallSetting()
{
    QStringList     string_list;
    QString         wk_string;

    int i = 0;
    readSettings();
    string_list = m_PTAuxCall.split(',', QString::SkipEmptyParts, Qt::CaseInsensitive);

    foreach(wk_string, string_list)
    {
        tx_text_buffs.aux_callsign[i]->clear();
        tx_text_buffs.aux_callsign[i]->append(wk_string.toLatin1());
        tx_text_buffs.aux_call_count    = i;
        PACTORUI_DEBUG << "ind M" << i << wk_string;
        i++;
    }

    tx_text_buffs.my_callsign->clear();
    tx_text_buffs.my_callsign->append(m_PTMyCall.toLatin1());
    tx_text_buffs.remote_callsign->clear();
    tx_text_buffs.remote_callsign->append(m_PTremoteCall.toLatin1());

    m_pactorDemodulator->setThreadCallsign(&tx_text_buffs);
}


//  read audio engine settings
void PactorTNC::readAudioEngineSettings(void)
{
    //  === Pactor settings   === //
    settings->beginGroup(QLatin1String("PactorTNC"));
}

void PactorTNC::writeAllSettings()
{
    QSettings settings("OpenModem", QLatin1String("Config"));
    //  === Pactor settings   === //
    settings.beginGroup(QLatin1String("PactorTNC"));
    settings.beginGroup(QLatin1String("AFC"));
    settings.setValue("CenterFrequency", center_frequency_start);
    settings.setValue("AFC_limit", center_frequency_AFC_limit);
    settings.setValue("AFC_on", AFC_on);
    settings.setValue("AGC_on", AGC_on);
    settings.endGroup();
    // default mode
    settings.beginGroup(QLatin1String("Mode"));
    settings.setValue("state", m_pactorTNCThread->PT_state);
    settings.setValue("mode", m_pactorTNCThread->engine_info->PT_Tx_Rx);
    settings.setValue("speed", m_pactorTNCThread->PT_speed);
    settings.setValue("connect_speed_limit", m_pactorTNCThread->PT_speed_limit);
    //    qDebug()<<"settings write all speed limit"<<m_pactorTNCThread->PT_speed_limit;
    settings.setValue("huffman_encode", m_pactorTNCThread->PT_allow_huffman);
    settings.setValue("path", short_path);
    // ARQ mode
    settings.setValue("ARQ", m_pactorDemodulator->m_thread->ARQ_mode);
    settings.endGroup();
    // Jack audio
    settings.beginGroup(QLatin1String("JackAudio"));
    settings.setValue("hard_audio_latency", m_pactoraudioengine->thread_info.hardware_audio_latency);
    settings.setValue("input_offset", m_pactoraudioengine->jack_input_offset);
    settings.setValue("output_offset", m_pactoraudioengine->jack_output_offset);
    settings.setValue("input_gain", (int)(m_pactoraudioengine->thread_info.audio_in_gain * 100.0));
    settings.setValue("output_gain", (int)(m_pactoraudioengine->thread_info.audio_out_gain * 1000.0));
    settings.endGroup();
    // UCMDs
    settings.beginGroup(QLatin1String("UCMD"));
    settings.setValue("UCMD0", m_pactorTNCThread->UCMD[0]);
    settings.setValue("UCMD1", m_pactorTNCThread->UCMD[1]);
    settings.setValue("UCMD2", m_pactorTNCThread->UCMD[2]);
    settings.setValue("UCMD3", m_pactorTNCThread->UCMD[3]);
    settings.endGroup();
#ifdef VOX_CONTROL
    // vox tone generator
    settings.beginGroup(QLatin1String("VOXcontrol"));
    settings.setValue("channel", m_pactoraudioengine->thread_info.vox_channel);
    settings.setValue("start", m_pactoraudioengine->thread_info.vox_start_delay);
    settings.setValue("stop", m_pactoraudioengine->thread_info.vox_stop_delay);
    settings.setValue("frequency", m_pactoraudioengine->thread_info.vox_frequency);
    settings.endGroup();
#endif
    // default callsign
    settings.beginGroup(QLatin1String("CallSigns"));
    settings.setValue("MyCall", m_PTMyCall);
    settings.setValue("AuxCall", m_PTAuxCall);
    settings.setValue("remoteCall", m_PTremoteCall);
    settings.endGroup();
    // waveform source
    settings.beginGroup(QLatin1String("WaveDisplay"));
    // waveform source for digiscope
    settings.setValue("WavfrmSrc", waveform_src);
    settings.setValue("WaveRatio", (int)waveform_ratio);
    settings.setValue("WaveRate", waveform_rate);
    settings.setValue("DebugWavfrm", debug_waveform);
    settings.endGroup();
    settings.endGroup();
    // the file is updated immediately
    settings.sync();  // so that the file is updated immediately
}

void PactorTNC::writeSettings(int setting_idx)
{
    QSettings settings("OpenModem", QLatin1String("Config"));
    //  === Pactor settings   === //
    settings.beginGroup(QLatin1String("PactorTNC"));
    switch (setting_idx)
    {
        case e_frequency_value:
            settings.setValue("CenterFrequency", center_frequency_start);
            settings.setValue("AFC_limit", center_frequency_AFC_limit);
            settings.setValue("AFC_on", AFC_on);
            settings.setValue("AGC_on", AGC_on);
            break;

        case e_mode_button:
            // default mode
            settings.setValue("state", m_pactorTNCThread->PT_state);
            settings.setValue("mode", m_pactorTNCThread->engine_info->PT_Tx_Rx);
            settings.setValue("speed", m_pactorTNCThread->PT_speed);
            settings.setValue("connect_speed_limit", m_pactorTNCThread->PT_speed_limit);
            //            qDebug()<<"settings write e_mode speed limit"<<m_pactorTNCThread->PT_speed_limit;
            settings.setValue("huffman_encode", m_pactorTNCThread->PT_allow_huffman);
            settings.setValue("path", short_path);
            // ARQ mode
            settings.setValue("ARQ", m_pactorDemodulator->m_thread->ARQ_mode);
            break;

        case e_waveform_source:
            settings.setValue("WavfrmSrc", waveform_src);
            settings.setValue("WaveRatio", (int)waveform_ratio);
            settings.setValue("WaveRate", waveform_rate);
            settings.setValue("DebugWavfrm", debug_waveform);
            break;

        case e_send_mycall:
            settings.setValue("MyCall", m_PTMyCall);
            settings.setValue("AuxCall", m_PTAuxCall);
            break;

        case e_remote_call:
            settings.setValue("remoteCall", m_PTremoteCall);
            break;

        case e_general_parameters:
            //settings.setValue("state", m_pactorTNCThread->PT_state);
            //settings.setValue("mode", m_pactorTNCThread->engine_info->PT_mode);
            settings.setValue("speed", m_pactorTNCThread->PT_speed);
            settings.setValue("connect_speed_limit", m_pactorTNCThread->PT_speed_limit);
            //            qDebug()<<"settings write e_gen speed limit"<<m_pactorTNCThread->PT_speed_limit;
            settings.setValue("huffman_encode", m_pactorTNCThread->PT_allow_huffman);
            //settings.setValue("path", short_path);
            // ARQ mode
            //settings.setValue("ARQ", m_pactorDemodulator.m_thread->ARQ_mode);
            // UCMDs
            settings.beginGroup(QLatin1String("UCMD"));
            settings.setValue("UCMD0", m_pactorTNCThread->UCMD[0]);
            settings.setValue("UCMD1", m_pactorTNCThread->UCMD[1]);
            settings.setValue("UCMD2", m_pactorTNCThread->UCMD[2]);
            settings.setValue("UCMD3", m_pactorTNCThread->UCMD[3]);
            break;

        case e_jack_audio:
            settings.setValue("hard_audio_latency", m_pactoraudioengine->thread_info.hardware_audio_latency);
            settings.setValue("input_offset", m_pactoraudioengine->jack_input_offset);
            settings.setValue("output_offset", m_pactoraudioengine->jack_output_offset);
            settings.setValue("input_gain", (int)(m_pactoraudioengine->thread_info.audio_in_gain * 100.0));
            settings.setValue("output_gain", (int)(m_pactoraudioengine->thread_info.audio_out_gain * 1000.0));
            break;

        case e_vox_tone:
#ifdef VOX_CONTROL
            settings.setValue("channel", m_pactoraudioengine->thread_info.vox_channel);
            settings.setValue("start", m_pactoraudioengine->thread_info.vox_start_delay);
            settings.setValue("stop", m_pactoraudioengine->thread_info.vox_stop_delay);
            settings.setValue("frequency", m_pactoraudioengine->thread_info.vox_frequency);
#endif
            break;

        default:
            break;
    }
    settings.endGroup();
    settings.sync();
}
