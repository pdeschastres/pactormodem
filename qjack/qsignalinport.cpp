/*
    QJack make you connect with jack soundserver system
    Copyright (C) 2011  Alessandro Siniscalchi <asiniscalchi@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#include <QtCore/QDebug>
#include <QtCore/QMetaType>

#include "qsignalinport.h"

Q_DECLARE_METATYPE(QVector<float>)

namespace QJack {

QSignalInPort::QSignalInPort(QString name, QJackClient *parent): QObject(parent), JackInPort(name)
{
    qRegisterMetaType<QVector<float> >("QVector<float>");
}

void QSignalInPort::setData(void *data, quint32 nFrames)
{
    //qDebug() << Q_FUNC_INFO << ": maxSize = " << maxSize;

    const float *fdata = reinterpret_cast<const float *>(data);

    if (quint32(m_data.size()) != nFrames)
    m_data.resize(nFrames);

    for (quint32 i = 0; i < nFrames ; ++i)
        m_data[i] = fdata[i];

    emit jackData(m_data);
}

} // namespace

