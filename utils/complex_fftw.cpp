#include "complex_fft.hpp"

ComplexFFT::ComplexFFT(int size, int direction, QObject *parent) : QObject(parent)
{
    this->size = size;
    inData = new std::complex<double>[size];
    outData = new std::complex<double>[size];
    this->plan = fftw_plan_dft_1d(size, (fftw_complex*)inData, (fftw_complex*)outData, direction, FFTW_ESTIMATE);
}

ComplexFFT::~ComplexFFT()
{
    fftw_destroy_plan(this->plan);
    delete inData;
    delete outData;
}

void ComplexFFT::execute()
{
        fftw_execute(this->plan);
}
