#include "crcwinmor.hpp"

crc8_t CrcWinMOR::crc8(byte_vec inbyts)
{
    //TODO: This algorithm is very inefficient. Replace it with one of those
    // table-driven jobs.
    crc8_t polynomial = 0xc6;
    crc8_t sregister = 0xff;       // Seed value.

    for (int ibyte = 0; ibyte < inbyts.size(); ibyte++ )
    {
        byte_t byt = inbyts[ibyte];
        for (int ibit = 7; ibit >= 0; ibit-- )
        {
            crc8_t p = (sregister & 0x80 ? polynomial : 0x00);
            sregister = ( (sregister<<1) | ((byt>>ibit) & 0x01) ) ^ p;
        }
    }
    return sregister;
}

void CrcWinMOR::appendCrc8(byte_vec &inbyts)
{
    crc8_t result = crc8(inbyts);
    inbyts.push_back( result );
}

crc16_t CrcWinMOR::crc16(byte_vec inbyts)
{
    //TODO: This algorithm is very inefficient. Replace it with one of those
    // table-driven jobs.
    crc16_t polynomial = 0x8810;
    crc16_t sregister = 0xffff;       // Seed value.
    for (int ibyte = 0; ibyte < inbyts.size(); ibyte++ )
    {
        byte_t byt = inbyts[ibyte];
        for ( int ibit = 7; ibit >= 0; ibit-- )
        {
            crc16_t p = (sregister & 0x8000 ? polynomial : 0x0000);
            sregister = ( (sregister<<1) | ((byt>>ibit) & 0x0001) ) ^ p;
        }
    }
    return sregister;
}

void CrcWinMOR::appendCrc16(byte_vec &inbyts)
{
    crc16_t result = crc16(inbyts);
    inbyts.push_back( (result>>8) & 0x00ff );
    inbyts.push_back( result & 0x00ff );
}
