#include <complex>

#include "inverse_complex_fft.hpp"

InverseComplexFFT::InverseComplexFFT(int size, QObject *parent) : ComplexFFT(size, FFTW_BACKWARD, parent)
{
    //
}

void InverseComplexFFT::clearInData()
{
    for ( int i = 0; i < this->size; i++ )
    {
        this->inData[i] = std::complex<double>(0.,0.);
    }
}

