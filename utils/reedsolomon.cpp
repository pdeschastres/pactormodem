#include "reedsolomon.hpp"

///
/// \brief ReedSolomon::ReedSolomon A Reed-Solomon encoder/decoder
///
/// \param mm Symbol size in bits
/// \param tt Number of errors to be corrected.
/// \param parent QObject parent.
///

int ReedSolomon::pp4[5] = {1, 1, 0, 0, 1};
int ReedSolomon::pp8[9] = {1, 0, 1, 1, 1, 0, 0, 0, 1};

ReedSolomon::ReedSolomon(int mm, int tt, QObject *parent) : QObject(parent)
{
    // Generate and initialize an R-S encoder/decoder with the given params.
    // Can't change the params - if you want different params make another one.


    this->mm = mm;              // Number of bits per symbol.
    this->tt = tt;              // Number of errors to be corrected.
    this->nn = pow(2,mm) - 1;   // Length of codeword.
    this->kk = nn - 2*tt;

    // Problem: can't actually do this.
    // TODO: Throw an exception or something, anything.
    if ( tt < 1 or tt > (nn-1) / 2 )   return;

    //    qDebug("RS: mm: %d, tt: %d, nn: %d, kk: %d",mm,tt,nn,kk);

    // Set the irreducible polynomial coefficients (look it up!)
    // TODO: this is bs - use a map or something. Sheesh.
    if ( mm == 4 )
    {
        pp = pp4;
    }
    else if ( mm == 8 )
    {
        pp = pp8;
    }
    else
    {
        // TODO: Should crap out here.
    }

    //    alpha_to = new int[nn+1];
    alpha_to = std::vector<index_t>(nn+1);
    //    index_of = new int[nn+1];
    index_of =  std::vector<index_t>(nn+1);
    //    gg = new int[nn-kk+1];
    gg = std::vector<index_t>(nn - kk);
    //    recd = new int[nn];
    //    recd = symbol_vec(nn);

    //    data = new int[kk];
    //    bb = new int[nn-kk];

    //    int alpha_to [nn+1], index_of [nn+1], gg [nn-kk+1] ;
    //    int recd [nn], data [kk], bb [nn-kk] ;

    //    qDebug("pp:");
    //    for(int i=0;i<mm+1;i++) qDebug("%d: %d",i,pp[i]);

    generate_gf();
    //    qDebug("gf: alpha_to index_of");
    //    for(int i=0;i<nn+1;i++) qDebug("%d: %d %d",i,alpha_to[i],index_of[i]);

    gen_poly();
    //    qDebug("gg:");
    //    for(int i=0;i<nn-kk;i++) qDebug("%d: %d ",i,gg[i]);
}

void ReedSolomon::generate_gf()
/* generate GF(2**mm) from the irreducible polynomial p(X) in pp[0]..pp[mm]
   lookup tables:  index->polynomial form   alpha_to[] contains j=alpha**i;
                   polynomial form -> index form  index_of[j=alpha**i] = i
   alpha=2 is the primitive element of GF(2**mm)
*/
{
    register int i, mask ;

    mask = 1 ;
    alpha_to[mm] = 0 ;
    for (i=0; i<mm; i++)
    { alpha_to[i] = mask ;
        index_of[alpha_to[i]] = i ;
        if (pp[i]!=0)
            alpha_to[mm] ^= mask ;
        mask <<= 1 ;
    }
    index_of[alpha_to[mm]] = mm ;
    mask >>= 1 ;
    for (i=mm+1; i<nn; i++)
    { if (alpha_to[i-1] >= mask)
            alpha_to[i] = alpha_to[mm] ^ ((alpha_to[i-1]^mask)<<1) ;
        else alpha_to[i] = alpha_to[i-1]<<1 ;
        index_of[alpha_to[i]] = i ;
    }
    index_of[0] = -1 ;
}


void ReedSolomon::gen_poly()
/* Obtain the generator polynomial of the tt-error correcting, length
  nn=(2**mm -1) Reed Solomon code  from the product of (X+alpha**i), i=1..2*tt
*/
{
    register int i,j ;

    gg[0] = 2 ;    /* primitive element alpha = 2  for GF(2**mm)  */
    gg[1] = 1 ;    /* g(x) = (X+alpha) initially */
    for (i=2; i<=nn-kk; i++)
    { gg[i] = 1 ;
        for (j=i-1; j>0; j--)
            if (gg[j] != 0)  gg[j] = gg[j-1]^ alpha_to[(index_of[gg[j]]+i)%nn] ;
            else gg[j] = gg[j-1] ;
        gg[0] = alpha_to[(index_of[gg[0]]+i)%nn] ;     /* gg[0] can never be zero */
    }
    /* convert gg[] to index form for quicker encoding */
    for (i=0; i<=nn-kk; i++)  gg[i] = index_of[gg[i]] ;
}


symbol_vec ReedSolomon::encode(const symbol_vec &indata)
/* 'indata' contains a vector of symbols no longer than kk symbols.
   It is padded out with zeroes on the front to length kk then
   encoded according to the following comment. The result bb is returned
   from the function as a vector.
*/
/* take the string of symbols in data[i], i=0..(k-1) and encode systematically
   to produce 2*tt parity symbols in bb[0]..bb[2*tt-1]
   data[] is input and bb[] is output in polynomial form.
   Encoding is done by using a feedback shift register with appropriate
   connections specified by the elements of gg[], which was generated above.
   Codeword is   c(X) = data(X)*X**(nn-kk)+ b(X)          */
{

    symbol_vec data;
    data.fill(0,kk);    // Resize to kk and fill with zero.

    symbol_vec bb;
    bb.resize(nn-kk);

    // Move data from indata to last part of data working array.
    for(int i=0;i<indata.size();i++) data[kk-indata.size()+i] = indata[i];

    /* original code follows. */
    register int i,j ;
    int feedback ;

    for (i=0; i<nn-kk; i++)   bb[i] = 0 ;
    for (i=kk-1; i>=0; i--)
    {  feedback = index_of[data[i]^(0xff&bb[nn-kk-1])] ; // DJC added 0xff&
        if (feedback != -1)
        { for (j=nn-kk-1; j>0; j--)
                if (gg[j] != -1)
                    bb[j] = bb[j-1]^alpha_to[(gg[j]+feedback)%nn] ;
                else
                    bb[j] = bb[j-1] ;
            bb[0] = alpha_to[(gg[0]+feedback)%nn] ;
        }
        else
        { for (j=nn-kk-1; j>0; j--)
                bb[j] = bb[j-1] ;
            bb[0] = 0 ;
        } ;
    } ;
    /* End of original code, results now in bb. */


    /* Now repack data into out. */
    // TODO: this whole thing stinks. Rip it up.
    symbol_vec out;
    out.resize(indata.size()+bb.size());

    for (i=0;i<(int)indata.size();i++)
    {
        out[i] = data[i + data.size() - indata.size()];
    }
    for(i=0;i<(int)bb.size();i++)
    {
        out[indata.size()+i] = bb[i];
    }

    return out;
}



symbol_vec ReedSolomon::decode(const symbol_vec &rxdata)
/* Use the parity in rxdata to decode a potentially altered data block.
 * Allow for the possibility that the data input to rxdata is shorter
 * than the full wordlength. If so, restore zeroes on the front that were
 * removed in the encode.
 */
/* assume we have received bits grouped into mm-bit symbols in recd[i],
   i=0..(nn-1),  and recd[i] is index form (ie as powers of alpha).
   We first compute the 2*tt syndromes by substituting alpha**i into rec(X) and
   evaluating, storing the syndromes in s[i], i=1..2tt (leave s[0] zero) .
   Then we use the Berlekamp iteration to find the error location polynomial
   elp[i].   If the degree of the elp is >tt, we cannot correct all the errors
   and hence just put out the information symbols uncorrected. If the degree of
   elp is <=tt, we substitute alpha**i , i=1..n into the elp to get the roots,
   hence the inverse roots, the error location numbers. If the number of errors
   located does not equal the degree of the elp, we have more than tt errors
   and cannot correct them.  Otherwise, we then solve for the error value at
   the error location and correct the error.  The procedure is that found in
   Lin and Costello. For the cases where the number of errors is known to be too
   large to correct, the information symbols as received are output (the
   advantage of systematic encoding is that hopefully some of the information
   symbols will be okay and that if we are in luck, the errors are in the
   parity part of the transmitted codeword).  Of course, these insoluble cases
   can be returned as error flags to the calling routine if desired.   */
{
    // Figure out if the data was shortened. If we had started with a
    // a full block then we would have data of total length nn, of which
    // nn-kk at the end would be parity words. If the data was shortened
    // then there will still be nn-kk parity but fewer than nn-kk data symbols
    // on the front. So we can figure it out...

    // Sanity check the size of the data:
    if(((int)rxdata.size()) > nn || ((int)rxdata.size()) <= 1 + 2*tt)
    {
        // TODO: throw an exception or something, hey?
        return (symbol_vec)0;
    }


    // Regardless we'll need a decoding vector of size nn:
    // This has to be signed because he uses -1 as a code value.
    // That's kinda cheesy but I don't think I'll try to fix it, thanks.
    std::vector<int> recd(nn);
    // Load up decoding vector and convert to indexed form, whatever that means.
    for ( int i = 0; i < nn - ((int)rxdata.size()); i++ )
    {
        recd[i] = index_of[0]; // I think this is always -1 but who cares?
    }
    for ( int i = 0; i < ((int)rxdata.size()); i++ )
    {
        recd[i+nn-rxdata.size()] = index_of[0xff&rxdata[i]]; // 0xff& added by DJC
    }

    // Original code follows.
    //*******************************************************************
    register int i,j,u,q ;
    int elp[nn-kk+2][nn-kk], d[nn-kk+2], l[nn-kk+2], u_lu[nn-kk+2], s[nn-kk+1] ;
    int count=0, syn_error=0, root[tt], loc[tt], z[tt+1], err[nn], reg[tt+1] ;

    /* first form the syndromes */
    for ( qint32 i = 1; i <= (nn - kk); i++ )
    {
        s[i] = 0 ;
        for ( j = 0; j < nn; j++ )
            if ( recd[j] != -1 )
                s[i] ^= alpha_to[(recd[j]+i*j)%nn] ;      /* recd[j] in index form */
        /* convert syndrome from polynomial form to index form  */
        if ( s[i] != 0 )  syn_error=1 ;        /* set flag if non-zero syndrome => error */
        s[i] = index_of[s[i]] ;
    } ;

    if ( syn_error ) /* if errors, try and correct */
    {
        /* compute the error location polynomial via the Berlekamp iterative algorithm,
           following the terminology of Lin and Costello :   d[u] is the 'mu'th
           discrepancy, where u='mu'+1 and 'mu' (the Greek letter!) is the step number
           ranging from -1 to 2*tt (see L&C),  l[u] is the
           degree of the elp at that step, and u_l[u] is the difference between the
           step number and the degree of the elp.
        */
        /* initialise table entries */
        d[0] = 0 ;           /* index form */
        d[1] = s[1] ;        /* index form */
        elp[0][0] = 0 ;      /* index form */
        elp[1][0] = 1 ;      /* polynomial form */
        for ( i = 1; i < (nn - kk); i++ )
        {   elp[0][i] = -1 ;   /* index form */
            elp[1][i] = 0 ;   /* polynomial form */
        }
        l[0] = 0 ;
        l[1] = 0 ;
        u_lu[0] = -1 ;
        u_lu[1] = 0 ;
        u = 0 ;

        do
        {
            u++ ;
            if ( d[u] == -1 )
            {
                l[u+1] = l[u] ;
                for ( i = 0; i <= l[u]; i++ )
                {
                    elp[u+1][i] = elp[u][i] ;
                    elp[u][i] = index_of[elp[u][i]] ;
                }
            }
            else
                /* search for words with greatest u_lu[q] for which d[q]!=0 */
            {
                q = u-1 ;
                while ( (d[q]==-1) && (q>0) ) q-- ;
                /* have found first non-zero d[q]  */
                if ( q > 0 )
                {
                    j = q;
                    do
                    {
                        j--;
                        if ( (d[j]!=-1) && (u_lu[q] < u_lu[j]) )  q = j ;
                    } while ( j > 0 );
                };

                /* have now found q such that d[u]!=0 and u_lu[q] is maximum */
                /* store degree of new elp polynomial */
                if ( l[u] > l[q] + u - q )
                    l[u+1] = l[u] ;
                else
                    l[u+1] = l[q]+u-q ;

                /* form new elp(x) */
                for ( i = 0; i < nn - kk; i++ )
                    elp[u+1][i] = 0 ;
                for ( i = 0; i <= l[q]; i++ )
                    if (elp[q][i]!=-1)  elp[u+1][i+u-q] = alpha_to[(d[u]+nn-d[q]+elp[q][i])%nn] ;
                for ( i = 0; i <= l[u]; i++ )
                {
                    elp[u+1][i] ^= elp[u][i] ;
                    elp[u][i] = index_of[elp[u][i]] ;  /*convert old elp value to index*/
                }
            }
            u_lu[u+1] = u-l[u+1];

            /* form (u+1)th discrepancy */
            if ( u < nn - kk )    /* no discrepancy computed on last iteration */
            {
                if ( s[u+1]!=-1 )
                    d[u+1] = alpha_to[s[u+1]] ;
                else
                    d[u+1] = 0 ;
                for ( i = 1; i <= l[u+1]; i++ )
                    if ( (s[u+1-i] != -1) && (elp[u+1][i] != 0) )  d[u+1] ^= alpha_to[(s[u+1-i]+index_of[elp[u+1][i]])%nn] ;
                d[u+1] = index_of[d[u+1]] ;    /* put d[u+1] into index form */
            }
        } while ( (u < nn - kk) && (l[u+1] <= tt) );
        u++ ;
        if ( l[u] <= tt ) /* can correct error */
        {
            /* put elp into index form */
            for ( i = 0; i <= l[u]; i++ ) elp[u][i] = index_of[elp[u][i]] ;
            /* find roots of the error location polynomial */
            for ( i = 1; i <= l[u]; i++ )
                reg[i] = elp[u][i] ;
            count = 0 ;
            for ( i = 1; i <= nn; i++ )
            {
                q = 1 ;
                for ( j = 1; j <= l[u]; j++ )
                    if ( reg[j] != -1 )
                    {
                        reg[j] = (reg[j]+j)%nn;
                        q ^= alpha_to[reg[j]];
                    };
                if ( !q )        /* store root and error location number indices */
                {
                    root[count] = i;
                    loc[count] = nn-i ;
                    count++ ;
                };
            } ;
            if ( count == l[u] )    /* no. roots = degree of elp hence <= tt errors */
            {
                /* form polynomial z(x) */
                for ( i = 1; i <= l[u]; i++ ) /* Z[0] = 1 always - do not need */
                {
                    if ( (s[i] != -1) && (elp[u][i] != -1) )
                        z[i] = alpha_to[s[i]] ^ alpha_to[elp[u][i]] ;
                    else if ( (s[i] != -1) && (elp[u][i] == -1) )
                        z[i] = alpha_to[s[i]] ;
                    else if ( (s[i] == -1) && (elp[u][i] != -1) )
                        z[i] = alpha_to[elp[u][i]] ;
                    else
                        z[i] = 0 ;
                    for ( j = 1; j < i; j++ )
                        if ( (s[j] != -1) && (elp[u][i-j] != -1) )  z[i] ^= alpha_to[(elp[u][i-j] + s[j])%nn] ;
                    z[i] = index_of[z[i]] ;         /* put into index form */
                } ;

                /* evaluate errors at locations given by error location numbers loc[i] */
                for ( i = 0; i < nn; i++ )
                {
                    err[i] = 0 ;
                    if ( recd[i]!=-1 )        /* convert recd[] to polynomial form */
                        recd[i] = alpha_to[recd[i]] ;
                    else
                        recd[i] = 0 ;
                }
                for ( i = 0; i < l[u]; i++ )    /* compute numerator of error term first */
                {
                    err[loc[i]] = 1;       /* accounts for z[0] */
                    for ( j = 1; j <= l[u]; j++ )
                        if ( z[j] != -1 )
                            err[loc[i]] ^= alpha_to[(z[j] + j * root[i]) % nn];
                    if ( err[loc[i]] != 0 )
                    {
                        err[loc[i]] = index_of[err[loc[i]]] ;
                        q = 0 ;     /* form denominator of error term */
                        for ( j = 0; j < l[u]; j++ )
                            if ( j != i )  q += index_of[1^alpha_to[(loc[j]+root[i])%nn]] ;
                        q = q % nn ;
                        err[loc[i]] = alpha_to[(err[loc[i]] - q + nn) % nn] ;
                        recd[loc[i]] ^= err[loc[i]] ;  /*recd[i] must be in polynomial form */
                    }
                }
            }
            else    /* no. roots != degree of elp => >tt errors and cannot solve */
                for ( i = 0; i < nn; i++ )        /* could return error flag if desired */
                    if ( recd[i] != -1 )        /* convert recd[] to polynomial form */
                        recd[i] = alpha_to[recd[i]] ;
                    else
                        recd[i] = 0 ;     /* just output received codeword as is */
        }
        else         /* elp has degree has degree >tt hence cannot solve */
        {
            for ( i = 0; i < nn; i++ )       /* could return error flag if desired */
                if (recd[i]!=-1)        /* convert recd[] to polynomial form */
                    recd[i] = alpha_to[recd[i]] ;
                else
                    recd[i] = 0 ;     /* just output received codeword as is */
        }
    }
    else       /* no non-zero syndromes => no errors: output received codeword */
    {
        for ( i = 0; i < nn; i++ )
            if ( recd[i] != -1 )        /* convert recd[] to polynomial form */
                recd[i] = alpha_to[recd[i]] ;
            else
                recd[i] = 0 ;
    }

    //**************************************************************************
    //End of original code.

    // This is what we are going to return, hopefully the original data.
    // nn-kk is the number of parity words.
    const int datasize = rxdata.size() - (nn-kk);

    symbol_vec out;
    out.resize(datasize);

    // We have the data in the last part of recd: data+parity.
    // The length of parity is nn-kk.
    for ( int i = 0; i < datasize; i++ )
    {
        out[i] = recd[ i + (nn - rxdata.size()) ];
    }
    return out;
}
