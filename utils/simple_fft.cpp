/****************************************************************************
** Copyright (C) 2012 OpenMOR development team.   All rights reserved.
** Contact: Project Leader Ken AD5XJ (ad5xj@arrl.net)
** or visit the project web site openmor.sourceforge.net
**
** This file is part of the OpenMOR Project in the modem or RMSLink executable.
**
** This software is intended for use on amateur radio only and is not a
** commercial product.  The software of the OpenMOR project is
** primarily useful for technical and scientific purposes in non-commercial
** applications only.   Further, this software is distributed in the hope
** that it will be useful.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** === No Commercial Usage ===
** This file contains Open Source code and may not be distributed except as
** outlined in the terms and conditions of the GNU GPL License v3 included
** with this source code.
**
**  === GNU Lesser General Public License Usage ===
** This file may be used under the terms of the GNU Lesser
** General Public License version 3 or higher as published by the
** Free Software Foundation and appearing in the file LICENSE.TXT included
** in the packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 3 (or higher)
** requirements will be met: http://www.gnu.org/licenses/gpl-3.0.txt.
**
** In addition, portions of this software are products originating with
** Nokia and other authors. Each author retains their rights as described
** in their respective LGPL Exception statements.  Any distribution of this
** software carries with it the implied restrictions of the GPLv3 license,
** even with modifications to the original from this source code.
**
** Portions of this software may be modified from the original and are subject
** to GPL or LGPL License restrictions of the original source code.
**
** The developers of the OpenMOR software wish to thank particular individuals
** for their generous contributions.   We wish to thank:
**
**     Thomas Sailer        for the original PACTOR I modem from hfterm
**     Dave Freeze, W1HKJ   from his project FLDIGI
**     Rick Muething, KN6KB of the Amateur Radio Safety Foundation Inc.
**                          for his outstanding WinMOR decoder and
**                          contributions to this project.
**     Volker Schroer       for his contributions from his project LinPSK
**
** - - - The OpenMOR development team.
****************************************************************************/
#include <QtCore/QObject>
#include <QtDebug>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>

#include "globals.h"
#include "simple_fft.h"

/************************************************************************************/

/************************************************************************************/
SimpleFFT::SimpleFFT(QObject *parent, int points) :
    QObject(parent)
    ,fft_points(points)
    ,bit_reverse(new int[points])
    ,A_re(new double[points])
    ,A_im(new double[points])
    ,W_re(new double[points / 2])
    ,W_im(new double[points / 2])
    ,amplitudes(new double[points / 2])
    ,BlackmanNuttal_coef(new double[points])
{
    fft_order   = log_2(fft_points);
    computeBitrev();
    computeWeights();
    initBlackmanNuttalCoefs();
}

SimpleFFT::~SimpleFFT()
{
    delete W_re;
    delete W_im;
}

/* gets no. of points from the user, initialize the points and roots of unity lookup table
 * and lets fft go. finally bit-reverses the results and outputs them into a file.
 * n should be a power of 2.
 * function returns a pointer to amplitudes array
 */
double *SimpleFFT::compute_fft(const double *input)

{
    // apply window
    BlackmanNuttal(input);

    // compute FFT
    fft();

    // bit-reverse output
    permuteBitrev();

    // compute amplitude dB
    computeAmplitude();

    return amplitudes;
}

/* W will contain roots of unity so that W[bitrev(i,fft_order-1)] = e^(2*pi*i/n)
 * n should be a power of 2
 * Note: W is bit-reversal permuted because fft(..) goes faster if this is done.
 *       see that function for more details on why we treat 'i' as a (fft_order-1) bit number.
 */
void SimpleFFT::computeWeights(void)
{
    int i, br;

    for (i = 0; i < fft_points / 2; i++)
    {
        br  = bit_reverse[i * 2];
        W_re[br] = cos(((double)i * 2.0 * M_PI) / ((double)fft_points));
        W_im[br] = sin(((double)i * 2.0 * M_PI) / ((double)fft_points));
    }
}


/* permutes the array using a bit-reversal permutation */
void SimpleFFT::permuteBitrev(void)
{
    int i, bri;
    double t_re, t_im;

    for (i = 0; i < fft_points; i++)
    {
        bri = bit_reverse[i];
        /* skip already swapped elements */
        if (bri <= i)
        {
            continue;
        }
        t_re = A_re[i];
        t_im = A_im[i];
        A_re[i] = A_re[bri];
        A_im[i] = A_im[bri];
        A_re[bri] = t_re;
        A_im[bri] = t_im;
    }
}

/* compute logarithm of amplitude */
void SimpleFFT::computeAmplitude(void)
{
    for (int i = 0; i < fft_points / 2; i++)
    {
        amplitudes[i] = log10(sqrt((A_re[i] * A_re[i]) + (A_im[i] * A_im[i])));
    }
}


/* treats inp as a numbits number and bitreverses it.
 * inp < 2^(numbits) for meaningful bit-reversal
 */
void SimpleFFT::computeBitrev(void)
{
    for (int j = 0; j < fft_points; j++)
    {
        int i, rev = 0;
        int inp = j;
        for (i = 0; i < fft_order; i++)
        {
            rev = (rev << 1) | (inp & 1);
            inp >>= 1;
        }
        bit_reverse[j]  = rev;
    }
}

/* returns log n (to the base 2), if n is positive and power of 2 */
int SimpleFFT::log_2(int n)
{
    int res;
    for (res = 0; n >= 2; res++)
    {
        n = n >> 1;
    }
    return res;
}

/* fft on a set of n points given by A_re and A_im. Bit-reversal permuted roots-of-unity lookup table
 * is given by W_re and W_im. More specifically,  W is the array of first n/2 nth roots of unity stored
 * in a permuted bitreversal order.
 *
 * FFT - Decimation In Time FFT with input array in correct order and output array in bit-reversed order.
 *
 * REQ: n should be a power of 2 to work.
 *
 * Note: - See www.cs.berkeley.edu/~randit for her thesis on VIRAM FFTs and other details about VHALF section of the algo
 *         (thesis link - http://www.cs.berkeley.edu/~randit/papers/csd-00-1106.pdf)
 *       - See the foll. CS267 website for details of the Decimation In Time FFT implemented here.
 *         (www.cs.berkeley.edu/~demmel/cs267/lecture24/lecture24.html)
 *       - Also, look "Cormen Leicester Rivest [CLR] - Introduction to Algorithms" book for another variant of Iterative-FFT
 */

void SimpleFFT::fft(void)
{
    double w_re, w_im, u_re, u_im, t_re, t_im;
    int m, g, b;
    int mt, k;

    /* for each stage */
    for (m = fft_points; m >= 2; m = m >> 1)
    {
        /* m = n/2^s; mt = m/2; */
        mt = m >> 1;

        /* for each group of butterfly */
        for (g = 0, k = 0; g < fft_points; g += m, k++)
        {
            /* each butterfly group uses only one root of unity. actually, it is the bitrev of this group's number k.
            * BUT 'bitrev' it as a fft_order-1 bit number because we are using a lookup array of nth root of unity and
            * using cancellation lemma to scale nth root to n/2, n/4,... th root.
            *
            * It turns out like the foll.
            *   w.re = W[bitrev(k, fft_order-1)].re;
            *   w.im = W[bitrev(k, fft_order-1)].im;
            * Still, we just use k, because the lookup array itself is bit-reversal permuted.
            */
            w_re = W_re[k];
            w_im = W_im[k];

            /* for each butterfly */
            for (b = g; b < (g + mt); b++)
            {
                /* t = w * A[b+mt] */
                t_re = w_re * A_re[b + mt] - w_im * A_im[b + mt];
                t_im = w_re * A_im[b + mt] + w_im * A_re[b + mt];

                /* u = A[b]; in[b] = u + t; in[b+mt] = u - t; */
                u_re = A_re[b];
                u_im = A_im[b];
                A_re[b] = u_re + t_re;
                A_im[b] = u_im + t_im;
                A_re[b + mt] = u_re - t_re;
                A_im[b + mt] = u_im - t_im;

            }
        }
    }
}

/* compute Blackman-Nuttal window coefficients */
void    SimpleFFT::initBlackmanNuttalCoefs(void)
{
    const double    a0          = 0.3635819;
    const double    a1          = 0.4891775;
    const double    a2          = 0.1365995;
    const double    a3          = 0.0106411;
    const double    cos_incr    = (2.0 * 3.1415926535897932384626433832795 / (double)(fft_points - 1));

    for (int i = 0; i < fft_points; i++)
    {
        BlackmanNuttal_coef[i]  = 1.0 - (a0
                                         + (a1 * cos((double)i * cos_incr))
                                         + (a2 * cos((double)(2 * i) * cos_incr))
                                         + (a3 * cos((double)(3 * i) * cos_incr)));
    }
}

/* apply a BlackmanNuttal window on incoming data */
void    SimpleFFT::BlackmanNuttal(const double *raw_data)
{
    for (int i = 0; i < fft_points; i++)
    {
        A_re[i] = raw_data[i] * BlackmanNuttal_coef[i];
        // initialize imaginary data
        A_im[i] = 0.0;
    }
}

