/****************************************************************
 *  constellation.h - constellation widget definition
 *
 * Purpose: This widget displays datagrams in a relation pattern
 *          that corresponds to the amplitude and / or phase
 *          of the decoded audio signal. In the case of WinMOR
 *          the graticule will be a center cross and display
 *          a dot for each block received, with amplitude
 *          represented by the distance from the center, and the
 *          relative phase as the radial position relative to
 *          phase 0 deg around the center.
 *
 * TODO:    Determine what signals should be sent and what
 *          public functions or slots will be exposed.
 *          So far we have one signal from the IQ calc thread
 *          to start the draw function.
 ***************************************************************/
#include <qwt_plot.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_layout.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_marker.h>
#include <qwt_scale_engine.h>
#include <qwt_plot_directpainter.h>
#include <qwt_curve_fitter.h>
#include <qwt_painter.h>

#include <QtCore/QEvent>
#include <QtCore/QVector>
#include <QtWidgets/QWidget>

#include "constellation.hpp"

Constellation::Constellation(QWidget *parent) : QwtPlot(parent)
{
    qRegisterMetaType<double>("double");
    qRegisterMetaType<QVector<double> >("QVector<double>");

    // connect signal from calc thread to draw function
    connect(&myiq, SIGNAL(signaldraw(QVector<double>, QVector<double>)), this, SLOT(drawData(QVector<double>, QVector<double>)));
    //

    curve_1 = new QwtPlotCurve("Curve 1");
    curve_1->setStyle(QwtPlotCurve::NoCurve);
    curve_1->attach(this);
    curve_2 = new QwtPlotCurve("Curve 2");
    curve_2->setStyle(QwtPlotCurve::NoCurve);
    curve_2->attach(this);
    curve_3 = new QwtPlotCurve("Curve 3");
    curve_3->setStyle(QwtPlotCurve::NoCurve);
    curve_3->attach(this);

    scatter_symbol_1 = new QwtSymbol;
    scatter_symbol_1->setStyle(QwtSymbol::Ellipse);
    scatter_symbol_1->setSize(5,5);
    scatter_symbol_1->setPen(QColor(175, 175, 175));
    scatter_symbol_1->setBrush(QColor(175, 175, 175));
    curve_1->setSymbol(scatter_symbol_1);
    scatter_symbol_2 = new QwtSymbol;
    scatter_symbol_2->setStyle(QwtSymbol::Ellipse);
    scatter_symbol_2->setSize(5,5);
    scatter_symbol_2->setPen(QColor(100, 100, 100));
    scatter_symbol_2->setBrush(QColor(100, 100, 100));
    curve_2->setSymbol(scatter_symbol_2);
    scatter_symbol_3 = new QwtSymbol;
    scatter_symbol_3->setStyle(QwtSymbol::Ellipse);
    scatter_symbol_3->setSize(5,5);
    scatter_symbol_3->setPen(QColor(Qt::yellow));
    scatter_symbol_3->setBrush(QColor(Qt::yellow));
    curve_3->setSymbol(scatter_symbol_3);

    setAxisTitle(QwtPlot::xBottom, "Real");
    setAxisTitle(QwtPlot::yLeft, "Imaginary");
    setAxisScale(QwtPlot::xBottom, -90, 90);
    setAxisScale(QwtPlot::yLeft, -90, 90);
    enableAxis(QwtPlot::xBottom ,0);
    enableAxis(QwtPlot::yLeft ,0);

    this->setCanvasBackground(QColor(Qt::black));
    scaleX = new QwtPlotScaleItem();
    scaleX->setAlignment(QwtScaleDraw::BottomScale);
    scaleX->setScaleDiv((new QwtLinearScaleEngine())->divideScale(-90, 90, 10, 5));
    QPalette xpal;
    QBrush   xbrush;
    xbrush.setColor(Qt::red);
    xbrush.setStyle(Qt::SolidPattern);
    xpal.setBrush(QPalette::Normal,QPalette::Foreground,xbrush);
    scaleX->setPalette(xpal);
    scaleX->attach(this);
    scaleY = new QwtPlotScaleItem();
    QPalette ypal;
    QBrush   ybrush;
    ybrush.setColor(Qt::red);
    ybrush.setStyle(Qt::SolidPattern);
    ypal.setBrush(QPalette::Normal,QPalette::Foreground,ybrush);
    scaleY->setPalette(xpal);
    scaleY->setAlignment(QwtScaleDraw::LeftScale);
    scaleY->setScaleDiv((new QwtLinearScaleEngine())->divideScale(-90, 90, 10, 5));
    scaleY->attach(this);
}

Constellation::~Constellation()
{
    //
}

void Constellation::closeEvent(QCloseEvent *)
{
    myiq.quit();
    myiq.wait();
}

void Constellation::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type())
    {
    case QEvent::LanguageChange:
        translateText();
        break;
    default:
        break;
    }
}

void Constellation::setNewSize(quint32 w, quint32 h)
{
    resize(QSize(w,h));
}

void Constellation::drawData(QVector<double> x, QVector<double> y)
{
    QVector<double> x_1;
    QVector<double> y_1;
    QVector<double> xy_1;
    QVector<double> x_2;
    QVector<double> y_2;
    QVector<double> xy_2;
    QVector<double> x_3;
    QVector<double> y_3;
    QVector<double> xy_3;

    qDebug() << "qwt_draw()";

    for ( int i = 0; i < x.size(); i++ )
    {
        if ( !xy_1.contains(x[i]*256 + y[i]) )
        {
            x_1.append(x[i]);
            y_1.append(y[i]);
            xy_1.append(x[i]*256 + y[i]);
            continue;
        }
        if ( !xy_2.contains(x[i]*256 + y[i]) )
        {
            x_2.append(x[i]);
            y_2.append(y[i]);
            xy_2.append(x[i]*256 + y[i]);
            continue;
        }
        if ( !xy_3.contains(x[i]*256 + y[i]) )
        {
            x_3.append(x[i]);
            y_3.append(y[i]);
            xy_3.append(x[i]*256 + y[i]);
            continue;
        }
    }

    curve_1->setSamples(x_1, y_1);
    curve_2->setSamples(x_2, y_2);
    curve_3->setSamples(x_3, y_3);
    replot();
    myiq.setup(0, false, 0); // constant values used but widgets could select
}

void Constellation::translateText()
{
    // put translated text assignments here
}

void Constellation::initGradient()
{
    QPalette pal = canvas()->palette();

#if QT_VERSION >= 0x040400
    QLinearGradient gradient( 0.0, 0.0, 1.0, 0.0 );
    gradient.setCoordinateMode( QGradient::StretchToDeviceMode );
    gradient.setColorAt( 0.0, QColor( 0, 49, 110 ) );
    gradient.setColorAt( 1.0, QColor( 0, 87, 174 ) );

    pal.setBrush( QPalette::Window, QBrush( gradient ) );
#else
    pal.setBrush( QPalette::Window, QBrush( color ) );
#endif

    canvas()->setPalette( pal );
}
