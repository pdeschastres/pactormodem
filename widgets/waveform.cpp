/****************************************************************************
** Copyright (C) 2012 OpenMOR development team.   All rights reserved.
** Contact: Project Leader Ken AD5XJ (ad5xj@arrl.net)
** or visit the project web site openmor.sourceforge.net
**
** This file is part of the OpenMOR Project in the modem or RMSLink executable.
**
** This software is intended for use on amateur radio only and is not a
** commercial product.  The software of the OpenMOR project is
** primarily useful for technical and scientific purposes in non-commercial
** applications only.   Further, this software is distributed in the hope
** that it will be useful.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** === No Commercial Usage ===
** This file contains Open Source code and may not be distributed except as
** outlined in the terms and conditions of the GNU GPL License v3 included
** with this source code.
**
**  === GNU Lesser General Public License Usage ===
** This file may be used under the terms of the GNU Lesser
** General Public License version 3 or higher as published by the
** Free Software Foundation and appearing in the file LICENSE.TXT included
** in the packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 3 (or higher)
** requirements will be met: http://www.gnu.org/licenses/gpl-3.0.txt.
**
** In addition, portions of this software are products originating with
** Nokia and other authors. Each author retains their rights as described
** in their respective LGPL Exception statements.  Any distribution of this
** software carries with it the implied restrictions of the GPLv3 license,
** even with modifications to the original from this source code.
**
** Portions of this software may be modified from the original and are subject
** to GPL or LGPL License restrictions of the original source code.
**
** The developers of the OpenMOR software wish to thank particular individuals
** for their generous contributions.   We wish to thank:
**
**     Thomas Sailer        for the original PACTOR I modem from hfterm
**     Dave Freeze, W1HKJ   from his project FLDIGI
**     Rick Muething, KN6KB of the Amateur Radio Safety Foundation Inc.
**                          for his outstanding WinMOR decoder and
**                          contributions to this project.
**     Volker Schroer       for his contributions from his project LinPSK
**
** - - - The OpenMOR development team.
****************************************************************************/

/*****************************************************************************
 * scrolling waveform display
 *
 * creation:
 *      call to Waveform constructor
 *
 * initialization:
 *      call to Waveform::initialize(int tileWidth, int tileHeight, int windowSize)
 *              args:
 *                  tileWidth   width of one tile in pixels
 *                  tileHeight  height of tile, also waveform window height
 *                  windowSize  window width in pixels
 *      This will redraw waveform widget to appropriate size.
 *
 * drawing:
 *      call to Waveform::newData(ccharptr buffer)
 *              args:
 *                  buffer  signed char buffer. Buffer size must at least tileWidth
 *                      do not forget to register ccharptr before attempting  connection
 *                      between signal and slot:
 *                          typedef const char *  ccharptr;
 *                          qRegisterMetaType<ccharptr>("ccharptr");
 *
 * Data length must be equal to tileWidth
 *
 * When data is added by a call to newData(), previously first tile is erased and is updated
 * with the new data. It becomes the rightmost tile.
 * Then window is updated.
 * CPU time is just required to erase and draw curve on one tile, and to
 * move tiles to the canvas.
 * CPU load to draw a waveform is about 1/10th of CPU required to demodulate and decode
 * Pactor.
 ****************************************************************************/
#include <QtCore/QDebug>
#include <QtGui/QPainter>
#include <QtGui/QResizeEvent>

#include "globals.h"

#include "waveform.h"

Waveform::Waveform(QWidget *parent) : QWidget(parent)
    ,m_active(false)
    ,m_dataLength(0)
    ,m_position(0)
    ,m_tileLength(0)
    ,m_tileArrayStart(0)
    ,m_windowPosition(0)
    ,m_windowLength(0)
    ,m_tiles(NULL)
{
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);

    this->setMinimumHeight(50);
    this->setMinimumWidth(400);
}

Waveform::~Waveform()
{
    deletePixmaps();
}

void Waveform::paintEvent(QPaintEvent * /*event*/)
{
    QPainter painter(this);

    painter.fillRect(rect(), Qt::lightGray);

    if (m_active)
    {
        int pixmap_width    = m_tileLength;
        int destLeft        = 0;
        int destRight       = pixmap_width;

        int     tile_index  = first_tile_index;
        for (int i = 0; i < m_tiles_count; i++)
        {
            Tile &tile = m_tiles[tile_index];

            if (tile.painted)
            {
                QRect destRect = rect();
                destRect.setLeft(destLeft);
                destRect.setRight(destRight);

                QRect sourceRect(QPoint(), m_pixmapSize);
                sourceRect.setLeft(0);
                sourceRect.setRight(pixmap_width);

                WAVEFORM_DEBUG << "Waveform::paintEvent" << "tileIndex" << tile_index
                               << "source" << 0 << pixmap_width
                               << "dest" << destLeft << destRight;

                painter.drawPixmap(destRect, *tile.pixmap, sourceRect);

                destLeft    = destRight;
                destRight   = destRight + pixmap_width;
            }
            // next tile
            tile_index  = (tile_index + 1) % m_tiles_count;
        }
    }
}

void Waveform::resizeEvent(QResizeEvent *event)
{
    if (event->size() != event->oldSize())
    {
        createPixmaps(event->size());
    }
}

void Waveform::initialize(int tileWidth, int tileHeight, int windowSize)
{
    //PACTOR_TNC_DEBUG/*WAVEFORM_DEBUG*/ << "Waveform::initialize"
    //                                   << "audioBufferSize" << tileWidth << tileHeight
    //                                   << "windowSize" << windowSize;

    setMinimumWidth(windowSize);
    setMinimumHeight(tileHeight);
    reset();

    // Calculate tile size
    m_tileLength = tileWidth;

    // Calculate window size
    m_windowLength = windowSize;

    // Calculate number of tiles required
    int nTiles;

    if (m_tileLength > m_windowLength)
    {
        nTiles = 2;
    }
    else
    {
        nTiles = m_windowLength / m_tileLength;

        if (m_windowLength % m_tileLength)
        {
            ++nTiles;
        }
    }
    m_tiles_count   = nTiles;

    // allocate tiles array
    if (m_tiles != NULL)
    {
        try
        {
            delete m_tiles;
        }
        catch
            (...)
        {
            PACTORUI_DEBUG << "delete error 3";
        }
    }
    m_tiles         = new Tile[nTiles];

    WAVEFORM_DEBUG << "Waveform::initialize"
                   << "tileLength" << m_tileLength
                   << "windowLength" << m_windowLength
                   << "nTiles" << nTiles;

    m_pixmaps.fill(0, nTiles);

    createPixmaps(rect().size());

    m_active = true;
}

void Waveform::reset()
{
    WAVEFORM_DEBUG << "Waveform::reset";

    m_dataLength = 0;
    m_position = 0;
    m_active = false;
    deletePixmaps();
    m_tileLength = 0;
    m_tileArrayStart = 0;
    m_windowPosition = 0;
    m_windowLength = 0;
    painted_tiles   = 0;
    previousCurveValue = 0.0;
}

// public SLOT
void Waveform::newData(ccharptr buffer)
{
    // start painting tiles
    if (painted_tiles < m_tiles_count)
    {
        const Tile &tile = m_tiles[painted_tiles];

        if (!tile.painted)
        {
            paintTile(painted_tiles, buffer);
            current_tile    = painted_tiles;
            next_free_tile  = (current_tile + 1) % m_tiles_count;
        }
        painted_tiles++;
    }
    // shift tiles
    else
    {
        shuffleTiles(1);
        WAVEFORM_DEBUG << "Waveform::paint tile" << current_tile ;
        paintTile(current_tile, buffer);
    }
    try
    {
        delete buffer;
    }
    catch
        (...)
    {
        PACTORUI_DEBUG << "delete error 8";
    }

    update();
}

void Waveform::deletePixmaps()
{
    QPixmap *pixmap;
    foreach(pixmap, m_pixmaps)
    {
        try
        {
            delete pixmap;
        }
        catch
            (...)
        {
            PACTORUI_DEBUG << "delete error 9";
        }
    }
    m_pixmaps.clear();
    painted_tiles       = 0;
    previousCurveValue  = 0.0;
    next_free_tile      = 0;
    first_tile_index    = 0;
}

void Waveform::createPixmaps(const QSize &widgetSize)
{
    painted_tiles   = 0;
    m_pixmapSize = widgetSize;
    m_pixmapSize.setWidth(m_tileLength);

    WAVEFORM_DEBUG << "Waveform::createPixmaps"
                   << "widgetSize" << widgetSize
                   << "pixmapSize" << m_pixmapSize;

    // (Re)create pixmaps
    for (int i = 0; i < m_pixmaps.size(); ++i)
    {
        try
        {
            delete m_pixmaps[i];
        }
        catch
            (...)
        {
            PACTORUI_DEBUG << "delete error 10";
        }
        m_pixmaps[i] = 0;
        m_pixmaps[i] = new QPixmap(m_pixmapSize);
    }

    // Update tile pixmap pointers, and mark for repainting
    for (int i = 0; i < m_tiles_count; ++i)
    {
        m_tiles[i].pixmap = m_pixmaps[i];
        m_tiles[i].painted = false;
        m_tiles[i].index    = i;
    }
}

int Waveform::tilePixelOffset(qint64 positionOffset) const
{
    Q_ASSERT(positionOffset >= 0 && positionOffset <= m_tileLength);
    const int result = (qreal(positionOffset) / m_tileLength) * m_pixmapSize.width();
    return result;
}

void Waveform::paintTile(int index, ccharptr buffer)
{
    WAVEFORM_DEBUG << "Waveform::paintTile" << "index" << index;
    double  curveValue;

    Tile &tile = m_tiles[index];
    Q_ASSERT(!tile.painted);

    const int numSamples = m_tileLength / sizeof(qint8);

    QPainter painter(tile.pixmap);

    painter.fillRect(tile.pixmap->rect(), Qt::lightGray);

    QPen pen(Qt::darkBlue);
    painter.setPen(pen);

    // Calculate initial point
    const int originY               = ((previousCurveValue + 128.0) / (double)256.0) * m_pixmapSize.height();
    const QPoint origin(0, originY);

    QLine line(origin, origin);

    for (int i = 0; i < numSamples; ++i)
    {
        if (buffer != NULL)
        {
            curveValue = (double)buffer[i];
        }
        else
        {
            curveValue    = 0.0;
        }
        previousCurveValue    = curveValue;

        const int x = tilePixelOffset(i * sizeof(qint8));
        const int y = ((curveValue + 128.0) / 256.0) * m_pixmapSize.height();

        line.setP2(QPoint(x, y));
        painter.drawLine(line);
        line.setP1(line.p2());
    }

    tile.painted = true;
}

void Waveform::shuffleTiles(int n)
{
    WAVEFORM_DEBUG << "Waveform::shuffleTiles" << "n" << n;

    while (n--)
    {
        Tile &tile     = m_tiles[next_free_tile];
        tile.painted    = false;
        current_tile    = next_free_tile;
        first_tile_index    = (first_tile_index + 1) % m_tiles_count;
        next_free_tile  = (current_tile + 1) % m_tiles_count;
    }

    m_tileArrayStart    = m_tileLength * next_free_tile;
    WAVEFORM_DEBUG << "Waveform::shuffleTiles" << "tileArrayStart" << m_tileArrayStart
                   << "next index" << next_free_tile
                   << "painted" << m_tiles[next_free_tile].painted;
}

