#include "stationid.hpp"

WMStationID::WMStationID( QObject *parent ) : QObject(parent)
{
    //
}

bool WMStationID::setCallSign(const QString &callsign)
{
    QString cs = callsign.trimmed().toUpper();      // Mutable working copy.

    // Generic callsign: 3 to 7 alphanumerics, optional hyphen, ssid.
    QRegExp rxcallsignssid("([A-Z0-9]{3,7})(\\-([1-9]|1[0-5]))?");

    if(!rxcallsignssid.exactMatch(cs))
    {
        return false; // No match to generic callsign pattern.
    }

    // The result of a valid match is 4 strings; e.g. for VE7GNU-13 we get
    // "VE7GNU-13", "VE7GNU", "-13", "13"


    // This is where we would put some validation of callsign.
    // I have done a little research and found that the variety of
    // alphanumeric callsigns is so huge that putting in valid format
    // checks is probably unwise.

    // Ken, however, thinks otherwise and so he has such checks as follows:
    //    QRegExp validAmateurCall("");
    //    QRegExp validMARSCall("");
    //    QRegExp validUKCadetCall("");

    // For now, we will just accept anything alphanumeric.
    this->callSign = rxcallsignssid.capturedTexts().at(1); // E.g. "VE7GNU"

    // The following toInt() returns zero if the conversion fails, e.g, no ssid.
    this->ssid = rxcallsignssid.capturedTexts().at(3).toInt();  // E.g. "13"

    return true;
}

bool WMStationID::setGridSquare(const QString &gridsquare)
{
    // Validate Maidenhead Gridsquare locater
    QRegExp rxgridsquare( "[A-R][A-R][0-9][0-9]([a-x][a-x](0-9][0-9])?)?" );
    if( !rxgridsquare.exactMatch( gridsquare.trimmed() ) )   return false;
    this->gridSquare = gridsquare.trimmed();
    return true;
}

QString WMStationID::getCallSign()
{
    return this->callSign;
}

int WMStationID::getSsid()
{
    return this->ssid;

}

QString WMStationID::getCallSignSsid()
{
    return (this->callSign) + "-" + QString::number(this->ssid);
}

QString WMStationID::getGridSquare()
{
    return this->gridSquare;
}

