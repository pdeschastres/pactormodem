TEMPLATE = lib
TARGET   = wmcodec
DESTDIR  = ../lib

DEFINES += WM_SHARED


QT       += core gui widgets printsupport network
CONFIG   += qt thread exceptions resources shared
unix:    { CONFIG += x11inc x11 }
windows: { CONFIG += windows }

OBJECTS_DIR = obj
MOC_DIR     = moc
RCC_DIR     = rcc

INCLUDEPATH += \
    ../includes \
    ../includes/utils \
    ../includes/widgets \
    ../includes/qjack \
    ../includes/Winmor \
    ../utils \
    ../widgets \
    ../qjack

win32: {
INCLUDEPATH += "C:\\fftw3\\"
INCLUDEPATH += "C:\\qwt-6.1.0\\include\\"
INCLUDEPATH += "C:\\Jack\\includes\\"
}

unix: {
INCLUDEPATH += /usr/include/fftw3
INCLUDEPATH += /home/ken/Projects/C++/qwt-6.1.0/src/
INCLUDEPATH += /usr/include/jack
}

DEPENDPATH += \
    ../includes \
    ../includes/utils \
    ../includes/widgets \
    ../includes/winmor \
    ../utils \
    .

win32: {
DEPENDPATH  += \
    "C:\\fftw3\\" \
    "C:\\Jack\\includes\\" \
    "C:\\Jack\\32bits\\" \
    "C:\\qwt-6.1.0\\src\\" \
    "C:\\qwt-6.1.0\\lib\\"
}

unix: {
DEPENDPATH  += \
    /usr/lib/i386-linux-gnu/fftw3 \
    /usr/lib/i386-linux-gnu/jack \
    /home/ken/Projects/C++/qwt-6.1.0/src \
    /home/ken/Projects/C++/qwt-6.1.0/lib
}


#   QJack wrapper definitions #
HEADERS += \
    ../includes/QJack/inport.h \
    ../includes/QJack/outport.h \
    ../includes/QJack/qsignalinport.h \
    ../includes/QJack/qoutport.h \
    ../includes/QJack/qinport.h \
    ../includes/QJack/jackport.h \
    ../includes/QJack/jackoutport.h \
    ../includes/QJack/jackinport.h \
    ../includes/QJack/ringbuffer.h \
    ../includes/QJack/qjackclient.h

#   WinMOR definitions #
HEADERS += \
    ../includes/WinMor/winmor_globals.hpp \
    ../includes/WinMor/linklayer.hpp \
    ../includes/WinMor/asciipacker.hpp \
    ../includes/WinMor/wmtxframe.hpp \
    ../includes/WinMor/modulator.hpp \
    ../includes/WinMor/stationid.hpp \
    ../includes/WinMor/winmorsession.hpp \
    ../includes/WinMor/winmortnc.hpp \
    ../includes/WinMor/winmor.hpp

#   Utilities         #
HEADERS += \
    ../includes/Utils/utils.h \
    ../includes/Utils/simple_fft.h \
    ../includes/Utils/complex_fft.hpp \
    ../includes/Utils/inverse_complex_fft.hpp \
    ../includes/Utils/crccalc.h \
    ../includes/Utils/reedsolomon.hpp \
    ../includes/Utils/crcwinmor.hpp

#   Custom Widgets    #
HEADERS += \
    ../includes/Widgets/waterfall.hpp \
    ../includes/Widgets/constellation.hpp \
    ../includes/Widgets/iqdata.hpp  \
    ../includes/Widgets/vu_meter.hpp


#   WinMOR implementation   #
SOURCES += \
    asciipacker.cpp \
    wmtxframe.cpp \
    modulator.cpp \
    linklayer.cpp \
    stationid.cpp \
    winmor.cpp \
    winmorsession.cpp \
    winmortnc.cpp

#   utilities implementation   #
SOURCES += \
    ../utils/simple_fft.cpp \
    ../utils/crccalc.cpp \
    ../utils/inverse_complex.cpp \
    ../utils/complex_fftw.cpp \
    ../utils/reedsolomon.cpp \
    ../utils/crcwinmor.cpp

#   custom widgets implementations  #
SOURCES += \
    ../widgets/constellation.cpp \
    ../widgets/iqdata.cpp \
    ../widgets/waterfall.cpp \
    ../widgets/vu_meter.cpp

#   QJack wrapper implementation #
SOURCES += \
    ../qjack/jackport.cpp \
    ../qjack/jackoutport.cpp \
    ../qjack/jackinport.cpp \
    ../qjack/qsignalinport.cpp

RESOURCES += $$PWD/../modemres.qrc

win32: LIBS += -L"C:\\fftw3\\" -lfftw3
win32: LIBS += -L"C:\\qwt-6.1.0\\lib\\" -lqwt
win32: LIBS += -L"C:\\Program Files\\Jack\\32bit" -ljack_winmme

unix: LIBS += -L/usr/lib/i386-linux-gnu/fftw3/ -lfftw3
unix: LIBS += -L/home/ken/Projects/C++/qwt-6.1.0/lib/ -lqwt
unix: LIBS += -L/usr/lib/i386-linux-gnu/jack/jack_alsa

if(!debug_and_release|build_pass):CONFIG(debug, debug|release) {
   mac:LIBS = $$member(LIBS, 0) $$member(LIBS, 1)_debug
   win32:LIBS = $$member(LIBS, 0) $$member(LIBS, 1)d
}
