#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextBrowser>

#include "winmorsession.hpp"

WinMorSession::WinMorSession(QWidget *parent) : QMainWindow(parent)
{
    setAttribute(Qt::WA_DeleteOnClose);
    setUnifiedTitleAndToolBarOnMac(true);

    createActions();
    createMenus();
    createObjects();
    translateText();

    resize(640, 394);
    // TODO: set up signals and slots to manage TNC interaction
}

WinMorSession::~WinMorSession()
{
    // destructor
}

// ===============================================
// EVENT HANDLERS
// ===============================================
void WinMorSession::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type())
    {
    case QEvent::LanguageChange:
        translateText();
        break;

    default:
        break;
    }
}

void WinMorSession::closeEvent(QCloseEvent *)
{
    qDebug() << "Session Window Closing... ";
}

// ================================================
// PRIVATE SLOTS
// ================================================


void WinMorSession::createObjects()
{
    centralwidget = new QWidget(this);
    setCentralWidget(centralwidget);

    statusbar = new QStatusBar(this);
    statusbar->setObjectName(QStringLiteral("statusbar"));
    setStatusBar(statusbar);


    textBrowser = new QTextBrowser(centralwidget);
    textBrowser->setObjectName(QStringLiteral("textBrowser"));
    textBrowser->setGeometry(QRect(10, 0, 621, 341));

}

void WinMorSession::createMenus()
{
    menubar = new QMenuBar(this);
    menu_File = new QMenu(menubar);
    menuSetup = new QMenu(menubar);
    menuShowHide = new QMenu(menubar);
    menuHelp = new QMenu(menubar);
    setMenuBar(menubar);

    menu_File->addSeparator();
    menu_File->addAction(actionE_xit);

    menuShowHide->addAction(actionShow_TNC);
    menuShowHide->addAction(actionHide_TNC);

    menuHelp->addAction(actionHelp_Idx);
    menuHelp->addSeparator();
    menuHelp->addAction(actionHelp_Contents);
    menuHelp->addSeparator();
    menuHelp->addAction(actionHelp_About);
}

void WinMorSession::createActions()
{
    actionE_xit = new QAction(this);

    actionShow_TNC = new QAction(this);
    actionShow_TNC->setCheckable(true);
    actionShow_TNC->setChecked(true);

    actionHide_TNC = new QAction(this);
    actionHide_TNC->setCheckable(true);
    actionShow_TNC->setChecked(false);

    actionHelp_Idx = new QAction(this);
    actionHelp_Contents = new QAction(this);
    actionHelp_About = new QAction(this);
    QObject::connect(actionE_xit, SIGNAL(triggered()), this, SLOT(close()));
    QMetaObject::connectSlotsByName(this);
}

void WinMorSession::translateText()
{
    setWindowTitle(tr("WinMor Session"));
    actionE_xit->setText(tr("Exit"));
    menuShowHide->setTitle(tr("Show/Hide"));
    actionShow_TNC->setText(tr("Show TNC"));
    actionHide_TNC->setText(tr("Hide TNC"));
    menu_File->setTitle(tr("&File"));
    menuSetup->setTitle(tr("&Setup"));
    menuHelp->setTitle(tr("&Help"));
    actionHelp_About->setText(tr("&About"));
    actionHelp_Idx->setText(tr("*Index"));
    actionHelp_Contents->setText(tr("&Contents"));
} // translate

