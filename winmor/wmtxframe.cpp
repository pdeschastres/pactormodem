#include "wmtxframe.hpp"

WMTxFrame::WMTxFrame( Modulator *modulator, QObject *parent) : QObject(parent)
{
    this->modulator = modulator;
    this->reedSolomon8x7 = new ReedSolomon(8,7,this);
    this->reedSolomon4x4 = new ReedSolomon(4,4,this);
}


void  WMTxFrame::sendConnectRequest(WMStationID *caller, WMStationID *called)
{
    // Frame type 0: ConnectRequest
//    appendGenericFrame(caller.getCallSignSsid(), called.getCallSignSsid(), 0);
    // Prepare a special format needed by the protocol:
    // A 7 character string starting with the plain callsign,
    // padded with blanks to 7 characters long,
    // appended with the SSID in hex, or 0 if there isn't one.
    QString callsign = caller->getCallSign().leftJustified(7,' ');
    callsign.append("0123456789ABCDEF"[caller->getSsid()] );

    byte_vec outbytes;  // Our return data.

    foreach(byte_t c, AsciiPacker::pack(callsign))
    {
        outbytes.push_back( c );
    }

    // Now same exact thing with called
    callsign = called->getCallSign().leftJustified(7,' ');
    callsign.append("0123456789ABCDEF"[called->getSsid()] );

    foreach(byte_t c, AsciiPacker::pack(callsign))
    {
        outbytes.push_back( c );
    }

    // Frame type 0.
    sendGenericFrame(0, outbytes);
}

void WMTxFrame::sendStationID(WMStationID *caller)
{
    // Frame type 15: Station ID
//    appendGenericFrame(caller.getCallSignSsid(), caller.getGridSquare(), 15);
    // Prepare a special format needed by the protocol:
    // A 7 character string starting with the plain callsign,
    // padded with blanks to 7 characters long,
    // appended with the SSID in hex, or 0 if there isn't one.
    QString callsign = caller->getCallSign().leftJustified(7,' ');
    callsign.append("0123456789ABCDEF"[caller->getSsid()] );

    byte_vec outbytes;  // Our return data.

    foreach(byte_t c, AsciiPacker::pack(callsign))
    {
        outbytes.push_back( c );
    }

    // Now the same thing with Gridsquare except there is no SSID.
    QString gs = caller->getGridSquare().leftJustified(7,' ');

    foreach(byte_t c, AsciiPacker::pack(gs))
    {
        outbytes.push_back( c );
    }

    // Frametype 15.
    sendGenericFrame(15, outbytes);
}

void WMTxFrame::sendCodedControlFrame(quint16 sessionId, byte_t code)
{
    // Send a coded control frame, frame type 1.
    // The basic payload is two bytes as follows:
    //  Byte 0: the code
    //  Byte 1: CRC8(code + CRC16(sessionId))
    // The payload is expanded to 4-bit words, high nybble first.
    // The Reed-Solomon 4bit, 4 error code is performed on the expanded payload,
    // giving 8 parity nybbles on 4 data nybbles, for a total of 12 nybbles.
    // The resulting array of nybbles is split again into 2-bit symbols,
    // most significant going first, and the symbols go to
    // a 2-carrier 4FSK modulator.

    byte_vec payload;
    payload.push_back( code );
    payload.push_back( (sessionId>>8)&0x00ff );
    payload.push_back( sessionId&0x00ff );
    crc8_t crc = CrcWinMOR::crc8(payload);

    payload.clear();
    payload.push_back( code );
    payload.push_back( crc );   // A total of two bytes.

    // Split into nybbles for the reed-solomon
    byte_vec paynybbles;
    foreach(byte_t paybyte,payload)
    {
        paynybbles.push_back( paybyte>>4 );
        paynybbles.push_back( paybyte&0x0f );
    }
    // Should now be 4 nybbles.

    byte_vec rsencoded = reedSolomon4x4->encode(paynybbles); // adds 8n parity for 12n

    // Split into 2-bit symbols, total of 24 symbols.
    //TODO: let the modulator do this somehow
    byte_vec symbols;
    foreach(byte_t sym,rsencoded) {
        symbols.push_back( sym>>2 );
        symbols.push_back( sym&0x03 );
    }

    modulator->appendPilot();
    modulator->appendFrameType( 1 );
    modulator->modulateMultiCarrier4FSK(symbols,2);
}

void WMTxFrame::sendGenericFrame(int frametype, byte_vec payload)
{
    modulator->appendPilot();
    modulator->appendFrameType( frametype );

    CrcWinMOR::appendCrc16(payload);
    byte_vec encodedPayload = reedSolomon8x7->encode(payload);

    // Break the payload data into symbols, 4 x 2bits per byte.
    // Each payload byte is decomposed into 4 2bit slices then
    // the slices are turned into symbols starting with the most significant
    // slice.
    // TODO: replace this with a macro and move it to the modulator.
    byte_vec symbols;
    symbols.resize( encodedPayload.size()*4 );

    for( int ipayload=0;ipayload<encodedPayload.size();ipayload++)
    {
        int isym = ipayload*4;
        byte_t paybyte = encodedPayload[ipayload];
        symbols[isym + 0] = paybyte>>6;
        symbols[isym + 1] = (paybyte>>4)&0x03;
        symbols[isym + 2] = (paybyte>>2)&0x03;
        symbols[isym + 3] = paybyte&0x03;
    }
    // Use dual carrier 4FSK, which is specified by the standard.
    modulator->modulateMultiCarrier4FSK(symbols,2);
}

void WMTxFrame::sendAck(quint16 sessionId, byte_t ackcode)
{
    // Send an ACK,  Frame type 2.
    // 'ackcode' is described in the standard this way:
    //      "Back is a 8 bit field. The 8 bits corresond to the ACK for each
    //       carrier. The LSbit represents the highest carrier frequency.
    //      "
    // This frame is the same as coded control but with a different ftype.
    //TODO: refactor to put this identical code into one place.
    // The basic payload is two bytes as follows:
    //  Byte 0: the ackcode
    //  Byte 1: CRC8(code + CRC16(sessionId))
    // The payload is expanded to 4-bit words, high nybble first.
    // The Reed-Solomon 4bit, 4 error code is performed on the expanded payload,
    // giving 8 parity nybbles on 4 data nybbles, for a total of 12 nybbles.
    // The resulting array of nybbles is split again into 2-bit symbols,
    // most significant going first, and the symbols go to
    // a 2-carrier 4FSK modulator.

    byte_vec payload;
    payload.push_back( ackcode );
    payload.push_back( (sessionId>>8)&0x00ff );
    payload.push_back( sessionId&0x00ff );
    crc8_t crc = CrcWinMOR::crc8(payload);

    payload.clear();
    payload.push_back( ackcode );
    payload.push_back( crc );   // A total of two bytes.

    // Split into nybbles for the reed-solomon
    byte_vec paynybbles;
    foreach(byte_t paybyte,payload)
    {
        paynybbles.push_back( paybyte>>4 );
        paynybbles.push_back( paybyte&0x0f );
    }
    // Should now be 4 nybbles.

    byte_vec rsencoded = reedSolomon4x4->encode(paynybbles); // adds 8n parity for 12n

    // Split into 2-bit symbols, total of 24 symbols.
    //TODO: let the modulator do this somehow
    byte_vec symbols;
    foreach(byte_t sym,rsencoded) {
        symbols.push_back( sym>>2 );
        symbols.push_back( sym&0x03 );
    }

    modulator->appendPilot();
    modulator->appendFrameType( 2 );
    modulator->modulateMultiCarrier4FSK(symbols,2);
}

//byte_vec WMTxFrame::packCallsign(WMStationID caller, WMStationID called )
//{
//    // Pack two callsigns and optional SSIDs into a vector of bytes
//    // ready to be encoded and included in a transmit frame.

////    QRegExp rxcallsignssid("([A-Z0-9]+)(\\-([1-9]|1[0-5]))?");

////    QString callercallsign("       0");  // Seven spaces, no SSID is default.

////    if(rxcallsignssid.exactMatch(caller))
////    {
////        // The result of this match is 4 strings; e.g. for VE7GNU-13 we get
////        // "VE7GNU-13", "VE7GNU", "-13", "13"

////        callercallsign = rxcallsignssid.capturedTexts().at(1); // E.g. "VE7GNU"
////        callercallsign = callercallsign.leftJustified(7,' ');
////        QString callerssid = rxcallsignssid.capturedTexts().at(3);  // E.g. "13"
////        if(callerssid.size() == 0)
////        {
////            callerssid = "0";   // "0" represents no SSID.
////        }
////        // Convert to hexadecimal notation.
////        bool conversionok = true;   // Did the conversion fail?
////        callerssid = "0123456789ABCDEF"[callerssid.toInt(&conversionok,10)];
////        // Ignore conversionok - it passed the regex, didn't it? Must be ok.

////        callercallsign += callerssid;   // E.g. "VE7GNU D"
////    }
////    else ; // It didn't match so replace with "       0" -

////    // Rinse and repeat for the called callsign.
////    QString calledcallsign("       0");  // Seven spaces, no SSID is default.

////    if(rxcallsignssid.exactMatch(calledcsign))
////    {
////        calledcallsign = rxcallsignssid.capturedTexts().at(1); // E.g. "VE7GNU"
////        calledcallsign = calledcallsign.leftJustified(7,' ');
////        QString calledssid = rxcallsignssid.capturedTexts().at(3);  // E.g. "13"
////        if(calledssid.size() == 0)
////        {
////            calledssid = "0";   // "0" represents no SSID.
////        }
////        bool conversionok = true;   // Did the conversion fail?
////        calledssid = "0123456789ABCDEF"[calledssid.toInt(&conversionok,10)];

////        calledcallsign += calledssid;   // E.g. "VE7GNU D"
////    }
////    else
////    {
////        // It didn't match so replace with "       0" -
////    }

//    // Prepare a special format needed by the protocol:
//    // A 7 character string starting with the plain callsign,
//    // padded with blanks to 7 characters long,
//    // appended with the SSID in hex, or 0 if there isn't one.
//    QString callsign = caller.getCallSign().leftJustified(7,' ');
//    callsign.append("0123456789ABCDEF"[caller.getSsid()] );

//    byte_vec outbytes;  // Our return data.

//    foreach(byte_t c, AsciiPacker::pack(callsign))
//    {
//        outbytes.push_back( c );
//    }

//    // Now same exact thing with called
//    callsign = called.getCallSign().leftJustified(7,' ');
//    callsign.append("0123456789ABCDEF"[called.getSsid()] );

//    foreach(byte_t c, AsciiPacker::pack(callsign))
//    {
//        outbytes.push_back( c );
//    }

//    return outbytes;
//}


void WMTxFrame::startTx()
{
    modulator->startTx();
}

